﻿using Helpers;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PosRot
{
    public Vector3 Position;
    public Quaternion Rotation;

    public PosRot(Vector3 position, Quaternion rotation)
    {
        Position = position;
        Rotation = rotation;
    }
}

public class PickupManager : Singleton<PickupManager>
{
    public bool ShowPickableTooltips = true;
    public bool LeaveKinematicOnReset = true;

    public static List<Pickable> Pickables
    {
        get
        {
            if (_pickables == null || _pickables.Count == 0 || _pickables[0] == null || _pickables[0].transform == null)
                FindPickables();

            return _pickables;
        }
        private set { }
    }
    public static Dictionary<PickupController, Pickable> ControllerPickables { get { return _controllerPickables; } private set{ } }
    
    static List<Pickable> _pickables = new List<Pickable>();
    static Dictionary<PickupController, Pickable> _controllerPickables = new Dictionary<PickupController, Pickable>();
    static Dictionary<Pickable, PosRot> _storedPosRots = new Dictionary<Pickable, PosRot>();
    static Dictionary<ViveController, PickupController> _pickupControllers = new Dictionary<ViveController, PickupController>();

    protected override void Awake()
    {
        base.Awake();

        FindPickables();
        _controllerPickables = new Dictionary<PickupController, Pickable>();
    }

    void Start()
    {
        FindPickupControllers();
    }

    static void FindPickupControllers()
    {
        _pickupControllers = new Dictionary<ViveController, PickupController>();
        var pcs = FindObjectsOfType<PickupController>();
        foreach(var pc in pcs)
        {
            _pickupControllers.Add(pc.Controller, pc);
        }
    }

    public static PickupController GetPickupController(ViveController controller)
    {
        if (controller == null)
        {
            Debug.LogWarning("PickupManager::GetPickupController - Controller is null");
            return null;
        }

        return _pickupControllers[controller];
    }

    public static void FindPickables()
    {
        _pickables = FindObjectsOfType<Pickable>().ToList();
    }

    public static void AddPickable(Pickable pickable)
    {
        if (_pickables == null || _pickables.Count == 0)
            FindPickables();

        Pickables.Add(pickable);
        //Debug.Log("###############" + pickable.name + " " + pickable.transform.parent.name);
        MaterialManager.AddRenderers(pickable);
    }

    public static void EnsurePickable(Pickable pickable)
    {
        if (!_pickables.Contains(pickable))
            AddPickable(pickable);
    }

    public static void RemovePickable(Pickable pickable)
    {
        if (_pickables == null || _pickables.Count == 0)
            FindPickables();

        if (_pickables.Contains(pickable))
            Pickables.Remove(pickable);
    }

    public static void Target(Pickable pickable)
    {
        pickable.Target();
    }

    public static void UnTarget(Pickable pickable)
    {
        pickable.UnTarget();
    }

    public static void SetControllerPickable(PickupController controller, Pickable pickable)
    {
        if(controller == null)
        {
            Debug.LogWarning("PickupManager::SetControllerPickable - Passed controller is null");
            return;
        }

        if (!_controllerPickables.ContainsKey(controller))
            _controllerPickables.Add(controller, null);

        _controllerPickables[controller] = pickable;
    }

    public static void ResetControllerPickable(PickupController controller)
    {
        SetControllerPickable(controller, null);
    }

    public static bool IsCarryingTool()
    {
        foreach(var controllerPickable in _controllerPickables)
        {
            if (controllerPickable.Value != null && controllerPickable.Value.GetComponent<Tool>() != null)
                return true;
        }

        return false;
    }

    public static bool IsCarryingTool(ToolType type)
    {
        foreach (var controllerPickable in _controllerPickables)
        {
            if (controllerPickable.Value != null)
            {
                var tool = controllerPickable.Value.GetComponent<Tool>();
                if (tool.Type == type)
                    return true;
            }
        }

        return false;
    }

    public static void ShakePickable(Pickable pickable, float duration)
    {
        if (!_storedPosRots.ContainsKey(pickable))
            _storedPosRots.Add(pickable, null);
        _storedPosRots[pickable] = new PosRot(pickable.transform.position, pickable.transform.rotation);

        Instance.StartCoroutine(Shake(pickable, duration));
    }

    static IEnumerator Shake(Pickable pickable, float duration)
    {
        var shakeSpeed = 40.0f;
        var spread = 0.02f;
        var time = duration;
        var originalPos = pickable.transform.position;
        var originalRot = pickable.transform.rotation;
        while(time > 0)
        {
            var pos = Vector3.zero;
            pos.x = Mathf.Sin(Time.time * shakeSpeed) * spread;
            pickable.transform.position = originalPos + pos;
            time -= Time.deltaTime;
            yield return null;
        }

        pickable.transform.position = _storedPosRots[pickable].Position;
        pickable.transform.rotation = _storedPosRots[pickable].Rotation;
        // If it is removed it will not work for a simultanously started Shake
        //_storedPosRots.Remove(pickable);
    }

    public static Pickable FindClosestProximityPickable(Vector3 position, float maxDistance, bool excludePickedUpPickables = true)
    {
        return FindClosestProximityPickable(position, maxDistance, Pickables, excludePickedUpPickables);
    }

    public static Pickable FindClosestProximityPickable(Vector3 position, float maxDistance, List<Pickable> pool, bool excludePickedUpPieces = true, bool group = false)
    {
        var distance = maxDistance;
        Pickable closestPickable = null;
        for (int i = 0; i < pool.Count; ++i)
        {
            if (excludePickedUpPieces && pool[i].IsPickedUp)
                continue;

            var distanceSqr = 0.0f;
            if (CheckProximity(position, distance, pool[i], ref distanceSqr))
            {
                distance = Mathf.Sqrt(distanceSqr);
                closestPickable = pool[i];
            }
        }

        return closestPickable;
    }

    public static bool CheckProximity(Vector3 position, float maxDistance, Pickable pickable, ref float distanceSqr)
    {
        if(pickable == null)
        {
            Debug.Log("PickupManager::CheckProximity - pickable is null");
            FindPickables();
            return false;
        }

        distanceSqr = (pickable.transform.position - position).sqrMagnitude;
        return distanceSqr < maxDistance * maxDistance;
    }

    public static bool CheckProximity(Vector3 position, float maxDistance, Pickable pickable)
    {
        return (pickable.transform.position - position).sqrMagnitude < maxDistance * maxDistance;
    }

    public static int GetPickedUpPieceID(ControllerIndex side)
    {
        var viveController = ViveHelper.Controllers[side];
        if (!_controllerPickables.ContainsKey(_pickupControllers[viveController]))
            return -1;
        var pickable = _controllerPickables[_pickupControllers[viveController]];
        if (pickable == null)
            return -1;
        if (pickable.Type == PickableType.AssemblyPiece)
            return pickable.GetComponent<AssemblyPiece>().ID;

        return -1;
    }

    public static string GetPickedUpPieceTypeID(ControllerIndex side)
    {
        var viveController = ViveHelper.Controllers[side];
        if (!_controllerPickables.ContainsKey(_pickupControllers[viveController]))
            return "No object grabbed";
        var pickable = _controllerPickables[_pickupControllers[viveController]];
        if (pickable == null)
            return "No object grabbed";
        if (pickable.Type == PickableType.AssemblyPiece)
            return pickable.name;//GetComponent<AssemblyPiece>().TypeID.ToString();

        return "No object grabbed";
    }
}
