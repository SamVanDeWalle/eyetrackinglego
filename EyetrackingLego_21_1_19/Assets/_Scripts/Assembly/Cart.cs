﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cart : MonoBehaviour
{
    public List<PullHandle> Handles = new List<PullHandle>();

    PullHandle _handle;
    ViveController _controller;
    Vector3 _startDragPosition;
    Vector3 _startDragControllerPosition;
    bool _bDragging;

    void Start()
    {
        foreach(var handle in Handles)
        {
            handle.OnHover += OnHandleHover;
        }
    }

    void OnHandleHover(PullHandle handle)
    {
        _handle = handle;
        _handle.OnLeave += OnHandleLeave;
    }

    void OnHandleLeave(PullHandle handle)
    {
        if(!_bDragging)
            PickupManager.GetPickupController(_controller).enabled = true;

        _handle = null;
    }

    void Update()
    {
        if(!_bDragging && _handle != null && _handle.Controller.GetPressDown(ControllerButton.Trigger))
        {
            StartDragging();
        }

        if (_bDragging)
        {
            if (!_controller.GetPress(ControllerButton.Trigger))
            {
                StopDragging();
            }
            else
            {
                Drag();
            }
        }
    }

    void StartDragging()
    {
        _controller = _handle.Controller;
        _bDragging = true;
        _startDragControllerPosition = _controller.Transform.position;
        _startDragPosition = transform.position;
    }

    void Drag()
    {
        var movement = _controller.Transform.position - _startDragControllerPosition;
        movement.y = 0;
        transform.position = _startDragPosition + movement;

        _controller.Pulse();
    }

    void StopDragging()
    {
        _bDragging = false;
        PickupManager.GetPickupController(_controller).enabled = true;
    }
}
