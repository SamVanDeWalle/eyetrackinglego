﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AssemblyFiche : MonoBehaviour
{
    public List<AssemblyStep> AssemblySteps = new List<AssemblyStep>();
    [HideInInspector]
    public AssemblyFiche_UI UI;

    Dictionary<int, int> _stepsPerType = new Dictionary<int, int>();
    Dictionary<int, int> _quantityPerType = new Dictionary<int, int>();
    Dictionary<int, bool> _stepProgress = new Dictionary<int, bool>();
    int _currentStep = 0;

    void Awake()
    {
        SetStepsPerType();
        SetQuantitiesPerType();
    }

    void Start()
    {
        UI.HighLightStep(_currentStep);
    }

    void SetStepsPerType()
    {
        _stepsPerType.Clear();

        for(int i = 0; i < AssemblySteps.Count; ++i)
        {
            if (_stepsPerType.ContainsKey(AssemblySteps[i].PrefabTypeID))
            {
                Debug.LogWarning("AssemblyFiche::SetStepsPerType - Multiple steps containing the same type");
                continue;
            }

            _stepsPerType.Add(AssemblySteps[i].PrefabTypeID, i);
            _stepProgress.Add(i, false);
        }
    }

    void SetQuantitiesPerType()
    {
        _quantityPerType.Clear();

        for (int i = 0; i < AssemblySteps.Count; ++i)
        {
            if (_quantityPerType.ContainsKey(AssemblySteps[i].PrefabTypeID))
            {
                Debug.LogWarning("AssemblyFiche::SetQuantitiesPerType - Multiple steps containing the same type");
                continue;
            }

            _quantityPerType.Add(AssemblySteps[i].PrefabTypeID, AssemblySteps[i].Quantity);
        }
    }

    public int GetStepFromType(int type)
    {
        if (_stepsPerType.ContainsKey(type))
            return _stepsPerType[type];

        return -1;
    }

    public int GetQuantityFromType(int type)
    {
        if (_quantityPerType == null || _quantityPerType.Count == 0)
            SetQuantitiesPerType();

        if (_quantityPerType.ContainsKey(type))
            return _quantityPerType[type];

        return 0;
    }

    public bool ComesBefore(int typeA, int typeB)
    {
        return _stepsPerType[typeA] < _stepsPerType[typeB];
    }

    public string GetIDFromType(int type)
    {
        var step = AssemblySteps.FirstOrDefault(s => s.PrefabTypeID == type);
        if (step != null)
            return step.ID;
        else
            return "";
    }

    public void ClearStep(int typeID)
    {
        if(!_stepsPerType.ContainsKey(typeID))
        {
            Debug.LogWarning("AssemblyFiche::ClearStep - Tried to clear a non existing type");
            return;
        }

        _stepProgress[_stepsPerType[typeID]] = true;
        EvaluateCurrentStep();
        UI.HighLightStep(_currentStep);
    }

    public void UnClearStep(int typeID)
    {
        if (!_stepsPerType.ContainsKey(typeID))
        {
            Debug.LogWarning("AssemblyFiche::UnClearStep - Tried to unclear a non existing type");
            return;
        }

        _stepProgress[_stepsPerType[typeID]] = false;
        EvaluateCurrentStep();
        UI.HighLightStep(_currentStep);
    }

    void EvaluateCurrentStep()
    {
        _currentStep = 0;
        for(int i = 0; i < AssemblySteps.Count; ++i)
        {
            if (_stepProgress[i] == true)
                _currentStep = i+1;
            else
                break;
        }
    }
}
