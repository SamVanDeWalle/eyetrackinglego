﻿using Helpers;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AssemblyPieceManager : Singleton<AssemblyPieceManager>
{
    const string PREFAB_PATH = "Prefabs/Assemblies/";
    public static List<AssemblyPiece> AssemblyPieces
    {
        get
        {
            if (_assemblyPieces == null || _assemblyPieces.Count == 0)
                FindAssemblyPieces();

            return _assemblyPieces;
        }
        private set { }
    }
    public static List<AssemblyPieceContainer> AssemblyPieceContainers
    {
        get
        {
            if (_assemblyPieceContainers == null || _assemblyPieceContainers.Count == 0)
                FindAssemblyPieceContainers();

            return _assemblyPieceContainers;
        }
        private set { }
    }
    public static Dictionary<int, AssemblyPiece> PrefabPool { get { return _prefabPool; } private set { } }

    static List<AssemblyPiece> _assemblyPieces = new List<AssemblyPiece>();
    static List<AssemblyPieceContainer> _assemblyPieceContainers = new List<AssemblyPieceContainer>();
    static Dictionary<int, AssemblyPiece> _prefabPool = new Dictionary<int, AssemblyPiece>();
    static List<AssemblyPiece> _spawnedPieces = new List<AssemblyPiece>();

    public delegate void DefaultAction();
    public static event DefaultAction OnPrefabPoolLoaded; 

    protected override void Awake()
    {
        base.Awake();
    }

    void Start()
    {
        GeneratePrefabPool();
        FindAssemblyPieces();
    }

    public static void GeneratePrefabPool()
    {
        GeneratePrefabPool(TrainingManager.Instance.AssemblyName);
    }

    public static void GeneratePrefabPool(string assemblyName)
    {
        _prefabPool.Clear();
        Debug.Log("trainingmanager: " + TrainingManager.Instance);
        var prefabs = Resources.LoadAll<AssemblyPiece>(PREFAB_PATH + assemblyName + "/");
        foreach (var ap in prefabs)
        {
            //Debug.Log("prefab: " + ap.TypeID);
            if (_prefabPool.ContainsKey(ap.TypeID))
            {
                Debug.LogWarning("AssemblyPieceManager::GeneratePrefabPool - Found multiple prefabs with same type id!");
                continue;
            }

            _prefabPool.Add(ap.TypeID, ap);
        }

        if (OnPrefabPoolLoaded != null)
            OnPrefabPoolLoaded();
    }

    public static void FindAssemblyPieces()
    {
        _assemblyPieces = FindObjectsOfType<AssemblyPiece>().Where(ap => ap.GetComponent<Workbench>() == null).ToList(); //TODO fix workbench != AssemblyPiece hack
    }

    public static void FindAssemblyPieceContainers()
    {
        _assemblyPieceContainers = FindObjectsOfType<AssemblyPieceContainer>().ToList();
    }

    public static AssemblyPiece SpawnPiece(int typeID)
    {
        return SpawnPiece(typeID, Vector3.zero, Quaternion.identity);
    }

    public static AssemblyPiece SpawnPiece(int typeID, Vector3 position, Quaternion rotation)
    {
        return SpawnPiece(typeID, position, rotation, Instance.transform);
    }

    public static AssemblyPiece SpawnPiece(int typeID, Vector3 position, Quaternion rotation, Transform parent)
    {
        if (!_prefabPool.ContainsKey(typeID))
        {
            Debug.LogWarning("AssemblyPieceManager::SpawnPiece - Unable to find prefab piece with type: " + typeID);
            return null;
        }

        var pieceGO = Instantiate(_prefabPool[typeID].gameObject, position, rotation);
        pieceGO.transform.SetParent(parent);
        pieceGO.transform.localScale = Vector3.one;
        var piece = pieceGO.GetComponent<AssemblyPiece>();
#if !UNITY_EDITOR
        piece.Setup();
        Ensure(piece);
#endif
        _spawnedPieces.Add(piece);
        piece.LoadConnectionPoints();
        return piece;
    }

    public static void AddPiece(AssemblyPiece piece)
    {
        AssemblyPieces.Add(piece);
        PickupManager.EnsurePickable(piece.GetComponent<Pickable>());
    }

    public static void Ensure(AssemblyPiece piece)
    {
        if (AssemblyPieces == null || AssemblyPieces.Count == 0)
            FindAssemblyPieces();

        if (!AssemblyPieces.Contains(piece))
            AddPiece(piece);
    }

    public static void Reset()
    {
        for(int i = _spawnedPieces.Count - 1; i > -1; --i)
        {
            PickupManager.RemovePickable(_spawnedPieces[i].Pickable);
            _assemblyPieces.Remove(_spawnedPieces[i]);
            Destroy(_spawnedPieces[i].gameObject);
        }

        _spawnedPieces.Clear();
    }

    public static void DestroyAllPieces()
    {
        for (int i = _assemblyPieces.Count - 1; i > -1; --i)
        {
            PickupManager.RemovePickable(_assemblyPieces[i].Pickable);
            DestroyImmediate(_assemblyPieces[i].gameObject);
            _assemblyPieces.RemoveAt(i);
        }

        _assemblyPieces.Clear();
    }

    public static void DestroyAllContainers()
    {
        for (int i = _assemblyPieceContainers.Count - 1; i > -1; --i)
        {
            PickupManager.RemovePickable(_assemblyPieceContainers[i].Pickable);
            DestroyImmediate(_assemblyPieceContainers[i].gameObject);
            _assemblyPieceContainers.RemoveAt(i);
        }

        _assemblyPieceContainers.Clear();
    }
}
