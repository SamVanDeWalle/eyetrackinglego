﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectionTuner : MonoBehaviour
{
    bool _bActive;
    bool _bDragging;
    ConnectionPoint _cp;
    ViveController _dragController;
    AssemblyPiece _slavePiece;
    Vector3 _startDragThread;
    Vector3 _startSlavePos;
    Quaternion _startSlaveRot;
    Transform _dragger;
    Transform _slaveParent;

    void Start()
    {
        PickupController.OnProxyCPAvailable -= Activate;
        PickupController.OnProxyCPAvailable += Activate;

        PickupController.OnProxyCPUnavailable -= Deactivate;
        PickupController.OnProxyCPUnavailable += Deactivate;
    }

    public void Activate(ConnectionPoint cp)
    {
        Deactivate();
        _bActive = true;
        _cp = cp;
        GetSlavePiece();
        HighlightSlave(true);
    }

    public void Deactivate()
    {
        if (_bDragging)
            StopDragging();

        _bActive = false;
        if(_slavePiece != null)
            HighlightSlave(false);
    }

    void GetSlavePiece()
    {
        if (_cp == null)
            return;

        if(_cp.ParentPiece.ParentPiece != _cp.ConnectedCP.ParentPiece)
        {
            _cp = _cp.ConnectedCP;
        }

        _slavePiece = _cp.ParentPiece;
    }

    void HighlightSlave(bool highlight = true)
    {
        if (highlight)
            MaterialManager.GetInstance().GlowObject(_slavePiece.GetComponent<Pickable>());
        else
            MaterialManager.GetInstance().UnglowAll();
    }

    void Update()
    {
        if (!_bActive)
            return;

        ViveController controller = null;
        bool triggerDown = false;

        foreach(var c in ViveHelper.Controllers.Values)
        {
            if (c.GetPress(ControllerButton.Trigger))
            {
                controller = c;
                triggerDown = true;
                break;
            }
        }

        // Dragging
        if ((!_bDragging || controller != _dragController) && triggerDown)
        {
            _dragController = controller;
            StartDragging();
        }

        if (!triggerDown && _bDragging)
        {
            StopDragging();
        }

        if(_bDragging)
        {
            var currentThread = _dragController.Transform.position - _cp.transform.position;
            _slavePiece.transform.localPosition = _startSlavePos;
            _slavePiece.transform.localRotation = _startSlaveRot;
            _dragger.forward = currentThread;
            _slavePiece.SetClosestRotation(_cp.ConnectedCP, _cp);
        }
    }

    void StartDragging()
    {
        _startDragThread = _dragController.Transform.position - _cp.transform.position;
        var draggerGO = new GameObject("dragger");
        _dragger = draggerGO.transform;
        _dragger.transform.position = _cp.transform.position;
        _dragger.forward = _startDragThread;
        _slaveParent = _slavePiece.transform.parent;
        _slavePiece.transform.SetParent(_dragger);
        _startSlavePos = _slavePiece.transform.localPosition;
        _startSlaveRot = _slavePiece.transform.localRotation;
        PickupController.GlobalAllowPicking = false;
        _bDragging = true;
    }

    void StopDragging()
    {
        _slavePiece.transform.SetParent(_slaveParent);
        if (_dragger != null)
            Destroy(_dragger.gameObject);

        PickupController.GlobalAllowPicking = true;
        _bDragging = false;
    }
}
