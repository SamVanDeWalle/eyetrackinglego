﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConnectionPointIndicator : MonoBehaviour
{
    public GameObject normalContainer;
    public GameObject correctContainer;

    public void SetCorrect(bool correct)
    {
        if(normalContainer != null)
            normalContainer.SetActive(!correct);

        if(correctContainer != null)
            correctContainer.SetActive(correct);
    }
}
