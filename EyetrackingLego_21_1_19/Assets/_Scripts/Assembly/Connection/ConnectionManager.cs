﻿/*
 * 
 * This script manages the possible connections between the pieces
 * Can be used to check for the closest possible connection in the scene
 * Can be used to check if connection is valid
 * 
 * 
 */

using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;
using Helpers;
using XBOX;
using System.IO;
using System.Text;

public enum RotationType
{
    Locked,
    Free,
    Step,
}

public enum ScrollStep
{
    None,
    ScrollX,
    ScrollY,
    ScrollZ
}

[Serializable]
public class ConnectionType
{
    public int ID;
    public string Name;

    public RotationType XRotationType = RotationType.Locked;
    public RotationType YRotationType = RotationType.Locked;
    public RotationType ZRotationType = RotationType.Locked;

    public bool XSymmetry, YSymmetry, ZSymmetry;

    public bool RequireOrientation = false;

    public Vector2 xRotationBounds = new Vector2(0, 360);
    public Vector2 yRotationBounds = new Vector2(0, 360);
    public Vector2 zRotationBounds = new Vector2(0, 360);

    public float StepX, StepY, StepZ;

    public ScrollStep Scrolling;
    public ScrollStep SecundaryScrolling;
}

[Obsolete]
public class ConnectionPair
{
    public int A;
    public int B;

    public ConnectionPair(int a, int b)
    {
        A = a;
        B = b;
    }

    public override bool Equals(System.Object obj)
    {
        if (obj == null)
        {
            return false;
        }

        ConnectionPair p = obj as ConnectionPair;
        if ((System.Object)p == null)
        {
            return false;
        }

        return ((A == p.A) && (B == p.B) || (A == p.B) && (B == p.A));
    }


    public bool Equals(ConnectionPair p)
    {
        if ((object)p == null)
        {
            return false;
        }

        return ((A == p.A) && (B == p.B) || (A == p.B) && (B == p.A));
    }

    public override int GetHashCode()
    {
        return (A ^ B) + (B ^ A);
    }
}

public class ConnectionMatch
{
    public ConnectionPoint A, B;
    public float Distance;
    public float DistanceSqr;

    public ConnectionMatch(ConnectionPoint a, ConnectionPoint b)
    {
        A = a;
        B = b;
    }

    public bool ConnectionCheck()
    {
        if(CheckSanityMatch())
        {
            //if(CalculateDistance() <= ConnectionManager.ConnectionDistance)
            if(GetDistanceSqr() <= ConnectionManager.ConnectionDistanceSqr)
            //if (A.transform.position == B.transform.position)
            {
                return true;
            }
        }

        return false;
    }

    public float CalculateDistance()
    {
        Distance = Vector3.Distance(A.transform.position, B.transform.position);
        return Distance;
    }

    public float GetDistanceSqr()
    {
        DistanceSqr = (A.transform.position - B.transform.position).sqrMagnitude;
        return DistanceSqr;
    }

    public bool CheckSanityMatch()
    {
        return ConnectionManager.CheckPair(A, B);
    }

    public bool CheckReservation()
    {
        return !A.IsReserved && !B.IsReserved;
    }

    public bool Connect()
    {
        return A.Connect(B); // (B automatically connects to A as well)
    }

    public bool IsConnected()
    {
        return A.ConnectedCP == B;
    }

    public ConnectionPoint GetMale()
    {
        return A.Gender == ConnectionGender.Male ? A : B;
    }

    public ConnectionPoint GetFemale()
    {
        return A.Gender == ConnectionGender.Female ? A : B;
    }

    public override bool Equals(System.Object obj)
    {
        if (obj == null)
        {
            return false;
        }

        ConnectionMatch m = obj as ConnectionMatch;
        if ((System.Object)m == null)
        {
            return false;
        }

        return ((A == m.A) && (B == m.B) || (A == m.B) && (B == m.A));
    }


    public bool Equals(ConnectionMatch m)
    {
        if ((object)m == null)
        {
            return false;
        }

        return ((A == m.A) && (B == m.B) || (A == m.B) && (B == m.A));
    }

    public override int GetHashCode()
    {
        return (A.ID ^ B.ID) + (B.ID ^ A.ID);
    }
}

public class ConnectionManager : Singleton<ConnectionManager>
{
    public GameObject ConnectionEffectPrefab;
    public static GameObject ConnectionEffect;
    public static int ScrollStep, SecundaryScrollStep;
    static float ScrollStepSpeed = 1.0f;
    public static float ConnectionDistance = 0.01f;
    public static float ConnectionDistanceSqr = ConnectionDistance * ConnectionDistance;
    [Obsolete]
    public static List<ConnectionPair> Pairs = new List<ConnectionPair>() //TODO store in file or editable in inspector
    {
        new ConnectionPair(0, 1),
        new ConnectionPair(2, 3),
        new ConnectionPair(4, 5),
        new ConnectionPair(6, 7),
        new ConnectionPair(8, 9),
        new ConnectionPair(10, 11),
        new ConnectionPair(12, 13),
        new ConnectionPair(14, 15),
        new ConnectionPair(16, 17),
        new ConnectionPair(18, 19),
        new ConnectionPair(20, 21),
        new ConnectionPair(22, 23),
        new ConnectionPair(100, 101),
        new ConnectionPair(102, 103),
        new ConnectionPair(104, 105),
        new ConnectionPair(106, 107)
    };
    public List<ConnectionType> Types = new List<ConnectionType>();
    public static List<ConnectionType> ConnectionTypes = new List<ConnectionType>();
    public static List<AssemblyPiece> AssemblyPieces { get { return AssemblyPieceManager.AssemblyPieces; } private set { } }
    public static List<Workbench> WorkBenches { get { return FurnitureManager.Workbenches; } private set { } }

    void Start()
    {
        LoadConnectionTypes();
        WorkBenches = FindObjectsOfType<Workbench>().ToList();
        ConnectionEffect = ConnectionEffectPrefab;
    }

    public static bool CheckPair(ConnectionPoint a, ConnectionPoint b)
    {
        return a.ParentPiece.RootPiece != b.ParentPiece.RootPiece && a.TypeID == b.TypeID && a.Gender != b.Gender;
        // Old
        // return Pairs.Contains(new ConnectionPair(a.TypeID, b.TypeID));
    }

    static List<int> FindPairedConnectionIds(int typeId)
    {
        var pairedIds = new List<int>();
        pairedIds.AddRange(Pairs.Where(p => p.A == typeId).Select(p => p.B));
        pairedIds.AddRange(Pairs.Where(p => p.B == typeId).Select(p => p.A));
        return pairedIds;
    }

    public static ConnectionType GetType(int id)
    {
        //return Types.FirstOrDefault(ct => ct.ID == id);
        return ConnectionTypes.FirstOrDefault(ct => ct.ID == id);
    }

    void Update()
    {
        var scrollDY = Input.mouseScrollDelta.y * ScrollStepSpeed;
        scrollDY += Input.GetButtonDown(XBOXONEController.BackButtonRight) ? 1 : 0;
        scrollDY += Input.GetButtonDown(XBOXONEController.BackButtonLeft) ? -1 : 0;
        
        ScrollStep += Mathf.RoundToInt(scrollDY);
        if (Input.GetMouseButtonDown(2) || Input.GetButtonDown(XBOXONEController.Y))
        {
            ++SecundaryScrollStep;
        }
    }

    public static List<AssemblyPiece> FindProximityPieces(AssemblyPiece piece, bool group = false)
    {
        return FindProximityPieces(piece, AssemblyPieces, group);
    }

    public static List<AssemblyPiece> FindProximityPieces(AssemblyPiece piece, List<AssemblyPiece> pool, bool group = false)
    {
        List<AssemblyPiece> proximityPieces = new List<AssemblyPiece>();
        for (int i = 0; i < pool.Count; i++)
        {
            if (pool[i] == piece)
                continue;

            if (BoundingSphere.CheckOverlap(group ? piece.GroupBoundingSphere : piece.BoundingSphere, pool[i].BoundingSphere) && pool[i].CanBeConnectedTo)
                proximityPieces.Add(pool[i]);

            if ((piece.transform.position - pool[i].transform.position).sqrMagnitude < ConnectionDistance)
                proximityPieces.Add(pool[i]);
        }

        return proximityPieces;
    }

    public static List<AssemblyPiece> FindProximityPieces(Vector3 position, float maxDistance)
    {
        return FindProximityPieces(position, maxDistance, AssemblyPieces);
    }

    public static List<AssemblyPiece> FindProximityPieces(Vector3 position, float maxDistance, List<AssemblyPiece> pool)
    {
        List<AssemblyPiece> proximityPieces = new List<AssemblyPiece>();
        for (int i = 0; i < pool.Count; ++i)
        {
            if(CheckProximity(position, maxDistance, pool[i]))
                proximityPieces.Add(pool[i]);
        }

        return proximityPieces;
    }

    public static AssemblyPiece FindClosestProximityPiece(Vector3 position, float maxDistance, bool excludePickedUpPieces = true)
    {
        return FindClosestProximityPiece(position, maxDistance, AssemblyPieces, excludePickedUpPieces);
    }

    public static AssemblyPiece FindClosestProximityPiece(Vector3 position, float maxDistance, List<AssemblyPiece> pool, bool excludePickedUpPieces = true, bool group = false)
    {
        var distance = maxDistance;
        AssemblyPiece closestPiece = null;
        for (int i = 0; i < pool.Count; ++i)
        {
            if (excludePickedUpPieces && pool[i].IsPickedUp)
                continue;

            var distanceSqr = 0.0f;
            if (CheckProximity(position, distance, pool[i], ref distanceSqr))
            {
                distance = Mathf.Sqrt(distanceSqr);
                closestPiece = pool[i];
            }
        }

        return closestPiece;
    }

    public static bool CheckProximity(Vector3 position, float maxDistance, AssemblyPiece piece, ref float distanceSqr)
    {
        distanceSqr = (piece.transform.position - position).sqrMagnitude;
        return distanceSqr < maxDistance * maxDistance;
    }

    public static bool CheckProximity(Vector3 position, float maxDistance, AssemblyPiece piece)
    {
        return (piece.transform.position - position).sqrMagnitude < maxDistance * maxDistance;
    }

    public static ConnectionMatch FindClosestConnection(AssemblyPiece piece, bool group = true)
    {
        // 1. Check Workbench connection

        if (piece.CanConnectToWorkbench)
        {
            var smallestWorkbenchDistance = PickupController.MaxConnectionDistanceSqr;
            Workbench proxyWorkbench = null;
            for (int i = 0; i < WorkBenches.Count; ++i)
            {
                if (!WorkBenches[i].IsAvailable || !WorkBenches[i].ConnectableTypes.Contains(piece.TypeID))
                    continue;

                var workbenchDistance = (piece.transform.position - WorkBenches[i].ConnectionPoint.transform.position).sqrMagnitude;
                if (workbenchDistance < smallestWorkbenchDistance)
                {
                    smallestWorkbenchDistance = workbenchDistance;
                    proxyWorkbench = WorkBenches[i];
                }
            }

            if(proxyWorkbench != null)
            {
                var wbMatch = new ConnectionMatch(piece.CenterCP, proxyWorkbench.ConnectionPoint);
                if(!wbMatch.A.IsReserved && !wbMatch.B.IsReserved)
                    return wbMatch;
            }
        }

        // 2. Check Assembly connection

        // Filter close enough pieces
        // Do range check with bounds on root ap
        // Loop over pickedup ap's
        //  Filter further on proximity objects
        //  Loop cp's in ap if prox ap's > 0
        //      Loop prox ap's
        //          Lookup on type on ap
        //              Loop cp matches
        //                  Distance comparison

        ConnectionMatch cMatch = null;
        var smallestDistanceSqr = PickupController.MaxConnectionDistanceSqr;
        List<AssemblyPiece> pieces;
        if(group)
        {
            pieces = AssemblyPiece.FindAllChildPieces(piece.RootPiece, true);
            //pieces = AssemblyPiece.FindAllConnectedPieces(piece.RootPiece, true);
        }
        else
        {
            pieces = new List<AssemblyPiece>() { piece };
        }

        var allButPieces = new List<AssemblyPiece>(AssemblyPieces);
        foreach (var p in pieces)
        {
            allButPieces.Remove(p);
        }

        var rootProximityPieces = FindProximityPieces(piece.RootPiece, allButPieces, true);
        if (rootProximityPieces.Count == 0)
            return cMatch;

        for (int i = 0; i < pieces.Count; i++)
        {
            var proximityPieces = rootProximityPieces;
            if(pieces[i] != piece.RootPiece)
            {
                proximityPieces = FindProximityPieces(pieces[i], proximityPieces);
                if (proximityPieces.Count == 0)
                    continue;
            }

            var connectionPoints = pieces[i].ConnectionPoints;
            foreach (var connectionPointList in pieces[i].ConnectionPoints)
            {
                for (int j = 0; j < connectionPointList.Value.Count; j++)
                {
                    var cp = connectionPointList.Value[j];
                    if (cp.IsReserved)
                        continue;
                    var otherGender = cp.Gender == ConnectionGender.Male ? ConnectionGender.Female : ConnectionGender.Male;
                    for (int k = 0; k < proximityPieces.Count; k++)
                    {
                        if (!proximityPieces[k].ConnectionPoints.ContainsKey(cp.TypeID))
                            continue;

                        var matchingCps = proximityPieces[k].ConnectionPoints[cp.TypeID].Where(c => c.Gender == otherGender && !c.IsReserved).ToList();
                        for (int m = 0; m < matchingCps.Count; m++)
                        {
                            var match = new ConnectionMatch(cp, matchingCps[m]);
                            if (match.A.ParentPiece.RootPiece != match.B.ParentPiece.RootPiece && match.GetDistanceSqr() < smallestDistanceSqr)
                            {
                                cMatch = match;
                                smallestDistanceSqr = match.DistanceSqr;
                            }
                        }
                    }
                }
            }
        }

        //Debug.Log(cMatch.A.RootPiece == cMatch.B.RootPiece);
        return cMatch;
    }

    public static ConnectionPoint FindClosestConnectedPoint(Vector3 position, float maximumDistance)
    {
        var proximityPieces = FindProximityPieces(position, maximumDistance);
        ConnectionPoint closestCP = null;
        float closestDistanceSqr = maximumDistance * maximumDistance;
        for(int i = 0; i < proximityPieces.Count; ++i)
        {
            for(int j = 0; j < proximityPieces[i].ConnectedPoints.Length; ++j)
            {
                var distanceSqr = (position - proximityPieces[i].ConnectedPoints[j].transform.position).sqrMagnitude;
                if(distanceSqr < closestDistanceSqr)
                {
                    closestDistanceSqr = distanceSqr;
                    closestCP = proximityPieces[i].ConnectedPoints[j];
                }
            }
        }

        return closestCP;
    }

    public static ConnectionPoint FindClosestConnectionPointToAP(AssemblyPiece piece, AssemblyPiece targetPiece)
    {
        ConnectionPoint closestCP = null;
        var connectionPoints = targetPiece.ConnectionPointsUnordered.Where(cp => !cp.IsConnected && !cp.IsReserved && piece.ConnectionPointsUnordered.Any(cpn => cpn.TypeID == cp.TypeID && cpn.Gender != cp.Gender && !cpn.IsReserved)).ToArray();
        var smallestDistance = float.MaxValue;
        for (int i = 0; i < connectionPoints.Length; ++i)
        {
            var distance = (piece.transform.position - connectionPoints[i].transform.position).sqrMagnitude;
            if (distance < smallestDistance)
            {
                smallestDistance = distance;
                closestCP = connectionPoints[i];
            }
        }

        return closestCP;
    }

    public static ConnectionMatch FindClosestConnectionMatch(AssemblyPiece pieceA, AssemblyPiece pieceB)
    {
        var matches = FindConnectionMatches(pieceA, pieceB);
        if (matches.Count == 0)
            return null;

        ConnectionMatch match = matches[0];
        match.GetDistanceSqr(); // caches match.DistanceSqr
        for(int i = 1; i < matches.Count; ++i)
        {
            if(matches[i].GetDistanceSqr() < match.DistanceSqr)
            {
                match = matches[i];
            }
        }

        return match;
    }

    public static List<ConnectionMatch> FindConnectionMatches(AssemblyPiece pieceA, AssemblyPiece pieceB)
    {
        var matches = new List<ConnectionMatch>();

        for(int a = 0; a < pieceA.ConnectionPointsUnordered.Length; ++a)
        {
            for(int b = 0; b < pieceB.ConnectionPointsUnordered.Length; ++b)
            {
                var match = new ConnectionMatch(pieceA.ConnectionPointsUnordered[a], pieceB.ConnectionPointsUnordered[b]);
                if (match.CheckSanityMatch() && match.CheckReservation())
                    matches.Add(match);
            }
        }

        return matches;
    }

    public static ConnectionPoint FindClosestConnectionPointToAP(AssemblyPiece piece, AssemblyPiece[] targetPieces)
    {
        ConnectionPoint closestCP = null;
        var smallestDistance = float.MaxValue;
        for(int a = 0; a < targetPieces.Length; ++a)
        {
            if (!targetPieces[a].CanBeConnectedTo)
                continue;

            var connectionPoints = targetPieces[a].ConnectionPointsUnordered.Where(cp => !cp.IsConnected && !cp.IsReserved && piece.ConnectionPointsUnordered.Any(cpn => cpn.TypeID == cp.TypeID && cpn.Gender != cp.Gender && !cpn.IsReserved)).ToArray();

            for (int i = 0; i < connectionPoints.Length; ++i)
            {
                var distance = (piece.transform.position - connectionPoints[i].transform.position).sqrMagnitude;
                if (distance < smallestDistance)
                {
                    smallestDistance = distance;
                    closestCP = connectionPoints[i];
                }
            }
        }
        
        return closestCP;
    }

    public static ConnectionMatch FindConnectionMatch(ConnectionPoint cp)
    {
        var rootPiece = cp.RootPiece;
        var connectedPieces = AssemblyPiece.FindAllConnectedPieces(rootPiece);

        return FindConnectionMatch(cp, connectedPieces);
    }

    public static ConnectionMatch FindConnectionMatch(ConnectionPoint cp, List<AssemblyPiece> pieces)
    {
        ConnectionMatch cMatch = null;
        for (int i = 0; i < pieces.Count; ++i)
        {
            var apMatch = FindConnectionMatch(cp, pieces[i]);

            if (cMatch == null && apMatch != null)
            {
                cMatch = apMatch;
            }
            else if (cMatch != null && apMatch != null)
            {
                if (apMatch.DistanceSqr < cMatch.DistanceSqr)
                    cMatch = apMatch;
            }
        }

        return cMatch;
    }

    static ConnectionMatch FindConnectionMatch(ConnectionPoint cp, AssemblyPiece ap)
    {
        ConnectionMatch cMatch = null;

        if (!ap.ConnectionPoints.ContainsKey(cp.TypeID))
            return cMatch;

        for(int i = 0; i < ap.ConnectionPoints[cp.TypeID].Count; ++i)
        {
            var match = new ConnectionMatch(cp, ap.ConnectionPoints[cp.TypeID][i]);
            if (match.ConnectionCheck() && match.CheckReservation())
            {
                if (cMatch == null || match.DistanceSqr < cMatch.DistanceSqr)
                    cMatch = match;
            }
        }

        return cMatch;
    }

    public static Workbench FindClosestWorkbench(Vector3 position)
    {
        Workbench closestBench = null;
        var smallestDistance = float.MaxValue;
        for(int i = 0; i < WorkBenches.Count; ++i)
        {
            if (WorkBenches[i].ConnectionPoint.IsConnected)
                continue;

            var distane = (WorkBenches[i].transform.position - position).sqrMagnitude;
            if (distane < smallestDistance)
            {
                smallestDistance = distane;
                closestBench = WorkBenches[i];
            }
        }

        return closestBench;
    }

    public static List<ConnectionPoint> GetCrossWiseConnectionTargets(AssemblyPiece targetPiece, int connectionType, bool indicate = true)
    {
        var targets = new List<ConnectionPoint>();

        var crosswiseCPs = targetPiece.ConnectionPointsUnordered.Where(cp => cp.TypeID == connectionType && cp.Crosswise).ToList();
        var connectedCrosswiseCPs = crosswiseCPs.Where(cp => cp.IsConnected || cp.IsReserved).ToList(); // Haven't tested with reserved addition
        var n = crosswiseCPs.Count;
        if (connectedCrosswiseCPs.Count == 0)
        {
            for (int i = 0; i < crosswiseCPs.Count; ++i)
            {
                crosswiseCPs[i].Indicate();
            }

            return crosswiseCPs;
        }
        else
        {
            for (int i = 0; i < crosswiseCPs.Count; ++i)
            {
                crosswiseCPs[i].Indicate(false);
            }
        }

        var even = connectedCrosswiseCPs.Count % 2 == 0;
        if (even)
        {
            // Find largest spacing between connected cps and pick middle cp in this space
            var largestSpaceCount = 0;
            List<int> nextIds = new List<int>();
            for (int i = 0; i < connectedCrosswiseCPs.Count; ++i)
            {
                var id = connectedCrosswiseCPs[i].ID;
                var nextId = i + 1 == connectedCrosswiseCPs.Count ? connectedCrosswiseCPs[0].ID : connectedCrosswiseCPs[i + 1].ID;
                var spaceCPCount = Mathf.Abs(nextId - id) - 1;
                // if spacing is bigger or equal to 6 take n - (spacing + 2)
                if (spaceCPCount >= n / 2)
                {
                    spaceCPCount = n - (spaceCPCount + 2);
                }

                if (spaceCPCount > largestSpaceCount)
                {
                    nextIds.Clear();
                    largestSpaceCount = spaceCPCount;
                    if (spaceCPCount == 1)
                    {
                        nextIds.Add((id + 1) % n);
                    }
                    else if (spaceCPCount % 2 == 0)
                    {
                        var firstNextId = id + (int)Mathf.Floor(spaceCPCount / 2.0f) % n;
                        var secondNextId = (firstNextId + 1) % n;
                        nextIds.Add(firstNextId);
                        nextIds.Add(secondNextId);
                    }
                    else
                    {
                        nextId = id + (int)Mathf.Ceil(spaceCPCount / 2.0f) % n;
                        nextIds.Add(nextId);
                    }
                }
                if (spaceCPCount == largestSpaceCount)
                {
                    if (spaceCPCount == 1)
                    {
                        nextIds.Add((id + 1) % n);
                    }
                    else if (spaceCPCount % 2 == 0)
                    {
                        var firstNextId = id + (int)Mathf.Floor(spaceCPCount / 2.0f) % n;
                        var secondNextId = (firstNextId + 1) % n;
                        nextIds.Add(firstNextId);
                        nextIds.Add(secondNextId);
                    }
                    else
                    {
                        nextId = id + (int)Mathf.Ceil(spaceCPCount / 2.0f) % n;
                        nextIds.Add(nextId);
                    }
                }
            }

            for (int i = 0; i < crosswiseCPs.Count; ++i)
            {
                if (nextIds.Contains(crosswiseCPs[i].ID))
                {
                    crosswiseCPs[i].Indicate(true);
                }
                else
                {
                    crosswiseCPs[i].Indicate(false);
                }
            }

            return crosswiseCPs.Where(cp => nextIds.Contains(cp.ID)).ToList();
        }
        else
        {
            for (int i = 0; i < connectedCrosswiseCPs.Count; ++i)
            {
                var matchCP = crosswiseCPs.FirstOrDefault(cp => cp.ID == (connectedCrosswiseCPs[i].ID + n / 2) % n);
                if (matchCP != null && !matchCP.IsConnected)
                {
                    matchCP.Indicate();
                    return new List<ConnectionPoint>() { matchCP };
                }
            }
        }

        return targets;
    }

    public static void LoadConnectionTypes(string assemblyName = null)
    {
        var name = assemblyName != null ? assemblyName : VR_Main.AssemblyName;
        var container = ConnectionTypeContainer.Load(name);
        ConnectionTypes = container.Types;
    }

    public static void SaveConnectionTypes(string assemblyName = null)
    {
        var name = assemblyName != null ? assemblyName : VR_Main.AssemblyName;
        var container = new ConnectionTypeContainer();
        container.Types = ConnectionTypes;
        ConnectionTypeContainer.Save(container, name);
    }

    public static bool CheckValidCrosswiseConnection(ConnectionPoint cp)
    {
        return false;
    }

    [Serializable]
    public class ConnectionTypeContainer
    {
        public List<ConnectionType> Types = new List<ConnectionType>();

        [NonSerialized]
        const string CONNECTIONTYPES_PATH = @"\connectiontypes";
        static string _connectionTypesPath { get { return Directory.GetCurrentDirectory() + @"\Datapath\" + CONNECTIONTYPES_PATH; } }
        
        public static ConnectionTypeContainer Load(string assemblyName)
        {
            var postfix = assemblyName != null && assemblyName != "" ? "_" + assemblyName : "";
            string json;
            
            try
            {
                StreamReader reader = new StreamReader(_connectionTypesPath + postfix + ".json", Encoding.Default);
                using (reader)
                {
                    json = reader.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                Debug.Log(string.Format("{0}\n", e.Message));
                return new ConnectionTypeContainer();
            }
            
            ConnectionTypeContainer container = JsonUtility.FromJson<ConnectionTypeContainer>(json);
            return container;
        }

        public static void Save(ConnectionTypeContainer container, string assemblyName)
        {
            var postfix = assemblyName != null && assemblyName != "" ? "_" + assemblyName : "";
            string json = JsonUtility.ToJson(container, true);

            try
            {
                StreamWriter writer = new StreamWriter(_connectionTypesPath + postfix + ".json", false, Encoding.Default);
                using (writer)
                {
                    writer.Write(json);
                }
            }
            catch (Exception e)
            {
                Debug.Log(string.Format("{0}\n", e.Message));
            }
        }
    }
}
