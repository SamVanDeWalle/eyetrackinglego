﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public enum ConnectionGender
{
    Male,
    Female
}

public class ConnectionPoint : MonoBehaviour
{
    public int ID;
    public int GroupID = -1;
    public int TypeID;
    public ConnectionGender Gender;
    public ToolAction Action = ToolAction.None;
    public List<Property> RequiredProperties = new List<Property>(); 
    public Vector3 PrePoseOffset = Vector3.zero;
    public AssemblyPiece RootPiece { get { return ParentPiece ? ParentPiece.RootPiece : null; } private set { } }
    public AssemblyPiece ParentPiece { get { return _parentPiece; } private set {} }
    public ConnectionPoint ConnectedCP { get { return _connectedCP; } private set { } }
    public List<ConnectionPoint> DependencyConnectionPoints = new List<ConnectionPoint>();
    public List<ConnectionPoint> DependantConnectionPoints = new List<ConnectionPoint>();
    public List<Transform> AidPoints = new List<Transform>();
    public bool IsConnected { get { return _connectedCP != null; } private set { } }
    public bool IsReserved { get { return _bReserved; } private set { } }
    public bool CanConnect
    {
        get
        {
            if (IsConnected == true || _bReserved) //(possibly dangerous to put reserved here)
                return false;
            //TODO use linq
            for (int i = 0; i < DependencyConnectionPoints.Count; ++i)
            {
                if (!DependencyConnectionPoints[i].IsConnected)
                    return false;
            }

            return true;
        }
        private set { }
    }
    public bool CanDisconnect
    {
        get
        {
            for (int i = 0; i < DependantConnectionPoints.Count; ++i)
            {
                if (DependantConnectionPoints[i].IsConnected)
                    return false;
            }

            return true;
        }
        private set { }
    }
    public bool CorrectProperties
    {
        get
        {
            return !RequiredProperties.Any(p => !p.IsCorrect);
        }
        private set { }
    }
    public bool Crosswise;

    public delegate void Connected();
    public event Connected OnConnected;

    public delegate void CPConnected(ConnectionPoint cp);
    public static event CPConnected OnGlobalConnected;

    public delegate void CPDisconnected(ConnectionPoint cp, ConnectionPoint otherCP);
    public static event CPDisconnected OnGlobalDisconnected;

    AssemblyPiece _parentPiece;
    ConnectionPointIndicator _indicator;
    [SerializeField]
    ConnectionPoint _connectedCP;

    bool _bReserved;

    void Awake()
    {
        _indicator = GetComponentInChildren<ConnectionPointIndicator>();
        _parentPiece = transform.parent ? transform.parent.GetComponent<AssemblyPiece>() : null;
        Indicate(false);
    }

    void Start()
    {
        // Assuming that for every dependency CP this CP is a dependant
        for(int i = 0; i < DependencyConnectionPoints.Count; ++i)
        {
            DependencyConnectionPoints[i].DependantConnectionPoints.Add(this);
        }
    }

    public void Reserve()
    {
        //Reserved connection points will not be found when looking for connection matches
        //this allows for making connections over time without interference
        if (!_bReserved && !IsConnected && CanConnect)
            _bReserved = true;
    }

    public bool Connect(ConnectionPoint other, bool effect = true, bool silent = false)
    {
        if (IsConnected)
        {
            Debug.LogWarning("ConnectionPoint::Connect - tried to connect an already connected point");
            return false;
        }

        _connectedCP = other;
        // Automatically connect the connected cp as well
        other.Connect(this, false, silent);

        if (effect)
        {
            var effectGO = Instantiate(ConnectionManager.ConnectionEffect, transform.position, Quaternion.identity);
            effectGO.transform.SetParent(transform, false);
            effectGO.transform.position = transform.position;

            SoundManager.Play("connection", transform.position);
        }

        if (!silent)
        {
            if (OnConnected != null)
                OnConnected();

            if (OnGlobalConnected != null)
                OnGlobalConnected(this);
        }
        
        return true;
    }

    public void Disconnect()
    {
        if (!IsConnected)
        {
            return;
        }

        var connectedCP = _connectedCP;
        _connectedCP = null;

        _bReserved = false;

        // Automatically disconnect the connected cp as well
        if (connectedCP != null)
            connectedCP.Disconnect();

        if (OnGlobalDisconnected != null)
            OnGlobalDisconnected(this, connectedCP);
    }

    public void Indicate(bool active = true)
    {
        if (_indicator == null)
        {
            //Debug.LogWarning("ConnectionPoint::Indicate - Missing indicator");
            return;
        }

        _indicator.gameObject.SetActive(active);
        _indicator.SetCorrect(false);

        if (active == true)
            PickupController.OnPlacedGlobal += StopIndicating;
    }

    public void SetIndicateCorrect(bool correct)
    {
        if(_indicator != null)
        {
            _indicator.SetCorrect(correct);
        }
    }

    public void StopIndicating()
    {
        Indicate(false);
        PickupController.OnPlacedGlobal -= StopIndicating;
    }

    public void ClearAidPoints()
    {
        for(int i = AidPoints.Count - 1; i >= 0; --i)
        {
            Destroy(AidPoints[i].gameObject);
        }

        AidPoints.Clear();
    }
}
