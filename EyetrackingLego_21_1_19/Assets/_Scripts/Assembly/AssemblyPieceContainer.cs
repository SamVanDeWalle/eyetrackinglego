﻿/*
 * 
 * This script is used to pick pieces from a certain type from a box 
 * 
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssemblyPieceContainer : MonoBehaviour
{
    public int APTypeID;
    public float JumpHeight = 0.5f;
    public Pickable Pickable { get { return _pickable; } private set { } }
    public Transform Parent;

    Pickable _pickable;

    void Awake()
    {
        SetPickable();
    }

    void SetPickable()
    {
        _pickable = GetComponent<Pickable>();
        if (!_pickable)
        {
            _pickable = gameObject.AddComponent<Pickable>();
        }

        _pickable.Type = PickableType.AssemblyPieceContainer;
        PickupManager.EnsurePickable(_pickable);
    }

    public AssemblyPiece SpawnPiece()
    {
        var piece = AssemblyPieceManager.SpawnPiece(APTypeID, transform.position + transform.up * 0.1f, Quaternion.identity);
        if (Parent != null)
        {
            piece.transform.SetParent(Parent);
            piece.transform.localScale = Vector3.one;
        }

        piece.CanBeConnectedTo = false;
        Debug.Log("Spawning: " + piece.gameObject.GetInstanceID());
        return piece;
    }
}
