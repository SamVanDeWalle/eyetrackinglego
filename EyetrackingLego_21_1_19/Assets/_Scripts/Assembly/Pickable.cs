﻿/*
 * 
 * This script belongs on all the items that can be picked up, including assembly pieces and tools
 * 
 */

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum PickableType
{
    AssemblyPiece,
    AssemblyPieceContainer,
    Tool,
    Misc
}

public class Pickable : MonoBehaviour
{
    public PickableType Type;
    public Vector3 LockRotation;
    public Vector3 OffsetPosition;
    public PickupController Controller { get { return _controller; } private set { } }
    public Renderer[] Renderers
    {
        get
        {
            if (_renderers == null || _renderers.Length == 0)
            {
                List<Renderer> renderers = new List<Renderer>();
                renderers.AddRange(GetComponentsInChildren<Renderer>().Where(x => x.enabled && x.GetComponent<PaintSurface>() == null));
                var r = GetComponent<Renderer>();
                if (r && r.enabled)
                {
                    renderers.Add(r);
                }
                _renderers = renderers.ToArray();
            }

            return _renderers;
        }
    }
    public bool IsPickedUp { get { return _bPickedUp; } set { } }

    Renderer[] _renderers;
    PickableInfoTooltip _infoTooltip;
    PickupController _controller;
    Transform _initialParent;
    Vector3 _initialPosition;
    Quaternion _initialRotation;
    const string TOOLTIP_PREFAB_PATH = "Prefabs/PickableInfoTooltip";
    bool _bPickedUp;

    public delegate void PickedUp();
    public event PickedUp OnPickedUp;

    protected virtual void Awake()
    {
        gameObject.layer = LayerMask.NameToLayer("PickItems");

        foreach (Transform trans in GetComponentsInChildren<Transform>(true))
        {
            if (trans.GetComponent<PaintSurface>() != null)
                continue;

            trans.gameObject.layer = gameObject.layer;
        }

        _initialParent = transform.parent;
        _initialPosition = transform.localPosition;
        _initialRotation = transform.localRotation;
    }

    void Start()
    {
        if (_infoTooltip == null)
        {
            var toolTip = GameObject.Instantiate(Resources.Load(TOOLTIP_PREFAB_PATH)) as GameObject;
            toolTip.transform.SetParent(transform);
            toolTip.name = "Tooltip";
            _infoTooltip = toolTip.GetComponent<PickableInfoTooltip>();
        }
        if (_infoTooltip != null)
        {
            var fiche = FindObjectOfType<AssemblyFiche>();
            var infoText = "";
            var typeID = -1;
            
            switch (Type)
            {
                case PickableType.AssemblyPiece:
                    typeID = GetComponent<AssemblyPiece>().TypeID;
                    break;
                case PickableType.AssemblyPieceContainer:
                    typeID = GetComponent<AssemblyPieceContainer>().APTypeID;
                    break;
                case PickableType.Tool:
                    infoText = GetComponent<Tool>().Type.ToString();
                    break;
            }
        
            if (typeID != -1)
            {
                infoText = typeID.ToString();
                //if (fiche != null)
                //    infoText = fiche.GetIDFromType(typeID);
                //else
                //    infoText = typeID.ToString();
            }
        
            _infoTooltip.InfoText.text = infoText;
            _infoTooltip.PopOut();
            _infoTooltip.gameObject.SetActive(false);
        }

        _initialParent = transform.parent;
        _initialPosition = transform.localPosition;
        _initialRotation = transform.localRotation;

        PickupManager.EnsurePickable(this);
    }

    public void Target()
    {
        var pickablesToHighlight = new List<Pickable>();
        pickablesToHighlight.AddRange(GetComponentsInChildren<Pickable>());
        //MaterialManager.GetInstance().FadeOthers(pickablesToHighlight.ToArray());
        MaterialManager.GetInstance().UnglowAll();
        MaterialManager.GetInstance().GlowObjects(pickablesToHighlight.ToArray());

        if(PickupManager.Instance.ShowPickableTooltips)
            PopOutTooltip();
    }

    public void UnTarget()
    {
        //MaterialManager.GetInstance().UnglowAll();
        MaterialManager.GetInstance().GlowObjects(new Pickable[0]);

        if (PickupManager.Instance.ShowPickableTooltips)
            PopBackTooltip();
    }

    public void PopOutTooltip()
    {
        if (_infoTooltip != null && _infoTooltip.InfoText.text != "")
        {
            _infoTooltip.gameObject.SetActive(true);
            _infoTooltip.PopOut();
        }
    }

    public void PopOutTooltip(Color color)
    {
        if (_infoTooltip != null && _infoTooltip.InfoText.text != "")
        {
            _infoTooltip.gameObject.SetActive(true);
            _infoTooltip.SetColor(color);
            _infoTooltip.PopOut();
        }
    }

    public void PopBackTooltip()
    {
        if (_infoTooltip != null)
        {
            _infoTooltip.PopBack();
            _infoTooltip.gameObject.SetActive(false);
        }
    }

    public void Pickup(PickupController controller)
    {
        _bPickedUp = true;
        PickupManager.SetControllerPickable(controller, this);
        _controller = controller;

        if (OnPickedUp != null)
            OnPickedUp();
    }

    public void Drop()
    {
        _bPickedUp = false;
        PickupManager.ResetControllerPickable(_controller);
        _controller = null;
    }

    public void ResetPosition(bool animated = true)
    {
        if(_bPickedUp)
            Drop();

        var rb = GetComponent<Rigidbody>();
        if (rb != null)
            rb.isKinematic = true;
        if (animated)
            StartCoroutine(MoveToInitialSpot());
        else
            SetSpot(_initialParent, _initialPosition, _initialRotation);
    }

    const float __updateSpeed = 5f;
    IEnumerator MoveToInitialSpot()
    {
        transform.SetParent(_initialParent);
        Vector3 startPos = transform.localPosition;
        Quaternion startRot = transform.localRotation;

        float t = 0f;
        while (t < 1f)
        {
            t = Mathf.Clamp01(t + Time.deltaTime * __updateSpeed);

            transform.localPosition = EasingFunctions.Ease(EasingFunctions.Type.In, t, startPos, _initialPosition);
            transform.localRotation = EasingFunctions.Ease(EasingFunctions.Type.In, t, startRot, _initialRotation);
            yield return null;
        }

        transform.localPosition = _initialPosition;
        transform.localRotation = _initialRotation;

        var rb = GetComponent<Rigidbody>();
        if (rb != null)
            rb.isKinematic = PickupManager.Instance.LeaveKinematicOnReset;
    }

    public void MoveTo(Transform parent, Vector3 position, Quaternion rotation, bool animated = true)
    {
        if (_bPickedUp)
            Drop();

        var rb = GetComponent<Rigidbody>();
        if (rb != null)
            rb.isKinematic = true;

        if (animated)
            StartCoroutine(MoveToSpot(parent, position, rotation));
        else
            SetSpot(parent, position, rotation);
    }

    IEnumerator MoveToSpot(Transform parent, Vector3 position, Quaternion rotation)
    {
        transform.SetParent(parent);
        Vector3 startPos = transform.localPosition;
        Quaternion startRot = transform.localRotation;

        float t = 0f;
        while (t < 1f)
        {
            t = Mathf.Clamp01(t + Time.deltaTime * __updateSpeed);

            transform.localPosition = EasingFunctions.Ease(EasingFunctions.Type.In, t, startPos, position);
            transform.localRotation = EasingFunctions.Ease(EasingFunctions.Type.In, t, startRot, rotation);
            yield return null;
        }

        transform.localPosition = position;
        transform.localRotation = rotation;
    }

    void SetSpot(Transform parent, Vector3 position, Quaternion rotation)
    {
        transform.SetParent(parent);
        transform.localPosition = position;
        transform.localRotation = rotation;
    }
}
