﻿//#define DEMO
/*
 * 
 * 
 * This script handles the interaction with assembly pieces.
 * The script is used for both real controllers and for 'ghost' controllers (controlled over network by a different player) 
 *  
 * TODO
 * Evaluate and remove obsolete functions
 * Make it more generic again so the script is not VR dependent
 * Seperate network logic from standard logic
 * 
 */

using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Helpers;
using XBOX;
using System.Threading;
using System.Collections;

public class PickupController : MonoBehaviour
{
    enum TriggerState
    {
        None,
        Down,
        Up,
        Pressed,
    }

    public ControllerIndex ControllerIndex;
    public ViveController Controller { get { return ViveHelper.Controllers[ControllerIndex]; } private set { } }
    public LineRenderer ProxyLine;
    public AudioClip RefusePickupClip;
    public Transform Hand;
    public float EditModeDistance = 0.04f;
    public Material GhostMaterial;
    public Material GhostSnapMaterial;
    public Material GhostOccupiedMaterial;
    public Material GhostMissingDependencyMaterial;
    public Material OutlineMaterial;
    public Material ControllerOutline;
    public static Material GlobalGhostMaterial;
    public static Material GlobalGhostSnapMaterial;
    public static Material GlobalGhostOccupiedMaterial;
    public static Material GlobalGhostMissingDependancyMaterial;
    public static Material GlobalOutlineMaterial;
    public bool HasController = true;
    public bool CatchEvents = true;
    public NetworkObject NetworkObject;
    //public AssemblyPiece PickedUpPiece { get { return _pickedUpPiece; } private set { } }
    public Pickable PickedUpPickable { get { return _pickedUpPickable; } private set { } }
    public bool CarriesObject { get { return _pickedUpPickable != null; } private set { } }
    public bool AllowPicking { get { return _bAllowPicking && GlobalAllowPicking; } set { _bAllowPicking = value; } }
    public static bool GlobalAllowPicking = true;
    public static bool GlobalAllowConnecting = true;
    public static float MaxConnectionDistance = 0.2f;//0.4 test Picanol?
    public static float ProxyPickupDistance = 0.2f;
    public static float MaxConnectionDistanceSqr = MaxConnectionDistance * MaxConnectionDistance;

    PickupBeam _beam;
    List<AssemblyPiece> _pieces;
    Pickable _gazeItem;
    Pickable _proxyItem;
    Pickable _pickedUpPickable;
    //AssemblyPiece _pickedUpPiece;
    Transform _head;
    float _gazeDistance = 100f;
    bool _bAllowPicking = true;

    bool _bGrabRoot = true;
    Vector3 _lastControllerPosition;
    LimitedQueue<Vector3> _lastControllerPositions = new LimitedQueue<Vector3>(3);
    float _releaseSpeed = 2500.0f;
    Dictionary<Renderer, Material> _controllerMaterials = new Dictionary<Renderer, Material>();
    Dictionary<Renderer, bool> _controllerRenders = new Dictionary<Renderer, bool>();
    ConnectionMatch _lastMatch = null;
    ConnectionPoint _lastProxyCP = null;
    bool _bProxyCPeAvailable = false;

    TriggerState simulatedTrigger = TriggerState.None;

    // Events
    public delegate void ActionDelegate();
    public delegate void APDelegate(AssemblyPiece ap);
    public delegate void CPDelegate(ConnectionPoint cp);
    public delegate void CMDelegate(ConnectionMatch cm);

    public event APDelegate OnAPPickedUp;
    public event APDelegate OnAPPlaced;
    public event APDelegate OnAPDropped;
    public event ActionDelegate OnDropped;
    public event ActionDelegate OnGhostConnectionLost;
    public event ActionDelegate OnValidGhostConnection;
    public static event CPDelegate OnProxyCPAvailable;
    public static event ActionDelegate OnProxyCPUnavailable;
    public static event ActionDelegate OnPickedUpGlobal;
    public static event APDelegate OnAPPickedUpGlobal;
    public static event ActionDelegate OnPlacedGlobal;
    public static event APDelegate OnAPPlacedGlobal;
    public static event ActionDelegate OnDroppedGlobal;
    public static event ActionDelegate OnRefusedPickup;
    public static event CMDelegate OnConnectionMatch;
    public static event CMDelegate OnConnectionMatchLost;

    void Awake()
    {
        _beam = GetComponent<PickupBeam>();
        if (_beam != null)
            _beam.AttachToHand(Hand);
        _head = Camera.main.transform;

        GlobalGhostMaterial = GhostMaterial;
        GlobalGhostSnapMaterial = GhostSnapMaterial;
        GlobalGhostOccupiedMaterial = GhostOccupiedMaterial;
        GlobalGhostMissingDependancyMaterial = GhostMissingDependencyMaterial;
        GlobalOutlineMaterial = OutlineMaterial;

        OnAPDropped += Dropped;
    }

    void Dropped(AssemblyPiece ap = null)
    {
        if (OnDropped != null)
            OnDropped();
    }

    void Start()
    {
        _pieces = FindObjectsOfType<AssemblyPiece>().ToList();
        //SetControllerHand();
    }

    void SetControllerHand()
    {
        if (HasController)
            Hand = Controller.Transform;
    }

    public void SimulateTriggerDown()
    {
        simulatedTrigger = TriggerState.Down;
    }

    public void SimulateTriggerUp()
    {

        simulatedTrigger = TriggerState.Up;
    }

    bool HasSimulated(TriggerState t)
    {
        return simulatedTrigger == t;
    }

    void Update()
    {
        ProxyLine.enabled = false;
        //Debug.Log("pickupcontroller: " + _pickedUpPickable + " | " + AllowPicking);
        if (_pickedUpPickable == null && AllowPicking) //pickup objects using beam
        {
            Pickable proxyItem = null;
            //var proxyPiece = ConnectionManager.FindClosestProximityPiece(Hand.position, ProxyPickupDistance);
            //if (proxyPiece != null)
            //    proxyItem = proxyPiece.Pickable;
            proxyItem = PickupManager.FindClosestProximityPickable(Hand.position, ProxyPickupDistance);
            // Disable proxy picking on containers for now
            if (proxyItem != null && proxyItem.GetComponent<AssemblyPieceContainer>() != null)
                proxyItem = null;

            if (Controller.GetPress(ControllerButton.Trigger) || HasSimulated(TriggerState.Pressed))
            {
                if(proxyItem != null)
                {
                    //_gazeItem = proxyPiece.GetComponent<Pickable>();
                    //proxyPiece.Pickable.UnTarget();
                    //Pickup();
                    //_gazeItem = null;
                    //proxyPiece = null;
                    HandleProxyItem(proxyItem);
                    return;
                }

                if (_beam != null)
                    //Draw gaze beam
                    _beam.ShowBeam();

                //Find objects with beam
                var prevGazeItem = _gazeItem;
                _gazeItem = GetGazeItem();
                //Debug.Log("gaze item: " + _gazeItem);
                if (_gazeItem != prevGazeItem)
                {
                    if (prevGazeItem != null)
                        prevGazeItem.UnTarget();

                    if (_gazeItem != null)
                        _gazeItem.Target();
                }
            }
            else if(Controller.GetPressUp(ControllerButton.Trigger) || HasSimulated(TriggerState.Up))//UNGAZE
            {
                ProxyLine.enabled = false;
                _proxyItem = null;

                if (_beam != null)
                    _beam.HideBeam();

                if (_gazeItem != null)
                {
                    _gazeItem.UnTarget();
                    MaterialManager.GetInstance().UnfadeAll();
                    //MaterialManager.GetInstance().UnglowAll();
                    if (_gazeItem.Type == PickableType.AssemblyPiece)
                        _gazeItem.GetComponent<AssemblyPiece>().SetOutlined(false, true);

                    //Pickup gazeitem
                    Pickup();
                }

                _gazeItem = null;
            }
            else
            {
                HandleProxyItem(proxyItem);
            }

            // Check for connection edit mode
            //if(_pickedUpPickable == null)
            //{
            //    var closestCP = ConnectionManager.FindClosestConnectedPoint(Hand.position, EditModeDistance);
            //    if(closestCP != null && closestCP != _lastProxyCP)
            //    {
            //        // Enable EditMode
            //        _bProxyCPeAvailable = true;
            //        _lastProxyCP = closestCP;
            //        if (OnProxyCPAvailable != null)
            //            OnProxyCPAvailable(closestCP);
            //    }
            //    else if(_lastProxyCP != null && closestCP == null)
            //    {
            //        // Stop EditMode
            //        _bProxyCPeAvailable = false;
            //        _lastProxyCP = null;
            //        if (OnProxyCPUnavailable != null)
            //            OnProxyCPUnavailable();
            //    }
            //}
            //else if(_bProxyCPeAvailable)
            //{
            //    // Stop EditMode
            //    _bProxyCPeAvailable = false;
            //    _lastProxyCP = null;
            //    if (OnProxyCPUnavailable != null)
            //        OnProxyCPUnavailable();
            //}
        }
        else if(_pickedUpPickable != null) //we have _pickedUpObject
        {
            if (_beam != null)
                _beam.HideBeam();

            switch (_pickedUpPickable.Type)
            {
                case PickableType.AssemblyPiece:
                    HandleCarryingAssemblyPiece();
                    break;
                case PickableType.Tool:
                    HandleCarryingTool();
                    break;
                case PickableType.Misc:
                    HandleCarryingMisc();
                    break;
            }  
        }

        _lastControllerPosition = Hand.position;
        _lastControllerPositions.Enqueue(Hand.position);

        //Simulate trigger update
        if (HasSimulated(TriggerState.Down))
        {
            simulatedTrigger = TriggerState.Pressed;
        }
        else if (HasSimulated(TriggerState.Up))
        {
            simulatedTrigger = TriggerState.None;
        }
    }

    void HandleProxyItem(Pickable proxyItem)
    {
        // If the controller is close to an ap simply pick it up, if not activate the beam to pull an ap inside the aim

        if (proxyItem != null && proxyItem != _gazeItem)
        {
            if (_gazeItem != null)
            {
                _gazeItem.UnTarget();
            }
            if (proxyItem.Type == PickableType.AssemblyPiece && proxyItem.GetComponent<AssemblyPiece>().IsLocked)
            {
                proxyItem = null;
                return;
            }

            _gazeItem = proxyItem;
            //ProxyLine.transform.SetParent(Hand);
            ProxyLine.enabled = true;
            ProxyLine.SetPosition(0, Hand.position);
            ProxyLine.SetPosition(1, proxyItem.transform.position);
            proxyItem.Target();
            _proxyItem = proxyItem;
            return;
        }
        else if (proxyItem == null)
        {
            if (_gazeItem != null && _proxyItem == _gazeItem)
            {
                _gazeItem.UnTarget();
                _gazeItem = null;
            }

            if (_proxyItem != null)
                _proxyItem.UnTarget();
            _proxyItem = null;

            ProxyLine.enabled = false;
        }
        else
        {
            ProxyLine.enabled = true;
            ProxyLine.SetPosition(0, Hand.position);
        }
    }

    void HandleCarryingAssemblyPiece()
    {
        if (_pickedUpPickable == null) Debug.LogWarning("_pickedUpPickable ln 341 is null");
        var ap = _pickedUpPickable.GetComponent<AssemblyPiece>();

        if (ap.OnPrePose)
        {
            if(Vector3.Distance(ap.transform.position, Hand.transform.position) > ConnectionManager.ConnectionDistance)
            {
                ap.transform.SetParent(Hand);
                ap.transform.localPosition = Vector3.zero;
                ap.UnSnapFromPrePose();
            }
            else
            {
                return;
            }
        }

        ConnectionMatch match = null;
        if (GlobalAllowConnecting)
            match = ConnectionManager.FindClosestConnection(ap);//GetClosestConnection();
        
        if (match != null)
        {
            if(match.B.ParentPiece.RootPiece == match.A.ParentPiece.RootPiece)
            {
                Debug.LogError("found invalid match!");
                ap.SetGhostActive(false);
                return;
            }

            var apCP = match.A.ParentPiece == ap ? match.A : match.B;
            var otherApCP = apCP == match.A ? match.B : match.A;
            var rootB = otherApCP.ParentPiece;
            var snapState = ObjectStateRecorder.Instance.GetPlaybackSnapState(ap, rootB);
            //Check Playback Match State

            ap.SetGhostConnected(match, snapState, this);
            if (ap.ValidPlacement)
            {
                if(apCP.Action != ToolAction.None)
                {
                    if(!ap.OnPrePose)
                        ap.SnapToPrePose(apCP, otherApCP);

                    if(!PickupManager.IsCarryingTool())
                    {
                        TooltipManager.AttachTooltip(TooltipType.MissingTool, ap.gameObject);
                    }
                    else
                    {
                        TooltipManager.ClearParent(ap.gameObject, TooltipType.MissingTool);
                    }
                }
                else if(otherApCP.Action != ToolAction.None)
                {
                    ap.SetGhostActive(false);
                }
                else if (!apCP.CorrectProperties)
                {
                    TooltipManager.AttachTooltip(TooltipType.IncorrectProperty, ap.gameObject);
                }
                else
                {
                    TooltipManager.ClearParent(ap.gameObject, TooltipType.IncorrectProperty);
                    //var rootB = match.B.GetComponentInParent<AssemblyPiece>();
                    //if (rootB == pickedUpAP) rootB = match.A.GetComponentInParent<AssemblyPiece>();
                    //var snapState = ObjectStateRecorder.Instance.GetPlaybackSnapState(pickedUpAP, rootB);
                    //if (snapState != null)
                    //{
                    //    if(pickedUpAP.GhostObject != null)
                    //    pickedUpAP.GhostObject.transform.localPosition = snapState.localPos;
                    //    pickedUpAP.GhostObject.transform.localRotation = snapState.localRot;
                    //}
                    if ((HasController && Controller.GetPressUp(ControllerButton.Trigger)) || HasSimulated(TriggerState.Up))
                    {
                        DropAPInPlace(ap, match);

                        if (OnAPDropped != null)
                            OnAPDropped(ap);

                        ObjectStateRecorder.Instance.DropAssemblyPiece(ap);
                    }
                }

                if (OnValidGhostConnection != null)
                    OnValidGhostConnection();
            }
            else if(ap.OnPrePose)
            {
                ap.transform.SetParent(Hand);
                ap.transform.localPosition = Vector3.zero;
                TooltipManager.ClearParent(ap.gameObject, TooltipType.MissingTool);
            }
            else
            {
                ViveHelper.PulseController(ControllerIndex, 0.1f);
            }

            if (OnConnectionMatch != null)
                OnConnectionMatch(match);
        }
        else
        {
            if (ap.OnPrePose)
            {
                ap.transform.SetParent(Hand);
                ap.transform.localPosition = Vector3.zero;
            }

            TooltipManager.ClearParent(ap.gameObject, TooltipType.MissingTool);
            TooltipManager.ClearParent(ap.gameObject, TooltipType.IncorrectProperty);

            if(GuideUtils.PreviewConnection != null && GuideUtils.PreviewConnection.Piece == ap)
            {
                ap.SetGhostActive(false);
                ap.PreviewGhostConnected(GuideUtils.PreviewConnection.ForeignCP, GuideUtils.PreviewConnection.OwnCP);
            }
            else
            {
                ap.SetGhostActive(false);
            }

            if ((HasController && Controller.GetPressUp(ControllerButton.Trigger)) || HasSimulated(TriggerState.Up))
            {
                ObjectStateRecorder.Instance.DropAssemblyPiece(ap);
                DropAPToGround(ap);

                if (OnAPDropped != null)
                    OnAPDropped(ap);
            }

            if (_lastMatch != null)
            {
                var action = OnGhostConnectionLost;
                if (action != null)
                    action();

                if (OnConnectionMatchLost != null)
                    OnConnectionMatchLost(_lastMatch);
            }
        }

        _lastMatch = match;
    }

    void HandleCarryingTool()
    {
        var tool = _pickedUpPickable.GetComponent<Tool>();

        if (!tool.IsHandLocked && (HasController && Controller.GetPressUp(ControllerButton.Trigger)) || HasSimulated(TriggerState.Up))
        {
            DropToolToGround(tool);
        }
    }

    void HandleCarryingMisc()
    {
        if ((HasController && Controller.GetPressUp(ControllerButton.Trigger)) || HasSimulated(TriggerState.Up))
        {
            DropMiscToGround(_pickedUpPickable);
        }
    }

    public void HideVisuals()
    {
        ProxyLine.enabled = false;
        _beam.HideBeam();
        //MaterialManager.GetInstance().UnfadeAll();
    }

    IEnumerator PickupAnimation()
    {
        Vector3 startPos = _pickedUpPickable.transform.localPosition;
        //Quaternion startRot = _pickedUpPickable.transform.localRotation;
        Vector3 startRot = _pickedUpPickable.transform.eulerAngles;
        Vector3 endRot = Vector3.zero;
        Vector3 endPos = Vector3.zero;
        if (_pickedUpPickable.Type == PickableType.Tool || _pickedUpPickable.Type == PickableType.Misc)
            endRot = _pickedUpPickable.LockRotation;
        if (_pickedUpPickable.Type == PickableType.Tool || _pickedUpPickable.Type == PickableType.Misc)
            endPos = _pickedUpPickable.OffsetPosition;
        float t = 0f;
        while (t < 1f)
        {
            if (_pickedUpPickable == null) yield break;
            t = Mathf.Clamp01(t + Time.deltaTime * 2f);

            _pickedUpPickable.transform.localPosition = EasingFunctions.Ease(EasingFunctions.Type.InOut, t, startPos, endPos);
            //_pickedUpObject.transform.localRotation = EasingFunctions.Ease(EasingFunctions.Type.InOut, t, startRot, Quaternion.identity);

            if (_pickedUpPickable.Type == PickableType.Tool || _pickedUpPickable.Type == PickableType.Misc)
                _pickedUpPickable.transform.localEulerAngles = EasingFunctions.Ease(EasingFunctions.Type.InOut, t, startRot, endRot);

            yield return null;
        }
    }

    public void Reset()
    {
        if (_pickedUpPickable != null)
        {
            if(_pickedUpPickable.Type == PickableType.AssemblyPiece)
            {
                var ap = _pickedUpPickable.GetComponent<AssemblyPiece>();
                ap.SetGhostActive(false);
                DropAPToGround(ap);
            }
            else if(_pickedUpPickable.Type == PickableType.Tool)
            {
                DropToolToGround(_pickedUpPickable.GetComponent<Tool>());
                EnableRenderers();
                _pickedUpPickable = null;
            }
        }
    }

    public bool Pickup(bool checkGuidance = true)
    {
        var pickedUpObject = _gazeItem;
        if (_gazeItem == null)
        {
            return false;
        }

        if(checkGuidance && _gazeItem.Type != PickableType.Misc && GuideUtils.AllowedPickables.Count > 0 && !GuideUtils.AllowedPickables.Contains(_gazeItem))
        {
            RefusePickup();
            return false;
        }

        switch (_gazeItem.Type)
        {
            case PickableType.AssemblyPieceContainer:
                return PickupAssemblyPieceContainer();
            case PickableType.AssemblyPiece:
                return PickupAssemblyPiece();
            case PickableType.Tool:
                return PickupTool();
            case PickableType.Misc:
                return PickupMisc();
        }

        return false;
    }

    public void RefusePickup()
    {
        var feedbackDuration = 0.6f;
        PickupManager.ShakePickable(_gazeItem, feedbackDuration);
        if (HasController)
        {
            ViveHelper.PulseController(ControllerIndex, feedbackDuration);
        }

        //Sound
        AudioSource.PlayClipAtPoint(RefusePickupClip, _gazeItem.transform.position, 0.5f);

        if (OnRefusedPickup != null)
            OnRefusedPickup();
    }

    bool PickupAssemblyPieceContainer()
    {
        var container = _gazeItem.GetComponent<AssemblyPieceContainer>();
        var spawnedPiece = container.SpawnPiece().GetComponent<Pickable>();
        StartCoroutine(MoveToAndPickup(spawnedPiece, spawnedPiece.transform.position + Vector3.up * container.JumpHeight));
        return true;
    }

    bool PickupAssemblyPiece()
    {
        if (_bGrabRoot)
        {
            var aPiece = _gazeItem.GetComponent<AssemblyPiece>();
            if (aPiece.PickableRootPiece != aPiece)
            {
                if (aPiece.CanDisconnect)
                    aPiece.Detach();
                _gazeItem = aPiece.PickableRootPiece.GetComponent<Pickable>();
            }
        }

        _pickedUpPickable = _gazeItem;
        var pickedUpPiece = _pickedUpPickable.GetComponent<AssemblyPiece>();

        // Check if object can be picked up due to its connection point dependencies
        if (!pickedUpPiece.CanDisconnect)
        {
            _pickedUpPickable = null;
            return false;
        }

        pickedUpPiece.Pickup(this);

        if (HasController)
        {
            ViveHelper.Controllers[ControllerIndex].Device.TriggerHapticPulse(1000);
            DisableRenderers();
        }

        string parent = _pickedUpPickable.transform.parent ? _pickedUpPickable.transform.parent.name : "world";
        Debug.Log("piece " + _pickedUpPickable.name + " from " + parent);
        _pickedUpPickable.transform.SetParent(Hand);
        //Debug.Log("piece: " + _pickedUpPickable.gameObject.GetInstanceID() + " - parent: " + _pickedUpPickable.transform.parent);
        
        StartCoroutine(PickupAnimation());

        ObjectStateRecorder.Instance.PickupAssemblyPiece(pickedUpPiece);

        //if (NetworkObject && CatchEvents)
        //{
        //
        //    NetworkHandling.Instance.SendTwinPosRotUpdate(pickedUpPiece.GetComponent<NetworkObject>(), true);
        //    Thread.Sleep(50);
        //    NEAT_Integer intArg = new NEAT_Integer();
        //    intArg.Value = pickedUpPiece.GetComponent<NetworkObject>().NUID;
        //    intArg.Serialize();
        //    if (_bGrabRoot)
        //        NetworkObject.CatchAction(NetworkActions.PickupController_Pickup_Root, intArg.Bytes);
        //    else
        //        NetworkObject.CatchAction(NetworkActions.PickupController_Pickup, intArg.Bytes);
        //}

        pickedUpPiece.CanBeConnectedTo = true;
        PickedUp(pickedUpPiece);

        return true;
    }

    bool PickupTool()
    {
        _pickedUpPickable = _gazeItem;
        _pickedUpPickable.GetComponent<Rigidbody>().isKinematic = true;

        if (HasController)
        {
            ViveHelper.Controllers[ControllerIndex].Device.TriggerHapticPulse(1000);
            HideController();
        }

        _pickedUpPickable.transform.SetParent(Hand);
        var tool = _pickedUpPickable.GetComponent<Tool>();
        tool.GetComponent<Rigidbody>().isKinematic = true;
        tool.Pickup(this);
        StartCoroutine(PickupAnimation());

        if (OnPickedUpGlobal != null)
            OnPickedUpGlobal();

        return true;
    }

    bool PickupMisc()
    {
        _pickedUpPickable = _gazeItem;
        _pickedUpPickable.GetComponent<Rigidbody>().isKinematic = true;

        if (HasController)
        {
            ViveHelper.Controllers[ControllerIndex].Device.TriggerHapticPulse(1000);
            HideController();
        }

        _pickedUpPickable.transform.SetParent(Hand);
        var pickable = _pickedUpPickable.GetComponent<Pickable>();
        pickable.GetComponent<Rigidbody>().isKinematic = true;
        pickable.Pickup(this);
        StartCoroutine(PickupAnimation());

        if (OnPickedUpGlobal != null)
            OnPickedUpGlobal();

        return true;
    }

    const float __updateSpeed = 5f;

    IEnumerator MoveToAndPickup(Pickable pickable, Vector3 position)
    {
        AllowPicking = false;
        var startPos = pickable.transform.position;
        float t = 0f;
        while (t < 1f)
        {
            t = Mathf.Clamp01(t + Time.deltaTime * __updateSpeed);
            pickable.transform.position = EasingFunctions.Ease(EasingFunctions.Type.Out, t, startPos, position);
            yield return null;
        }

        _gazeItem = pickable;
        Pickup(false);
        AllowPicking = true;
    }

    public void Pickup(Pickable pickable)
    {
        Pickup(pickable, pickable.transform.position);
    }

    public void Pickup(Pickable pickable, Vector3 position)
    {
        if (PickedUpPickable != null)
            return;

        StartCoroutine(MoveToAndPickup(pickable, position));
    }

    public void PickupFromNetwork(byte[] data)
    {
        NEAT_Integer intArg = new NEAT_Integer();
        intArg.Bytes = data;
        intArg.Deserialize();
        _gazeItem = NetworkHandling.NetworkObjects[intArg.Value].GetComponent<Pickable>();
        PickupSeperate();
    }

    public void PickupRoot()
    {
        _bGrabRoot = true;
        Pickup();
    }

    public void PickupSeperate()
    {
        _bGrabRoot = false;
        Pickup();
    }

    public void PickupRootFromNetwork(byte[] data)
    {
        NEAT_Integer intArg = new NEAT_Integer();
        intArg.Bytes = data;
        intArg.Deserialize();
        _gazeItem = NetworkHandling.NetworkObjects[intArg.Value].GetComponent<Pickable>();
        PickupRoot();
    }

    void PickedUp(AssemblyPiece piece)
    {
        if (OnAPPickedUp != null)
            OnAPPickedUp(piece);

        if (OnAPPickedUpGlobal != null)
            OnAPPickedUpGlobal(piece);

        if (OnPickedUpGlobal != null)
            OnPickedUpGlobal();
    }

    public static GameObject FindRootPiece(GameObject piece)
    {
        var parentPiece = piece.transform.parent;
        if (parentPiece && parentPiece.GetComponent<AssemblyPiece>())
        {
            return FindRootPiece(parentPiece.gameObject);
        }

        return piece;
    }

    public void DropAPInPlace(AssemblyPiece ap, ConnectionMatch cMatch = null)
    {
        NEAT_GhostConnection ghostConnectionArg = new NEAT_GhostConnection();

        ap.DropAsGhost(ref ghostConnectionArg, cMatch);

        //if (NetworkObject && CatchEvents)
        //{
        //    NetworkHandling.Instance.SendTwinPosRotUpdate(ap.RootPiece.GetComponent<NetworkObject>(), true);
        //    Thread.Sleep(50);
        //    //NetworkHandling.Instance.SendTwinPosRotUpdate(_pickedUpObject.GetComponent<NetworkObject>(), true);
        //    ghostConnectionArg.Serialize();
        //    NetworkObject.CatchAction(NetworkActions.PickupController_DropInPlace, ghostConnectionArg.Bytes);
        //}


        //before we set the pickd up object to null we have to destroy the trail

        // check if the picked up object has a trail connector
        if (ap.GetComponent<TrailConnection>() != null)
        {
            // get trail connection component
            TrailConnection tc = ap.GetComponent<TrailConnection>();

            // destroy the trail
            tc.DestroyTrail();
            Debug.Log("released the object with trail connection");
        }

        if (HasController)
        {
            //ViveHelper.Controllers[ControllerIndex].Device.TriggerHapticPulse(1000);
            ViveHelper.PulseController(ControllerIndex, .5f);
            EnableRenderers();
        }

        //pickedUpAP.OnDetach += pickedUpAP.Unlock;
        Placed(ap);
        //_pickedUpPiece.CanBeConnectedTo = true;

        // now we set the picked up object to null
        _pickedUpPickable = null;
        _gazeItem = null;
    }

    void Placed(AssemblyPiece piece)
    {
        if (OnAPPlaced != null)
            OnAPPlaced(piece);

        if (OnAPPlacedGlobal != null)
            OnAPPlacedGlobal(piece);

        if (OnPlacedGlobal != null)
            OnPlacedGlobal();
    }

    void DisableRenderers()
    {
        foreach (var rend in ViveHelper.Controllers[ControllerIndex].Transform.GetComponentsInChildren<Renderer>(true))
        {
            if (rend.gameObject.name != "body") //Don't disable body
            {
                if (!_controllerRenders.ContainsKey(rend))
                    _controllerRenders.Add(rend, rend.enabled);
                _controllerRenders[rend] = rend.enabled;
                rend.enabled = false;
            }
            else
            {
                if (!_controllerMaterials.ContainsKey(rend))
                    _controllerMaterials.Add(rend, null);

                _controllerMaterials[rend] = rend.sharedMaterial;
                rend.material = ControllerOutline;
            }
        }
    }

    void HideController()
    {
        foreach (var rend in ViveHelper.Controllers[ControllerIndex].Transform.GetComponentsInChildren<Renderer>(true))
        {
            if (!_controllerMaterials.ContainsKey(rend))
                _controllerMaterials.Add(rend, null);

            if (!_controllerRenders.ContainsKey(rend))
                _controllerRenders.Add(rend, rend.enabled);

            _controllerRenders[rend] = rend.enabled;
            _controllerMaterials[rend] = rend.sharedMaterial;

            rend.enabled = false;
        }
    }

    public void EnableRenderers()
    {
        foreach (var rend in ViveHelper.Controllers[ControllerIndex].Transform.GetComponentsInChildren<Renderer>(true))
        {
            if(_controllerRenders.ContainsKey(rend))
                rend.enabled = _controllerRenders[rend];
            if (_controllerMaterials.ContainsKey(rend))
                rend.material = _controllerMaterials[rend];
        }
    }

    public void DropInPlaceFromNetwork(byte[] data)
    {
        NEAT_GhostConnection ghostConnectionArg = new NEAT_GhostConnection();
        ghostConnectionArg.Bytes = data;
        ghostConnectionArg.Deserialize();
        var ap = _pickedUpPickable.GetComponent<AssemblyPiece>();
        var parentAP = NetworkHandling.NetworkObjects[ghostConnectionArg.ParentNuid].GetComponent<AssemblyPiece>();
        ap.DropAsGhost(parentAP, ghostConnectionArg.GetPosition(), ghostConnectionArg.GetRotation());
        _pickedUpPickable = null;
    }

    public void ForceReleaseObject()
    {
        if (_pickedUpPickable == null) return;

        _pickedUpPickable.transform.SetParent(null);
        _pickedUpPickable = null;

        if (HasController)
        {
            ViveHelper.Controllers[ControllerIndex].Device.TriggerHapticPulse(1000);
            EnableRenderers();
        }
    }

    public void ReleaseAndResetPickable()
    {
        if (_pickedUpPickable == null)
            return;

        if (HasController)
        {
            ViveHelper.Controllers[ControllerIndex].Device.TriggerHapticPulse(1000);
            EnableRenderers();
        }

        if(_pickedUpPickable.Type == PickableType.AssemblyPiece)
            _pickedUpPickable.GetComponent<AssemblyPiece>().DropToGround(Vector3.zero);

        _pickedUpPickable.ResetPosition();
        _pickedUpPickable = null;
        _gazeItem = null;
    }

    public void DropAPToGround(AssemblyPiece ap)
    {
        var force = (Hand.position - _lastControllerPositions.Dequeue()) * _releaseSpeed;
        DropAPToGround(ap, force);
    }

    public void DropAPToGround(AssemblyPiece ap, Vector3 force)
    {
        ap.DropToGround(force);
        ap.RecordStateChanged();
        if (NetworkObject && CatchEvents)
        {
            NEAT_Vector3 nforce = new NEAT_Vector3();
            nforce.X = force.x;
            nforce.Y = force.y;
            nforce.Z = force.z;
            nforce.Serialize();
            NetworkObject.CatchAction(NetworkActions.PickupController_DropToGround, nforce.Bytes);
            //Thread.Sleep(50);
            //NetworkHandling.Instance.SendTwinPosRotUpdate(_pickedUpObject.GetComponent<NetworkObject>(), true);
        }

        //before we set the pickd up object to null we have to destroy the trail

        // check if the picked up object has a trail connector
        if (ap.GetComponent<TrailConnection>() != null)
        {
            // get trail connection component
            TrailConnection tc = ap.GetComponent<TrailConnection>();

            // destroy the trail
            tc.DestroyTrail();
            Debug.Log("released the object with trail connection");
        }

        _pickedUpPickable = null;
        _gazeItem = null;

        if (HasController)
        {
            ViveHelper.PulseController(ControllerIndex, 0.3f);
            //ViveHelper.Controllers[ControllerIndex].Device.TriggerHapticPulse(1000);
            EnableRenderers();
        }

        if (OnDroppedGlobal != null)
            OnDroppedGlobal();
    }

    public void DropToolToGround(Tool tool)
    {
        var force = (Hand.position - _lastControllerPositions.Dequeue()) * _releaseSpeed;
        DropToolToGround(tool, force);
    }

    public void DropToolToGround(Tool tool, Vector3 force)
    {
        tool.DropToGround(force);

        _pickedUpPickable = null;
        _gazeItem = null;

        if (HasController)
        {
            ViveHelper.Controllers[ControllerIndex].Device.TriggerHapticPulse(1000);

            EnableRenderers();
        }

        if (OnDroppedGlobal != null)
            OnDroppedGlobal();
    }

    public void DropMiscToGround(Pickable pickable)
    {
        var force = (Hand.position - _lastControllerPositions.Dequeue()) * _releaseSpeed;
        DropMiscToGround(pickable, force);
    }

    public void DropMiscToGround(Pickable pickable, Vector3 force)
    {
        pickable.ResetPosition();

        _pickedUpPickable = null;
        _gazeItem = null;

        if (HasController)
        {
            ViveHelper.Controllers[ControllerIndex].Device.TriggerHapticPulse(1000);

            EnableRenderers();
        }

        if (OnDroppedGlobal != null)
            OnDroppedGlobal();
    }

    public void DropToGroundFromNetwork(byte[] data)
    {
        NEAT_Vector3 forceArg = new NEAT_Vector3();
        forceArg.Bytes = data;
        forceArg.Deserialize();
        var force = new Vector3(forceArg.X, forceArg.Y, forceArg.Z);
        DropAPToGround(_pickedUpPickable.GetComponent<AssemblyPiece>(), force);
    }

    GameObject GetGazeObject()
    {
        GameObject gazeObject = null;
        RaycastHit hit;

        if (!Hand)
            SetControllerHand();

        var viveControllerPos = Hand.position;
        //var rayStart = _head.position;
        //var rayDirection = _head.forward;
        //var radius = 1.5f;
        RaycastHit hitInfo;
        Ray r = new Ray(Hand.transform.position, Hand.transform.forward);
        Debug.DrawRay(r.origin, r.direction);
        if (Physics.Raycast(r, out hitInfo, 100f, 1 << LayerMask.NameToLayer("PickItems")))//Physics.OverlapSphere(viveControllerPos, 3, LayerMask.NameToLayer("Floor"));
        {
            gazeObject = hitInfo.collider.gameObject;
        }
        //if (colliders.Length > 0)
        //{
        //    gazeObject = colliders[0].gameObject;
        //    //Debug.Log("gazing at: " + gazeObject);
        //}
        //if (Physics.SphereCast(rayStart, radius, rayDirection, out hit))
        //{
        //    gazeObject = hit.collider.gameObject;
        //}

        return gazeObject;
    }

    Pickable GetGazeItem()
    {
        var gazeObject = GetGazeObject();
        if (!gazeObject)
            return null;

        var pickable = gazeObject.GetComponent<Pickable>();
        if (pickable == null)
        {
            pickable = gazeObject.transform.parent.GetComponent<Pickable>();
        }

        if (pickable == null)
        {
            return null;
        }

        if(pickable.Type == PickableType.AssemblyPiece)
        {
            var ap = pickable.GetComponent<AssemblyPiece>();
            while (ap.InPlace)
            {
                ap = ap.GetComponentInParent<AssemblyPiece>();
                if (ap == null) break;
            }

            if (!ap.IsLocked)
                return ap.GetComponent<Pickable>();
        }
        else if(pickable.Type == PickableType.AssemblyPieceContainer)
        {
            return pickable;
        }
        else if(pickable.Type == PickableType.Tool || pickable.Type == PickableType.Misc)
        {
            return pickable;
        }

        return null;
    }

    GameObject GetSnapObject(int id)
    {
        var piece = _pieces.FirstOrDefault(p => p.ID == id && p.IsLocked == true);
        if (piece != null)
            return piece.gameObject;

        return null;
    }

    bool CheckRequiredIDs(AssemblyPiece ap)
    {
        var requiredIDObjects = _pieces.Where(p => ap.RequiredIDs.Contains(p.ID) && !p.IsLocked).ToList();

        int passed = 0;
        foreach (var p in requiredIDObjects)
        {
            if (p.InPlace && !p.IsLocked)
            {
                ++passed;
            }
        }

        Debug.Log("passed and req " + passed + " : " + requiredIDObjects.Count);

        if (passed == requiredIDObjects.Count)
        {
            return true;
        }

        return false;
    }
}

public class LimitedQueue<T> : Queue<T>
{
    public T Last { get; private set; }
    private int limit = -1;

    public int Limit
    {
        get { return limit; }
        set { limit = value; }
    }

    public LimitedQueue(int limit)
        : base(limit)
    {
        this.Limit = limit;
    }

    public new void Enqueue(T item)
    {
        if (this.Count >= this.Limit)
        {
            this.Dequeue();
        }
        base.Enqueue(item);
        Last = item;
    }
}
