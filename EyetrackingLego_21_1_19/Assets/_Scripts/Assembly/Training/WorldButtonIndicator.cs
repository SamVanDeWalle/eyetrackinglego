﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldButtonIndicator : MonoBehaviour
{
    public GameObject Arrow;
    public Material IndicateMat;
    PushButton _pushButton;

    void Start()
    {
        _pushButton = GetComponent<PushButton>();
        StopIndicating();
    }

    public void Indicate()
    {
        Arrow.SetActive(true);
        _pushButton.SetMaterial(IndicateMat);
    }

    public void StopIndicating()
    {
        Arrow.SetActive(false);
        _pushButton.ResetMaterial();
    }
}
