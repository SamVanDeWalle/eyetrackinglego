﻿using Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

[Serializable]
public enum TrainingMode
{
    Build,
    Record,
    Guided,
    None,
}

public class TrainingManager : Singleton<TrainingManager>
{
    public Station Station;
    public PositionHighlight PositionHighlight;
    public ViewCircleIndicator ViewCircleIndicator;
    public Texture LoadingTexture;
    public GameObject FinishScreen;
    public string AssemblyName;
    public static int IndexToLoad = 3;
    public static TrainingMode TrainingMode = TrainingMode.Guided;

    public delegate void ActionDelegate();
    public static event ActionDelegate OnTrainingDone;
    public static event ActionDelegate OnReset;

    protected override void Awake()
    {
        base.Awake();

        //DontDestroyOnLoad(gameObject);
        VR_Main.AssemblyName = AssemblyName;
        PositionHighlight.enabled = false;
    }

    void Start()
    {
        if (IndexToLoad == -1)
            return;

        HUDMenu.SubscribeToAction("quit", QuitTraining);
        HUDMenu.SubscribeToAction("test", TestGuided);
        HUDMenu.SubscribeToAction("reset", Reset);
        HandleTrainingMode();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            ResetTraining();
        }

        if (Input.GetKeyDown(KeyCode.Backslash))
        {
            PositionHighlight.enabled = !PositionHighlight.enabled;
        }

        if (Input.GetKeyDown(KeyCode.BackQuote))
        {
            ViewCircleIndicator.ToggleVisibility();
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            AssemblyChecker.AutoCompleteCurrentStep();
        }

        if (Input.GetKeyDown(KeyCode.Asterisk))
        {
            AssemblyRecorder.SaveRecording();
        }
    }

    static void HandleTrainingMode()
    {
        switch (TrainingMode)
        {
            case (TrainingMode.Guided):
                if(Instance.Station != null)
                    Instance.Station.Load();
                var renderers = FindObjectsOfType<Renderer>().ToList();
                foreach (var r in renderers)
                {
                    r.gameObject.AddComponent<MaterialCacher>();
                }
                AssemblyChecker.Activate();
                PickupController.GlobalAllowConnecting = true;
                PickupController.GlobalAllowPicking = true;
                RotationIndicator.Enabled = true;
                break;
                
            case (TrainingMode.Record):
                Instance.Station.Load();
                PickupController.GlobalAllowConnecting = true;
                PickupController.GlobalAllowPicking = true;
                RotationIndicator.Enabled = false;
                AssemblyRecorder.StartRecording();
                break;
                
            case (TrainingMode.Build):
                // change hud menu
                // enable station item manipulation 
                Instance.Station.Load();
                PickupManager.FindPickables();
                // Disable Connecting
                PickupController.GlobalAllowConnecting = false;
                PickupController.GlobalAllowPicking = false;

                break;
                
        }
    }

    public static void StartTraining()
    {
        var trainingScene = "VR_Training_Scene_" + IndexToLoad.ToString("D3");
        //SteamVR_LoadLevel.Begin(trainingScene);
        SceneManager.LoadScene(trainingScene);
    }

    public static void QuitTraining()
    {
        TrainingMode = TrainingMode.None;
        AssemblyChecker.Deactivate();
        //SteamVR_LoadLevel.Begin("VR_Menu_Scene");
        SceneManager.LoadScene("VR_Menu_Scene");
    }

    public static void TrainingDone()
    {
        if(Instance.FinishScreen != null)
        {
            Instance.FinishScreen.SetActive(true);
            Instance.FinishScreen.GetComponent<PopOut>().Pop();
        }

        Instance.StartCoroutine(ResetAfterDone(10.0f));

        if (OnTrainingDone != null)
            OnTrainingDone();
    }

    static IEnumerator ResetAfterDone(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        ResetTraining();
    }

    public static void ResetTraining()
    {
        if (Instance.FinishScreen != null)
        {
            Instance.FinishScreen.GetComponent<PopOut>().Reset();
        }
        GuideUtils.UnHighlightAll();
        
        foreach (var controller in FindObjectsOfType<PickupController>())
        {
            controller.Reset();
        }

        AssemblyPieceManager.Reset();
        foreach (var ap in AssemblyPieceManager.AssemblyPieces)
        {
            ap.Reset();
        }

        //Hardcore material check
        var mcs = FindObjectsOfType<MaterialCacher>().ToList();
        foreach (var mc in mcs)
        {
            mc.Reset();
        }

        AssemblyChecker.Reset();

        ResetManager.Reset();

        if (OnReset != null)
            OnReset();
    }

    public static void TestGuided()
    {
        //AssemblyRecorder.SaveRecording();
        TrainingMode = TrainingMode.Guided;
        HandleTrainingMode();
    }

    public static void Reset()
    {
        switch (TrainingMode)
        {
            case TrainingMode.Guided:
                ResetTraining();
                break;
            case TrainingMode.Record:
                StepRecorder.Instance.UndoAll();
                break;
        }
    }
}
