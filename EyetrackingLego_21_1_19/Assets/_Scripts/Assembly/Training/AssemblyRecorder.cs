﻿using Helpers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssemblyRecorder : Singleton<AssemblyRecorder>
{
    public string RecordingsFolder = "AssemblyRecordings/";
    static AssemblyRecording _recording;
    ConnectionStep _lastConnectionStep;
    ConnectionPoint _lastCP;

    public static void StartRecording()
    {
        _recording = new AssemblyRecording();
        HUDMenu.SubscribeToAction("save", SaveRecording);
    }

    public static void SaveRecording()
    {
        SetRecording();
        _recording.Save("assembly_recording_" + VR_Main.AssemblyName);
    }

    static void SetRecording()
    {
        _recording.TaskSteps.Clear();

        var steps = StepRecorder.Instance.UndoSteps;
        for(var it = steps.First; it != null;)
        {
            var next = it.Next;
            _recording.TaskSteps.Add(it.Value.TaskStep);
            if(it.Value.TaskStep.Type == StepType.Connection)
            {
                var connectionStep = it.Value.TaskStep as ConnectionStep;
                while(next != null && next.Value.TaskStep.Type == StepType.Connection)
                {
                    var nextConnectionStep = next.Value.TaskStep as ConnectionStep;
                    if(connectionStep.Combinable(nextConnectionStep))
                    {
                        connectionStep.Quantity++;
                        next = next.Next;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            it = next;
        }
    }
}
