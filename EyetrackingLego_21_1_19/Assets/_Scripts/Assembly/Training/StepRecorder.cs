﻿using Helpers;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AssemblyPieceRecordState
{
    public AssemblyPiece AssemblyPiece;
    public Transform Parent;
    public Vector3 Position;
    public Quaternion Rotation;
    public Dictionary<ConnectionPoint, ConnectionPoint> Connections = new Dictionary<ConnectionPoint, ConnectionPoint>();

    public AssemblyPieceRecordState(AssemblyPiece piece)
    {
        AssemblyPiece = piece;
        Parent = piece.transform.parent;
        Position = piece.transform.localPosition;
        Rotation = piece.transform.localRotation;

        for(int i = 0; i < piece.ConnectedPoints.Length; ++i)
        {
            Connections.Add(piece.ConnectedPoints[i], piece.ConnectedPoints[i].ConnectedCP);
        }
    }
}

public class RecordedStep
{
    public TaskStep TaskStep;
    public List<ConnectionPoint> ConnectionPoints = new List<ConnectionPoint>();
    public Property Property;
    public Dictionary<AssemblyPiece, AssemblyPieceRecordState> TransformStates = new Dictionary<AssemblyPiece, AssemblyPieceRecordState>();

    public override bool Equals(object obj)
    {
        return obj is RecordedStep && (RecordedStep)obj == this;
    }

    public override int GetHashCode()
    {
        int hash = 17;
        hash = hash * 23 + TaskStep.GetHashCode();
        hash = Property != null ? hash * 23 + Property.GetHashCode() : hash;
        for(int i = 0; i < ConnectionPoints.Count; ++i)
        {
            hash = hash * 23 + ConnectionPoints[i].GetHashCode();
        }
        
        return hash;
    }

    public static bool operator ==(RecordedStep a, RecordedStep b)
    {
        if(a.TaskStep.Type == b.TaskStep.Type)
        {
            if(a.TaskStep.Type == StepType.Connection)
            {
                if (a.ConnectionPoints.Count > 0)
                {
                    if (a.ConnectionPoints.Any(cp => !b.ConnectionPoints.Contains(cp)))
                        return false;
                    else
                        return true;
                }
            }
            else if(a.TaskStep.Type == StepType.PropertyChange)
            {
                return (a.Property != null && a.Property == b.Property);
            }
        }

        return false;
    }

    public static bool operator !=(RecordedStep a, RecordedStep b)
    {
        return !(a == b);
    }
}

public class StepRecorder : Singleton<StepRecorder>
{
    public LinkedList<RecordedStep> UndoSteps = new LinkedList<RecordedStep>();
    public LinkedList<RecordedStep> RedoSteps = new LinkedList<RecordedStep>();

    public delegate void StepAction(RecordedStep step);
    public static event StepAction OnStepRecorded;
    public static event StepAction OnStepUndo;
    public static event StepAction OnStepRedo;

    void Start()
    {
        ConnectionPoint.OnGlobalConnected += HandleCPConnected;
        PropertyHolder.OnPropertyResolved += HandlePropertyResolved;

        HUDMenu.SubscribeToAction("undo", Undo);
        HUDMenu.SubscribeToAction("redo", Redo);
    }

    void HandleCPConnected(ConnectionPoint cp)
    {
        // Check if cp connection is not mirror of previous connection
        if (UndoSteps.Count > 0 && UndoSteps.Last != null && UndoSteps.Last.Value.ConnectionPoints.Contains(cp))
            return;

        RecordedStep recordedStep = new RecordedStep();
        recordedStep.ConnectionPoints.Add(cp);
        recordedStep.ConnectionPoints.Add(cp.ConnectedCP);
        AssemblyPieceRecordState transformStateA = new AssemblyPieceRecordState(cp.ParentPiece);
        recordedStep.TransformStates.Add(cp.ParentPiece, transformStateA);
        AssemblyPieceRecordState transformStateB = new AssemblyPieceRecordState(cp.ConnectedCP.ParentPiece);
        recordedStep.TransformStates.Add(cp.ConnectedCP.ParentPiece, transformStateB);
        ConnectionStep connectionStep = new ConnectionStep();
        connectionStep.ID = UndoSteps.Count;
        connectionStep.Type = StepType.Connection;
        connectionStep.ConnectionTypeID = cp.TypeID;
        connectionStep.Quantity = 1;
        connectionStep.ParentTypeID = cp.Gender == ConnectionGender.Female ? cp.ParentPiece.TypeID : cp.ConnectedCP.ParentPiece.TypeID;
        connectionStep.PrefabTypeID = cp.Gender == ConnectionGender.Female ? cp.ConnectedCP.ParentPiece.TypeID : cp.ParentPiece.TypeID;
        // If the connection is made to a piece connected to a rotating workbench, auto save the current workbench angle
        var workbench = cp.ParentPiece.RootPiece.GetComponent<Workbench>();
        var holderRotator = workbench != null ? workbench.HolderRotator : null; 
        connectionStep.WorkbenchAngle = holderRotator == null ? 0 : holderRotator.GetCurrentAngle();
        connectionStep.RequireWorkbenchAngle = workbench != null;

        recordedStep.TaskStep = connectionStep;

        AddRecordedStep(recordedStep);
    }

    void HandlePropertyResolved(PropertyHolder holder, Property property)
    {
        RecordedStep recordedStep = new RecordedStep();
        recordedStep.Property = property;
        PropertyChangeStep propertyStep = new PropertyChangeStep();
        propertyStep.ID = UndoSteps.Count;
        propertyStep.Type = StepType.PropertyChange;
        propertyStep.PropertyType = property.Type;
        propertyStep.PropertyID = property.ID;
        propertyStep.Quantity = 1;
        propertyStep.PrefabTypeID = holder.GetComponent<AssemblyPiece>().TypeID;
        recordedStep.TaskStep = propertyStep;

        AddRecordedStep(recordedStep);
    }

    void AddRecordedStep(RecordedStep step)
    {
        // If the step is the same as a step in the redo list, remove that one from the redo list
        for(var it = RedoSteps.First; it != null;)
        {
            var next = it.Next;
            if (step == it.Value)
                RedoSteps.Remove(it);

            it = next;
        }

        UndoSteps.AddLast(step);

        if (OnStepRecorded != null)
            OnStepRecorded(step);
    }

    public void Undo()
    {
        Undo(true);
    }

    public void Undo(bool animated = true)
    {
        if (UndoSteps.Count == 0)
            return;

        var recordedStep = UndoSteps.Last.Value;

        if(recordedStep.TaskStep.Type == StepType.Connection)
        {
            // Disconnecting the connection points
            for(int i = 0; i < recordedStep.ConnectionPoints.Count; ++i)
            {
                recordedStep.ConnectionPoints[i].Disconnect();
            }

            foreach(var pieceState in recordedStep.TransformStates)
            {
                var previousTransformState = GetLastPieceState(pieceState.Key);
                if(previousTransformState == null)
                {
                    // Move the piece to its initial state
                    pieceState.Key.Pickable.ResetPosition(animated);
                    pieceState.Key.Unlock();
                    foreach(var controller in PickupManager.ControllerPickables)
                    {
                        Debug.Log("controllerPickable: " + controller.Key + " | " + controller.Value);
                    }
                }
                else
                {
                    // Move the piece to its previous state
                    Debug.Log("prev parent: " + previousTransformState.Parent.gameObject);
                    pieceState.Key.Pickable.MoveTo(previousTransformState.Parent, previousTransformState.Position, previousTransformState.Rotation, animated);
                    // Reconnect disconnected connection points
                    foreach(var connection in pieceState.Value.Connections)
                    {
                        connection.Key.Connect(connection.Value, false, true);
                    }
                }
            }
        }
        else if(recordedStep.TaskStep.Type == StepType.PropertyChange)
        {
            // Reset the property to its initial state, ! currently no multiple property states supported !
            recordedStep.Property.Reset();
        }

        // Shift the step from undo to redo list
        RedoSteps.AddLast(recordedStep);
        UndoSteps.RemoveLast();

        if (OnStepUndo != null)
            OnStepUndo(recordedStep);
    }

    public void Redo()
    {
        if (RedoSteps.Count == 0)
            return;

        var recordedStep = RedoSteps.Last.Value;

        if (recordedStep.TaskStep.Type == StepType.Connection)
        {
            foreach (var pieceState in recordedStep.TransformStates)
            {
                pieceState.Key.Pickable.MoveTo(pieceState.Value.Parent, pieceState.Value.Position, pieceState.Value.Rotation);

                // Disconnect all current connections
                for(int i = 0; i < pieceState.Key.ConnectedPoints.Length; ++i)
                {
                    pieceState.Key.ConnectedPoints[i].Disconnect();
                }

                // Connect state connections
                foreach (var connection in pieceState.Value.Connections)
                {
                    connection.Key.Connect(connection.Value, false, true);
                }
            }
        }
        else if (recordedStep.TaskStep.Type == StepType.PropertyChange)
        {
            recordedStep.Property.AutoCorrectComplete(true);
        }

        // Shift the step from undo to redo list
        UndoSteps.AddLast(recordedStep);
        RedoSteps.RemoveLast();

        if (OnStepRedo != null)
            OnStepRedo(recordedStep);
    }

    AssemblyPieceRecordState GetLastPieceState(AssemblyPiece piece)
    {
        if (UndoSteps.Count < 2)
            return null;

        for(var it = UndoSteps.Last.Previous; it != null;)
        {
            if(it.Value.TransformStates.ContainsKey(piece))
            {
                return it.Value.TransformStates[piece];
            }

            it = it.Previous;
        }

        return null;
    }

    public void UndoAll()
    {
        while(UndoSteps.Count > 0)
        {
            Undo();
        }
    }
}
