﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskStepDisplay : MonoBehaviour
{
    public Image CurrentStepImage, FillVisual;
    public GameObject StepTextContainer, FeedbackContainer;
    public GameObject MainPanel;
    public Text InfoText, NumberingText, RequiredCountText;
    public int Offset;

    AssemblyFicheGeneric _fiche;
    bool _bHasFinishedTraining;

    void Start()
    {
        _fiche = FindObjectOfType<AssemblyFicheGeneric>();
        TaskStep.OnDone += ShowFeedbackAndNextStep;
        TaskStep.OnReset += SetCurrentStep;
        TrainingManager.OnTrainingDone += TrainingHasFinished;
        SetCurrentStep();
    }

    void OnDestroy()
    {
        TaskStep.OnDone -= SetCurrentStep;
        TrainingManager.OnTrainingDone -= TrainingHasFinished;
    }

    void ShowFeedbackAndNextStep()
    {
        StartCoroutine(ShowFeedbackAndShowNextStep());
    }

    IEnumerator ShowFeedbackAndShowNextStep()
    {
        FillVisual.gameObject.SetActive(true);
        float fillSpeed = 4.0f;
        float fill = 0;
        while (fill < 1.0f)
        {
            FillVisual.fillAmount = fill;
            fill += Time.deltaTime * fillSpeed;
            yield return new WaitForSeconds(Time.deltaTime);
        }

        fill = 1.0f;
        FillVisual.fillAmount = fill;
        StepTextContainer.SetActive(false);
        FeedbackContainer.SetActive(true);

        yield return new WaitForSeconds(1.5f);

        while (fill > 0.0f)
        {
            FillVisual.fillAmount = fill;
            fill -= Time.deltaTime * fillSpeed;
            yield return new WaitForSeconds(Time.deltaTime);
        }

        fill = 0;
        FillVisual.fillAmount = fill;
        FillVisual.gameObject.SetActive(false);
        FeedbackContainer.SetActive(false);
        StepTextContainer.SetActive(true);
        SetCurrentStep();
    } 

    void SetCurrentStep()
    {
        var currentStep = _fiche.GetStep(_fiche.GetStepID(Offset));
        if (currentStep == null)
        {
            if (_bHasFinishedTraining)
            {
                SetFinishFeedback();
            }
            else
            {
                MainPanel.SetActive(false);
                return;
            }
        }

        MainPanel.SetActive(true);
        if (CurrentStepImage != null)
        {
            CurrentStepImage.sprite = currentStep.Image;
        }

        if(InfoText != null)
        {
            InfoText.text = currentStep.Info;
        }

        if (NumberingText != null)
        {
            NumberingText.enabled = true;
            NumberingText.text = "" + (_fiche.GetStepIndex(0)+1) + " / " + _fiche.Steps.Count;
        }

        RequiredCountText.enabled = false;
        if (currentStep.Type == StepType.Connection)
        {
            ConnectionStep cStep = currentStep as ConnectionStep;
            if (cStep.AutoCompleteQuantity)
            {
                RequiredCountText.enabled = true;
                RequiredCountText.text = string.Format("Enkel {0} van de {1} hoeven geconnecteerd te worden", cStep.RequiredQuantity, cStep.Quantity);
            }
        }
    }

    void TrainingHasFinished()
    {
        _bHasFinishedTraining = true;
    }

    void SetFinishFeedback()
    {
        MainPanel.SetActive(true);

        if (NumberingText != null)
        {
            NumberingText.enabled = false;
        }

        RequiredCountText.enabled = false;

        if (InfoText != null)
        {
            InfoText.text = "Proficiat! U heeft de motorblok correct geassembleerd.";
        }

        SoundManager.Play("finish", transform.position);
    }
}
