﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AssemblyFicheGeneric : MonoBehaviour
{
    public List<TaskStep> Steps = new List<TaskStep>();
    public List<ConnectionStep> ConnectionSteps = new List<ConnectionStep>();
    public List<PropertyChangeStep> PropertyChangeSteps = new List<PropertyChangeStep>();
    public Dictionary<StepType, List<TaskStep>> StepsPerType = new Dictionary<StepType, List<TaskStep>>()
    {
        { StepType.Connection, new List<TaskStep>() },
        { StepType.PropertyChange, new List<TaskStep>() }
    };

    public static Dictionary<StepType, Type> StepTypes = new Dictionary<StepType, Type>()
    {
        { StepType.Connection, typeof(ConnectionStep) },
        { StepType.PropertyChange, typeof(PropertyChangeStep) }
    };

    public static Dictionary<StepType, StepCreator> StepCreators = new Dictionary<StepType, StepCreator>()
    {
        { StepType.Connection, new StepCreator<ConnectionStep>() },
        { StepType.PropertyChange, new StepCreator<PropertyChangeStep>() }
    };

    void Awake()
    {
        LoadSteps();
    }

    public void LoadSteps()
    {
        var recording = AssemblyRecording.Load("assembly_recording_" + VR_Main.AssemblyName);
        Steps = recording.TaskSteps;
    }

    public TaskStep CurrentStep
    {
        get
        {
            var index = GetCurrentStepIndex();
            return index != -1 ? Steps[index] : null;
        }
        private set { }
    }

    int GetCurrentStepIndex()
    {
        int index = -1;
        for (int i = 0; i < Steps.Count; ++i)
        {
            switch (Steps[i].Type)
            {
                case StepType.Connection:
                    {
                        var step = Steps[i] as ConnectionStep;
                        if (!step.IsDone)
                        {
                            if (step.CombinedStep)
                            {
                                var combinedStep = Steps.FirstOrDefault(s => s.ID == step.CombinedStepID) as ConnectionStep;//i < Steps.Count - 1 ? GetConnectionStep(Steps[i + 1].ID) : null;
                                if (combinedStep != null && step.Progress > combinedStep.Progress)
                                    return i + 1;
                            }
                            
                            return i;
                        }
                        break;
                    }
                case StepType.PropertyChange:
                    {
                        var step = Steps[i] as PropertyChangeStep;
                        if (!step.IsDone)
                            return i;
                        break;
                    }
            }
        }

        return index;
    }

    public int GetStepIndex(int offset)
    {
        var index = GetCurrentStepIndex() + offset;
        if (index >= Steps.Count || index < 0)
            index = -1;
        return index;
    }

    public int GetStepID(int offset = 0)
    {
        var index = GetStepIndex(offset);
        return index != -1 ? Steps[index].ID : -1;
    }

    public TaskStep GetStep(int id)
    {
        return Steps.FirstOrDefault(s => s.ID == id);
    }
}
