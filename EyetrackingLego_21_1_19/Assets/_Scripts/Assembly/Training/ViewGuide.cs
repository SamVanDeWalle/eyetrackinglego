﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewGuide : MonoBehaviour
{
    public static float RequiredPan { get { return _requiredPan; } private set { } }
    public static float RequiredTilt { get { return _requiredTilt; } private set { } }
    public Transform Camera;
    public GameObject VisualContainer;

    static Transform[] _targetTransforms;
    static Vector3 _target;
    static Canvas _canvas;
    static GameObject _visualContainer;
    static Transform _camera;
    static float _requiredPan;
    static float _requiredTilt;
    static bool _bActive;
    static bool _bUseTransforms;

    public delegate void LookedAtTarget();
    public static event LookedAtTarget OnLookedAtTarget;

    void Awake()
    {
        //_camera = GetComponentInParent<Camera>().transform;
        _camera = Camera;
        _canvas = GetComponent<Canvas>();
        _visualContainer = VisualContainer;
        //_canvas.enabled = false;
    }

    public static void Activate(Vector3 target)
    {
        _target = target;
        _bUseTransforms = false;
        if (_canvas)
            _canvas.enabled = true;
        if (_visualContainer != null)
            _visualContainer.SetActive(true);
        _bActive = true;
    }

    public static void Activate(Transform[] targets)
    {
        _targetTransforms = targets;
        _bUseTransforms = _targetTransforms != null && _targetTransforms.Length > 0 && _targetTransforms[0] != null;
        if (_canvas)
            _canvas.enabled = _bUseTransforms;
        if (_visualContainer != null)
            _visualContainer.SetActive(_bUseTransforms);
        _bActive = _bUseTransforms;
    }

    public static void Deactivate()
    {
        if (_canvas)
            _canvas.enabled = false;
        if(_visualContainer != null)
            _visualContainer.SetActive(false);
        _bActive = false;
    }

    void Update()
    {
        if (!_bActive)
            return;

        var target = _target;
        if (_bUseTransforms)
        {
            target = PickTarget();
        }

        SetRequiredPan(target);

        var targetRay = (target - _camera.position).normalized;
        var angleDiff = Vector3.Angle(targetRay, _camera.forward);
        if (angleDiff < 50)
        {
            _canvas.enabled = false;
            if(angleDiff < 30)
                if (OnLookedAtTarget != null)
                    OnLookedAtTarget();
        }
        else
        {
            _canvas.enabled = true;
        }

        var projectedTargetRay = Vector3.ProjectOnPlane(targetRay, _camera.forward);
        var angle = Mathf.Acos(Vector3.Dot(projectedTargetRay, -_camera.right)) * Mathf.Rad2Deg;
        if (angle < 90)
            angle = 0;
        else
            angle = 180;

        var canvasRotation = _canvas.transform.localEulerAngles;
        canvasRotation.z = angle;
        _canvas.transform.localEulerAngles = canvasRotation;
    }

    static void SetRequiredPan(Vector3 target)
    {
        var targetRayRaw = target - _camera.position;
        var targetRay = targetRayRaw.normalized;
        //var projectedTargetRay = Vector3.ProjectOnPlane(targetRay, _camera.up);
        //_requiredPan = Mathf.Acos(Vector3.Dot(projectedTargetRay, _camera.forward)) * Mathf.Rad2Deg;
        //var crossPan = Vector3.Cross(_camera.forward, projectedTargetRay);
        //if (Vector3.Dot(crossPan, _camera.up) < 0)
        //    _requiredPan *= -1;

        var projectedTargetRay = Vector3.ProjectOnPlane(targetRay, Vector3.up);
        var projectedForward = Vector3.ProjectOnPlane(_camera.forward, Vector3.up);
        _requiredPan = Vector3.Angle(projectedForward, projectedTargetRay);//  Mathf.Acos(Vector3.Dot(projectedTargetRay, projectedForward)) * Mathf.Rad2Deg;
        var crossPan = Vector3.Cross(projectedTargetRay, projectedForward);
        if (Vector3.Dot(crossPan, Vector3.up) >= 0)
            _requiredPan *= -1;

        //_requiredTilt = Mathf.Acos(Vector3.Dot(projectedTargetRay, targetRay)) * Mathf.Rad2Deg;
        //var projToTargetRay = targetRay - projectedTargetRay;
        //if (Vector3.Dot(_camera.up, projToTargetRay) < 0)
        //    _requiredTilt *= -1;

        var targetY = targetRayRaw.y;
        var forwardY = _camera.forward.y;
        var headTargetY = targetRay.y - _camera.position.y;
        var tiltDirection = targetY < forwardY ? -1 : 1;
        var tiltAngle = Mathf.Asin(forwardY) * Mathf.Rad2Deg - Mathf.Asin(targetY / targetRayRaw.magnitude) * Mathf.Rad2Deg;
        //var headTargetAngle = tiltAngle += Mathf.Asin(headTargetY / targetRay.magnitude) * Mathf.Rad2Deg;
        //Debug.Log("headTargetY " + headTargetY);
        //Debug.Log("headTargetAngle " + headTargetAngle);
        //if(headTargetY != 0.0f)
        //    tiltAngle +=  Mathf.Asin(headTargetY / targetRay.magnitude) * Mathf.Rad2Deg;
        _requiredTilt = tiltAngle * -1;// * tiltDirection;
        //Debug.Log("tilt " + _requiredTilt);
        //var tiltY = targetRay.y - _camera
        //var tiltHelpProjection = Vector3.ProjectOnPlane(targetRay, _camera.right);
        //var crossTilt = Vector3.Cross(tiltHelpProjection, _camera.forward);
        //if (Vector3.Dot(crossTilt, _camera.right) < 0)
        //    _requiredTilt *= -1;
    }

    public static Vector2 GetViewPosition()
    {
        var pos = new Vector2(0.1f, 0.55f);
        if (RequiredPan > 90)
        {
            pos.x = 1;
        }
        else if (RequiredPan < -90)
        {
            pos.x = 0;
        }
        else
        {
            pos.x = (RequiredPan / 180.0f) + 0.5f;
        }

        if (RequiredTilt > 90)
        {
            pos.y = 1;
        }
        else if (RequiredTilt < -90)
        {
            pos.y = 0;
        }
        else
        {
            pos.y = (RequiredTilt / 180.0f) + 0.5f;
        }

        // Apply inverse roll
        var roll = _camera.eulerAngles.z;
        //Debug.Log(roll);
        pos = pos.Rotate(-roll);

        return pos;
    }

    Vector3 PickTarget()
    {
        if (_targetTransforms[0] == null)
        {
            Deactivate();
            return Vector3.zero;
        }

        var target = _targetTransforms[0].position;
        var targetRay = (target - _camera.position).normalized;
        var smallestAngle = Vector3.Angle(targetRay, _camera.forward);
        for(int i = 0; i < _targetTransforms.Length; ++i)
        {
            targetRay = (_targetTransforms[i].position - _camera.position).normalized;
            var angle = Vector3.Angle(targetRay, _camera.forward);
            if(angle < smallestAngle)
            {
                smallestAngle = angle;
                target = _targetTransforms[i].position;
            }
        }

        return target;
    }
}
