﻿/*
 * 
 * This script allows for checking if the current assembly is correct,
 * according to a predefined correct version
 * 
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using Helpers;
using System.Linq;
using UnityEngine.SceneManagement;

class CheckFlag
{
    public int TypeID;
    public int ParentTypeID;
    public AssemblyPiece[] ParentPieces;

    public CheckFlag()
    {
        TypeID = -1;
        ParentTypeID = -1;
    }

    public CheckFlag(int typeID, int parentTypeID)
    {
        TypeID = typeID;
        ParentTypeID = parentTypeID;
    }
}

public class AssemblyChecker : Singleton<AssemblyChecker>
{
    public int StartTypeID;
    public GameObject CorrectTooltip, IncorrectTooltip;
    //public TargetLine TargetLine;
    public CurvedTrailBehaviour Trail;

    [SerializeField]
    ProceduralBox _proceduralBox = null;
    static List<ProceduralBox> _proceduralBoxPool = new List<ProceduralBox>();
    static AssemblyFiche _fiche;
    static AssemblyFicheGeneric _ficheGeneric;
    static bool _bActive;

    protected override void Awake()
    {
        base.Awake();

        //DontDestroyOnLoad(gameObject);

        _fiche = FindObjectOfType<AssemblyFiche>();
        _ficheGeneric = FindObjectOfType<AssemblyFicheGeneric>();
    }

    void Start()
    {
        //if (_bActive)
        //    CheckStep();
    }

    public static void Activate()
    {
        if (_bActive)
            return;

        _bActive = true;
        _ficheGeneric.LoadSteps();
        UnHighlightAll();
        PickupController.OnPlacedGlobal += CheckStep;
        PickupController.OnPickedUpGlobal += UnHighlightAll;
        PickupController.OnPickedUpGlobal += CheckStep;
        PickupController.OnDroppedGlobal += CheckStep;
        PickupController.OnRefusedPickup += CheckStep;
        Property.OnPropertyResolved += CheckStep;
        TaskStep.OnDone += CheckStep;
        PushButton.OnPressedGlobal += CheckStep;
        AssemblyPiece.OnAutoConnected += CheckStep;

        //HUDMenu.SubscribeToAction("saveascorrect", SaveCurrentAssemblyAsCorrect);
        //HUDMenu.SubscribeToAction("check", Check);
        //HUDMenu.SubscribeToAction("hint", HighlightPossiblePiecesBF);

        CheckStep();
    }

    public static void Deactivate()
    {
        if (!_bActive)
            return;

        _bActive = false;

        PickupController.OnPlacedGlobal -= CheckStep;
        PickupController.OnPickedUpGlobal -= UnHighlightAll;
        PickupController.OnPickedUpGlobal -= CheckStep;
        PickupController.OnDroppedGlobal -= CheckStep;
        PickupController.OnRefusedPickup -= CheckStep;
        Property.OnPropertyResolved -= CheckStep;
        TaskStep.OnDone -= CheckStep;
        PushButton.OnPressedGlobal -= CheckStep;
        AssemblyPiece.OnAutoConnected -= CheckStep;
    }

    public static void Reset()
    {
        GuideUtils.UnHighlightAll();
        if (_ficheGeneric != null)
        {
            foreach (var step in _ficheGeneric.Steps)
            {
                step.Reset();
            }

            foreach (var step in _ficheGeneric.ConnectionSteps)
            {
                step.Reset();
            }

            foreach (var step in _ficheGeneric.PropertyChangeSteps)
            {
                step.Reset();
            }
        }

        CheckStep();
    }

    static void CheckStep()
    {
        //HighlightPossiblePiecesBF();
        if (_ficheGeneric != null)
            CheckTaskSteps();
    }

    static void UnHighlightAll()
    {
        SetProceduralBoxes(new Pickable[0]);
    }

    static Assembly GetCurrentAssembly()
    {
        var assembly = new Assembly();
        var pieces = AssemblyPieceManager.AssemblyPieces;

        for (int i = 0; i < pieces.Count; ++i)
        {
            var assemblyData = new AssemblyData();
            assemblyData.APD = new AssemblyPieceData(pieces[i].ID, pieces[i].TypeID);
            var connectedPoints = pieces[i].ConnectedPoints;
            for (int j = 0; j < connectedPoints.Length; ++j)
            {
                var connectedAP = connectedPoints[j].ConnectedCP.ParentPiece;
                assemblyData.ConnectedAPD.Add(new AssemblyPieceData(connectedAP.ID, connectedAP.TypeID));
            }

            assembly.AssemblyData.Add(assemblyData);
        }

        return assembly;
    }

    // Function is part of old auto detect steps system
    public static void SaveCurrentAssemblyAsCorrect()
    {
        GetCurrentAssembly().Save("_" + VR_Main.AssemblyName + "_correct");
    }

    public static void Check()
    {
        var correctAssembly = Assembly.Load();
        var currentAssembly = GetCurrentAssembly();
        currentAssembly.Save("_" + VR_Main.AssemblyName + "_current");

        var correct = correctAssembly.Compare(currentAssembly);
        if (correct)
        {
            Instance.IncorrectTooltip.SetActive(false);
            Instance.CorrectTooltip.SetActive(true);
        }
        else
        {
            Instance.CorrectTooltip.SetActive(false);
            Instance.IncorrectTooltip.SetActive(true);
        }
        Debug.Log("assembly is correct: " + correct);
    }

    public static void HighlightPossiblePieces()
    {
        var currentAssembly = GetCurrentAssembly(); //TODO create update assembly function
        var correctAssembly = Assembly.Load("_" + VR_Main.AssemblyName + "_correct"); // TODO store 
        //currentAssembly.Save("_" + SceneManager.GetActiveScene().name + "_current");
        // TODO depth first vs breadth first

        // Start from pieces with the start id
        var startPieces = ConnectionManager.AssemblyPieces.Where(ap => ap.TypeID == Instance.StartTypeID).ToArray();
        var highlightPieces = new List<AssemblyPiece>();
        var checkedPieces = new List<AssemblyPiece>();

        for (int i = 0; i < startPieces.Length; ++i)
        {
            GetPossibleNextPieces(correctAssembly, currentAssembly, startPieces[i], ref checkedPieces, ref highlightPieces);
        }

        //SetProceduralBoxes(highlightPieces.ToArray());
    }

    public static void HighlightPossiblePiecesBF()
    {
        var currentAssembly = GetCurrentAssembly(); //TODO create update assembly function
        var correctAssembly = Assembly.Load("_" + VR_Main.AssemblyName + "_correct"); // TODO store 

        Instance.Trail.DeactivateTrail();

        // Start from pieces with the start id
        var startPieces = ConnectionManager.AssemblyPieces.Where(ap => ap.TypeID == Instance.StartTypeID).ToArray();
        var highlightPickables = new List<Pickable>();

        var startPieceQuantity = _fiche != null ? _fiche.GetQuantityFromType(Instance.StartTypeID) : -1;
        var connectedPieces = new List<AssemblyPiece>();
        var unconnectedPieces = new List<AssemblyPiece>();
        GetConnectedPieces(startPieces, ref connectedPieces, ref unconnectedPieces);
        var checkedTypes = new List<int>();
        UnHighlightAll();
        if (connectedPieces.Count < startPieceQuantity || startPieceQuantity == -1)
        {
            _fiche.UnClearStep(Instance.StartTypeID);
            var pickedUpStartPiece = PickupManager.ControllerPickables.FirstOrDefault(cp => cp.Value != null && cp.Value.Type == PickableType.AssemblyPiece && unconnectedPieces.Contains(cp.Value.GetComponent<AssemblyPiece>())).Value;
            if (pickedUpStartPiece != null)
            {
                var workbench = ConnectionManager.WorkBenches.FirstOrDefault();
                if (workbench != null)
                {
                    var workBenchCP = workbench.ConnectionPoint;
                    // Show trail from pickedup start piece to workbench
                    Instance.Trail.SetTrail(workBenchCP.transform, pickedUpStartPiece.transform);
                }
            }
            else
            {
                highlightPickables.AddRange(unconnectedPieces.Select(ap => ap.Pickable));
            }
        }
        else
        {
            _fiche.ClearStep(Instance.StartTypeID);
            var checkFlag = new CheckFlag();
            GetFirstConnectablePieces(Instance.StartTypeID, connectedPieces.ToArray(), correctAssembly, ref highlightPickables, ref checkedTypes, -1, ref checkFlag);
        }

        MaterialManager.GetInstance().HintObjects(highlightPickables.ToArray());
        SetProceduralBoxes(highlightPickables.ToArray());
    }

    public static void CheckTaskSteps()
    {
        GuideUtils.DeactivateTrail();
        GuideUtils.UnHighlightAll();
        GuideUtils.StopIndicatingAllCPs();
        if (GuideUtils.HasWorkbenchGuide)
            GuideUtils.WorkbenchGuide.GuideDesiredAnge(false);
        var currentStep = _ficheGeneric.CurrentStep;
        if (currentStep == null)
            return;
        Debug.Log("cstep: " + currentStep.ID);
        switch (currentStep.Type)
        {
            case StepType.Connection:
                {
                    var step = currentStep as ConnectionStep;
                    CheckTaskStep(step);
                    break;
                }
            case StepType.PropertyChange:
                {
                    var step = currentStep as PropertyChangeStep;
                    CheckTaskStep(step);
                    break;
                }
        }
    }

    public static void CheckTaskStep(TaskStep step)
    {
        switch (step.Type)
        {
            case StepType.Connection:
                {
                    var connectionStep = step as ConnectionStep;
                    CheckConnectionStep(connectionStep);
                    break;
                }
            case StepType.PropertyChange:
                {
                    var propertyChangeStep = step as PropertyChangeStep;
                    CheckPropertyChangeStep(propertyChangeStep);
                    break;
                }
        }
    }

    static void CheckConnectionStep(ConnectionStep step)
    {
        GuideUtils.StopGuidingConnection();
        GuideUtils.AllowedPickables.Clear();
        var pieces = AssemblyPieceManager.AssemblyPieces.Where(ap => ap.TypeID == step.PrefabTypeID);
        //var pieces = AssemblyPieceManager.AssemblyPieces.Where(ap => ap.TypeID == step.PrefabTypeID && ap.ConnectionPoints[step.ConnectionTypeID].Any(cp => cp.CanConnect));
        Debug.Log("si: " + step.PrefabTypeID + " | " + step.ParentTypeID + " | ");
        var connectedPieces = pieces.Where(ap => !ap.ConnectionPoints[step.ConnectionTypeID].Any(cp => !cp.IsConnected || (cp.ConnectedCP != null && cp.ConnectedCP.ParentPiece.TypeID != step.ParentTypeID))).ToList();

        if (step.Progress != connectedPieces.Count)
        {
            GuideUtils.UnHighlightAll();
            step.Progress = connectedPieces.Count;
            CheckTaskSteps();
        }

        if (connectedPieces.Count >= step.Quantity)
        {
            // Resolved
            ConnectionStepDone(step);
        }
        else if (step.AutoCompleteQuantity && connectedPieces.Count >= step.RequiredQuantity)
        {
            //PickupManager.dr
            step.Progress = connectedPieces.Count;
            AutoCompleteConnectionStep(step);
        }
        else
        {
            // Unresolved
            var unconnectedPieces = pieces.Except(connectedPieces).ToList();
            var possibleParents = AssemblyPieceManager.AssemblyPieces.Where(ap => ap.TypeID == step.ParentTypeID).ToList();
            if (step.ConnectionTypeID == -1)
                possibleParents = ConnectionManager.WorkBenches.Where(wb => wb.GetComponent<AssemblyPiece>().TypeID == step.ParentTypeID).Select(wb => wb.GetComponent<AssemblyPiece>()).ToList();
            if (possibleParents == null || possibleParents.Count == 0)
            {
                var container = AssemblyPieceManager.AssemblyPieceContainers.FirstOrDefault(c => c.APTypeID == step.ParentTypeID);
                if (container == null)
                {
                    // If no pieces and containers are present to resolve the step, skip to the next step
                    Debug.LogWarning("AssemblyChecker::CheckConnectionStep - Not enough pieces or containers in scene to resolve the step!");
                    ConnectionStepDone(step);
                    return;
                }

                GuideUtils.Highlight(container.Pickable);
                return;
            }

            var parent = possibleParents[0]; // TODO smart check
            var parentConnectionCount = parent.ConnectedPoints.Count();
            int chosenFirstParentIndex = 0;
            for (int i = 1; i < possibleParents.Count; ++i)
            {
                var count = possibleParents[i].ConnectedPoints.Count();
                if (count > parentConnectionCount)
                {
                    parent = possibleParents[i];
                    chosenFirstParentIndex = i;
                    parentConnectionCount = count;
                }
            }

            if (!GuideConnection(unconnectedPieces, step.PrefabTypeID, step.ConnectionTypeID, parent))
            {
                for (int i = 0; i < possibleParents.Count; ++i)
                {
                    if (i == chosenFirstParentIndex)
                        continue;

                    if (GuideConnection(unconnectedPieces, step.PrefabTypeID, step.ConnectionTypeID, possibleParents[i]))
                        break;
                }
            }

            GuideUtils.AllowedPickables.AddRange(unconnectedPieces.Select(p => p.Pickable));
            GuideUtils.AllowedPickables.AddRange(possibleParents.Select(p => p.Pickable));
            if (GuideUtils.HasWorkbenchGuide)
                GuideUtils.WorkbenchGuide.GuideDesiredAnge(true, step.WorkbenchAngle);
        }
    }

    static void ConnectionStepDone(ConnectionStep step)
    {
        Debug.Log("step done");
        // If the step was the last step the training is over. Unless the step is combined with another step that is not done yet.
        // If the step is not the last step but it has a combined step that is the last step, the training is also over
        step.Done();
        var isLast = step.ID == _ficheGeneric.Steps.Last().ID;

        if (!step.CombinedStep)
        {
            if (isLast)
            {
                TrainingManager.TrainingDone();
            }
        }
        else
        {
            var combinedStep = _ficheGeneric.GetStep(step.CombinedStepID);
            if (isLast)
            {
                if (combinedStep.IsDone)
                {
                    TrainingManager.TrainingDone();
                }
            }
            else
            {
                if (_ficheGeneric.Steps.Last().ID == combinedStep.ID && combinedStep.IsDone)
                {
                    TrainingManager.TrainingDone();
                }
            }
        }
    }

    static void CheckPropertyChangeStep(PropertyChangeStep step)
    {
        GuideUtils.AllowedPickables.Clear();
        GuideUtils.StopIndicatingAllCPs();
        var pieces = AssemblyPieceManager.AssemblyPieces.Where(ap => ap.TypeID == step.PrefabTypeID).ToList();// && ap.GetComponent<PropertyHolder>().GetProperty(step.PropertyType, step.PropertyID) != null);
        var resolvedPieces = new List<AssemblyPiece>();
        for (int i = 0; i < pieces.Count; ++i)
        {
            var propertyHolder = pieces[i].GetComponent<PropertyHolder>();
            var property = propertyHolder.GetProperty(step.PropertyType, step.PropertyID);
            if (property != null && property.IsCorrect)
                resolvedPieces.Add(pieces[i]);
        }
        //pieces.Where(ap => ap.GetComponent<PropertyHolder>().GetProperty(step.PropertyType, step.PropertyID).IsCorrect).ToList();
        if (resolvedPieces.Count >= step.Quantity)
        {
            // Resolved
            PropertyChangeStepDone(step);
        }
        else
        {
            var unresolvedPieces = pieces.Except(resolvedPieces).ToList();
            GuideUtils.AllowedPickables.AddRange(unresolvedPieces.Select(p => p.Pickable));
            var ph = unresolvedPieces[0].GetComponent<PropertyHolder>();
            ph.GetProperty(step.PropertyType, step.PropertyID).Guide();
        }
    }

    static void PropertyChangeStepDone(PropertyChangeStep step)
    {
        Debug.Log("step done");
        step.Done();
        GuideUtils.DeactivateTrail();
        if (step.ID == _ficheGeneric.Steps.Last().ID)
            TrainingManager.TrainingDone();
    }

    static bool GuideConnection(List<AssemblyPiece> pieces, int pieceType, int connectionType, AssemblyPiece parent)
    {
        var pickedUpPieces = pieces.Where(p => p.IsPickedUp).ToList();
        var highlightPickables = new List<Pickable>();
        if (pickedUpPieces.Count == 0)
        {
            // Highlight pieces if none is in hand
            var pickables = pieces.Select(p => p.Pickable).ToList();
            // If None in scene, try highlighting container
            //if(pickables == null || pickables.Length == 0)
            //{
            //    pickables = AssemblyPieceManager.AssemblyPieceContainers.Where(apc => apc.APTypeID == pieceType).Select(apc => apc.Pickable).ToArray();
            //}
            //highlight containers regardles if piece is already in scene
            pickables.AddRange(AssemblyPieceManager.AssemblyPieceContainers.Where(apc => apc.APTypeID == pieceType).Select(apc => apc.Pickable).ToArray());
            highlightPickables.AddRange(pickables);
            GuideUtils.AllowedPickables.AddRange(pickables);

            // If unneeded tool is in hand -> return it
            //PickupManager.ControllerPickables.FirstOrDefault(cp => cp.Value.Type == PickableType.Tool)

            if (pickables.Count > 0 && pickables[0] != null)
            {
                var validCrosswiseCPs = ConnectionManager.GetCrossWiseConnectionTargets(parent, connectionType);
                var targetCPs = parent.ConnectionPoints.ContainsKey(connectionType) ? parent.ConnectionPoints[connectionType].Where(cp => cp.CanConnect && (!cp.Crosswise || validCrosswiseCPs.Count == 0 ? true : validCrosswiseCPs.Contains(cp))).ToList() : new List<ConnectionPoint>();
                var targetCP = targetCPs.Count > 0 ? targetCPs[0] : null;
                if (targetCP == null)
                    return false;
                var requiredToolType = targetCP.Action == ToolAction.None ? ToolType.None : ToolManager.GetToolTypeFromAction(targetCP.Action);
                var otherHandPickable = PickupManager.ControllerPickables.FirstOrDefault(p => p.Value != null).Value;

                if (otherHandPickable != null && otherHandPickable.Type == PickableType.Tool && otherHandPickable.GetComponent<Tool>().Type != requiredToolType)
                {
                    otherHandPickable.Controller.Reset();
                    PickupManager.ResetControllerPickable(otherHandPickable.Controller);
                }

                if (requiredToolType != ToolType.None)
                {
                    // Check if the other picked up item is the required tool, if not, highlight a tool
                    if (otherHandPickable != null && otherHandPickable.Type == PickableType.Tool && otherHandPickable.GetComponent<Tool>().Type == requiredToolType)
                    {
                        // Show trail from pickedup piece to closest cp on a parent type piece
                        //GuideUtils.SetTrail(pickedUpPieces[0].transform, targetCPs.Select(cp => cp.transform).ToList());
                        GuideUtils.IndicateCPs(targetCPs);
                        //var ownCP = pickedUpPieces[0].ConnectionPoints[targetCP.TypeID].FirstOrDefault(cp => cp.Gender != targetCP.Gender); // TODO closest check
                        //                                                                                                                    //pickedUpPieces[0].PreviewGhostConnected(targetCP, ownCP);
                        //GuideUtils.PreviewConnection = new AssemblyPiecePreviewConnection(pickedUpPieces[0], ownCP, targetCP);
                    }
                    else
                    {
                        // Find and highlight required tools
                        var toolPickables = ToolManager.GetTools(requiredToolType).Select(t => t.Pickable);
                        highlightPickables.AddRange(toolPickables);
                        GuideUtils.AllowedPickables.AddRange(toolPickables);
                    }
                }
            }
        }
        else
        {
            // Show trail if one is in hand
            // Highlight tool if tool is required
            var validCrosswiseCPs = ConnectionManager.GetCrossWiseConnectionTargets(parent, connectionType);
            var targetCPs = parent.ConnectionPoints[connectionType].Where(cp => cp.CanConnect && (!cp.Crosswise || validCrosswiseCPs.Count == 0 ? true : validCrosswiseCPs.Contains(cp))).ToList();
            var targetCP = targetCPs.Count > 0 ? targetCPs[0] : null;
            if (targetCP == null)
                return false;
            var requiredToolType = targetCP.Action == ToolAction.None ? ToolType.None : ToolManager.GetToolTypeFromAction(targetCP.Action);
            var otherHandPickable = PickupManager.ControllerPickables.FirstOrDefault(p => p.Value != pickedUpPieces[0].Pickable).Value;

            // Check if wrong tool is in hand -> return it
            if (otherHandPickable != null && otherHandPickable.Type == PickableType.Tool && otherHandPickable.GetComponent<Tool>().Type != requiredToolType)
            {
                otherHandPickable.Controller.Reset();
                PickupManager.ResetControllerPickable(otherHandPickable.Controller);
            }

            if (requiredToolType != ToolType.None)
            {
                // Check if the other picked up item is the required tool, if not, highlight a tool
                if (otherHandPickable != null && otherHandPickable.Type == PickableType.Tool && otherHandPickable.GetComponent<Tool>().Type == requiredToolType)
                {
                    // Show trail from pickedup piece to closest cp on a parent type piece
                    var ownCP = pickedUpPieces[0].ConnectionPoints[targetCP.TypeID].FirstOrDefault(cp => cp.Gender != targetCP.Gender); // TODO closest check!
                    GuideUtils.GuideConnection(ownCP, targetCPs);
                    GuideUtils.IndicateCPs(targetCPs);
                    GuideUtils.PreviewConnection = new AssemblyPiecePreviewConnection(pickedUpPieces[0], ownCP, targetCP);
                }
                else
                {
                    // Find and highlight required tools
                    var toolPickables = ToolManager.GetTools(requiredToolType).Select(t => t.Pickable);
                    highlightPickables.AddRange(toolPickables);
                    GuideUtils.AllowedPickables.AddRange(toolPickables);
                }
            }
            else
            {
                var ownCP = pickedUpPieces[0].ConnectionPoints[targetCP.TypeID].FirstOrDefault(cp => cp.Gender != targetCP.Gender); // TODO closest check!
                var forwardTransform = pickedUpPieces[0].transform.parent;
                GuideUtils.SetTrail(ownCP.transform, forwardTransform, targetCPs.Select(cp => cp.transform).ToList());
                if (ownCP.AidPoints.Count > 0)
                {
                    for (int i = 0; i < ownCP.AidPoints.Count; ++i)
                    {
                        var aidTargets = new List<Transform>();
                        for (int t = 0; t < targetCPs.Count; ++t)
                        {
                            if (i == 0)
                                targetCPs[t].ClearAidPoints();

                            var aidGO = Instantiate(new GameObject());
                            aidGO.transform.SetParent(targetCPs[t].transform);
                            aidGO.transform.localPosition = ownCP.AidPoints[i].localPosition;
                            aidGO.transform.localRotation = ownCP.AidPoints[i].localRotation;
                            targetCPs[t].AidPoints.Add(aidGO.transform);
                            aidTargets.Add(aidGO.transform);
                        }

                        GuideUtils.AddAidTrail(ownCP.AidPoints[i], forwardTransform, aidTargets);
                    }
                }
                GuideUtils.IndicateCPs(targetCPs);
                GuideUtils.PreviewConnection = new AssemblyPiecePreviewConnection(pickedUpPieces[0], ownCP, targetCP);
            }
        }

        GuideUtils.Highlight(highlightPickables);
        return true;
    }

    static void GetConnectedPieces(AssemblyPiece[] pieces, ref List<AssemblyPiece> connectedPieces, ref List<AssemblyPiece> unconnectedPieces)
    {
        for (int i = 0; i < pieces.Length; ++i)
        {
            if (pieces[i].ParentPiece == null)
            {
                unconnectedPieces.Add(pieces[i]);
            }
            else
            {
                connectedPieces.Add(pieces[i]);
            }
        }
    }

    static void GetConnectedToTypePieces(AssemblyPiece[] pieces, int typeID, ref List<AssemblyPiece> connectedPieces, ref List<AssemblyPiece> unconnectedPieces)
    {
        for (int i = 0; i < pieces.Length; ++i)
        {
            if (pieces[i].ConnectedPoints.Any(cp => cp.ConnectedCP.ParentPiece.TypeID == typeID))
            {
                connectedPieces.Add(pieces[i]);
            }
            else
            {
                unconnectedPieces.Add(pieces[i]);
            }
        }
    }

    static bool GetFirstConnectablePieces(int typeID, AssemblyPiece[] parentPieces, Assembly assembly, ref List<Pickable> highlightPickables, ref List<int> checkedTypes, int parentNextTypeID, ref CheckFlag checkFlag)
    {
        if (checkedTypes.Contains(typeID))
        {
            return false;
        }
        checkedTypes.Add(typeID);

        // Get all types that are connected with the given type in the correct assembly
        var connectedTypes = assembly.GetConnectedTypes(typeID);
        var refCheckedTypes = checkedTypes;
        connectedTypes = connectedTypes.Where(ct => !refCheckedTypes.Contains(ct)).ToList();

        // Sort the types according to the order of the assembly fiche
        SortTypes(ref connectedTypes);

        // Check if the first connected type comes earlier than the parentNextType otherwise leave and continue from parent connected types
        if (connectedTypes.Count == 0)
        {
            return false;
        }

        var allowContinueParent = true;
        // Loop over all the sorted connected types
        for (int i = 0; i < connectedTypes.Count; ++i)
        {
            // If we shouldn't do this type because the parentnexttype has priority but the type has priority over the flagged type, overwrite the flag with this type
            var bLeave = false;
            var bFlag = false;
            if (checkFlag.TypeID == -1 || _fiche.ComesBefore(connectedTypes[i], checkFlag.TypeID))
            {
                bFlag = true;
            }

            if (parentNextTypeID != -1 && _fiche.ComesBefore(parentNextTypeID, connectedTypes[i]))
            {
                bLeave = true;
            }

            // If the flagged type has priority over this type do that type instead
            if (!bLeave && checkFlag.TypeID != -1 && checkFlag.TypeID != connectedTypes[i] && _fiche.ComesBefore(checkFlag.TypeID, connectedTypes[i]))
            {
                GetFirstConnectablePieces(checkFlag.ParentTypeID, checkFlag.ParentPieces, assembly, ref highlightPickables, ref checkedTypes, -1, ref checkFlag);
                return true;
            }

            // Find all the pieces in the scene of the connected type
            // TODO find containers as well
            var typePieces = AssemblyPieceManager.AssemblyPieces.Where(ap => ap.TypeID == connectedTypes[i]).ToArray();

            // If 1 or more pieces are found, see if it is connected already, otherwise highlight it
            if (typePieces.Length > 0)
            {
                // Check how many pieces of the type need to be connected according to the assembly fiche
                var pieceQuantity = _fiche.GetQuantityFromType(connectedTypes[i]);

                // Filter all the pieces into connected or unconnected to parent type
                var connectedTypePieces = new List<AssemblyPiece>();
                var unconnectedTypePieces = new List<AssemblyPiece>();
                GetConnectedToTypePieces(typePieces, typeID, ref connectedTypePieces, ref unconnectedTypePieces);

                // If less pieces are connected than should according to the fiche, highlight the unconnected ones
                if (connectedTypePieces.Count < pieceQuantity)
                {
                    _fiche.UnClearStep(connectedTypes[i]);

                    if (bFlag)
                    {
                        checkFlag.TypeID = connectedTypes[i];
                        checkFlag.ParentTypeID = typeID;
                        checkFlag.ParentPieces = parentPieces;
                        if (checkFlag.TypeID != -1)
                        {
                            if (!checkedTypes.Contains(checkFlag.ParentTypeID))
                                checkedTypes.Add(checkFlag.ParentTypeID);

                            checkedTypes.Remove(typeID);
                        }
                    }

                    if (bLeave)
                        break;

                    allowContinueParent = false;
                    // If one of the unconnected pieces is in hand, show the trail and don't highlight the others
                    var pickedUpUnconnectedPiece = unconnectedTypePieces.FirstOrDefault(ucp => ucp.IsPickedUp);
                    if (pickedUpUnconnectedPiece != null)
                    {
                        // Check if picked up piece has unresolved dependencies or requires a tool
                        var targetCP = ConnectionManager.FindClosestConnectionPointToAP(pickedUpUnconnectedPiece, parentPieces);
                        var requiredToolType = targetCP.Action == ToolAction.None ? ToolType.None : ToolManager.GetToolTypeFromAction(targetCP.Action);
                        var otherHandPickable = PickupManager.ControllerPickables.FirstOrDefault(p => p.Value != pickedUpUnconnectedPiece.Pickable).Value;
                        // Get unresolvedRequiredDependencyCPs
                        var unresolvedRequiredDependencyCPs = GetUnresolvedRequiredDependencyCPs(pickedUpUnconnectedPiece);
                        var connectableTypes = assembly.GetConnectedTypes(pickedUpUnconnectedPiece.TypeID);

                        // If the pickedup piece has unresolved dependencies
                        if (unresolvedRequiredDependencyCPs != null && unresolvedRequiredDependencyCPs.Count > 0)
                        {
                            // Check if the other picked up item is an required dependency of this pickedup piece
                            if (otherHandPickable != null)
                            {
                                var otherHandPiece = otherHandPickable.GetComponent<AssemblyPiece>();
                                if (otherHandPickable.Type == PickableType.AssemblyPiece && otherHandPiece.TypeID != pickedUpUnconnectedPiece.TypeID)
                                {
                                    var selfConnectableRequiredCP = pickedUpUnconnectedPiece.ConnectionPointsUnordered.FirstOrDefault(cp => otherHandPiece.ConnectionPointsUnordered.Any(cpn => cpn.TypeID == cp.TypeID) && cp.CanConnect);

                                    if (selfConnectableRequiredCP != null)
                                    {
                                        // Show trail from other pickedup piece to this pickedup piece
                                        Instance.Trail.SetTrail(selfConnectableRequiredCP.transform, otherHandPickable.transform);
                                        return true;
                                    }
                                }
                            }

                            for (int ct = 0; ct < connectableTypes.Count; ++ct)
                            {
                                var pieces = AssemblyPieceManager.AssemblyPieces.Where(ap => ap.TypeID == connectableTypes[ct] && Array.Exists(ap.ConnectionPointsUnordered, cp => unresolvedRequiredDependencyCPs.Any(ucp => ucp.TypeID == cp.TypeID))).ToList();
                                var pieceContainers = AssemblyPieceManager.AssemblyPieceContainers.Where(ap => ap.APTypeID == connectableTypes[ct]).ToArray();
                                if (pieces != null && pieces.Count != 0)
                                {
                                    // Highlight pieces
                                    highlightPickables.AddRange(pieces.Select(ap => ap.Pickable));

                                    return true;
                                }
                                else if (pieceContainers != null && pieceContainers.Length != 0)
                                {
                                    // Highlight containers
                                    highlightPickables.AddRange(pieceContainers.Select(pc => pc.Pickable));

                                    return true;
                                }
                            }
                        }
                        // If the connection requires a tool
                        else if (requiredToolType != ToolType.None)
                        {
                            // Check if the other picked up item is the required tool, if not, highlight a tool
                            if (otherHandPickable != null && otherHandPickable.Type == PickableType.Tool && otherHandPickable.GetComponent<Tool>().Type == requiredToolType)
                            {
                                // Show trail from pickedup piece to closest cp on a parent type piece
                                Instance.Trail.SetTrail(targetCP.transform, pickedUpUnconnectedPiece.transform);
                                return true;
                            }
                            else
                            {
                                // Find and highlight required tools
                                highlightPickables.AddRange(ToolManager.GetTools(requiredToolType).Select(t => t.Pickable));
                                Debug.Log("highlighted tools: " + highlightPickables.FirstOrDefault(p => p.Type == PickableType.Tool));
                                return true;
                            }
                        }
                        else
                        {
                            // Show trail from pickedup piece to closest cp on a parent type piece
                            Instance.Trail.SetTrail(targetCP.transform, pickedUpUnconnectedPiece.transform);
                            return true;
                        }
                    }
                    else
                    {
                        // Check if the unconnected pieces should be highlighted
                        //for(int j = 0; j < unconnectedTypePieces.Count; ++j)
                        //{
                        //
                        //}
                        highlightPickables.AddRange(unconnectedTypePieces.Select(ap => ap.Pickable));
                        break;
                    }
                }
                else
                {
                    _fiche.ClearStep(connectedTypes[i]);
                    //if (bLeave)
                    //    break;
                    // Cascade and find connected pieces for the connected pieces but only if the first sub-connected type comes earlier according to the fiche than the next connected type
                    var nextType = i < connectedTypes.Count - 1 ? connectedTypes[i + 1] : -1;

                    // Compare nexttype with parentnexttype
                    var checkNextType = parentNextTypeID == -1 || (nextType != -1 && _fiche.ComesBefore(nextType, parentNextTypeID)) ? nextType : parentNextTypeID;

                    if (!GetFirstConnectablePieces(connectedTypes[i], typePieces, assembly, ref highlightPickables, ref checkedTypes, checkNextType, ref checkFlag))
                    {
                        continue;
                    }
                    else
                    {
                        allowContinueParent = false;
                        break;
                    }
                }
            }
            else
            {
                if (bFlag)
                {
                    checkFlag.TypeID = connectedTypes[i];
                    checkFlag.ParentTypeID = typeID;
                    checkFlag.ParentPieces = parentPieces;
                    if (checkFlag.TypeID != -1)
                    {
                        if (!checkedTypes.Contains(checkFlag.ParentTypeID))
                            checkedTypes.Add(checkFlag.ParentTypeID);

                        checkedTypes.Remove(typeID);
                    }
                }

                if (bLeave)
                    break;

                // Look for a container
                var typeContainers = AssemblyPieceManager.AssemblyPieceContainers.Where(apc => apc.APTypeID == connectedTypes[i]).ToArray();
                highlightPickables.AddRange(typeContainers.Select(apc => apc.Pickable));
                allowContinueParent = false;
            }
        }

        return !allowContinueParent;
    }

    static void SortTypes(ref List<int> types)
    {
        types.Sort((a, b) => _fiche.GetStepFromType(a).CompareTo(_fiche.GetStepFromType(b)));
    }

    static List<ConnectionPoint> GetUnresolvedRequiredDependencyCPs(AssemblyPiece piece)
    {
        var cps = new List<ConnectionPoint>();
        for (int i = 0; i < piece.ConnectionPointsUnordered.Length; ++i)
        {
            var cp = piece.ConnectionPointsUnordered[i];
            if (cp.DependencyConnectionPoints.Count > 0)
            {
                cps.AddRange(cp.DependencyConnectionPoints.Where(cpn => !cpn.IsConnected));
            }
        }

        return cps;
    }

    static void GetPossibleNextPieces(Assembly correctAssembly, Assembly currentAssembly, AssemblyPiece piece, ref List<AssemblyPiece> checkedPieces, ref List<AssemblyPiece> highlightPieces)
    {
        // Prevent double checking and possible loops
        if (checkedPieces.Contains(piece))
            return;

        checkedPieces.Add(piece);

        // If the piece is not connected and not on workbench highlight it and leave
        if (piece.ParentPiece == null)
        {
            if (!highlightPieces.Contains(piece) && !piece.IsPickedUp)
                highlightPieces.Add(piece);

            return;
        }


        // Else look through the connectable pieces of the piece

        var connectedTypes = correctAssembly.GetConnectedTypes(piece.TypeID);
        var pieces = AssemblyPieceManager.AssemblyPieces.Where(ap => connectedTypes.Contains(ap.TypeID)).ToArray();

        // Check dependencies
        var startCPs = piece.ConnectionPointsUnordered.Select(cp => cp.TypeID);
        for (int i = 0; i < pieces.Length; ++i)
        {
            // Find shared connection points between piece and startPiece
            var canConnect = piece.ConnectionPointsUnordered.Any(cp => pieces[i].ConnectionPointsUnordered.Any(cpn => cpn.TypeID == cp.TypeID) && cp.CanConnect);

            // Find out if piece requires unresolved dependencies and is on workbench or in hand
            var hasUnresolvedRequiredDependencies = Array.Exists(pieces[i].ConnectionPointsUnordered, (cp => startCPs.Contains(cp.TypeID) && cp.DependencyConnectionPoints.Count > 0 && cp.DependencyConnectionPoints.Exists(cpn => !cpn.IsConnected)));
            var pickedUp = pieces[i].IsPickedUp;
            if (pieces[i].IsConnectedTo(piece) || (hasUnresolvedRequiredDependencies && (pieces[i].ParentPiece != null && pieces[i].IsOnWorkbench)) || pickedUp)
            {
                // Piece is connected to the piece or needs subassembly and is on workbench
                GetPossibleNextPieces(correctAssembly, currentAssembly, pieces[i], ref checkedPieces, ref highlightPieces);

                if (pickedUp)
                {
                    if (hasUnresolvedRequiredDependencies)
                    {
                        // Show connection to workbench
                        // TODO Check workbench occupation
                        Instance.Trail.SetTrail(ConnectionManager.FindClosestWorkbench(pieces[i].transform.position).ConnectionPoint.transform, pieces[i].transform);
                    }
                    else
                    {
                        // Show line to possible destination(s) on piece
                        // Find destination points
                        var closestCP = ConnectionManager.FindClosestConnectionPointToAP(pieces[i], piece); // (This should never be null)

                        Instance.Trail.SetTrail(closestCP.transform, pieces[i].transform);
                    }
                }
            }
            else
            {
                if (canConnect && !highlightPieces.Contains(pieces[i]))
                    highlightPieces.Add(pieces[i]);
            }
        }
    }

    public static void AutoCompleteCurrentStep()
    {
        var currentStep = _ficheGeneric.CurrentStep;
        if (currentStep == null)
            return;

        switch (currentStep.Type)
        {
            case StepType.Connection:
                {
                    var step = currentStep as ConnectionStep;
                    AutoCompleteStep(step);
                    break;
                }
            case StepType.PropertyChange:
                {
                    var step = currentStep as PropertyChangeStep;
                    AutoCompleteStep(step);
                    break;
                }
        }
    }

    static void AutoCompleteStep(TaskStep step)
    {
        switch (step.Type)
        {
            case StepType.Connection:
                {
                    var connectionStep = step as ConnectionStep;
                    AutoCompleteConnectionStep(connectionStep);
                    break;
                }
            case StepType.PropertyChange:
                {
                    var propertyChangeStep = step as PropertyChangeStep;
                    CheckPropertyChangeStep(propertyChangeStep);
                    break;
                }
        }

        CheckStep();
    }

    static void AutoCompleteConnectionStep(ConnectionStep step)
    {
        GuideUtils.UnHighlightAll();
        var pieces = AssemblyPieceManager.AssemblyPieces.Where(ap => ap.TypeID == step.PrefabTypeID);
        var connectedPieces = pieces.Where(ap => !ap.ConnectionPoints[step.ConnectionTypeID].Any(cp => !cp.IsConnected || (cp.ConnectedCP != null && cp.ConnectedCP.ParentPiece.TypeID != step.ParentTypeID))).ToList();
        var reservedPieces = pieces.Where(ap => ap.ConnectionPoints[step.ConnectionTypeID].Any(cp => cp.IsReserved)).ToList();
        connectedPieces.AddRange(reservedPieces);

        if (step.Progress != connectedPieces.Count)
        {
            step.Progress = connectedPieces.Count;
            AutoCompleteCurrentStep();
        }

        if (connectedPieces.Count >= step.Quantity)
        {
            //Resolved
            ConnectionStepDone(step);
        }
        else
        {
            //Unresolved
            var unconnectedPieces = pieces.Except(connectedPieces).ToList();
            var possibleParents = AssemblyPieceManager.AssemblyPieces.Where(ap => ap.TypeID == step.ParentTypeID).ToList();
            if (step.ConnectionTypeID == -1)
                possibleParents = ConnectionManager.WorkBenches.Where(wb => wb.GetComponent<AssemblyPiece>().TypeID == step.ParentTypeID).Select(wb => wb.GetComponent<AssemblyPiece>()).ToList();


            //// Spawn new parent if necessary & possible
            //if (possibleParents == null || possibleParents.Count == 0)
            //{
            //    var container = AssemblyPieceManager.AssemblyPieceContainers.FirstOrDefault(c => c.APTypeID == step.ParentTypeID);
            //    if (container == null)
            //        return;
            //
            //    //Spawn item 
            //    possibleParents.Add(container.SpawnPiece());
            //}
            //
            //var parent = possibleParents[0]; // TODO smart check
            //var parentConnectionCount = parent.ConnectedPoints.Count();
            //for (int i = 1; i < possibleParents.Count; ++i)
            //{
            //    var count = possibleParents[i].ConnectedPoints.Count();
            //    if (count > parentConnectionCount)
            //    {
            //        parent = possibleParents[i];
            //        parentConnectionCount = count;
            //    }
            //}

            // Connect required remaining number of pieces with the chosen parent
            var numberOfPieces = step.Quantity - connectedPieces.Count;

            // Spawn new piece if necessary & possible
            if (unconnectedPieces.Count < numberOfPieces)
            {
                var container = AssemblyPieceManager.AssemblyPieceContainers.FirstOrDefault(c => c.APTypeID == step.PrefabTypeID);
                if (container == null)
                    return;

                var required = numberOfPieces - unconnectedPieces.Count;
                for (int i = 0; i < required; ++i)
                {
                    //Spawn item 
                    unconnectedPieces.Add(container.SpawnPiece());
                }        
            }

            var parent = FindPossibleTarget(step.ConnectionTypeID, possibleParents);
            var ctime = Time.time;
            for (int i = 0; i < numberOfPieces; ++i)
            {
                unconnectedPieces[i].LoadConnectionPoints();
                var ownCP = unconnectedPieces[i].ConnectionPoints[step.ConnectionTypeID][0];
                if (!CanConnect(parent, step.ConnectionTypeID))
                    parent = FindPossibleTarget(step.ConnectionTypeID, possibleParents);
                parent.LoadConnectionPoints();
                var targetCP = parent.ConnectionPointsUnordered.FirstOrDefault(cp => cp.TypeID == step.ConnectionTypeID && cp.CanConnect);
                unconnectedPieces[i].AutoConnectOverTime(ownCP, targetCP);
            }

            // Start with connecting pieces that are in hand
            // Spawn piece from container if non available
        }
    }

    //TODO Move to connectionmanager
    static bool CanConnect(AssemblyPiece piece, int connectionTypeID)
    {
        return piece.ConnectionPointsUnordered.Any(cp => cp.TypeID == connectionTypeID && cp.CanConnect);
    }

    static AssemblyPiece FindPossibleTarget(int connectionTypeID, List<AssemblyPiece> possibleTargets)
    {
        AssemblyPiece target = null;

        var targets = new List<AssemblyPiece>(possibleTargets);
        // Spawn new parent if necessary & possible
        if (possibleTargets == null || possibleTargets.Count == 0)
        {
            var container = AssemblyPieceManager.AssemblyPieceContainers.FirstOrDefault(c => c.APTypeID == connectionTypeID);
            if (container == null)
                return target;

            //Spawn item 
            targets.Add(container.SpawnPiece());
        }
        else
        {
            for (int i = 0; i < possibleTargets.Count; ++i)
            {
                if (!CanConnect(possibleTargets[i], connectionTypeID))
                {
                    targets.Remove(possibleTargets[i]);
                }
            }

            if (targets.Count == 0)
                return FindPossibleTarget(connectionTypeID, targets);
        }

        target = targets[0]; // TODO smart check
        //var parentConnectionCount = parent.ConnectedPoints.Count();
        //for (int i = 1; i < possibleTargets.Count; ++i)
        //{
        //    var count = possibleTargets[i].ConnectedPoints.Count();
        //    if (count > parentConnectionCount)
        //    {
        //        parent = possibleParents[i];
        //        parentConnectionCount = count;
        //    }
        //}

        return target;
    }

    static void AutoCompletePropertyChangeStep(PropertyChangeStep step)
    {

    }

    public static void UndoLastStep()
    {

    }

    // TEMP stolen from PlaybackManager
    public static void SetProceduralBoxes(Pickable[] pieces)
    {
        Debug.Log("Highlighting some items");
        for (int i = 0; i < pieces.Length; ++i)
        {
            Debug.Log("highlighting: " + pieces[i].gameObject);

            ProceduralBox box;
            if (_proceduralBoxPool.Count > i)
            {
                box = _proceduralBoxPool[i];
            }
            else
            {
                box = Instantiate(Instance._proceduralBox.gameObject).GetComponent<ProceduralBox>();
                _proceduralBoxPool.Add(box);
            }

            box.Renderers = pieces[i].Renderers;
            box.TransformToFollow = pieces[i].transform;
            box.gameObject.SetActive(true);
            box.gameObject.name = "HBOX_" + pieces[i].gameObject.name;
        }

        //hide all other Proceduralboxes
        for (int i = pieces.Length; i < _proceduralBoxPool.Count; ++i)
        {
            _proceduralBoxPool[i].gameObject.SetActive(false);
        }
    }
}

[Serializable]
public class AssemblyPieceData
{
    public int APID;
    public int APTypeID;

    public AssemblyPieceData(int apID, int apTypeID)
    {
        APID = apID;
        APTypeID = apTypeID;
    }

    public override bool Equals(System.Object obj)
    {
        if (obj == null)
        {
            return false;
        }

        AssemblyPieceData other = obj as AssemblyPieceData;
        if ((System.Object)other == null)
        {
            return false;
        }

        return (APID == other.APID && APTypeID == other.APTypeID);
    }


    public bool Equals(AssemblyPieceData other)
    {
        if ((object)other == null)
        {
            return false;
        }

        return (APID == other.APID && APTypeID == other.APTypeID);
    }

    public override int GetHashCode()
    {
        return (APID ^ APTypeID) + (APTypeID ^ APID);
    }
}

[Serializable]
public class AssemblyData
{
    public AssemblyPieceData APD;
    public List<AssemblyPieceData> ConnectedAPD = new List<AssemblyPieceData>();

    public bool CompareAPD(AssemblyData other)
    {
        return APD.Equals(other.APD);
    }

    public bool CompareAPDType(AssemblyData other)
    {
        return APD.APTypeID == other.APD.APTypeID;
    }

    public bool CompareConnections(AssemblyData other)
    {
        // Only comparing on typeID for now
        // TODO if not exists check a twin piece
        // When comparing the correct with the current this will allow the current to have more connections than the correct
        for (int i = 0; i < ConnectedAPD.Count; ++i)
        {
            if (!other.ConnectedAPD.Exists(a => a.APTypeID == ConnectedAPD[i].APTypeID))
                return false;
        }

        //for (int i = 0; i < other.ConnectedAPD.Count; ++i)
        //{
        //    if (!ConnectedAPD.Exists(a => a.APTypeID == other.ConnectedAPD[i].APTypeID))
        //        return false;
        //}

        return true;
    }
}

[Serializable]
public class Assembly
{
    public List<AssemblyData> AssemblyData = new List<AssemblyData>();

    [NonSerialized]
    const string PATH = @"\assembly";
    static string _path { get { return Directory.GetCurrentDirectory() + @"\Datapath\" + PATH; } }

    public bool Compare(Assembly other)
    {
        List<AssemblyData> lockedPieces = new List<AssemblyData>();
        //List<int> lockedIDs = new List<int>();
        for (int i = 0; i < AssemblyData.Count; ++i)
        {
            var correspondingPieces = other.AssemblyData.Where(ap => ap.CompareAPDType(AssemblyData[i])/* && !lockedPieces.Contains(ap)*/).ToArray();
            var hasCorrectCorrespondingPiece = false;
            for (int j = 0; j < correspondingPieces.Length; ++j)
            {
                if (lockedPieces.Contains(correspondingPieces[j]))
                    Debug.LogError("locked piece: " + correspondingPieces[j].APD.APTypeID);
                if (AssemblyData[i].CompareConnections(correspondingPieces[j]))
                {
                    hasCorrectCorrespondingPiece = true;
                    // correspondingPieces[j] is now locked as corresponding piece for AssemblyData[i] 
                    lockedPieces.Add(correspondingPieces[j]);
                    break;
                }
            }

            if (!hasCorrectCorrespondingPiece)
                return false;

            //for(int j = 0; j < other.AssemblyData.Count; ++j)
            //{
            //    if (AssemblyData[i].CompareAPD(other.AssemblyData[j]))
            //    {
            //        if (!AssemblyData[i].CompareConnections(other.AssemblyData[j]))
            //        {
            //            return false;
            //        }
            //    }
            //}
        }

        return true;
    }

    public List<int> GetConnectedTypes(int typeID)
    {
        var connectedTypes = new List<int>();
        var assemblyDatas = AssemblyData.Where(ad => ad.APD.APTypeID == typeID).ToArray();
        for (int i = 0; i < assemblyDatas.Length; ++i)
        {
            connectedTypes.AddRange(assemblyDatas[i].ConnectedAPD.Select(apd => apd.APTypeID));
        }

        return connectedTypes;
    }

    public static bool Compare(Assembly a, Assembly b)
    {
        return a.Compare(b);
    }

    public void Save(string path_suffix = "")
    {
        Save(this, path_suffix);
    }

    public static Assembly Load(string path_suffix = "")
    {
        string json;

        try
        {
            StreamReader reader = new StreamReader(_path + path_suffix + ".json", Encoding.Default);
            using (reader)
            {
                json = reader.ReadToEnd();
            }
        }
        catch (Exception e)
        {
            Debug.Log(string.Format("{0}\n", e.Message));
            return new Assembly();
        }

        Assembly assembly = JsonUtility.FromJson<Assembly>(json);
        return assembly;
    }

    public static void Save(Assembly assembly, string path_suffix = "")
    {
        string json = JsonUtility.ToJson(assembly, true);

        try
        {
            StreamWriter writer = new StreamWriter(_path + path_suffix + ".json", false, Encoding.Default);
            using (writer)
            {
                writer.Write(json);
            }
        }
        catch (Exception e)
        {
            Debug.Log(string.Format("{0}\n", e.Message));
        }
    }
}
