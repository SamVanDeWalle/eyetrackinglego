﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using UnityEngine;

[Serializable]
public class AssemblyRecording 
{
    public List<TaskStep> TaskSteps = new List<TaskStep>();

    static string _fullPath { get { return Directory.GetCurrentDirectory() + @"\Datapath\"; } }
    const string EXT = ".json";

    public void Save(string path_suffix = "")
    {
        var fullPath = _fullPath + path_suffix + EXT;
        //TODO backup previous file
        //if (File.Exists(fullPath))
        //{
        //    var backupFileName = 
        //    string tempFileName = string.Format("{0}({1})", fileNameOnly, count++);
        //    newFullPath = Path.Combine(path, tempFileName + extension);
        //}

        var settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All };
        settings.Formatting = Formatting.Indented;
        var json = JsonConvert.SerializeObject(TaskSteps, settings);

        try
        {
            Directory.CreateDirectory(_fullPath);
            StreamWriter writer = new StreamWriter(fullPath, false, Encoding.Default);
            using (writer)
            {
                writer.Write(json);
            }
        }
        catch (Exception e)
        {
            Debug.Log(string.Format("{0}\n", e.Message));
        }
    }

    public static AssemblyRecording Load(string path_suffix = "")
    {
        var recording = new AssemblyRecording();
        string json;

        try
        {
            StreamReader reader = new StreamReader(_fullPath + path_suffix + EXT, Encoding.Default);
            using (reader)
            {
                json = reader.ReadToEnd();
            }
        }
        catch (Exception e)
        {
            Debug.Log(string.Format("{0}\n", e.Message));
            return recording;
        }

        var settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All };
        recording.TaskSteps = JsonConvert.DeserializeObject<List<TaskStep>>(json, settings);

        return recording;
    }
}
