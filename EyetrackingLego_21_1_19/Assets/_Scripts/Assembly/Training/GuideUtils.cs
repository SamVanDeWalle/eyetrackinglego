﻿using Helpers;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AssemblyPiecePreviewConnection
{
    public AssemblyPiece Piece;
    public ConnectionPoint OwnCP;
    public ConnectionPoint ForeignCP;

    public AssemblyPiecePreviewConnection(AssemblyPiece piece, ConnectionPoint ownCP, ConnectionPoint foreignCP)
    {
        Piece = piece;
        OwnCP = ownCP;
        ForeignCP = foreignCP;
    }
}

public class GuideUtils : Singleton<GuideUtils>
{
    public static List<Pickable> AllowedPickables = new List<Pickable>();
    public static AssemblyPiecePreviewConnection PreviewConnection;
    public static WorkbenchGuide WorkbenchGuide
    {
        get
        {
            if (_workbenchGuide == null)
                _workbenchGuide = FindObjectOfType<WorkbenchGuide>();

            return _workbenchGuide;
        }
        private set { }
    }
    public static Material PreviewGhostMat
    {
        get
        {
            if (_previewGhostMat == null)
                LoadPreviewGhostMat();
            return _previewGhostMat;
        }
        private set { }
    }
    public static bool HasWorkbenchGuide { get { return WorkbenchGuide != null; } private set { } }

    static string ProceduralBoxPath = "Prefabs/GuideUtils/ProceduralBox";
    static GameObject _proceduralBoxPref = null;
    static List<ProceduralBox> _proceduralBoxPool = new List<ProceduralBox>();

    static string TrailPath = "Prefabs/GuideUtils/CurvedTrailObject";
    static CurvedTrailBehaviour _trail;
    static List<CurvedTrailBehaviour> AidTrails = new List<CurvedTrailBehaviour>();

    static string PreviewGhostMatPath = "Materials/GuideUtils/MAT_PreviewGhost";
    static Material _previewGhostMat;

    static List<ConnectionPoint> _indicatedCPs = new List<ConnectionPoint>();
    static WorkbenchGuide _workbenchGuide;

    void Start()
    {
        _proceduralBoxPool.Clear();
    }

    static void LoadProceduralBoxPrefab()
    {
        if(_proceduralBoxPref == null)
        {
            _proceduralBoxPref = Resources.Load(ProceduralBoxPath) as GameObject;
        }
    }

    static void LoadTrail()
    {
        if(_trail == null)
        {
            var trailGO = Instantiate(Resources.Load(TrailPath) as GameObject) as GameObject;
            trailGO.name = "GuideTrail";
            _trail = trailGO.GetComponent<CurvedTrailBehaviour>();
        }
    }

    public static void AddAidTrail(Transform start, Transform forwardTransform, List<Transform> targets)
    {
        var trailGO = Instantiate(Resources.Load(TrailPath) as GameObject) as GameObject;
        trailGO.name = "GuideTrail";
        var trail = trailGO.GetComponent<CurvedTrailBehaviour>();
        AidTrails.Add(trail);
        trail.SetTrail(start, forwardTransform, targets);
    }

    static void LoadPreviewGhostMat()
    {
        if (_previewGhostMat == null)
        {
            _previewGhostMat = Resources.Load(PreviewGhostMatPath) as Material;
        }
    }

    public static void SetProceduralBoxes(Pickable[] pieces)
    {
        LoadProceduralBoxPrefab();

        if (_proceduralBoxPool.Count > 0 && _proceduralBoxPool[0] == null)
            _proceduralBoxPool.Clear();

        for (int i = 0; i < pieces.Length; ++i)
        {
            ProceduralBox box;
            if (_proceduralBoxPool.Count > i)
            {
                box = _proceduralBoxPool[i];
            }
            else
            {
                box = Instantiate(_proceduralBoxPref).GetComponent<ProceduralBox>();
                _proceduralBoxPool.Add(box);
            }

            box.Renderers = pieces[i].Renderers;
            box.TransformToFollow = pieces[i].transform;
            box.gameObject.SetActive(true);
            box.gameObject.name = "HBOX_" + pieces[i].gameObject.name;
        }

        //hide all other Proceduralboxes
        for (int i = pieces.Length; i < _proceduralBoxPool.Count; ++i)
        {
            _proceduralBoxPool[i].gameObject.SetActive(false);
        }
    }

    public static void Highlight(Pickable pickable)
    {
        var array = new Pickable[1] { pickable };
        Highlight(array);
    }

    public static void Highlight(List<Pickable> pickables)
    {
        Highlight(pickables.ToArray());
    }

    public static void Highlight(Pickable[] pickables)
    {
        SetProceduralBoxes(pickables);
        MaterialManager.GetInstance().HintObjects(pickables);
        ViewGuide.Activate(pickables.Select(p => p.transform).ToArray());
    }

    public static void UnHighlightAll()
    {
        //MaterialManager.GetInstance().UnglowAll();
        MaterialManager.GetInstance().HintObjects(new Pickable[0]);
        SetProceduralBoxes(new Pickable[0]);
        ViewGuide.Deactivate();
    }

    public static void SetTrail(Transform start, Transform forwardTransform, List<Transform> targets)
    {
        LoadTrail();
        _trail.SetTrail(start, forwardTransform, targets);
        _trail.SetCorrect(false);
    }

    public static void SetTrail(Transform start, List<Transform> targets)
    {
        LoadTrail();
        _trail.SetTrail(start, targets);
        _trail.SetCorrect(false);
    }

    public static void SetTrail(Transform start, Transform target)
    {
        LoadTrail();
        _trail.SetTrail(start, new List<Transform>() { target });
        _trail.SetCorrect(false);
    }

    public static void DeactivateTrail()
    {
        LoadTrail();
        _trail.DeactivateTrail();
    }

    public static void DeactivateAidTrails()
    {
        for(int i = AidTrails.Count - 1; i >= 0; --i)
        {
            Destroy(AidTrails[i].gameObject);
        }

        AidTrails.Clear();
    }

    public static void SetTrailCorrect(bool correct)
    {
        LoadTrail();
        _trail.SetCorrect(correct);
    }

    public static void IndicateCPs(List<ConnectionPoint> cps)
    {
        for(int i = 0; i < cps.Count; ++i)
        {
            IndicateCP(cps[i]);
        }
    }

    public static void IndicateCP(ConnectionPoint cp)
    {
        cp.Indicate();
        if (!_indicatedCPs.Contains(cp))
            _indicatedCPs.Add(cp);

        PickupController.OnConnectionMatchLost -= EvaluateConnectionMatchLost;
        PickupController.OnConnectionMatchLost += EvaluateConnectionMatchLost;
        PickupController.OnConnectionMatch -= EvaluateConnectionMatch;
        PickupController.OnConnectionMatch += EvaluateConnectionMatch;
    }

    public static void StopIndicatingAllCPs()
    {
        for(int i = 0; i < _indicatedCPs.Count; ++i)
        {
            _indicatedCPs[i].StopIndicating();
        }

        _indicatedCPs.Clear();
    }

    static void EvaluateConnectionMatchLost(ConnectionMatch cm)
    {
        for (int i = 0; i < _indicatedCPs.Count; ++i)
        {
            if (_indicatedCPs[i] == cm.A || _indicatedCPs[i] == cm.B)
            {
                _indicatedCPs[i].SetIndicateCorrect(false);
            }
        }

        SetTrailCorrect(false);
    }

    static void EvaluateConnectionMatch(ConnectionMatch cm)
    {
        for (int i = 0; i < _indicatedCPs.Count; ++i)
        {
            if (_indicatedCPs[i] != cm.A && _indicatedCPs[i] != cm.B)
            {
                _indicatedCPs[i].SetIndicateCorrect(false);
            }
        }

        SetTrailCorrect(false);
    }

    public static void GuideConnection(ConnectionPoint ownCP, ConnectionPoint targetCP)
    {
        GuideConnection(ownCP, new List<ConnectionPoint>() { targetCP });
    }

    public static void GuideConnection(ConnectionPoint ownCP, List<ConnectionPoint> targetCPs)
    {
        var forwardTransform = ownCP.ParentPiece.transform.parent != null ? ownCP.ParentPiece.transform.parent : ownCP.ParentPiece.transform;
        SetTrail(ownCP.transform, forwardTransform, targetCPs.Select(cp => cp.transform).ToList());
        if (ownCP.AidPoints.Count > 0)
        {
            for (int i = 0; i < ownCP.AidPoints.Count; ++i)
            {
                var aidTargets = new List<Transform>();
                for (int t = 0; t < targetCPs.Count; ++t)
                {
                    if (i == 0)
                        targetCPs[t].ClearAidPoints();

                    var aidGO = Instantiate(new GameObject());
                    aidGO.transform.SetParent(targetCPs[t].transform);
                    aidGO.transform.localPosition = ownCP.AidPoints[i].localPosition;
                    aidGO.transform.localRotation = ownCP.AidPoints[i].localRotation;
                    targetCPs[t].AidPoints.Add(aidGO.transform);
                    aidTargets.Add(aidGO.transform);
                }

                AddAidTrail(ownCP.AidPoints[i], forwardTransform, aidTargets);
            }
        }
    }

    public static void StopGuidingConnection()
    {
        DeactivateTrail();
        DeactivateAidTrails();
    }
}
