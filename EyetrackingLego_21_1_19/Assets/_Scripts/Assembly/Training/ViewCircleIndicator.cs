﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewCircleIndicator : MonoBehaviour
{
    public Animation Animation;
    public CanvasGroup CanvasGroup;
    public Image Left, Right;

    bool _bVisible = true;

    void Update()
    {
        var viewPosition = ViewGuide.GetViewPosition() - new Vector2(0.5f, 0.5f);
        //Debug.Log(viewPosition);
        //var angle = Vector2.Angle(new Vector2(0, 1), viewPosition);
        var angle = Mathf.Atan2(viewPosition.y, viewPosition.x) * Mathf.Rad2Deg;
        if (float.IsNaN(angle))
            angle = 0;
        //Debug.Log(angle);
        var rot = transform.localEulerAngles;
        rot.z = angle;
        transform.localEulerAngles = rot;

        var distance = viewPosition.magnitude;
        //Debug.Log(distance);

        if(distance < 0.4f)
        {
            if(distance < 0.3f)
            {
                SetOpacity(0);
            }
            else
            {
                SetOpacity(Mathf.Pow((distance - 0.3f) / 0.1f, 2));
            }
        }
        else
        {
            SetOpacity(1);
        }
    }

    void SetOpacity(float value)
    {
        CanvasGroup.alpha = _bVisible ? value : 0;
    }

    public void ToggleVisibility()
    {
        _bVisible = !_bVisible;
    }
}
