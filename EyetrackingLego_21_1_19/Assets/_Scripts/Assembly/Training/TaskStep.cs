﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StepType
{
    Connection,
    PropertyChange
}

[Serializable]
public class TaskStep 
{
    public int ID;
    public string Info;
    public StepType Type;
    public Sprite Image;
    public bool CombinedStep;
    public int CombinedStepID;
    public bool RequireWorkbenchAngle;
    public float WorkbenchAngle; // Value between 0 - 360, -1 is ignored

    public delegate void DefaultDelegate();
    public static event DefaultDelegate OnDone;
    public static event DefaultDelegate OnReset;
    public bool IsDone { get { return _bDone; } private set { } }

    bool _bDone = false;

    public void Done()
    {
        if (_bDone == true)
            return;

        SoundManager.Play("taskdone", new Vector3());

        _bDone = true;
        if (OnDone != null)
            OnDone();
    }

    public virtual void Reset()
    {
        _bDone = false;

        if (OnReset != null)
            OnReset();
    }
}

[Serializable]
public class ConnectionStep : TaskStep
{
    public int PrefabTypeID;
    public int ConnectionTypeID;
    public int Quantity;
    public int Progress;
    public int ParentTypeID;
    public bool AutoCompleteQuantity;
    public int RequiredQuantity;

    public ConnectionStep()
    {
        Type = StepType.Connection;
    }

    public override void Reset()
    {
        base.Reset();

        Progress = 0;
    }

    public override string ToString()
    {
        string parent = ParentTypeID == 500 ? "Workbench" : ParentTypeID.ToString();
        return PrefabTypeID + " on " + parent;
    }

    public bool Combinable(ConnectionStep other)
    {
        return PrefabTypeID == other.PrefabTypeID && ConnectionTypeID == other.ConnectionTypeID && ParentTypeID == other.ParentTypeID;
    }
}

[Serializable]
public class PropertyChangeStep : TaskStep
{
    public int PrefabTypeID;
    public PropertyType PropertyType;
    public int PropertyID;
    public int Quantity;

    public PropertyChangeStep()
    {
        Type = StepType.PropertyChange;
    }

    public override void Reset()
    {
        base.Reset();
    }

    public override string ToString()
    {
        return PrefabTypeID + " < " + PropertyType.ToString();
    }
}

public class StepCreator
{
    public virtual TaskStep Create()
    {
        return new TaskStep();
    }
}

public class StepCreator<T> : StepCreator where T : TaskStep
{
    public override TaskStep Create()
    {
        return Activator.CreateInstance(typeof(T)) as T;
    }

    //public static T Create()
    //{
    //    return Activator.CreateInstance(typeof(T),
    //              new object[] { null, null }) as T;
    //}
}
