﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class AssemblyStep
{
    public string ID; // Example 004-214267-000
    public int PrefabTypeID;
    public int Quantity;
}
