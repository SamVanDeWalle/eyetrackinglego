﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkbenchGuide : MonoBehaviour
{
    public RotationIndicator StickIndicator;

	public void GuideDesiredAnge(bool active, float angle = 0)
    {
        StickIndicator.gameObject.SetActive(active);
        StickIndicator.DesiredAngle = angle;
    }
}
