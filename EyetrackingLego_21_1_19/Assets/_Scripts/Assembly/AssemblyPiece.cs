﻿/*
 * 
 * 
 * An assembly piece is an object that can be connected to other pieces
 *  
 * TODO
 * Evaluate and remove unnecessary prototyping code
 * 
 */

using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System;

public class BoundingSphere
{
    public Transform Transform;
    public Vector3 Center;
    public float Radius;
    public Vector3 Position
    {
        get
        {
            return Transform.position + Center;
        }
        private set
        {
        }
    }

    public BoundingSphere(Transform transform, Vector3 center, float radius)
    {
        Transform = transform;
        Center = center;
        Radius = radius;
    }

    public static BoundingSphere Combine(BoundingSphere a, BoundingSphere b)
    {
        //TODO implement case where circle a or b fully overlaps b or a
        var boundingSphere = new BoundingSphere(a.Transform, a.Center, a.Radius);
        var aToB = b.Position - a.Position;
        var distance = Mathf.Abs(aToB.magnitude);
        boundingSphere.Radius = (distance + a.Radius + b.Radius) / 2.0f;
        var directionFromA = aToB.normalized;
        float difference = boundingSphere.Radius - a.Radius;
        var shift = directionFromA * (float)(boundingSphere.Radius - a.Radius);
        boundingSphere.Center = a.Center + shift;
        return boundingSphere;
    }

    public static bool CheckOverlap(BoundingSphere a, BoundingSphere b, float distanceSqr = -1)
    {
        if (a == null || b == null)
            return false;

        if (distanceSqr == -1)
        {
            distanceSqr = (a.Position - b.Position).sqrMagnitude;
        }

        return distanceSqr < (a.Radius + b.Radius) * (a.Radius + b.Radius);
    }
}

[RequireComponent(typeof(PropertyHolder))]
public class AssemblyPiece : MonoBehaviour
{
    public SimpleEvent OnPickup = new SimpleEvent();
    public SimpleEvent OnPutBack = new SimpleEvent();

    bool _firstPickup = false;
    //RECORDED STEP INDEX
    LinkedList<int> RecordedStepIndices = new LinkedList<int>();
    public int LastRecordedStepIndex
    {
        get { return RecordedStepIndices.Count == 0 ? 0 : RecordedStepIndices.Last.Value; }
        set { RecordedStepIndices.AddLast(value); }
    }
    public void ResetRecordedStepIds()
    {
        RecordedStepIndices.Clear();
    }
    public int TypeID;
    public int ID;
    public bool InPlace;
    public bool IsLocked;
    public bool CanConnectToWorkbench;
    public ConnectionPoint ExplicitCenterCP;
    public bool CanBeConnectedTo = true;
    public bool IsPickable = true;
    public bool OnPrePose { get { return _bOnPrePose; } set { } }

    public List<int> RequiredIDs = new List<int>();

    static float _moveSpeed = 8.0f;
    public Vector3 TargetPosition;
    public bool CanTravel = false;
    public bool ValidPlacement
    {
        get
        {
            return _validPlacement;
        }
        private set
        {
        }
    }
    public bool IsOnWorkbench { get { return transform.parent != null && transform.parent.GetComponent<Workbench>() != null; } private set { } }
    public bool IsPickedUp { get { return _bPickedUp; } private set { } }
    public Pickable Pickable { get { return _pickable; } private set { } }
    public Renderer[] Renderers { get { return _pickable.Renderers; } private set { } }

    bool _bPickedUp = false;
    bool _validPlacement = false;
    bool _ghostStateChanged = false;
    public GameObject GhostObject;
    public ConnectionPoint CenterCP { get { return _centerCP; } private set { } }
    public AudioClip ConnectSound;

    BoundingSphere _boundingSphere;
    public BoundingSphere GroupBoundingSphere;
    public BoundingSphere BoundingSphere
    {
        get
        {
            if (_boundingSphere == null)
            {
                CalculateBoundingSphere();
                GroupBoundingSphere = _boundingSphere;
            }
            return _boundingSphere;
        }
        private set
        {
        }
    }
    public AssemblyPiece PickableRootPiece;
    public AssemblyPiece RootPiece { get { return ParentPiece ? ParentPiece.RootPiece : this; } private set { } }
    public AssemblyPiece ParentPiece { get { return _parentPiece; } private set { } }
    public Dictionary<int, List<ConnectionPoint>> ConnectionPoints
    {
        get
        {
            return _connectionPointsOrdered;
        }
        private set
        {
        }
    }
    Dictionary<int, List<ConnectionPoint>> _connectionPointsOrdered = new Dictionary<int, List<ConnectionPoint>>();
    ConnectionPoint[] _connectionPoints;
    public ConnectionPoint[] ConnectionPointsUnordered
    {
        get
        {
            return _connectionPoints;
        }
        private set
        {
        }
    }
    public ConnectionPoint[] ConnectedPoints
    {
        get
        {
            return _connectionPoints.Where(cp => cp.IsConnected).ToArray();
        }
        private set
        {
        }
    }
    public ConnectionPoint PrePoseCP { get { return _prePoseCP; } private set { } }

    //List<ConnectionPoint> _connectedPoints = new List<ConnectionPoint>();
    List<AssemblyPiece> _connectedPieces = new List<AssemblyPiece>();
    //public Dictionary<Renderer, List<Material>> _meshMaterials = new Dictionary<Renderer, List<Material>>();
    List<ConnectionPoint> _connectedPointsStart = new List<ConnectionPoint>();

    AssemblyPiece _parentPiece;
    Vector3 _lastGhostPosition;

    Vector3 _firstPosition;
    Quaternion _firstRotation;
    Transform _firstParent;
    Transform _firstGhostParent;
    ConnectionPoint _prePoseCP, _prePoseOtherCP;
    bool _hadRigidbodyStart;
    bool _bOnPrePose;

    PickupController _holdingController = null;
    ConnectionPoint _centerCP;
    Pickable _pickable;

    public delegate void defaultDelegate();
    public event defaultDelegate OnPickedUp;
    public event defaultDelegate OnDetach;
    public delegate void apDelegate(AssemblyPiece piece);
    public event apDelegate OnAttach;
    public event defaultDelegate OnEnteredValid;
    public event defaultDelegate OnLeftValid;
    public event defaultDelegate OnEnteredPrePose;
    public static event defaultDelegate OnAutoConnected;

    public bool CanDisconnect
    {
        get
        {
            //TODO check if connected child pieces can disconnect from foreign piece that does not have this as root
            for (int i = 0; i < _connectionPoints.Length; ++i)
            {
                if (_connectionPoints[i].TypeID != -1 && _connectionPoints[i].IsConnected && _connectionPoints[i].ConnectedCP.RootPiece != this && !_connectionPoints[i].ConnectedCP.CanDisconnect)
                    return false;
            }

            return true;
        }
        private set { }
    }

    public Bounds GetBounds()
    {
        Bounds bounds = new Bounds();
        bool boundsSet = false;
        foreach (var r in Renderers)
        {
            Bounds currentBounds = r.bounds;

            if (!boundsSet)
                bounds = currentBounds;
            else
                bounds.Encapsulate(currentBounds);
            boundsSet = true;

        }
        return bounds;
    }

    public Vector3[] GetBoundingPointsUp()
    {
        var storedPos = transform.position;
        //var storedRot = transform.rotation;
        transform.position = Vector3.zero;
        //Quaternion ignoreRot = Quaternion.Euler(0f, storedRot.eulerAngles.y, 0f);
        //Quaternion ignoreRot = Quaternion.Euler(storedRot.eulerAngles.x, 0f, storedRot.eulerAngles.z);
        //Quaternion ignoreRot = transform.rotation;
        //Quaternion ignoreRot =  Quaternion.FromToRotation(transform.right, Vector3.right);

        //Quaternion ignoreRot = Quaternion.identity;
        //transform.rotation = Quaternion.Inverse(ignoreRot) ;
        Bounds b = GetBounds();
        //transform.rotation = storedRot;
        Vector3[] points = new Vector3[4];

        points[0] = b.center + new Vector3(b.extents.x, 0f, b.extents.z);
        points[1] = b.center + new Vector3(b.extents.x, 0f, -b.extents.z);
        points[2] = b.center + new Vector3(-b.extents.x, 0f, -b.extents.z);
        points[3] = b.center + new Vector3(-b.extents.x, 0f, b.extents.z);

        for (int i = 0; i < points.Length; ++i)
        {
            //points[i] = storedRot * points[i];
            points[i].y = 0;

        }

        transform.position = storedPos;
        return points;
    }

    void Awake()
    {
        SetPickable();
        SetWorkbenchCP();
        LoadConnectionPoints();
        AssemblyPieceManager.Ensure(this);
    }

    void SetPickable()
    {
        _pickable = GetComponent<Pickable>();
        if (!_pickable)
        {
            _pickable = gameObject.AddComponent<Pickable>();
        }

        _pickable.Type = PickableType.AssemblyPiece;
        PickupManager.EnsurePickable(_pickable);
    }

    void Start()
    {
        _firstPosition = transform.position;
        _firstRotation = transform.rotation;
        _firstParent = transform.parent;
        if (GhostObject != null)
            _firstGhostParent = GhostObject.transform.parent;

        RootPiece = FindRootPiece(this);
        
        //UpdateConnectionPointOccupation();
        //_connectedPointsStart = _connectedPoints.ToList();
        var renderers = GetComponentsInChildren<Renderer>();
        _connectedPieces = FindAllConnectedPieces(this, false);
        Setup();
        _hadRigidbodyStart = GetComponent<Rigidbody>() != null;

        //_lastAPState = GetCurrentAPState();
    }

    void SetWorkbenchCP()
    {
        _centerCP = ExplicitCenterCP;

        if (CanConnectToWorkbench && _centerCP == null)
        {
            var centerCPGO = new GameObject("CenterCP");
            centerCPGO.transform.position = transform.position;
            centerCPGO.transform.SetParent(transform);
            centerCPGO.transform.localRotation = Quaternion.identity;
            _centerCP = centerCPGO.AddComponent<ConnectionPoint>();
            _centerCP.TypeID = -1;
        }
    }

    public void Setup()
    {
        CalculateBoundingSphere();
        SetInitialMass();
        GroupBoundingSphere = _boundingSphere;
    }

    public void LoadConnectionPoints()
    {
        _connectionPointsOrdered.Clear();
        // Find the connection points directly under the assembly piece
        _connectionPoints = GetComponentsInChildren<ConnectionPoint>().Where(c => c.transform.parent == transform).ToArray();
        for (int i = 0; i < _connectionPoints.Length; i++)
        {
            if (_connectionPoints[i].transform.parent != transform)
                continue;

            if (!_connectionPointsOrdered.ContainsKey(_connectionPoints[i].TypeID))
                _connectionPointsOrdered.Add(_connectionPoints[i].TypeID, new List<ConnectionPoint>());

            _connectionPointsOrdered[_connectionPoints[i].TypeID].Add(_connectionPoints[i]);
        }
    }

    void Update()
    {
        if (CanTravel && transform.position != TargetPosition)
        {
            transform.position += (TargetPosition - transform.position).normalized * Time.deltaTime * _moveSpeed;
            if (Vector3.Distance(transform.position, TargetPosition) < 0.2f)
            {
                transform.position = TargetPosition;
            }
        }

        //Fell through ground
        if (transform.position.y < -10)
        {
            var newPosition = transform.position;
            newPosition.y = 15;
            transform.position = newPosition;
        }
    }
    //#if UNITY_EDITOR
    //    void OnDrawGizmosSelected()
    //    {
    //        if (BoundingSphere != null)
    //        {
    //            Gizmos.color = new Color(1f, 1f, 1f, 0.5f);
    //            Gizmos.DrawSphere(BoundingSphere.Center + transform.position, BoundingSphere.Radius);
    //        }
    //        if (GroupBoundingSphere != null)
    //        {
    //            Gizmos.color = new Color(0f, 1f, 0f, 0.5f);
    //            Gizmos.DrawSphere(GroupBoundingSphere.Center + transform.position, BoundingSphere.Radius);
    //        }
    //    }
    //#endif
    private AssemblyPieceState _lastAPState;
    public void Pickup(PickupController controller)
    {
        if (!_firstPickup) OnPickup.Invoke();
        _firstPickup = true;

        _holdingController = controller;

        var rb = GetComponent<Rigidbody>();
        if (rb)
            rb.isKinematic = true;

        SetCollidersTrigger(true);
        SetColliders(false);
        SetOutlined(false, true);

        _lastAPState = GetCurrentAPState();

        Detach();

        _bPickedUp = true;
        _pickable.Pickup(controller);

        var handler = OnPickedUp;
        if (handler != null)
            handler();
    }

    public AssemblyPieceState GetCurrentAPState()
    {
        AssemblyPiece parent = null;
        if (transform.parent != null)
        {
            parent = transform.parent.GetComponent<AssemblyPiece>();
        }
        if (parent == null) transform.parent = null;
        return new AssemblyPieceState() { localPos = transform.localPosition, localRot = transform.localRotation, Parent = parent };
    }

    public void SetGhostConnected(ConnectionMatch match, AssemblyPieceLooseState snapState = null, PickupController controller = null)
    {
        var aRoot = PickupController.FindRootPiece(match.A.transform.parent.gameObject);
        if (aRoot == gameObject)
        {
            SetGhostConnected(match.B, match.A, snapState, controller);
        }
        else
        {
            SetGhostConnected(match.A, match.B, snapState, controller);
        }
    }

    public void SetGhostActive(bool active = true)
    {
        if (GhostObject.activeInHierarchy == active)
            return;

        GhostObject.SetActive(active);
        _ghostStateChanged = true;
    }

    public void SetGhostConnected(ConnectionPoint foreignCP, ConnectionPoint ownCP, AssemblyPieceLooseState snapState, PickupController controller)
    {
        SetGhostActive();
        var ownParent = ownCP.transform.parent;
        ownCP.transform.SetParent(null, true);
        // Helper object that defines the new temporary pivot point of the ghost object
        var ghostCP = new GameObject();
        var ghostCPTransform = ghostCP.transform;
        ghostCPTransform.position = ownCP.transform.position;
        ghostCPTransform.rotation = ownCP.transform.rotation;
        ownCP.transform.SetParent(ownParent, true);
        // Reset Ghost position & orientation
        GhostObject.transform.position = transform.position;
        GhostObject.transform.rotation = transform.rotation;
        // Set the ghost parent to the connection point
        GhostObject.transform.SetParent(ghostCPTransform);

        // Find the translation and rotation difference between the connection points
        var ct = ConnectionManager.GetType(foreignCP.TypeID);
        Debug.Log(ct);
        var foreignParent = foreignCP.transform.parent;
        foreignCP.transform.SetParent(null, true);
        Vector3 foreignRotation = foreignCP.transform.eulerAngles;
        Vector3 currentRotation = ghostCPTransform.eulerAngles;
        Quaternion relative = Quaternion.Inverse(foreignCP.transform.rotation) * ownCP.transform.rotation;
        //Quaternion relative = ownCP.transform.rotation * Quaternion.Inverse(foreignCP.transform.rotation);
        //Quaternion relative = Quaternion.Inverse(foreignCP.transform.localRotation) * ownCP.transform.localRotation;
        var relativeEuler = relative.eulerAngles;
        //var flipped = Quaternion.Angle(foreignCP.transform.rotation, ownCP.transform.rotation) > 90;
        var flipped = false;
        //var xF = foreignCP.transform.eulerAngles.x;
        //var xO = ownCP.transform.eulerAngles.x;
        //xF = xF > 180 ? xF -= 360 : xF;
        //xO = xO > 180 ? xO -= 360 : xO;
        //var xDiff = Mathf.Abs(xF - xO);

        //Debug.Log(xO);
        //Debug.Log(xDiff);
        //var flippedX = xDiff > 180;
        // The ghost with the cp as pivot will get the same position and rotation as the foreign cp
        // Vector3 ghostRotation = foreignCP.transform.eulerAngles;
        // Then the ghost will get an extra rotation, in order to make the ghost rotation as close to the own rotation as possible
        Vector3 postRotation = new Vector3();
        foreignCP.transform.SetParent(foreignParent);

        // Tooltip
        RotationTooltip.LockOn(foreignCP, controller);
        RotationTooltip.Activate(true);
        RotationTooltip.ActivateX(false);
        RotationTooltip.ActivateY(false);
        RotationTooltip.ActivateZ(false);

        // X
        if (ct.XRotationType == RotationType.Step)
        {
            // Find the closest allowed X angle
            // Find the angle difference in angle with the foreign connection point
            var delta = relativeEuler.x;
            if (flipped)
            {
                delta = (delta * -1.0f) + 180;
            }

            if (delta < 0)
                delta = 360 + delta;

            var boundAngle = (delta + 180) % 360;
            if (boundAngle > ct.xRotationBounds.y)
            {
                boundAngle = ct.xRotationBounds.y;
            }
            else if (boundAngle < ct.xRotationBounds.x)
            {
                boundAngle = ct.xRotationBounds.x;
            }

            delta = (boundAngle + 180) % 360;

            int steps = Mathf.RoundToInt(delta / ct.StepX);
            //int steps = Mathf.FloorToInt(delta / ct.StepX);
            //int steps = Mathf.CeilToInt(delta / ct.StepX);
            postRotation.x = steps * ct.StepX;

            if ((ct.xRotationBounds.x != 0 || ct.xRotationBounds.y != 360) && snapState == null)
            {
                RotationTooltip.ActivateX(true);
                RotationTooltip.SetXBounds(ct.xRotationBounds.x, ct.xRotationBounds.y);
            }
            

            // Scrolling for non VR
            //if (ct.Scrolling == ScrollStep.ScrollX)
            //{
            //    postRotation.x += ConnectionManager.ScrollStep * ct.StepX;
            //}
            //
            //if (ct.SecundaryScrolling == ScrollStep.ScrollX)
            //{
            //    postRotation.x += ConnectionManager.ScrollStep * ct.StepX;
            //}
        }
        else if (ct.XRotationType == RotationType.Free)
        {
            var delta = relativeEuler.x;
            if (flipped)
            {
                delta = (delta * -1.0f) + 180;
            }

            if (delta < 0)
                delta = 360 + delta;

            var boundAngle = (delta + 180) % 360;
            if (boundAngle > ct.xRotationBounds.y)
            {
                boundAngle = ct.xRotationBounds.y;
            }
            else if (boundAngle < ct.xRotationBounds.x)
            {
                boundAngle = ct.xRotationBounds.x;
            }

            delta = (boundAngle + 180) % 360;

            postRotation.x = delta;
        }

        // Y
        if (ct.YRotationType == RotationType.Step)
        {
            // Find the closest allowed Y angle
            // Find the angle difference in angle with the foreign connection point

            var delta = relativeEuler.y;

            if (flipped)
            {
                delta = (delta * -1.0f) + 180;
            }

            if (delta < 0)
                delta = 360 + delta;

            var boundAngle = (delta + 180) % 360;
            if (boundAngle > ct.yRotationBounds.y)
            {
                boundAngle = ct.yRotationBounds.y;
            }
            else if (boundAngle < ct.yRotationBounds.x)
            {
                boundAngle = ct.yRotationBounds.x;
            }

            delta = (boundAngle + 180) % 360;

            int steps = Mathf.RoundToInt(delta / ct.StepY);
            postRotation.y = steps * ct.StepY;

            if ((ct.yRotationBounds.x != 0 || ct.yRotationBounds.y != 360) && snapState == null)
            {
                RotationTooltip.ActivateY(true);
                RotationTooltip.SetYBounds(ct.yRotationBounds.x, ct.yRotationBounds.y);
            }
            //if (ct.Scrolling == ScrollStep.ScrollY)
            //{
            //    postRotation.y += ConnectionManager.ScrollStep * ct.StepY;
            //}
            //
            //if (ct.SecundaryScrolling == ScrollStep.ScrollY)
            //{
            //    postRotation.y += ConnectionManager.SecundaryScrollStep * ct.StepY;
            //}
        }
        else if (ct.YRotationType == RotationType.Free)
        {
            postRotation.y = relativeEuler.y;
        }

        // Z
        if (ct.ZRotationType == RotationType.Step)
        {
            // Find the closest allowed Z angle
            // Find the angle difference in angle with the foreign connection point

            var delta = relativeEuler.z;

            if (flipped)
            {
                delta = (delta * -1.0f) + 180;
            }

            if (delta < 0)
                delta = 360 + delta;

            var boundAngle = (delta + 180) % 360;
            if (boundAngle > ct.zRotationBounds.y)
            {
                boundAngle = ct.zRotationBounds.y;
            }
            else if (boundAngle < ct.zRotationBounds.x)
            {
                boundAngle = ct.zRotationBounds.x;
            }

            delta = (boundAngle + 180) % 360;

            int steps = Mathf.RoundToInt(delta / ct.StepZ);
            postRotation.z = steps * ct.StepZ;

            if ((ct.zRotationBounds.x != 0 || ct.zRotationBounds.y != 360) && snapState == null)
            {
                RotationTooltip.ActivateZ(true);
                RotationTooltip.SetZBounds(ct.zRotationBounds.x, ct.zRotationBounds.y);
            }
            //if (ct.Scrolling == ScrollStep.ScrollZ)
            //{
            //    postRotation.z += ConnectionManager.ScrollStep * ct.StepZ;
            //}
            //
            //if (ct.SecundaryScrolling == ScrollStep.ScrollZ)
            //{
            //    postRotation.z += ConnectionManager.SecundaryScrollStep * ct.StepZ;
            //}
        }
        else if (ct.ZRotationType == RotationType.Free)
        {
            postRotation.z = relativeEuler.z;
        }

        ghostCPTransform.eulerAngles = foreignRotation;
        ghostCPTransform.position = foreignCP.transform.position;
        ghostCPTransform.Rotate(postRotation.x, postRotation.y, postRotation.z, Space.Self);
        RotationTooltip.SetIndicators(postRotation);
        GhostObject.transform.SetParent(foreignCP.transform.parent);
        //ViveHelper.Controllers[ControllerIndex.Left].Device.TriggerHapticPulse(60);
        //ViveHelper.Controllers[ControllerIndex.Right].Device.TriggerHapticPulse(60);
        if (snapState != null)
        {
            // Ignoring the snapstate position, since it is possible we have a connection with a different connection point of the same type

            // Overriding the ghost rotation with preservation of the connection point as rotation point
            var oldPos = ghostCPTransform.position;
            ghostCPTransform.SetParent(GhostObject.transform);
            GhostObject.transform.localRotation = snapState.localRot;
            ghostCPTransform.SetParent(null);
            GhostObject.transform.SetParent(ghostCPTransform);
            ghostCPTransform.position = oldPos;
            GhostObject.transform.SetParent(foreignCP.transform.parent);
        }

        Destroy(ghostCP);

        ValidateConnection(ownCP, foreignCP, ct, snapState);

        _lastGhostPosition = GhostObject.transform.position;
    }

    public void PreviewGhostConnected(ConnectionPoint foreignCP, ConnectionPoint ownCP)
    {
        if (GhostObject.activeInHierarchy == true)
            return;

        SetGhostActive();
        var ghostRenderers = GhostObject.GetComponentsInChildren<Renderer>();
        foreach (var ghostRenderer in ghostRenderers)
        {
            ghostRenderer.material = GuideUtils.PreviewGhostMat;
        }
        var ownParent = ownCP.transform.parent;
        ownCP.transform.SetParent(null, true);
        // Helper object that defines the new temporary pivot point of the ghost object
        var ghostCP = new GameObject("ghostCP");
        var ghostCPTransform = ghostCP.transform;
        ghostCPTransform.position = ownCP.transform.position;
        ghostCPTransform.rotation = ownCP.transform.rotation;
        ownCP.transform.SetParent(ownParent, true);
        // Reset Ghost position & orientation
        GhostObject.transform.position = transform.position;
        GhostObject.transform.rotation = transform.rotation;
        // Set the ghost parent to the connection point
        GhostObject.transform.SetParent(ghostCPTransform);

        //ghostCPTransform.rotation = GetClosestRotation(foreignCP, ownCP);
        ghostCPTransform.rotation = foreignCP.transform.rotation;
        ghostCPTransform.position = foreignCP.transform.position;
        GhostObject.transform.SetParent(foreignCP.transform.parent);
        DestroyImmediate(ghostCP);
        RotationTooltip.Activate(false);
        TooltipManager.ClearParent(GhostObject);
    }

    public Quaternion GetClosestRotation(ConnectionPoint foreignCP, ConnectionPoint ownCP)
    {
        // Find the translation and rotation difference between the connection points
        var ct = ConnectionManager.GetType(foreignCP.TypeID);

        var foreignRotation = foreignCP.transform.rotation;
        var currentRotation = ownCP.transform.rotation;

        // Helper object that defines the new temporary pivot point of the ghost object
        var ghostCP = new GameObject();
        var ghostCPTransform = ghostCP.transform;
        ghostCPTransform.position = ownCP.transform.position;
        ghostCPTransform.rotation = currentRotation;

        var relative = Quaternion.Inverse(foreignRotation) * currentRotation;
        var relativeEuler = relative.eulerAngles;
        var flipped = Quaternion.Angle(foreignRotation, currentRotation) > 90;

        var postRotation = new Vector3();

        // Tooltip
        RotationTooltip.LockOn(foreignCP);
        RotationTooltip.Activate(true);
        RotationTooltip.ActivateX(false);
        RotationTooltip.ActivateY(false);
        RotationTooltip.ActivateZ(false);

        // X
        if (ct.XRotationType == RotationType.Step)
        {
            // Find the closest allowed X angle
            postRotation.x = GetClosestAngle(relativeEuler.x, ct.xRotationBounds, ct.StepX, flipped);

            RotationTooltip.ActivateX(true);
            RotationTooltip.SetXBounds(ct.xRotationBounds.x, ct.xRotationBounds.y);

            // TODO Check back later
            #region NONVR
            // Scrolling for non VR
            //if (ct.Scrolling == ScrollStep.ScrollX)
            //{
            //    postRotation.x += ConnectionManager.ScrollStep * ct.StepX;
            //}
            //
            //if (ct.SecundaryScrolling == ScrollStep.ScrollX)
            //{
            //    postRotation.x += ConnectionManager.ScrollStep * ct.StepX;
            //}
            #endregion
        }
        else if (ct.XRotationType == RotationType.Free)
        {
            postRotation.x = GetClosestAngle(relativeEuler.x, ct.xRotationBounds, flipped);
        }

        // Y
        if (ct.YRotationType == RotationType.Step)
        {
            // Find the closest allowed Y angle
            postRotation.y = GetClosestAngle(relativeEuler.y, ct.yRotationBounds, ct.StepY, flipped);

            RotationTooltip.ActivateY(true);
            RotationTooltip.SetYBounds(ct.yRotationBounds.x, ct.yRotationBounds.y);
        }
        else if (ct.YRotationType == RotationType.Free)
        {
            postRotation.y = GetClosestAngle(relativeEuler.y, ct.yRotationBounds, flipped);
        }

        // Z
        if (ct.ZRotationType == RotationType.Step)
        {
            // Find the closest allowed Z angle
            postRotation.z = GetClosestAngle(relativeEuler.z, ct.zRotationBounds, ct.StepZ, flipped);

            RotationTooltip.ActivateZ(true);
            RotationTooltip.SetZBounds(ct.zRotationBounds.x, ct.zRotationBounds.y);
        }
        else if (ct.ZRotationType == RotationType.Free)
        {
            postRotation.z = GetClosestAngle(relativeEuler.z, ct.zRotationBounds, flipped);
        }

        RotationTooltip.SetIndicators(postRotation);
        ghostCPTransform.rotation = foreignRotation;
        ghostCPTransform.Rotate(postRotation.x, postRotation.y, postRotation.z, Space.Self);
        var rotation = ghostCPTransform.rotation;
        Destroy(ghostCP);

        return rotation;
    }

    float GetClosestAngle(float delta, Vector2 bounds, bool flipped = false)
    {
        return GetClosestAngle(delta, bounds, -1, flipped);
    }

    float GetClosestAngle(float delta, Vector2 bounds, float step, bool flipped = false)
    {
        if (flipped)
        {
            delta = (delta * -1.0f) + 180;
        }

        if (delta < 0)
            delta = 360 + delta;

        var boundAngle = (delta + 180) % 360;
        if (boundAngle > bounds.y)
        {
            boundAngle = bounds.y;
        }
        else if (boundAngle < bounds.x)
        {
            boundAngle = bounds.x;
        }

        delta = (boundAngle + 180) % 360;

        if (step == -1)
            return delta;

        int steps = Mathf.RoundToInt(delta / step);
        return steps * step;
    }

    public void SetClosestRotation(ConnectionPoint foreignCP, ConnectionPoint ownCP)
    {
        var pieceParent = transform.parent;
        var rotation = GetClosestRotation(foreignCP, ownCP);
        ownCP.transform.SetParent(pieceParent);
        transform.SetParent(ownCP.transform);
        ownCP.transform.rotation = rotation;
        transform.SetParent(pieceParent);
        ownCP.transform.SetParent(transform);
        ViveHelper.Controllers[ControllerIndex.Left].Device.TriggerHapticPulse(200, Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger);
        ViveHelper.Controllers[ControllerIndex.Right].Device.TriggerHapticPulse(200, Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger);
    }

    bool ValidateConnection(ConnectionPoint ownCP, ConnectionPoint foreignCP, ConnectionType ct, AssemblyPieceLooseState snapState = null)
    {
        var lastValidationState = _validPlacement;
        // Check if the ghost is in an available position, only check if the ghost has moved
        // TODO what if rotation is important
        if (GhostObject.transform.position != _lastGhostPosition || _ghostStateChanged)
        {
            var ghostRenderers = GhostObject.GetComponentsInChildren<Renderer>();
            var workBenchMatch = ownCP.TypeID == -1;
            if (workBenchMatch)
            {
                _validPlacement = true;
                var material = snapState == null ? PickupController.GlobalGhostMaterial : PickupController.GlobalGhostSnapMaterial;
                foreach (var ghostRenderer in ghostRenderers)
                {
                    ghostRenderer.material = material;
                }
            }
            else
            {
                // Check sanity of connection
                //Lego hack
                var legoPiece = GetComponent<LegoPiece>();
                var saneConnection = legoPiece != null ? LegoHandler.CheckCollision(legoPiece) : true;//GhostSanityCheck();
                var crosswiseCheck = CrosswiseCheck(ownCP, foreignCP);
                var validDependencies = false;

                var validOrientation = true;
                if (ct.RequireOrientation)
                {
                    var validAxisOffset = 30;
                    var validXAxis = ct.XRotationType == RotationType.Locked ? true : Vector3.Angle(foreignCP.transform.right, ownCP.transform.right) < validAxisOffset || (ct.XSymmetry ? Vector3.Angle(-foreignCP.transform.right, ownCP.transform.right) < validAxisOffset : false);
                    var validYAxis = ct.YRotationType == RotationType.Locked ? true : Vector3.Angle(foreignCP.transform.up, ownCP.transform.up) < validAxisOffset || (ct.YSymmetry ? Vector3.Angle(-foreignCP.transform.up, ownCP.transform.up) < validAxisOffset : false);
                    var validZAxis = ct.ZRotationType == RotationType.Locked ? true : Vector3.Angle(foreignCP.transform.forward, ownCP.transform.forward) < validAxisOffset || (ct.ZSymmetry ? Vector3.Angle(-foreignCP.transform.forward, ownCP.transform.forward) < validAxisOffset : false);

                    validOrientation = ct.XRotationType == RotationType.Locked && ct.YRotationType == RotationType.Locked && ct.ZRotationType == RotationType.Locked ? Vector3.Angle(foreignCP.transform.forward, ownCP.transform.forward) < validAxisOffset : validXAxis && validYAxis && validZAxis;
                }
                //validOrientation = true; // !!!!QUICK DEMO FIX
                //var validOrientation = true;
                if (saneConnection)
                {
                    // Check dependencies of connection
                    // TODO: This check is also required for connections that can occur simulanously with this one
                    validDependencies = ownCP.CanConnect && foreignCP.CanConnect;
                    
                    if (validDependencies)
                    {
                        var material = snapState == null ? PickupController.GlobalGhostMaterial : PickupController.GlobalGhostSnapMaterial;
                        if (/*snapState != null && */!validOrientation)
                        {
                            material = PickupController.GlobalGhostMissingDependancyMaterial;
                            TooltipManager.AttachTooltip(TooltipType.Orientation, GhostObject);
                        }
                        else
                        {
                            if (!crosswiseCheck)
                            {
                                material = PickupController.GlobalGhostMissingDependancyMaterial;
                                TooltipManager.AttachTooltip(TooltipType.Crosswise, GhostObject);
                            }
                            else
                            {
                                TooltipManager.ClearParent(GhostObject);
                            }
                        }
                        foreach (var ghostRenderer in ghostRenderers)
                        {
                            ghostRenderer.material = material;
                        }
                    }
                    else
                    {
                        foreach (var ghostRenderer in ghostRenderers)
                        {
                            ghostRenderer.material = PickupController.GlobalGhostMissingDependancyMaterial;
                        }

                        TooltipManager.AttachTooltip(TooltipType.Dependency, GhostObject);
                    }
                }
                else
                {
                    foreach (var ghostRenderer in ghostRenderers)
                    {
                        ghostRenderer.material = PickupController.GlobalGhostOccupiedMaterial;
                    }

                    TooltipManager.AttachTooltip(TooltipType.Occupied, GhostObject);
                }

                _validPlacement = saneConnection && validDependencies;
                if (/*snapState != null && */!validOrientation || !crosswiseCheck)
                    _validPlacement = false;
            }
        }

        foreignCP.SetIndicateCorrect(_validPlacement);
        GuideUtils.SetTrailCorrect(_validPlacement);

        if(OnEnteredValid != null && !lastValidationState && _validPlacement)
            OnEnteredValid();

        if (OnLeftValid != null && lastValidationState && !_validPlacement)
            OnLeftValid();

        return _validPlacement;
    }

    // !!! Extremely unperformant, needs better solution
    bool GhostSanityCheck()
    {
        // Check the connected points with the parent(group) 
        // Sanity check fails if a match contains an occupied point

        var returnValue = true;
        // Set the transform temporary to ghost
        var oldPos = transform.position;
        var oldRot = transform.rotation;
        transform.position = GhostObject.transform.position;
        transform.rotation = GhostObject.transform.rotation;

        var ghostParent = GhostObject.transform.parent.GetComponent<AssemblyPiece>();
        var occupiedPoints = ghostParent.GetConnectedPoints(true);

        //Debug.Log("ocps: " + occupiedPoints.Count);
        foreach (var ocp in occupiedPoints)
        {
            if (!returnValue)
                break;

            foreach (var cp in _connectionPoints)
            {
                var match = new ConnectionMatch(cp, ocp);
                if (match.ConnectionCheck())
                {
                    returnValue = false;
                    break;
                }
            }
        }

        transform.position = oldPos;
        transform.rotation = oldRot;

        return returnValue;
    }

    bool CrosswiseCheck(ConnectionPoint ownCP, ConnectionPoint foreignCP, bool indicateOptions = false)
    {
        if (!ownCP.Crosswise && !foreignCP.Crosswise)
            return true;

        if (foreignCP.Crosswise)
        {
            var crosswiseCPs = foreignCP.ParentPiece.ConnectionPointsUnordered.Where(cp => cp.TypeID == foreignCP.TypeID && cp.Crosswise).ToList();
            var connectedCrosswiseCPs = crosswiseCPs.Where(cp => cp.IsConnected).ToList();
            var n = crosswiseCPs.Count;
            if (connectedCrosswiseCPs.Count == 0)
            {
                for(int i = 0; i < crosswiseCPs.Count; ++i)
                {
                    crosswiseCPs[i].Indicate();
                }

                return true;
            }
            else
            {
                for (int i = 0; i < crosswiseCPs.Count; ++i)
                {
                    crosswiseCPs[i].Indicate(false);
                }
            }

            if (connectedCrosswiseCPs.Contains(foreignCP))
                return false;

            var even = connectedCrosswiseCPs.Count % 2 == 0;
            if (even)
            {
                // Find largest spacing between connected cps and pick middle cp in this space
                var largestSpaceCount = 0;
                List<int> nextIds = new List<int>();
                for(int i = 0; i < connectedCrosswiseCPs.Count; ++i)
                {
                    var id = connectedCrosswiseCPs[i].ID;
                    var nextId = i + 1 == connectedCrosswiseCPs.Count ? connectedCrosswiseCPs[0].ID : connectedCrosswiseCPs[i + 1].ID;
                    var spaceCPCount = Mathf.Abs(nextId - id) - 1;
                    // if spacing is bigger or equal to 6 take n - (spacing + 2)
                    if(spaceCPCount >= n / 2)
                    {
                        spaceCPCount = n - (spaceCPCount + 2);
                    }

                    if(spaceCPCount > largestSpaceCount)
                    {
                        nextIds.Clear();
                        largestSpaceCount = spaceCPCount;
                        if(spaceCPCount == 1)
                        {
                            nextIds.Add((id + 1) % n);
                        }
                        else if(spaceCPCount % 2 == 0)
                        {
                            var firstNextId = id + (int)Mathf.Floor(spaceCPCount / 2.0f) % n;
                            var secondNextId = (firstNextId + 1) % n;
                            nextIds.Add(firstNextId);
                            nextIds.Add(secondNextId);
                        }
                        else
                        {
                            nextId = id + (int)Mathf.Ceil(spaceCPCount / 2.0f) % n;
                            nextIds.Add(nextId);
                        }
                    }
                    if (spaceCPCount == largestSpaceCount)
                    {
                        if (spaceCPCount == 1)
                        {
                            nextIds.Add((id + 1) % n);
                        }
                        else if (spaceCPCount % 2 == 0)
                        {
                            var firstNextId = id + (int)Mathf.Floor(spaceCPCount / 2.0f) % n;
                            var secondNextId = (firstNextId + 1) % n;
                            nextIds.Add(firstNextId);
                            nextIds.Add(secondNextId);
                        }
                        else
                        {
                            nextId = id + (int)Mathf.Ceil(spaceCPCount / 2.0f) % n;
                            nextIds.Add(nextId);
                        }
                    }
                }

                for(int i = 0; i < crosswiseCPs.Count; ++i)
                {
                    if (nextIds.Contains(crosswiseCPs[i].ID))
                    {
                        crosswiseCPs[i].Indicate(true);
                    }
                    else
                    {
                        crosswiseCPs[i].Indicate(false);
                    }
                }

                if (nextIds.Contains(foreignCP.ID))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                for(int i = 0; i < connectedCrosswiseCPs.Count; ++i)
                {
                    var matchCP = crosswiseCPs.FirstOrDefault(cp => cp.ID == (connectedCrosswiseCPs[i].ID + n / 2) % n);
                    if (matchCP != null && !matchCP.IsConnected)
                    {
                        matchCP.Indicate();
                        if (matchCP == foreignCP)
                            return true;
                        else
                            return false;
                    }
                }
            }
        }

        return true;
    }

    public void ResetGhost(bool keepActive = false)
    {
        GhostObject.transform.localPosition = Vector3.zero;
        GhostObject.transform.localEulerAngles = Vector3.zero;
        //GhostObject.transform.position = transform.position;
        //GhostObject.transform.rotation = transform.rotation;

        GhostObject.SetActive(keepActive);
    }

    public List<ConnectionPoint> GetConnectedPoints(bool cascade)
    {
        var connectedPoints = new List<ConnectionPoint>(ConnectedPoints);

        if (cascade)
        {
            var connectedPieces = FindAllConnectedPieces(this, false);
            foreach (var piece in connectedPieces)
            {
                connectedPoints.AddRange(piece.ConnectedPoints);
            }
        }

        return connectedPoints;
    }

    public void AttachPiece(AssemblyPiece piece)
    {
        _connectedPieces.Add(piece);

        if (OnAttach != null)
            OnAttach(piece);
    }

    void AttachForeignConnectionPoints()
    {
        // This function tries to make connections based on proximity matches with connection points

        var connectedPieces = FindAllConnectedPieces(RootPiece);

        for (int i = 0; i < _connectionPoints.Length; ++i)
        {
            if (_connectionPoints[i].IsConnected)
                continue;

            var match = ConnectionManager.FindConnectionMatch(_connectionPoints[i], connectedPieces);
            if (match != null)
                match.Connect();
        }
    }

    public void Detach()
    {
        // Unlock from parent
        transform.SetParent(null);
        _parentPiece = null;

        // Update connected points for both self and previously connected pieces
        DetachForeignConnectionPoints(true);

        // Reset GhostObject to proper owner
        GhostObject.transform.SetParent(transform);
        GhostObject.SetActive(false);

        if (IsPickable)
            PickableRootPiece = this;

        RootPiece = this;
        SetChildrensRoot();

        Unlock();
        if (OnDetach != null)
            OnDetach();
    }

    void DetachForeignConnectionPoints(bool cascade = false)
    {
        // This function disconnects all the connection points that are part of the AssemblyPiece 
        // or its children and currently have a connection with an object that is not a child of
        // the common rootpiece (the picked up or detached piece)
        // * When cascading it is important to only trigger the direct children to prevent possible endless loops

        var connectedChildren = new List<AssemblyPiece>();
        for (int i = 0; i < _connectionPoints.Length; ++i)
        {
            if (!_connectionPoints[i].IsConnected)
                continue;

            if (_connectionPoints[i].ConnectedCP.RootPiece != RootPiece)
            {
                _connectionPoints[i].Disconnect();
            }
            else if (_connectionPoints[i].ConnectedCP.ParentPiece.ParentPiece == this) // *
            {
                if (!connectedChildren.Contains(_connectionPoints[i].ConnectedCP.ParentPiece))
                {
                    connectedChildren.Add(_connectionPoints[i].ConnectedCP.ParentPiece);
                }
            }
        }

        if (cascade)
        {
            for (int i = 0; i < connectedChildren.Count; ++i)
            {
                connectedChildren[i].DetachForeignConnectionPoints(cascade);
            }
        }
    }

    void DetachConnectionPoints()
    {
        // This function detaches all the connection points of the assembly piece

        for (int i = 0; i < _connectionPoints.Length; ++i)
        {
            _connectionPoints[i].Disconnect();
        }
    }

    void SetChildrensRoot()
    {
        var children = AssemblyPiece.FindAllChildPieces(this);
        for (int i = 0; i < children.Count; i++)
        {
            children[i].RootPiece = this;
            if (IsPickable)
                children[i].PickableRootPiece = this;
        }
    }

    public void AutoConnectOverTime(ConnectionPoint ownCP, ConnectionPoint foreignCP)
    {
        // Making sure the connection stays valid during the animation
        ownCP.Reserve();
        foreignCP.Reserve();
        var speed = 3.0f;
        StartCoroutine(MoveAndConnect(ownCP, foreignCP, speed));
    }

    IEnumerator MoveAndConnect(ConnectionPoint ownCP, ConnectionPoint foreignCP, float speed = 3.0f)
    {
        //Taking advantage of connection point parenting to get piece to its target position and rotation
        SetColliders(false);
        var rb = GetComponent<Rigidbody>();
        if(rb != null)
            rb.isKinematic = true;

        ownCP.transform.SetParent(foreignCP.transform);
        transform.SetParent(ownCP.transform);

        Vector3 startPos = ownCP.transform.localPosition;
        Quaternion startRot = ownCP.transform.localRotation;

        float t = 0f;
        while (t < 1f)
        {
            t = Mathf.Clamp01(t + Time.deltaTime * speed);

            ownCP.transform.localPosition = EasingFunctions.Ease(EasingFunctions.Type.In, t, startPos, Vector3.zero);
            ownCP.transform.localRotation = EasingFunctions.Ease(EasingFunctions.Type.In, t, startRot, Quaternion.identity);
            yield return null;
        }
        
        transform.SetParent(foreignCP.ParentPiece.transform);
        _parentPiece = foreignCP.ParentPiece;
        ownCP.transform.SetParent(transform);
        foreignCP.ParentPiece.AttachPiece(this);
        AttachPiece(foreignCP.ParentPiece);
        GhostObject.SetActive(false);
        
        if (rb)
            Destroy(rb);
        SetColliders(true);
        SetCollidersTrigger(false);
        
        ownCP.Connect(foreignCP);
        AttachForeignConnectionPoints();
        
        RootPiece = foreignCP.ParentPiece.RootPiece;
        if (RootPiece.IsPickable)
            PickableRootPiece = RootPiece;
        
        GhostObject.SetActive(true);
        if(GhostObject != null && foreignCP.ParentPiece != null && foreignCP.ParentPiece.GhostObject != null)
            GhostObject.transform.SetParent(foreignCP.ParentPiece.GhostObject.transform);
        
        ResetGhost();
        
        if (ConnectSound != null)
            AudioSource.PlayClipAtPoint(ConnectSound, transform.position, 1);

        if (OnAutoConnected != null)
            OnAutoConnected();
    }

    const float __updateSpeed = 5f;

    IEnumerator UpdateAttach(AssemblyPiece parentAP, Vector3 position, Quaternion rotation, Action finished)
    {
        var Colliders = GetComponentsInChildren<Collider>().Where(x => x.gameObject.activeInHierarchy && x.enabled);
        var myRigidBody = GetComponent<Rigidbody>();
        foreach (var c in Colliders)
        {
            c.enabled = false;
        }

        if (myRigidBody)
            myRigidBody.isKinematic = true;

        transform.SetParent(parentAP.transform);
        _parentPiece = parentAP;
        Vector3 startPos = transform.localPosition;
        Quaternion startRot = transform.localRotation;

        float t = 0f;
        while (t < 1f)
        {
            t = Mathf.Clamp01(t + Time.deltaTime * __updateSpeed);

            transform.localPosition = EasingFunctions.Ease(EasingFunctions.Type.In, t, startPos, position);
            transform.localRotation = EasingFunctions.Ease(EasingFunctions.Type.In, t, startRot, rotation);
            yield return null;
        }

        transform.localPosition = position;
        transform.localRotation = rotation;
        foreach (var c in Colliders)
        {
            c.enabled = true;
        }

        if (myRigidBody)
            Destroy(myRigidBody);
        SetColliders(true);
        SetCollidersTrigger(false);


        transform.localPosition = position;
        transform.localRotation = rotation;
        ResetGhost();

        GhostObject.transform.SetParent(transform);
        parentAP.AttachPiece(this);
        AttachPiece(parentAP);
        GhostObject.SetActive(false);
        //UpdateConnectionPointOccupation(false);
        //parentAP.UpdateConnectionPointOccupation(false);
        AttachForeignConnectionPoints();

        RootPiece = parentAP.RootPiece;
        if (RootPiece.IsPickable)
            PickableRootPiece = RootPiece;


        // The Ghostobject should act as child of the parent ghostobject's child
        GhostObject.SetActive(true);
        GhostObject.transform.SetParent(parentAP.GhostObject.transform);

        //GhostObject.transform.localPosition = transform.localPosition;
        //GhostObject.transform.localRotation = transform.localRotation;
        ResetGhost();

        if (ConnectSound != null)
            AudioSource.PlayClipAtPoint(ConnectSound, transform.position, 1);

        finished();
        //RootPiece.GhostObject.SetActive(false);
    }

    IEnumerator UpdateChangeState(AssemblyPieceState oldState, AssemblyPieceState newState, Action finished)
    {
        var oldroot = RootPiece;
        if (oldState.Parent != null)
        {
            Detach();
        }

        var Colliders = GetComponentsInChildren<Collider>().Where(x => x.gameObject.activeInHierarchy && x.enabled);
        var myRigidBody = GetComponent<Rigidbody>();
        if (myRigidBody)
            myRigidBody.isKinematic = true;

        if (newState.Parent != null)
            transform.SetParent(newState.Parent.transform);
        else
            transform.SetParent(null);

        Vector3 startPos = transform.localPosition;
        Quaternion startRot = transform.localRotation;

        float t = 0f;
        while (t < 1f)
        {
            t = Mathf.Clamp01(t + Time.deltaTime * __updateSpeed);

            transform.localPosition = EasingFunctions.Ease(EasingFunctions.Type.In, t, startPos, newState.localPos);
            transform.localRotation = EasingFunctions.Ease(EasingFunctions.Type.In, t, startRot, newState.localRot);
            yield return null;
        }

        transform.localPosition = newState.localPos;
        transform.localRotation = newState.localRot;
        ResetGhost();
        foreach (var c in Colliders)
        {
            c.enabled = true;
        }

        if (newState.Parent)
        {
            if (myRigidBody)
                Destroy(myRigidBody);
            SetColliders(true);
            SetCollidersTrigger(false);

            GhostObject.transform.SetParent(transform);
            newState.Parent.AttachPiece(this);
            AttachPiece(newState.Parent);
            GhostObject.SetActive(false);
            //UpdateConnectionPointOccupation(false);
            //newState.Parent.UpdateConnectionPointOccupation(false);
            AttachForeignConnectionPoints();

            RootPiece = newState.Parent.RootPiece;
            if (RootPiece.IsPickable)
                PickableRootPiece = RootPiece;


            // The Ghostobject should act as child of the parent ghostobject's child
            GhostObject.SetActive(true);
            if (newState.Parent.GhostObject != null)
            {
                GhostObject.transform.SetParent(newState.Parent.GhostObject.transform);
                //GhostObject.transform.localPosition = transform.localPosition;
                //GhostObject.transform.localRotation = transform.localRotation;
                ResetGhost();
            }
            if (ConnectSound != null)
                AudioSource.PlayClipAtPoint(ConnectSound, transform.position, 1);
        }
        else
        {
            //if (myRigidBody)
            //    myRigidBody.isKinematic = false;
            //DropToGround(Vector3.zero);
            if (oldroot.GetComponent<Rigidbody>())
            {
                var rbody = oldroot.GetComponent<Rigidbody>();
                bool prevKinematic = rbody.isKinematic;
                rbody.isKinematic = true;
                rbody.isKinematic = prevKinematic;
            }
        }
        Vector3 pos = transform.position;
        Vector3 firstPos = _firstPosition;
        if ((pos - firstPos).sqrMagnitude < 0.1f)
        {
            if (myRigidBody) myRigidBody.isKinematic = true;
            OnPutBack.Invoke();
            _firstPickup = false;
        }

        finished();
    }

    //IEnumerator UpdateUndoAttach(Vector3 pos, Quaternion rot, Action finished)
    //{
    //    var oldroot = RootPiece;

    //    Detach();




    //    foreach (var c in Colliders)
    //    {
    //        c.enabled = false;
    //    }

    //    if (myRigidBody)
    //        myRigidBody.isKinematic = true;


    //    Vector3 startPos = transform.position;
    //    Quaternion startRot = transform.rotation;

    //    float t = 0f;
    //    while (t < 1f)
    //    {
    //        t = Mathf.Clamp01(t + Time.deltaTime * __updateSpeed);

    //        transform.position = EasingFunctions.Ease(EasingFunctions.Type.In, t, startPos, pos);
    //        transform.rotation = EasingFunctions.Ease(EasingFunctions.Type.In, t, startRot, rot);
    //        yield return null;
    //    }

    //    transform.position = pos;
    //    transform.rotation = rot;
    //    foreach (var c in Colliders)
    //    {
    //        c.enabled = true;
    //    }
    //    if (myRigidBody)
    //        GetComponent<Rigidbody>().isKinematic = false;
    //    DropToGround(Vector3.zero);
    //    if (oldroot.GetComponent<Rigidbody>())
    //    {
    //        var rbody = oldroot.GetComponent<Rigidbody>();
    //        bool prevKinematic = rbody.isKinematic;
    //        rbody.isKinematic = true;
    //        rbody.isKinematic = prevKinematic;
    //    }

    //    finished();
    //}
    public void ChangeState(AssemblyPieceState oldState, AssemblyPieceState newState, Action finished)
    {
        if (_holdingController != null)
        {
            _holdingController.ForceReleaseObject();
        }
        StartCoroutine(UpdateChangeState(oldState, newState, finished));
    }
    //public void UndoAttach(Vector3 position, Quaternion rotation, Action finished)
    //{
    //    if (_holdingController != null)
    //    {
    //        _holdingController.ForceReleaseObject();
    //    }

    //    StartCoroutine(UpdateUndoAttach(position, rotation, finished));
    //}

    //public void Attach(AssemblyPiece parentAP, Vector3 position, Quaternion rotation, Action finished)
    //{
    //    if (_holdingController != null)
    //    {
    //        _holdingController.ForceReleaseObject();
    //    }

    //    StartCoroutine(UpdateAttach(parentAP, position, rotation, finished));

    //}

    public void DropAsGhost(ref NEAT_GhostConnection ghostConnectionArg, ConnectionMatch cMatch = null)
    {
        if (cMatch != null && cMatch.A.TypeID < 0)
        {
            // Workbench drop

            Workbench workbench = null;
            if (cMatch.A.ParentPiece == this)
            {
                workbench = cMatch.B.transform.parent.GetComponent<Workbench>();
            }
            else
            {
                workbench = cMatch.A.transform.parent.GetComponent<Workbench>();
            }

            DropAsGhost(workbench, GhostObject.transform.localPosition, GhostObject.transform.localRotation);
            cMatch.Connect();
        }
        else
        {
            // Assembly piece connection

            var parentAP = GhostObject.transform.parent.GetComponent<AssemblyPiece>();
            ghostConnectionArg.SetParentNuid(parentAP);
            ghostConnectionArg.SetPosition(GhostObject.transform.localPosition);
            ghostConnectionArg.SetRotation(GhostObject.transform.localRotation);

            DropAsGhost(parentAP, GhostObject.transform.localPosition, GhostObject.transform.localRotation);
            cMatch.Connect();
        }

        _pickable.Drop();
        _bPickedUp = false;
        RecordStateChanged();
    }


    public void DropAsGhost(AssemblyPiece parentAP, Vector3 position, Quaternion rotation)
    {
        _holdingController = null;
        Destroy(GetComponent<Rigidbody>());
        SetColliders(true);
        SetCollidersTrigger(false);

        // Set the assembly piece under it's new parent
        transform.SetParent(parentAP.transform);
        _parentPiece = parentAP;
        transform.localPosition = position;
        transform.localRotation = rotation;

        GhostObject.transform.SetParent(transform);
        parentAP.AttachPiece(this);
        AttachPiece(parentAP);
        GhostObject.SetActive(false);
        AttachForeignConnectionPoints();
        RootPiece = parentAP.RootPiece;
        //parentAP.GhostObject.SetActive(false);

        if (RootPiece.IsPickable)
            PickableRootPiece = RootPiece;
        // The Ghostobject should act as child of the parent ghostobject's child
        GhostObject.SetActive(true);
        GhostObject.transform.SetParent(parentAP.GhostObject.transform);
        GhostObject.transform.localPosition = transform.localPosition;
        GhostObject.transform.localRotation = transform.localRotation;
        GhostObject.SetActive(false);

        if (ConnectSound != null)
            AudioSource.PlayClipAtPoint(ConnectSound, transform.position, 1);

        if (VR_Settings.Get("Lock Connected Pieces"))
            Lock();

        _bPickedUp = false;
    }

    public void DropAsGhost(Workbench workbench, Vector3 position, Quaternion rotation)
    {
        _holdingController = null;
        Destroy(GetComponent<Rigidbody>());
        SetColliders(true);
        SetCollidersTrigger(false);

        transform.SetParent(workbench.transform);
        transform.localPosition = position;
        transform.localRotation = rotation;

        GhostObject.transform.SetParent(transform);
        workbench.PlacePiece(this);
        //AttachPiece(parentAP);
        GhostObject.SetActive(false);
        //UpdateConnectionPointOccupation(false);
        //parentAP.UpdateConnectionPointOccupation(false);
        //RootPiece = parentAP.RootPiece;

        if (RootPiece.IsPickable)
            PickableRootPiece = RootPiece;
        // The Ghostobject should act as child of the parent ghostobject's child
        GhostObject.SetActive(true);
        GhostObject.transform.SetParent(transform);
        GhostObject.transform.localPosition = transform.localPosition;
        GhostObject.transform.localRotation = transform.localRotation;
        GhostObject.SetActive(false);

        //AudioSource.PlayClipAtPoint(ConnectSound, transform.position, 1);

        _bPickedUp = false;
    }

    public void SnapToPrePose(ConnectionPoint ownCP, ConnectionPoint foreignCP)
    {
        SetGhostConnected(new ConnectionMatch(ownCP, foreignCP));
        transform.position = GhostObject.transform.position;
        transform.rotation = GhostObject.transform.rotation;
        transform.position += transform.forward * ownCP.PrePoseOffset.z;
        transform.position += transform.up * ownCP.PrePoseOffset.y;
        transform.position += transform.right * ownCP.PrePoseOffset.x;
        transform.SetParent(foreignCP.ParentPiece.transform);
        SetGhostActive(false);
        _prePoseCP = ownCP;
        _prePoseOtherCP = foreignCP;
        _bOnPrePose = true;

        if (OnEnteredPrePose != null)
            OnEnteredPrePose();
    }

    public void Place()
    {
        _pickable.Controller.DropAPInPlace(this, new ConnectionMatch(_prePoseCP, _prePoseOtherCP));
    }

    public void UnSnapFromPrePose()
    {
        _bOnPrePose = false;
    }

    //[Obsolete]
    //void ClearConnectedPoints()
    //{
    //    for (int i = 0; i < _connectedPoints.Count; ++i)
    //    {
    //        if (_connectedPoints[i].IsConnected && _connectedPoints[i].ConnectedCP.RootPiece != RootPiece)
    //        {
    //            _connectedPoints[i].ConnectedCP.Disconnect();
    //            _connectedPoints[i].Disconnect();
    //        }
    //    }
    //    //foreach ( var p in _connectedPoints )
    //    //{
    //    //    p.Taken = false;
    //    //}
    //    _connectedPoints.Clear();
    //}

    //[Obsolete]
    //public void UpdateConnectionPointOccupation(bool cascade = false)
    //{
    //    var connectedPieces = FindAllConnectedPieces(this, true);
    //    //Debug.Log("updating connectedPoints: " + connectedPieces.Count);
    //    ClearConnectedPoints();
    //    foreach (var piece in connectedPieces)
    //    {
    //        _connectedPoints.AddRange(FindOccupiedConnectionPoints(piece));
    //
    //        if (cascade && piece != this)
    //        {
    //            piece.UpdateConnectionPointOccupation();
    //        }
    //    }
    //
    //    //SetConnectionPointsAsTaken();
    //}

    public List<ConnectionPoint> FindOccupiedConnectionPoints(AssemblyPiece piece)
    {
        List<ConnectionPoint> connectedPoints = new List<ConnectionPoint>();
        var foreignCPs = piece._connectionPoints;
        ConnectionMatch match;
        for (int i = 0; i < _connectionPoints.Length; ++i)
        {
            for (int j = 0; j < foreignCPs.Length; ++j)
            {
                match = new ConnectionMatch(_connectionPoints[i], foreignCPs[j]);
                if (match.ConnectionCheck())
                {
                    connectedPoints.Add(_connectionPoints[i]);
                    match.A.Connect(match.B);
                    match.B.Connect(match.A);
                }
            }
        }

        return connectedPoints;
    }

    public void RecordStateChanged()
    {
        AssemblyPieceState newState = GetCurrentAPState();
        if (newState == _lastAPState) return;
        ObjectStateRecorder.Instance.AddAction(new RecordedConnection(this, _lastAPState, newState));
        _lastAPState = newState;
    }

    public void DropToGround(Vector3 force)
    {
        _holdingController = null;
        GhostObject.SetActive(false);
        SetColliders(true);
        transform.SetParent(null);
        _parentPiece = null;
        var rb = GetComponent<Rigidbody>();
        if (!rb)
        {
            rb = gameObject.AddComponent<Rigidbody>();
        }

        rb.isKinematic = false;
        SetCollidersTrigger(false);
        rb.AddForce(force);

        _pickable.Drop();
        _bPickedUp = false;
    }

    public void SetCollidersTrigger(bool active)
    {
        var colliders = GetComponentsInChildren<Collider>();
        foreach (var col in colliders)
        {
            if (!col.GetComponent<ConnectionPoint>())
                col.isTrigger = active;
        }
    }

    public void SetColliders(bool active)
    {
        var colliders = GetComponentsInChildren<Collider>();
        foreach (var col in colliders)
        {
            if (!col.GetComponent<ConnectionPoint>() && !col.GetComponent<PaintSurface>())
                col.enabled = active;
        }
    }

    [Obsolete]
    public static List<AssemblyPiece> FindAllConnectedPiecesOld(AssemblyPiece piece, bool includeSelf = true)
    {
        var pieces = new List<AssemblyPiece>();

        var rootPiece = FindRootPiece(piece);
        pieces = FindAllChildPieces(rootPiece, true);
        if (!includeSelf)
            pieces.Remove(piece);

        return pieces;
    }

    public static List<AssemblyPiece> FindAllConnectedPieces(AssemblyPiece piece, bool includeSelf = true)
    {
        var pieces = new List<AssemblyPiece>();
        if (piece.RootPiece != piece)
        {
            pieces = FindAllConnectedPieces(piece.RootPiece);
            //if (!includeSelf)
            //    pieces.Remove(piece);

            return pieces;
        }

        var connectedChildren = FindConnectedPieces(piece);
        pieces.AddRange(connectedChildren);

        //for(int i = 0; i < connectedChildren.Count; ++i)
        //{
        //    pieces.AddRange(FindConnectedPieces(connectedChildren[i], true));
        //}

        if (!includeSelf && pieces.Contains(piece))
            pieces.Remove(piece);

        if (includeSelf && !pieces.Contains(piece))
            pieces.Add(piece);

        return pieces;
    }

    public static List<AssemblyPiece> FindConnectedPieces(AssemblyPiece piece, bool childrenOnly = true, bool cascade = true)
    {
        var pieces = new List<AssemblyPiece>();
        for (int i = 0; i < piece.ConnectionPointsUnordered.Length; ++i)
        {
            if (!piece.ConnectionPointsUnordered[i].IsConnected)
                continue;

            if (piece.ConnectionPointsUnordered[i].ConnectedCP.RootPiece == piece.RootPiece &&
               !pieces.Contains(piece.ConnectionPointsUnordered[i].ConnectedCP.ParentPiece))
            {
                if (childrenOnly && piece.ConnectionPointsUnordered[i].ConnectedCP.ParentPiece.ParentPiece != piece)
                    continue;

                pieces.Add(piece.ConnectionPointsUnordered[i].ConnectedCP.ParentPiece);
            }
        }

        if (cascade)
        {
            for (int i = 0; i < pieces.Count; ++i)
            {
                pieces.AddRange(FindConnectedPieces(pieces[i]));
            }
        }

        return pieces;
    }

    public bool IsConnectedTo(AssemblyPiece other)
    {
        return Array.Exists(ConnectionPointsUnordered, cp => cp.IsConnected && cp.ConnectedCP.ParentPiece == other);
    }

    public static bool Connected(AssemblyPiece a, AssemblyPiece b)
    {
        return a.IsConnectedTo(b);
    }

    public static AssemblyPiece FindRootPiece(AssemblyPiece piece)
    {
        var parentTransform = piece.transform.parent;
        if (parentTransform)
        {
            var parentPiece = parentTransform.GetComponent<AssemblyPiece>();
            if (parentPiece != null)
                return AssemblyPiece.FindRootPiece(parentPiece);
        }

        return piece;
    }

    public static List<AssemblyPiece> FindAllChildPieces(AssemblyPiece piece, bool includeSelf = false)
    {
        List<AssemblyPiece> pieces = new List<AssemblyPiece>();

        //foreach (Transform child in piece.transform)
        //{
        //    if (child.GetComponent<AssemblyPiece>() != null)
        //    {
        //        pieces.Add(child.GetComponent<AssemblyPiece>());
        //    }
        //}
        pieces = piece.gameObject.GetComponentsInChildren<AssemblyPiece>().ToList();
        if (!includeSelf)
            pieces.Remove(piece);

        return pieces;
    }

    public void SetMaterial(Material material, bool includeConnected = true)
    {
        //if (includeConnected)
        //{
        //    var connectedPieces = FindAllConnectedPieces(this, false);
        //    foreach (var piece in connectedPieces)
        //    {
        //        piece.SetMaterial(material, false);
        //    }
        //}

        //foreach (var mr in _meshMaterials)
        //{
        //    mr.Value.Add(material);
        //}

        //UpdateMaterials();
    }

    public void UnsetMaterial(Material material, bool includeConnected = true)
    {
        //if (includeConnected)
        //{
        //    var connectedPieces = FindAllConnectedPieces(this, false);
        //    foreach (var piece in connectedPieces)
        //    {
        //        piece.UnsetMaterial(material, false);
        //    }
        //}

        //foreach (var mr in _meshMaterials)
        //{
        //    mr.Value.Remove(material);
        //}

        //UpdateMaterials();
    }

    void UpdateMaterials()
    {
        //foreach (var mr in _meshMaterials)
        //{
        //    var baseColor = mr.Key.material.color;
        //    mr.Key.material = mr.Value.Last();
        //    mr.Key.material.color = baseColor;
        //}
    }

    public void SetOutlined(bool outlined, bool includeConnected)
    {
        if (outlined)
        {
            SetMaterial(PickupController.GlobalOutlineMaterial, includeConnected);
        }
        else
        {
            UnsetMaterial(PickupController.GlobalOutlineMaterial, includeConnected);
        }
    }

    public ConnectionMatch FindClosestConnectionMatch(ConnectionPoint foreignCP)
    {
        var smallestDistance = PickupController.MaxConnectionDistance;
        ConnectionPoint matchCP = null;

        foreach (var cp in _connectionPoints)
        {
            // Check compatibility
            if (!ConnectionManager.CheckPair(foreignCP, cp))
                continue;

            // Check Distance
            var distance = Vector3.Distance(cp.transform.position, foreignCP.transform.position);
            if (distance < smallestDistance)
            {
                smallestDistance = distance;
                matchCP = cp;
            }
        }

        ConnectionMatch cm = new ConnectionMatch(foreignCP, matchCP);
        cm.Distance = smallestDistance;
        if (cm.A == null || cm.B == null)
            cm = null;

        return cm;
    }

    public ConnectionMatch FindClosestConnection(AssemblyPiece foreignPiece)
    {
        var smallestDistance = PickupController.MaxConnectionDistance;
        ConnectionMatch cMatch = null;

        foreach (var cp in _connectionPoints)
        {
            var cm = foreignPiece.FindClosestConnectionMatch(cp);

            if (cm == null)
                continue;

            // Check Distance
            if (cm.Distance < smallestDistance)
            {
                smallestDistance = cm.Distance;
                cMatch = cm;
            }
        }

        return cMatch;
    }

    public ConnectionMatch FindClosestConnection(List<AssemblyPiece> foreignPieces)
    {
        var smallestDistance = PickupController.MaxConnectionDistance;
        ConnectionMatch cMatch = null;

        foreach (var foreignPiece in foreignPieces)
        {
            var cm = FindClosestConnection(foreignPiece);

            if (cm == null)
                continue;

            // Check Distance
            if (cm.Distance < smallestDistance)
            {
                smallestDistance = cm.Distance;
                cMatch = cm;
            }
        }

        return cMatch;
    }

    public void CalculateBoundingSphere()
    {
        var renderers = GetComponentsInChildren<Collider>();
        if (renderers.Length == 0) return;
        var renderer = renderers[0];
        var combinedBounds = renderer.bounds;
        foreach (var render in renderers)
        {
            if (render != renderer)
                combinedBounds.Encapsulate(render.bounds);
        }

        var radius = Vector3.Distance(combinedBounds.center, combinedBounds.min);

        _boundingSphere = new BoundingSphere(transform, combinedBounds.center - transform.position, radius);
    }

    public void CalculateGroupBoundingSphere()
    {
        var connectedPieces = FindAllConnectedPieces(this, false);
        GroupBoundingSphere = _boundingSphere;

        for (int i = 0; i < connectedPieces.Count; ++i)
        {
            GroupBoundingSphere = BoundingSphere.Combine(GroupBoundingSphere, connectedPieces[i].BoundingSphere);
        }

        for (int i = 0; i < connectedPieces.Count; ++i)
        {
            connectedPieces[i].GroupBoundingSphere = GroupBoundingSphere;
        }

        //TEMP
        //var sphereCollider = gameObject.AddComponent<SphereCollider>();
        //sphereCollider.isTrigger = true;
        //sphereCollider.center = GroupBoundingSphere.Center;
        //sphereCollider.radius = GroupBoundingSphere.Radius;
    }

    void SetInitialMass()
    {
        if (BoundingSphere == null) return;
        var mass = 1.0f + BoundingSphere.Radius / 2.0f;
        var rb = GetComponent<Rigidbody>();
        if (rb)
            rb.mass = mass;
    }

    public void Reset()
    {
        if (!Application.isPlaying)
            return;

        // Detach all connection points
        DetachConnectionPoints();

        if (GhostObject != null)
            GhostObject.SetActive(false);

        // Handle RigidBody reset
        var rb = GetComponent<Rigidbody>();
        if (!rb && _hadRigidbodyStart)
        {
            rb = gameObject.AddComponent<Rigidbody>();
        }
        else if (rb && !_hadRigidbodyStart)
        {
            Destroy(rb);
        }

        if (rb)
        {
            rb.isKinematic = true;
            SetCollidersTrigger(false);
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
        }


        //transform.SetParent(_firstParent);
        //transform.position = _firstPosition;
        //transform.rotation = _firstRotation;
        Pickable.ResetPosition();
        var propertyHolder = GetComponent<PropertyHolder>();
        if(propertyHolder != null)
            propertyHolder.ResetAllProperties();
        Unlock();

        GhostObject.transform.position = transform.position;
        GhostObject.transform.rotation = transform.rotation;
        GhostObject.transform.SetParent(_firstGhostParent);
        GhostObject.SetActive(false);

        // Try to make connections in its start location
        AttachForeignConnectionPoints();
        GroupBoundingSphere = _boundingSphere;
    }

    public void Lock()
    {
        IsLocked = true;
    }

    public void Unlock()
    {
        IsLocked = false;
    }
}
