﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brush : Tool
{
    public PaintType PaintType;
    public Collider HitCollider;
    public LayerMask CheckLayer;
    Pickable _collidingPickable;

    protected override void Awake()
    {
        base.Awake();

        _type = ToolType.Brush;
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void HandleCarrying()
    {
        base.HandleCarrying();

        // TODO Highlight proxy paintzones
        CheckAPCollision();
    }

    void CheckAPCollision()
    {
        var colliders = Physics.OverlapSphere(HitCollider.transform.position, HitCollider.bounds.extents[0], CheckLayer);

        for (int i = 0; i < colliders.Length; ++i)
        {
            //Debug.Log("brush hit: " + colliders[i].gameObject);
            colliders[i].GetComponent<PaintSurface>().Paint(PaintType);
        }

        if(colliders.Length > 0)
        {
            if(Pickable.Controller != null)
                ViveHelper.PulseController(Pickable.Controller.ControllerIndex, 0.3f);
        }
    }
}
