﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Screwdriver : Tool
{
    public Collider HitCollider;
    Pickable _collidingPickable;

    protected override void Awake()
    {
        base.Awake();

        _type = ToolType.Screwdriver;
    }

    void Start()
    {
        TaskStep.OnDone += Unlock;
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void HandleCarrying()
    {
        base.HandleCarrying();

        CheckAPCollision();
    }

    void CheckAPCollision()
    {
        foreach (var controller in PickupManager.ControllerPickables.Keys)
        {
            if (controller == _pickable.Controller)
                continue;

            if (PickupManager.ControllerPickables[controller] == null)
                continue;

            var ap = PickupManager.ControllerPickables[controller].GetComponent<AssemblyPiece>();
            if (ap.OnPrePose && ap.PrePoseCP.Action == ToolAction.ScrewDriverTwist)
            {
                // Prevent player from dropping tool TODO setting
                if(TrainingManager.TrainingMode == TrainingMode.Guided)
                    _handLocked = true;
                var colliders = ap.GetComponentsInChildren<Collider>().Where(c => !c.GetComponent<ConnectionPoint>());
                foreach (var col in colliders)
                {
                    if (HitCollider.bounds.Intersects(col.bounds))
                    {
                        Debug.Log("Screwdriver Hit!!");
                        ap.Place();
                        return;
                    }
                }
            }
        }
    }

    void Unlock()
    {
        _handLocked = false;
    }
}
