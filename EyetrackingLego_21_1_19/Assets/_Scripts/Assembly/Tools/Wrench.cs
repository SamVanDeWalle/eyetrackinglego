﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Wrench : Tool
{
    public Collider HitCollider;
    Pickable _collidingPickable;

    protected override void Awake()
    {
        base.Awake();

        _type = ToolType.Wrench;
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void HandleCarrying()
    {
        base.HandleCarrying();

        CheckAPCollision();
    }

    void CheckAPCollision()
    {
        foreach (var controllerPickable in PickupManager.ControllerPickables)
        {
            if (controllerPickable.Key == _pickable.Controller)
                continue;

            if (controllerPickable.Value == null)
                continue;

            var ap = controllerPickable.Value.GetComponent<AssemblyPiece>();
            if (ap.OnPrePose && ap.PrePoseCP.Action == ToolAction.WrenchTwist)
            {
                var colliders = ap.GetComponentsInChildren<Collider>().Where(c => !c.GetComponent<ConnectionPoint>());
                foreach (var col in colliders)
                {
                    if (HitCollider.bounds.Intersects(col.bounds))
                    {
                        Debug.Log("Wrench Hit!!");
                        ap.Place();
                        return;
                    }
                }
            }
        }
    }
}
