﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Hammer : Tool
{
    public Collider HitCollider;

    Pickable _collidingPickable;

    protected override void Awake()
    {
        base.Awake();

        _type = ToolType.Hammer;
    }

    protected override void Update()
    {
        base.Update();
    }

    protected override void HandleCarrying()
    {
        base.HandleCarrying();

        CheckAPCollision();
    }

    void CheckAPCollision()
    {
        foreach(var controllerPickable in PickupManager.ControllerPickables)
        {
            if (controllerPickable.Key == _pickable.Controller)
                continue;

            if (controllerPickable.Value == null)
                continue;

            var ap = controllerPickable.Value.GetComponent<AssemblyPiece>();
            if(ap != null && ap.OnPrePose && ap.PrePoseCP.Action == ToolAction.HammerHit)
            {
                var colliders = ap.GetComponentsInChildren<Collider>().Where(c => !c.GetComponent<ConnectionPoint>());
                foreach (var col in colliders)
                {
                    if (HitCollider.bounds.Intersects(col.bounds))
                    {
                        Debug.Log("Hammer Hit!!");
                        // TODO only place if on prepose
                        ap.Place();
                        var args = new ToolEventArgs();
                        args.ToolAction = ToolAction.HammerHit;
                        args.Collider = col;
                        OnAction(args);
                        ViveHelper.PulseAllControllers(0.3f);
                    }
                }
            }
        }

        var actions = ToolManager.ToolTypeActions[_type];
        for (int a = 0; a < actions.Count; ++a)
        {
            var colliderSubscriptions = ToolManager.GetToolActionColliderSubscriptions(actions[a]);

            //var hitFound = false;
            for(int i = 0; i < colliderSubscriptions.Count; ++i)
            {
                Debug.Log("cs: " + i);
                var colliders = colliderSubscriptions[i].Colliders;
                for(int j = 0; j < colliders.Count; ++j)
                {
                    if (HitCollider.bounds.Intersects(colliders[j].bounds))
                    {
                        var args = new ToolEventArgs();
                        args.ToolAction = actions[a];
                        args.Parent = colliderSubscriptions[i].Parent;
                        args.Collider = colliders[j];
                        OnAction(args);
                        ViveHelper.PulseAllControllers(0.3f);
                        //hitFound = true;
                        //break;
                    }
                }

                //if (hitFound)
                //    break;
            }
        }
    }
}
