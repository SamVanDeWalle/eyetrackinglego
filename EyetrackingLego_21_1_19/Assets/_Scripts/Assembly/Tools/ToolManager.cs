﻿using Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class ColliderSubscription
{
    public Transform Parent;
    public List<Collider> Colliders = new List<Collider>();

    public ColliderSubscription(Transform parent, List<Collider> colliders)
    {
        Parent = parent;
        Colliders = colliders;
    }
}

public class ToolManager : Singleton<ToolManager>
{
    public static List<Tool> Tools
    {
        get
        {
            if (_tools == null || _tools.Count == 0)
                FindTools();

            return _tools;
        }
        private set { }
    }
    public static Dictionary<ToolType, List<ToolAction>> ToolTypeActions { get { return _toolTypeActions; } private set { } }
    
    static List<Tool> _tools = new List<Tool>();
    static Dictionary<ToolType, List<ToolAction>> _toolTypeActions = new Dictionary<ToolType, List<ToolAction>>()
    {
        { ToolType.Hammer, new List<ToolAction>() { ToolAction.HammerHit } },
        { ToolType.Wrench, new List<ToolAction>() { ToolAction.WrenchTwist } },
        { ToolType.Screwdriver, new List<ToolAction>() {ToolAction.ScrewDriverTwist } },
    };
    static Dictionary<ToolAction, ToolType> _toolTypePerAction = new Dictionary<ToolAction, ToolType>();
    static Dictionary<ToolAction, List<ColliderSubscription>> _toolColliderSubscriptions = new Dictionary<ToolAction, List<ColliderSubscription>>();
    static Dictionary<ToolAction, List<Collider>> _toolColliders = new Dictionary<ToolAction, List<Collider>>();

    protected override void Awake()
    {
        base.Awake();

        FindTools();
        FillToolTypePerAction();
    }

    public static void FindTools()
    {
        _tools = FindObjectsOfType<Tool>().ToList();
    }

    static void FillToolTypePerAction()
    {
        _toolTypePerAction.Clear();
        foreach(var toolTypeActions in _toolTypeActions)
        {
            foreach(var action in toolTypeActions.Value)
            {
                _toolTypePerAction.Add(action, toolTypeActions.Key);
            }
        }
    }

    public static ToolType GetToolTypeFromAction(ToolAction action)
    {
        if (_toolTypePerAction.Count == 0)
            FillToolTypePerAction();

        if (_toolTypePerAction.ContainsKey(action))
            return _toolTypePerAction[action];

        return ToolType.None;
    }

    public static List<Tool> GetTools(ToolType type)
    {
        if (_tools.Count == 0)
            FindTools();

        return _tools.Where(t => t.Type == type).ToList();
    }

    public static void SubscribeColliders(ToolAction toolAction, Transform parent, List<Collider> colliders)
    {
        if (!_toolColliderSubscriptions.ContainsKey(toolAction))
            _toolColliderSubscriptions.Add(toolAction, new List<ColliderSubscription>());

        if(_toolColliderSubscriptions[toolAction].Any(cs => cs.Parent == parent))
        {
            Debug.LogWarning("ToolManager::SubscribeColliders - Tried to subscribe an already existing subscription");
            return;
        }

        _toolColliderSubscriptions[toolAction].Add(new ColliderSubscription(parent, colliders));

        if (!_toolColliders.ContainsKey(toolAction))
            _toolColliders.Add(toolAction, colliders);
        else
            _toolColliders[toolAction].AddRange(colliders);
    }

    public static List<ColliderSubscription> GetToolActionColliderSubscriptions(ToolAction action)
    {
        if (!_toolColliderSubscriptions.ContainsKey(action))
        {
            //Debug.LogWarning("ToolManager::GetToolActionColliderSubscriptions - no subscriptions of the given key");
            return new List<ColliderSubscription>();
        }

        return _toolColliderSubscriptions[action];
    }

    public static List<Collider> GetToolActionColliders(ToolAction action)
    {
        return _toolColliders.ContainsKey(action) ? _toolColliders[action] : null;
    }
}
