﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ToolType
{
    None,
    Hammer,
    Wrench,
    Brush,
    Screwdriver,
}

public enum ToolAction
{
    None,
    HammerHit,
    WrenchTwist,
    ScrewDriverTwist,
    BrushTouch
}

public class ToolEventArgs : EventArgs
{
    public ToolAction ToolAction { get; set; }
    public Transform Parent { get; set; }
    public Collider Collider { get; set; }
}

[RequireComponent(typeof(Pickable))]
public class Tool : MonoBehaviour
{
    public Pickable Pickable { get { return _pickable; } private set { } }
    public ToolType Type { get { return _type; } private set { } }
    public bool IsHandLocked { get { return _handLocked; } private set { } }

    protected Pickable _pickable;
    protected ToolType _type;
    protected bool _handLocked;

    public static event EventHandler<ToolEventArgs> OnActionEvent;

    protected virtual void Awake()
    {
        SetPickable();
    }

    void SetPickable()
    {
        _pickable = GetComponent<Pickable>();
        if (!_pickable)
        {
            _pickable = gameObject.AddComponent<Pickable>();
        }

        _pickable.Type = PickableType.Tool;
        PickupManager.EnsurePickable(_pickable);
    }

    public void Pickup(PickupController controller)
    {
        _pickable.Pickup(controller);
        SetCollidersTrigger(true);
        GC.Collect();
    }

    protected virtual void Update()
    {
        if (_pickable == null)
            SetPickable();

        if (_pickable.IsPickedUp)
            HandleCarrying();
    }

    protected virtual void HandleCarrying() {}

    public void SetCollidersTrigger(bool active)
    {
        var colliders = GetComponentsInChildren<Collider>();
        foreach (var col in colliders)
        {
            if (!col.GetComponent<ConnectionPoint>())
                col.isTrigger = active;
        }
    }

    public void DropToGround(Vector3 force)
    {
        _pickable.ResetPosition();

        // TODO tool mode
        //transform.SetParent(null);
        //var rb = GetComponent<Rigidbody>();
        //if (!rb)
        //{
        //    rb = gameObject.AddComponent<Rigidbody>();
        //}
        //
        //rb.isKinematic = false;
        //SetCollidersTrigger(false);
        //rb.AddForce(force);
        //
        //_pickable.Drop();
    }

    protected virtual void OnAction(ToolEventArgs e)
    {
        var handler = OnActionEvent;
        if (handler != null)
            handler(this, e);
    }
}
