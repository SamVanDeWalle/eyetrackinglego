﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Workbench : MonoBehaviour
{
    public ConnectionPoint ConnectionPoint;
    public List<int> ConnectableTypes = new List<int>();
    public bool IsAvailable { get { return _workPiece == null; } private set { } }
    public HolderRotator HolderRotator;

    AssemblyPiece _workPiece;

    public delegate void apDelegate(AssemblyPiece piece);
    public event apDelegate OnAPPlaced;
    public event apDelegate OnAPRemoved;

    public void PlacePiece(AssemblyPiece piece)
    {
        if(IsAvailable)
        {
            _workPiece = piece;
            piece.OnPickedUp += Clear;

            if (OnAPPlaced != null)
                OnAPPlaced(piece);
        }
    }

    public void Clear()
    {
        _workPiece.OnPickedUp -= Clear;

        if (OnAPRemoved != null)
            OnAPRemoved(_workPiece);

        _workPiece = null;
    }
}
