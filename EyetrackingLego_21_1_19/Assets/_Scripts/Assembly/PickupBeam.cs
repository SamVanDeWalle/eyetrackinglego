﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupBeam : MonoBehaviour {

    private Vector3 _defaultSize;
    private Vector3 _hiddenSize;
    [SerializeField]
    private Transform _beamTransform;

    private bool _showingBeam = false;

    public void AttachToHand(Transform Hand)
    {
        _beamTransform.parent.SetParent(Hand, false);
        _beamTransform.parent.localPosition = Vector3.zero;
        _beamTransform.parent.localRotation = Quaternion.Euler(90f,0f,0f);
    }


    void Awake()
    {
        _beamTransform.gameObject.SetActive(false);
        _defaultSize = _beamTransform.localScale;
        _hiddenSize = _defaultSize;
        _hiddenSize.x = _hiddenSize.z = 0f;
        _beamTransform.localScale = _hiddenSize;

        _showingBeam = false;

    }

    public void ShowBeam()
    {
        if (_showingBeam) return;
        _showingBeam = true;
        StopAllCoroutines();
        StartCoroutine("ShowBeamUpdate");
    }

    public void HideBeam()
    {
        if (!_showingBeam) return;
        _showingBeam = false;

        StopAllCoroutines();
        StartCoroutine("HideBeamUpdate");
    }

    IEnumerator ShowBeamUpdate()
    {
        Vector3 _startScale = _beamTransform.localScale;
        _beamTransform.gameObject.SetActive(true);
        float t = 0f;
        while (t < 1f)
        {
            t = Mathf.Clamp01(t + Time.deltaTime*4f);

            Vector3 size = EasingFunctions.Ease(EasingFunctions.Type.OutElasticBig, t, _startScale, _defaultSize);
            _beamTransform.localScale = size;

            yield return null;
        }


    }

    IEnumerator HideBeamUpdate()
    {
        Vector3 _startScale = _beamTransform.localScale;
        float t = 0f;
        while (t < 1f)
        {
            t = Mathf.Clamp01(t + Time.deltaTime*2f);

            Vector3 size = EasingFunctions.Ease(EasingFunctions.Type.In, t, _defaultSize, _hiddenSize);
            _beamTransform.localScale = size;

            yield return null;
        }


        _beamTransform.gameObject.SetActive(false);

    }
}
