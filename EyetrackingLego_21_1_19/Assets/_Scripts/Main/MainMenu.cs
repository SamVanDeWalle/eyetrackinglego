﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MenuState
{
    SelectTraining,
    SelectTrainingMode,
    Settings,
}

[Serializable]
public class MenuStatePanel
{
    public MenuState State;
    public List<GameObject> Objects = new List<GameObject>();

    public void Activate(bool active = true)
    {
        foreach(var element in Objects)
        {
            element.SetActive(active);
        }
    }
}

public class MainMenu : MonoBehaviour
{
    public List<LinePointer> LinePointers = new List<LinePointer>();
    public List<MenuStatePanel> StatePanels = new List<MenuStatePanel>();
    public GameObject SettingsBtn;

    Dictionary<MenuState, MenuStatePanel> _statePanels = new Dictionary<MenuState, MenuStatePanel>();
    MenuState _currentState;
    MenuState _previousState;
	
    void Start()
    {
        foreach(var statePanel in StatePanels)
        {
            if (_statePanels.ContainsKey(statePanel.State))
            {
                if (_statePanels[statePanel.State] != null)
                    Debug.LogWarning("MainMenu::Start - Multiple panels with same state given, previous panels are overriden");
            }
            else
            {
                _statePanels.Add(statePanel.State, null);
            }

            _statePanels[statePanel.State] = statePanel;
        }
    }

    void OnEnable()
    {
        foreach(var linePointer in LinePointers)
        {
            if (linePointer != null)
                linePointer.gameObject.SetActive(true);
        } 
    }

    void OnDisable()
    {
        foreach (var linePointer in LinePointers)
        {
            if (linePointer != null)
                linePointer.gameObject.SetActive(false);
        }
    }

    public void GoToState(MenuState state)
    {
        _previousState = _currentState;
        _statePanels[_currentState].Activate(false);
        _statePanels[state].Activate(true);
        SettingsBtn.SetActive(state != MenuState.Settings);
        _currentState = state;
    }

    public void GoBack()
    {
        GoToState(_previousState);
    }

    public void SelectTraining(int index)
    {
        TrainingManager.IndexToLoad = index;
        GoToState(MenuState.SelectTrainingMode);
    }

    public void SelectTutorial()
    {
        VR_Main.LoadTutorial();
    }

    public void SelectTrainingMode(int mode)
    {
        SelectTrainingMode((TrainingMode)mode);
    }

    public void SelectTrainingMode(TrainingMode mode)
    {
        Debug.Log("training mode: " + mode);
        TrainingManager.TrainingMode = mode;
        TrainingManager.StartTraining();
    }

    public void SelectSettings()
    {
        GoToState(MenuState.Settings);
    }
}
