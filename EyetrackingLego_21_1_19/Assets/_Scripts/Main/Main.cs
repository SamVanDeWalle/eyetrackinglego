﻿using UnityEngine;
using System.Collections;
using Helpers;
using UnityEngine.SceneManagement;
using System.Net;

public class Main : Singleton<Main>
{
    public string IP;
    public string HostIP;
    public bool IsHost;
    public int Port;
    public UserType UserType = UserType.Player;

    public string Player_Prefab, Spectate_Prefab;

    enum Level
    {
        Menu,
        Play,
        Spectate
    }

    protected override void Awake()
    {
        base.Awake();

        //DontDestroyOnLoad(gameObject);

        //IP = Dns.GetHostAddresses(Dns.GetHostName())[0].ToString();
        //IP = Network.player.ipAddress;
        IP = GetLocalIPAddress();

        //SceneManager.sceneLoaded += OnSceneLoaded;
    }

    public static string GetLocalIPAddress()
    {
        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach(var ip in host.AddressList)
        {
            if(ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
            {
                return ip.ToString();
            }
        }

        return "0";
    }
   
    void Update()
    {
        //if(Input.GetKeyDown(KeyCode.R))
        //{
        //    Reset();
        //}
    }

    public void Host()
    {
        IsHost = true;
        UserType = UserType.Player;
        HostIP = IP;
        UnityEngine.XR.XRSettings.enabled = true;
        SceneManager.LoadScene((int)Level.Play);
    }

    public void Connect()
    {
        IsHost = false;
        UserType = UserType.Player;
        HostIP = Menu_UI.Instance.HostIPText.text;
        SceneManager.LoadScene((int)Level.Play);
    }

    public void Spectate()
    {
        IsHost = false;
        UserType = UserType.Spectator;
        HostIP = Menu_UI.Instance.HostIPText.text;
        UnityEngine.XR.XRSettings.enabled = false;
        SceneManager.LoadScene((int)Level.Spectate);
    }

    public void Reset()
    {
        NetworkHandling.Instance.ShutDown();
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        SceneManager.LoadScene((int)Level.Menu);
        Destroy(gameObject);
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if(scene.buildIndex == (int)Level.Play)
        {
            switch (UserType)
            {
                case UserType.Player:
                    GameObject.Find(Player_Prefab).SetActive(true);
                    GameObject.Find(Spectate_Prefab).SetActive(false);
                    break;
                case UserType.Spectator:
                    GameObject.Find(Spectate_Prefab).SetActive(true);
                    GameObject.Find(Player_Prefab).SetActive(false);
                    break;
            }
        }
    }
}
