﻿using UnityEngine;
using System.Collections;
using XBOX;

public class PlayerController : MonoBehaviour
{
    public float Speed = 6.0f;

    CharacterController _controller;
    Vector3 _moveDirection;
    float _moveSpeed = 20.0f;
    Vector2 _heightRange = new Vector2( -3.0f, 16 );

    public GameObject CameraRig;
    private PickupController _pickUpController;
    public Transform HandParent;
    void Awake()
    {
        _controller = GetComponent<CharacterController>();
        Cursor.lockState = CursorLockMode.Locked;
        //var cam = FindObjectOfType<SteamVR_ControllerManager>();
        //    CameraRig = cam.gameObject;
        //    CameraRig.SetActive( false );
        //foreach ( var p in FindObjectsOfType<PickupController>() )
        //{
        //    if ( _pickUpController == null )
        //    {

        //        _pickUpController = p;
        //        _pickUpController.Hand.SetParent( HandParent );
        //        _pickUpController.Hand.localRotation = Quaternion.identity;
        //        _pickUpController.Hand.localPosition = Vector3.zero;
        //    }
        //    else
        //        p.gameObject.SetActive( false );
        //}

    }

    void Update()
    {
        if ( Input.GetAxisRaw( "Vertical" ) > 0 )
            transform.position += transform.forward * Time.deltaTime * _moveSpeed;

        if ( Input.GetAxisRaw( "Vertical" ) < 0 )
            transform.position -= transform.forward * Time.deltaTime * _moveSpeed;

        if ( Input.GetAxisRaw( "Horizontal" ) < 0 )
            transform.position -= transform.right * Time.deltaTime * _moveSpeed;

        if ( Input.GetAxisRaw( "Horizontal" ) > 0 )
            transform.position += transform.right * Time.deltaTime * _moveSpeed;

        if ( ( Input.GetKey( KeyCode.Space ) || Input.GetAxis( XBOXONEController.BackTriggerRight ) > 0 ) && transform.position.y < _heightRange.y )
            transform.position += transform.up * Time.deltaTime * _moveSpeed;

        if ( ( Input.GetKey( KeyCode.LeftControl ) || Input.GetAxis( XBOXONEController.BackTriggerLeft ) > 0 ) && transform.position.y > _heightRange.x )
            transform.position -= transform.up * Time.deltaTime * _moveSpeed;

        //if ( Input.GetMouseButtonDown( 0 ) )
        //{
        //    _pickUpController.SimulateTriggerDown();
        //}
        //if ( Input.GetMouseButtonUp( 0 ) )
        //{

        //    _pickUpController.SimulateTriggerUp();
        //}
        //_moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        //_moveDirection = transform.TransformDirection(_moveDirection);
        //_moveDirection *= Speed;
        //if (_controller.isGrounded)
        //{
        //    _moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        //    _moveDirection = transform.TransformDirection(_moveDirection);
        //    _moveDirection *= Speed;
        //}
        //
        //_controller.Move(_moveDirection * Time.deltaTime);
    }
}
