﻿using System;
using System.Collections;
using System.Collections.Generic;
using Helpers;
using UnityEngine;

[Serializable]
public class SoundEffect
{
    public string Name;
    public AudioClip Clip;
}

public class SoundManager : Singleton<SoundManager>
{
    public List<SoundEffect> Sounds = new List<SoundEffect>();
    static Dictionary<string, AudioClip> SoundClips = new Dictionary<string, AudioClip>();

    protected override void Awake()
    {
        base.Awake();

        LoadSounds();
    }

    void LoadSounds()
    {
        SoundClips.Clear();
        foreach (var soundEffect in Sounds)
        {
            if (SoundClips.ContainsKey(soundEffect.Name))
            {
                Debug.LogWarning("SoundManager::LoadSounds - Multiple sounds with same name | " + soundEffect.Name);
                continue;
            }

            SoundClips.Add(soundEffect.Name, soundEffect.Clip);
        }
    }

    public static AudioClip GetSound(string name)
    {
        if (!SoundClips.ContainsKey(name))
        {
            Debug.LogWarning("SoundManager::GetSound - Tried to get nonexisting sound | " + name);
            return null;
        }

        return SoundClips[name];
    }

    public static void Play(string soundName, Vector3 position)
    {
        AudioClip clip = GetSound(soundName);

        if (clip == null)
        {
            Debug.LogWarning("SoundManager::Play - Couldn't play nonexisting sound | " + soundName);
            return;
        }

        AudioSource.PlayClipAtPoint(clip, position);
    }
}
