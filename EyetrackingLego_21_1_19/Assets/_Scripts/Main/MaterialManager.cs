﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class MaterialExtensions
{
    public static Material CopyProperties(this Material to, Material from)
    {
        if (to.shader != from.shader)
        {
            return to;
        }

        to.SetColor("_Color", from.GetColor("_Color"));
        to.SetFloat("_Glossiness", from.GetFloat("_Glossiness"));
        to.SetFloat("_Metallic", from.GetFloat("_Metallic"));
        to.SetColor("_EmissionColor", from.GetColor("_EmissionColor"));
        to.SetTexture("_MainTex", from.GetTexture("_MainTex"));

        return to;
    }
}

public class FadeMaterialData
{
    //public float DefaultAlpha;
    public Color DefaultColor;
    public float DefaultSmoothness;
    public Material Material;
}
public class GlowMaterialData
{
    //public float DefaultAlpha;
    //public Color DefaultColor;
    //public float DefaultSmoothness;
    public Color DefaultEmission;
    public Material Material;
}
public class HintMaterialData
{
    //public float DefaultAlpha;
    //public Color DefaultColor;
    //public float DefaultSmoothness;
    public Color DefaultEmission;
    public Material Material;
}

public class MaterialGroup
{
    public Material HighlightMaterial,
        PointMaterial,
        FadeMaterial,
        DefaultMaterial,
        HintMaterial;
}

public class MaterialManager : MonoBehaviour
{


    private enum MaterialState
    {
        Default,
        Highlight,
        Point,
        Fade,
        Hint,
    }

    private static MaterialManager _instance;

    public static MaterialManager GetInstance()
    {
        if(_instance == null)
        {
            _instance = FindObjectOfType<MaterialManager>();
        }

        return _instance;
    }

    public Material CorrectMaterial { get { return _correctMaterial; } private set { } }

    [SerializeField]
    Material HighlightMaterial, PointMaterial, FadeMaterial, HintMaterial, _correctMaterial;

    private Dictionary<Material, MaterialGroup> _groupsByMat = new Dictionary<Material, MaterialGroup>();
    private Dictionary<Renderer, MaterialGroup[]> _groupsByRenderer = new Dictionary<Renderer, MaterialGroup[]>();
    private Dictionary<Renderer, MaterialState> _CurrentMaterialStates = new Dictionary<Renderer, MaterialState>();

    private List<FadeMaterialData> FadeMaterials = new List<FadeMaterialData>();
    private List<GlowMaterialData> GlowMaterials = new List<GlowMaterialData>();
    private List<HintMaterialData> HintMaterials = new List<HintMaterialData>();
    private List<Pickable> AllPickables = new List<Pickable>();
    public List<Renderer> CurrentGlowRenderers = new List<Renderer>();
    public List<Renderer> CurrentHintRenderers = new List<Renderer>();
    //private List<Material> HighlightMaterials = new List<Material>();
    //private List<Material> PointMaterials = new List<Material>();
    float _currentHintRimPower = 1.0f;
    float _hintSpeed = 14.0f;
    bool _hintingIn = true;

    private Coroutine 
        _fadeCoroutine = null,
        _GlowCoroutine = null,
        _HintCoroutine = null;

    private float 
        _currentFadePct = 0f,
        _currentGlowPct = 0f,
        _currentHintPct = 0f;
    bool _anyFaded = false;

    public void Awake()
    {
        _instance = this;
        LoadRenderers();
    }

    void LoadRenderers()
    {
        foreach (var pickable in PickupManager.Pickables)
        {
            AllPickables.Add(pickable);
            AddRenderer(pickable.GetComponent<Renderer>());
            foreach (var r in pickable.Renderers)
            {
                AddRenderer(r);
            }
        }
    }

    void Update()
    {
        if (CurrentHintRenderers.Count > 0)
        {
            _currentHintRimPower += Time.deltaTime * (_hintingIn ? _hintSpeed : - _hintSpeed);
            if (_currentHintRimPower > 6.0f)
            {
                _currentHintRimPower = 6.0f - _currentHintRimPower % 6.0f;
                _hintingIn = false;
            }
            else if(_currentHintRimPower < 1.0f)
            {
                _currentHintRimPower = 1.0f + (1 - _currentHintRimPower);
                _hintingIn = true;
            }

            foreach (var mat in HintMaterials)
            {
                mat.Material.SetFloat("_RimPower", _currentHintRimPower);
            }
        }
    }

    MaterialGroup CreateMaterialGroup(Material defaultMat)
    {
        if (_groupsByMat.ContainsKey(defaultMat)) return _groupsByMat[defaultMat];

        //Create point and highlight material
        var pointMaterial = new Material(defaultMat);
        var highlightMaterial = new Material(HighlightMaterial);
        var hintMaterial = new Material(HintMaterial);
        var fadeMat = new Material(defaultMat);

        //highlightMaterial.CopyProperties(defaultMat);
        //fadeMat.SetFloat("_Mode", FadeMaterial.GetFloat("_Mode"));

        //Copy properties from material


        //Create new materialgroup
        MaterialGroup g = new MaterialGroup()
        {
            DefaultMaterial = defaultMat,
            FadeMaterial = fadeMat,
            PointMaterial = pointMaterial,
            HighlightMaterial = highlightMaterial,
            HintMaterial = hintMaterial
        };

        FadeMaterials.Add(new FadeMaterialData()
        {
            Material = fadeMat,
            DefaultColor = defaultMat.HasProperty("_Color") ? defaultMat.color : Color.black,
            DefaultSmoothness = defaultMat.HasProperty("_Glossiness") ? defaultMat.GetFloat("_Glossiness") : 0.0f
        });
        GlowMaterials.Add(new GlowMaterialData()
        {
            Material = highlightMaterial,
            //DefaultColor = defaultMat.color,
            DefaultEmission = defaultMat.HasProperty("_Glossiness") ? defaultMat.GetColor("_EmissionColor") : Color.white
        });
        HintMaterials.Add(new HintMaterialData()
        {
            Material = hintMaterial,
            //DefaultColor = defaultMat.color,
            DefaultEmission = defaultMat.HasProperty("_Glossiness") ? defaultMat.GetColor("_EmissionColor") : Color.white
        });

        _groupsByMat.Add(defaultMat, g);

        return g;
    }

    public static void AddRenderers(Pickable pickable)
    {
        GetInstance().AddRenderer(pickable.GetComponent<Renderer>());
        foreach (var r in pickable.Renderers)
        {
            GetInstance().AddRenderer(r);
        }
    }

    void AddRenderer(Renderer r)
    {
        if (r == null) return;
        if (_groupsByRenderer.ContainsKey(r)) return;//Already handled this renderer

        MaterialGroup[] allgroups = new MaterialGroup[r.sharedMaterials.Length];
        for (int i = 0; i < allgroups.Length; ++i)
        {
            allgroups[i] = CreateMaterialGroup(r.sharedMaterials[i]);
        }

        _groupsByRenderer.Add(r, allgroups);
        _CurrentMaterialStates.Add(r, MaterialState.Default);
    }
    const float __MAXFADE = 0.6f;
    static readonly Color __FADECOLOR = new Color(0.2f, 0.22f, 0.25f, 0.2f);
    void UpdateFadePct(float pct)
    {
        _currentFadePct = pct;
        float fadePct = pct * __MAXFADE;
        foreach (var mat in FadeMaterials)
        {

            mat.Material.color = Color.Lerp(mat.DefaultColor, __FADECOLOR, fadePct);
            mat.Material.SetFloat("_Glossiness", Mathf.Lerp(mat.DefaultSmoothness, 0f, fadePct));
        }
    }

    const float __MAXGLOW = 1f;
    static readonly Color __GLOWCOLOR = new Color(1f, 0f, 0.517f, 1f);
    void UpdateGlowPct(float pct)
    {
        _currentGlowPct = pct;
        float glowPct = pct * __MAXGLOW;
        foreach (var mat in GlowMaterials)
        {

            //mat.Material.color = Color.Lerp(mat.DefaultColor, __FADECOLOR, fadePct);
            mat.Material.SetColor("_EmissionColor", Color.Lerp(mat.DefaultEmission, __GLOWCOLOR, glowPct));
        }
    }

    const float __MAXHINT = 1f;
    static readonly Color __HINTCOLOR = new Color(1f, 0f, 0.517f, 1f);
    void UpdateHintPct(float pct)
    {
        _currentHintPct = pct;
        float hintPct = pct * __MAXHINT;
        foreach (var mat in HintMaterials)
        {

            //mat.Material.color = Color.Lerp(mat.DefaultColor, __FADECOLOR, fadePct);
            mat.Material.SetColor("_EmissionColor", Color.Lerp(mat.DefaultEmission, __HINTCOLOR, hintPct));
        }
    }

    IEnumerator UnglowGlow(List<Renderer> Unglow, List<Renderer> Glow)
    {
        float startGlowPct = _currentFadePct;
        //First unfade
        if (_anyFaded)
        {
            float t = 0f;
            while (t < 1f)
            {
                t += 1.0f;// Time.deltaTime * 5f;
                float glowPct = EasingFunctions.Ease(EasingFunctions.Type.InOut, t, startGlowPct, 0f);

                UpdateGlowPct(glowPct);
                yield return null;
            }
        }

        //Swap materials

        foreach (var r in Unglow)
        {
            SwapState(r, MaterialState.Default);

        }
        foreach (var r in Glow)
        {
            SwapState(r, MaterialState.Highlight);

        }
        _anyFaded = Glow.Count > 0;

        if (_anyFaded)
        {
            float t = 0f;
            while (t < 1f)
            {
                t += 1.0f;// Time.deltaTime * 5f;
                float glowpct = EasingFunctions.Ease(EasingFunctions.Type.InOut, t, 0f, 1f);

                UpdateGlowPct(glowpct);
                yield return null;
            }
        }
        yield break;
    }

    void UnhintHintDirect(List<Renderer> Unhint, List<Renderer> Hint)
    {
        foreach (var r in Unhint)
        {
            SwapState(r, MaterialState.Default);
        }
        foreach (var r in Hint)
        {
            SwapState(r, MaterialState.Hint);
        }
    }

    IEnumerator UnhintHint(List<Renderer> Unhint, List<Renderer> Hint)
    {
        float startHintPct = _currentFadePct;
        //First unfade
        if (_anyFaded)
        {
            float t = 0f;
            while (t < 1f)
            {
                t += 1.0f;// Time.deltaTime * 5f;
                float hintPct = EasingFunctions.Ease(EasingFunctions.Type.InOut, t, startHintPct, 0f);

                UpdateHintPct(hintPct);
                yield return null;
            }
        }

        //Swap materials

        foreach (var r in Unhint)
        {
            SwapState(r, MaterialState.Default);

        }
        foreach (var r in Hint)
        {
            SwapState(r, MaterialState.Hint);

        }
        _anyFaded = Hint.Count > 0;

        if (_anyFaded)
        {
            float t = 0f;
            while (t < 1f)
            {
                t += 1.0f;// Time.deltaTime * 5f;
                float hintpct = EasingFunctions.Ease(EasingFunctions.Type.InOut, t, 0f, 1f);

                UpdateHintPct(hintpct);
                yield return null;
            }
        }
        yield break;
    }

    IEnumerator UnFadeFade(List<Renderer> Unfade, List<Renderer> fade)
    {
        float startFadePct = _currentFadePct;
        //First unfade
        if (_anyFaded)
        {
            float t = 0f;
            while (t < 1f)
            {
                t += 1.0f;// Time.deltaTime * 5f;
                float fadePct = EasingFunctions.Ease(EasingFunctions.Type.InOut, t, startFadePct, 0f);

                UpdateFadePct(fadePct);
                yield return null;
            }
        }

        //Swap materials

        foreach (var r in Unfade)
        {
            SwapState(r, MaterialState.Default);

        }
        foreach (var r in fade)
        {
            SwapState(r, MaterialState.Fade);

        }
        _anyFaded = fade.Count > 0;

        if (_anyFaded)
        {
            float t = 0f;
            while (t < 1f)
            {
                t += 1.0f;// Time.deltaTime * 5f;
                float fadePct = EasingFunctions.Ease(EasingFunctions.Type.InOut, t, 0f, 1f);

                UpdateFadePct(fadePct);
                yield return null;
            }
        }

        yield break;
    }

    void SwapState(Renderer r, MaterialState s)
    {
        MaterialState current; //only swap if needed
        if (!_CurrentMaterialStates.TryGetValue(r, out current))
        {
            _CurrentMaterialStates.Add(r, s);
        }
        else if (current == s) { return; }
        else
            _CurrentMaterialStates[r] = s;

        if (!_groupsByRenderer.ContainsKey(r))
            LoadRenderers();

        MaterialGroup[] currentGroups = _groupsByRenderer[r];
        Material[] sharedMats = r.sharedMaterials;
        switch (s)
        {
            case MaterialState.Fade:

                for (int i = 0; i < r.sharedMaterials.Length; ++i)
                {
                    sharedMats[i] = currentGroups[i].FadeMaterial;
                }
                break;
            case MaterialState.Default:
                for (int i = 0; i < r.sharedMaterials.Length; ++i)
                {
                    sharedMats[i] = currentGroups[i].DefaultMaterial;
                }
                break;
            case MaterialState.Highlight:
                for (int i = 0; i < r.sharedMaterials.Length; ++i)
                {
                    sharedMats[i] = currentGroups[i].HighlightMaterial;
                }
                break;
            case MaterialState.Hint:
                for (int i = 0; i < r.sharedMaterials.Length; ++i)
                {
                    sharedMats[i] = currentGroups[i].HintMaterial;
                }
                break;
        }

        r.sharedMaterials = sharedMats;
    }

    public void UnfadeAll()
    {
        List<Renderer> RenderersToFade = new List<Renderer>();
        List<Renderer> RenderersNotToFade = new List<Renderer>();
        foreach (Pickable p in PickupManager.Pickables)
        {
            RenderersNotToFade.AddRange(p.Renderers);
        }
        if (_fadeCoroutine != null)
            StopCoroutine(_fadeCoroutine);
        _fadeCoroutine = StartCoroutine(UnFadeFade(RenderersNotToFade, RenderersToFade));
    }

    public void UnglowAll()
    {
        List<Renderer> RenderersToGlow = new List<Renderer>();
        List<Renderer> RenderersToUnglow = CurrentGlowRenderers;
        List<Renderer> renderersToHint = CurrentGlowRenderers.Intersect(CurrentHintRenderers).ToList();
        //foreach (AssemblyPiece p in AllPieces)
        //{
        //    RenderersNotToFade.AddRange(p.Renderers);
        //}
        //StartCoroutine(UnFadeFade(RenderersToUnglow, RenderersToGlow));
        if (_GlowCoroutine != null)
            StopCoroutine(_GlowCoroutine);
        _GlowCoroutine = StartCoroutine(UnglowGlow(RenderersToUnglow, RenderersToGlow));

        if (_HintCoroutine != null)
            StopCoroutine(_HintCoroutine);
        _HintCoroutine = StartCoroutine(UnhintHint(new List<Renderer>(), renderersToHint));
    }
    
    public void GlowObjects(Pickable[] PickablesToGlow)
    {
        List<Renderer> renderersToGlow = new List<Renderer>();
        List<Renderer> renderersToUnglow = CurrentGlowRenderers;
        List<Renderer> renderersToHint = renderersToUnglow.Intersect(CurrentHintRenderers).ToList();

        foreach (Pickable p in PickablesToGlow)
        {
            renderersToGlow.AddRange(p.Renderers);
        }

        CurrentGlowRenderers = renderersToGlow;

        //Remove duplicates
        var duplicates = renderersToGlow.Intersect(renderersToUnglow);
        renderersToUnglow.RemoveAll(x => duplicates.Contains(x));
        renderersToUnglow.RemoveAll(x => renderersToHint.Contains(x));
        //RenderersToGlow.RemoveAll(x => duplicates.Contains(x));

        if (_GlowCoroutine != null)
            StopCoroutine(_GlowCoroutine);
        _GlowCoroutine = StartCoroutine(UnglowGlow(renderersToUnglow, renderersToGlow));

        if (_HintCoroutine != null)
            StopCoroutine(_HintCoroutine);
        _HintCoroutine = StartCoroutine(UnhintHint(new List<Renderer>(), renderersToHint));
    }

    public void HintObjects(Pickable[] PickablesToHint)
    {
        List<Renderer> RenderersToHint = new List<Renderer>();
        List<Renderer> RenderersToUnhint = CurrentHintRenderers;

        foreach (Pickable p in PickablesToHint)
        {
            RenderersToHint.AddRange(p.Renderers);
        }

        CurrentHintRenderers = RenderersToHint;

        //Remove duplicates
        var duplicates = RenderersToHint.Intersect(RenderersToUnhint);
        RenderersToUnhint.RemoveAll(x => duplicates.Contains(x));
        //RenderersToGlow.RemoveAll(x => duplicates.Contains(x));

        UnhintHintDirect(RenderersToUnhint, RenderersToHint);
        //if (_HintCoroutine != null)
        //    StopCoroutine(_HintCoroutine);
        //_HintCoroutine = StartCoroutine(UnhintHint(RenderersToUnhint, RenderersToHint));
    }

    public void GlowObject(Pickable pickable)
    {
        List<Renderer> RenderersToGlow = new List<Renderer>();
        List<Renderer> RenderersToUnglow = CurrentGlowRenderers;

        RenderersToGlow.AddRange(pickable.Renderers);
        CurrentGlowRenderers = RenderersToGlow;

        //Remove duplicates
        var duplicates = RenderersToGlow.Intersect(RenderersToUnglow);
        RenderersToUnglow.RemoveAll(x => duplicates.Contains(x));
        //RenderersToGlow.RemoveAll(x => duplicates.Contains(x));

        if (_GlowCoroutine != null)
            StopCoroutine(_GlowCoroutine);
        _GlowCoroutine = StartCoroutine(UnglowGlow(RenderersToUnglow, RenderersToGlow));
    }
   
    public void FadeOthers(Pickable[] PickablesNotToFade)
    {
        List<Renderer> RenderersToFade = new List<Renderer>();
        List<Renderer> RenderersNotToFade = new List<Renderer>();
        foreach (Pickable p in PickupManager.Pickables.Where(x => !PickablesNotToFade.Any(f => f == x)))
        {
            RenderersToFade.AddRange(p.Renderers);
        }
        foreach (Pickable p in PickablesNotToFade)
        {
            RenderersNotToFade.AddRange(p.Renderers);
        }
        if (_fadeCoroutine != null)
            StopCoroutine(_fadeCoroutine);
        _fadeCoroutine = StartCoroutine(UnFadeFade(RenderersNotToFade, RenderersToFade));
    }
}
