﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using XBOX;

public class MainManager : MonoBehaviour
{
    static MainManager _instance;

    public static MainManager Instance
    {
        get { return _instance; }
    }
    void Awake()
    {
        _instance = this;
    } 
    void Start()
    {
        //Cursor.visible = false;
    }

    void OnDestroy()
    {
        Cursor.visible = true;
    }

	void Update ()
    {
        if (Input.GetKeyUp(KeyCode.Escape) || Input.GetButtonUp(XBOXONEController.Menu))
        {
            Application.Quit();
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            CleanScene();
        }

        if(Input.GetKeyDown(KeyCode.T))
        {
            ToggleTooltips();
        }

        foreach (ViveController controller in ViveHelper.Controllers.Values)
        {
            if (controller.GetPress(ControllerButton.Menu))
                ToggleTooltips();
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            foreach(var dragger in FindObjectsOfType<VR_Dragger>())
            {
                dragger.Toggle();
            }
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            foreach (var dragger in FindObjectsOfType<VR_Dragger>())
            {
                dragger.Load();
            }
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            foreach (var dragger in FindObjectsOfType<VR_Dragger>())
            {
                dragger.Save();
            }
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            if(Physics.gravity != Vector3.zero)
            {
                Physics.gravity = new Vector3(0, 0, 0);
            }
            else
            {
                Physics.gravity = new Vector3(0, -9.81f, 0);
            }
        }

        if (Input.GetKeyDown(KeyCode.Slash))
        {
            Debug.Log("slash");
            var teleportEnabled = VR_Settings.Get("Enable Teleport");
            Debug.Log("te" + teleportEnabled);
            VR_Settings.Set("Enable Teleport", !teleportEnabled);
        }
    }

    public void CleanScene()
    {
        ResetWorld();
        //Send CleanScene over Network
        NetworkHandling.Instance.SendReset();
    }

    public void ResetWorld()
    {
        foreach ( var controller in FindObjectsOfType<PickupController>() )
        {
            controller.Reset();
        }
        foreach ( var ap in AssemblyPieceManager.AssemblyPieces )
        {
            ap.Reset();
        }
    }

    public void ToggleTooltips()
    {
        ControllerTooltip.Active = !ControllerTooltip.Active;
        var tooltips = FindObjectsOfType<ControllerTooltip>();
        for(int i = 0; i < tooltips.Length; ++i)
        {
            var renderers = tooltips[i].transform.GetComponentsInChildren<Renderer>();
            foreach(var rend in renderers)
            {
                rend.enabled = ControllerTooltip.Active;
            }
        }
    }
}
