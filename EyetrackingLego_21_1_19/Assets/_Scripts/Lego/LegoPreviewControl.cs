﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegoPreviewControl : MonoBehaviour
{
    public ViveController Controller;

    Vector2 _lastTouchPosition = new Vector2(-10, -10);
    float _scrollSpeed = 8000.0f;

    void Update()
    {
        var touching = Controller.GetTouch();
        var touchpad = Controller.GetTouchPadPosition();
        var touchUp = Controller.GetTouchUp();

        if (touching)
        {
            if (_lastTouchPosition != new Vector2(-10, -10))
            {
                var touchDeltaX = touchpad.x - _lastTouchPosition.x;
                var touchDeltaY = touchpad.y - _lastTouchPosition.y;
                transform.Rotate(0/*-touchDeltaY * _scrollSpeed * Time.deltaTime*/, -touchDeltaX * _scrollSpeed * Time.deltaTime, 0);
            }

            _lastTouchPosition = touchpad;
        }
        else
        {
            
        }

        if (touchUp)
        {
            _lastTouchPosition = new Vector2(-10, -10);
        }
    }
}
