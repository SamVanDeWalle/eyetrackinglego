﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/* This script is written to supress a weird bug in which there 
 * is some force applied to the lego pieces at the start of the
 * execution of the program. Temporarily freezing the pieces 
 * solves this problem. Preferably this bug gets fixed soon.
 */
public class QuickFix : MonoBehaviour {
    void Start() {
        GetComponent<Rigidbody>().isKinematic = true;
        StartCoroutine(Release());
    }

    IEnumerator Release() {     
        yield return new WaitForSeconds(0.1f);
        GetComponent<Rigidbody>().isKinematic = false;
    }
}
