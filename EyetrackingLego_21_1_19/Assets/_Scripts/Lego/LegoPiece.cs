﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum LegoSide
{
    Top,
    Bot
}

[Serializable]
public class Int3
{
    public int A, B, C;

    public Int3(int a, int b, int c)
    {
        A = a;
        B = b;
        C = c;
    }

    public Int3(Int3 other)
    {
        A = other.A;
        B = other.B;
        C = other.C;
    }
}

[Serializable]
public class LegoConnectionData
{
    public int ID;

    public int[,] TopMatrix;
    public int[,] BotMatrix;
    public int[] TopArray;
    public int[] BotArray;

    public List<LegoConnection> TopConnections = new List<LegoConnection>();
    public List<LegoConnection> BotConnections = new List<LegoConnection>();

    public static int[] ConvertToArray(int[,] matrix)
    {
        //We store the column count of the matrix in the first array value
        var list = new List<int>();
        list.Add(matrix.GetLength(0));
        for (int i = 0; i < matrix.GetLength(0); ++i)
        {
            for (int j = 0; j < matrix.GetLength(1); ++j)
            {
                list.Add(matrix[i, j]);
            }
        }

        return list.ToArray();
    }

    public static int[,] ConvertToMatrix(int[] array)
    {
        var columns = array[0];
        var rows = (array.Length - 1) / columns;
        var matrix = new int[columns, rows];
        var index = 1;
        for (int i = 0; i < columns; ++i)
        {
            for (int j = 0; j < rows; ++j)
            {
                matrix[i, j] = array[index++];
            }
        }

        return matrix;
    }

    public void Load()
    {
        TopMatrix = ConvertToMatrix(TopArray);
        BotMatrix = ConvertToMatrix(BotArray);
        //Matrix<int>.Print2DArray(BotMatrix);
    }
}

[Serializable]
public class LegoConnection
{
    public int PieceID;
    public LegoSide Side;
    public int ShiftLeft;
    public int ShiftTop;
    public int[,] OtherMatrix;
    public int[,] OverlapMatrix;
    public int[] OtherArray;
    public Int3 Pos;
    public Int3 OtherPos;

    public void Store()
    {
        OtherArray = LegoConnectionData.ConvertToArray(OtherMatrix);
    }

    public void Load()
    {
        OtherMatrix = LegoConnectionData.ConvertToMatrix(OtherArray);
    }
}

public class LegoPiece : MonoBehaviour
{
    public LegoConnectionData ConnectionData
    {
        get { return _connectionData; }
        private set { }
    }
    public Bounds OverlapBounds { get { return OverlapCollider.bounds; } private set { } }
    public Bounds GhostOverlapBounds { get { return GhostOverlapCollider.bounds; } private set { } }
    public Collider OverlapCollider;
    public Collider GhostOverlapCollider;
    public int GridSizeX = 2, GridSizeY = 2;
    public int ID
    {
        get { return _id; }
        private set { }
    }

    Dictionary<int, Int3> _lookup = new Dictionary<int, Int3>();
    Dictionary<int, LegoConnection> _topConnections = new Dictionary<int, LegoConnection>();
    Dictionary<int, LegoConnection> _botConnections = new Dictionary<int, LegoConnection>();
    LegoConnectionData _connectionData;
    LegoCollisionChecker _collisionChecker;

    static float _topYOffset = 0.04f;
    static float _botYOffset = -0.056f;
    static float _semiCPDistance = 0.04f;
    static int _cpTypeID = 0;

    int[,] _topMatrix;
    int[,] _botMatrix;
    int[,] _topOverlapMatrix;
    int[,] _botOverlapMatrix;
    
    int _topOverlapShiftX, _topOverlapShiftY;
    int _botOverlapShiftX, _botOverlapShiftY;

    int _id;

    void Awake()
    {
        _collisionChecker = GetComponentInChildren<LegoCollisionChecker>();
    }

    void Start()
    {
        LoadLookup();

        _topMatrix = Matrix<int>.GetOneMatrix(GridSizeX, GridSizeY);
        _botMatrix = Matrix<int>.GetOneMatrix(GridSizeX, GridSizeY);
        _topOverlapMatrix = Matrix<int>.GetOneMatrix(GridSizeX, GridSizeY);
        _botOverlapMatrix = Matrix<int>.GetOneMatrix(GridSizeX, GridSizeY);

        _id = GetComponent<AssemblyPiece>().ID;
        _connectionData = new LegoConnectionData();
        _connectionData.ID = _id;
        SetConnectionData();

        //Debug.Log("legoPiece: " + gameObject.GetInstanceID() + ", " + GetComponent<AssemblyPiece>().ID + ", " + GetComponentInChildren<Renderer>().material.color);
    }

    void SetConnectionData()
    {
        //Flipping the original part of the overlapping matrix to embed the piece information
        _connectionData.TopMatrix = Matrix<int>.FlipPart(_topOverlapMatrix, _topMatrix, _topOverlapShiftX, _topOverlapShiftY);
        _connectionData.BotMatrix = Matrix<int>.FlipPart(_botOverlapMatrix, _botMatrix, _botOverlapShiftX, _botOverlapShiftY);
        //Matrix<int>.Print2DArray(_connectionData.BotMatrix);
        _connectionData.TopArray = LegoConnectionData.ConvertToArray(_connectionData.TopMatrix);
        _connectionData.BotArray = LegoConnectionData.ConvertToArray(_connectionData.BotMatrix);

        _connectionData.TopConnections.Clear();
        foreach(var connection in _topConnections)
        {
            connection.Value.Store();
            _connectionData.TopConnections.Add(connection.Value);
        }

        _connectionData.BotConnections.Clear();
        foreach (var connection in _botConnections)
        {
            connection.Value.Store();
            _connectionData.BotConnections.Add(connection.Value);
        }
    }

    public void CreateMatricesAndConnectionPoints()
    {
        var startX = GridSizeX % 2 == 0 ? (1 + (GridSizeX * 0.5f - 1) * 2) * -_semiCPDistance : (GridSizeX - 1) * -_semiCPDistance;
        var startZ = GridSizeY % 2 == 0 ? (1 + (GridSizeY * 0.5f - 1) * 2) * _semiCPDistance : (GridSizeY - 1) * _semiCPDistance;

        for (int i = 0; i < GridSizeX; ++i)
        {
            for (int j = 0; j < GridSizeY; ++j)
            {
                //Top
                var cpGO = new GameObject("CP_" + 0 + "_" + i + "_" + j);
                var cp = cpGO.AddComponent<ConnectionPoint>();
                cp.Gender = ConnectionGender.Male;
                cp.TypeID = _cpTypeID;
                cpGO.transform.SetParent(transform);
                cpGO.transform.localPosition = new Vector3(startX + _semiCPDistance * 2 * i, _topYOffset, startZ - _semiCPDistance * 2 * j);
                cp.ID = int.Parse(2 + "" + i + "" + j);
                _lookup.Add(cp.ID, new Int3(i, j, 0));

                //Bot
                var cpGOBot = new GameObject("CP_" + 1 + "_" + i + "_" + j);
                var cpBot = cpGOBot.AddComponent<ConnectionPoint>();
                cpBot.Gender = ConnectionGender.Female;
                cpBot.TypeID = _cpTypeID;
                cpGOBot.transform.SetParent(transform);
                cpGOBot.transform.localPosition = new Vector3(startX + _semiCPDistance * 2 * i, _botYOffset, startZ - _semiCPDistance * 2 * j);
                cpBot.ID = int.Parse(1 + "" + i + "" + j);
                _lookup.Add(cpBot.ID, new Int3(i, j, 1));
            }
        }
    }

    void LoadLookup()
    {
        _lookup.Clear();
        var cps = GetComponentsInChildren<ConnectionPoint>();
        foreach (var cp in cps)
        {
            var ids = cp.gameObject.name.Split('_');
            _lookup.Add(cp.ID, new Int3(int.Parse(ids[2]), int.Parse(ids[3]), int.Parse(ids[1])));
            //Debug.Log("lookup | " + gameObject.name + "|" + cp.name + "|" + cp.ID + "|" + _lookup[cp.ID].A + "|" + _lookup[cp.ID].B);
        }
    }

    public int[,] GetMatrix(int cpID)
    {
        return _lookup[cpID].C == 0 ? _topMatrix : _botMatrix;
    }

    public int[,] GetMatrix(LegoSide side)
    {
        return side == LegoSide.Top ? _topMatrix : _botMatrix;
    }

    public int[,] GetOverlapMatrix(int cpID)
    {
        return _lookup[cpID].C == 0 ? _topOverlapMatrix : _botOverlapMatrix;
    }

    public Int3 GetIDPosition(int cpID)
    {
        return _lookup[cpID];
    }

    public Int3 GetIDPositionOverlap(int cpID)
    {
        var overlapPos = new Int3(_lookup[cpID]);
        overlapPos.A += overlapPos.C == 0 ? _topOverlapShiftX : _botOverlapShiftX;
        overlapPos.B += overlapPos.C == 0 ? _topOverlapShiftY : _botOverlapShiftY;
        return overlapPos;
    }

    public void Connect(LegoPiece otherPiece, ConnectionPoint otherCP, ConnectionPoint ownCP)
    {
        Debug.Log("connect: " + gameObject.name + " on " + otherPiece.gameObject.name);
        //Update the connectionmatrix

        //Check matrix rotation
        var angle = Vector3.SignedAngle(otherPiece.transform.right, transform.right, transform.up);
        angle *= -1;
        angle = angle < 0 ? angle + 360 : angle;
        var ownPos = new Int3(GetIDPositionOverlap(ownCP.ID)); 
        var otherPos = new Int3(otherPiece.GetIDPosition(otherCP.ID));
        //int[,] ownMatrix = RotateMatrix((int)angle, GetMatrix(ownCP.ID), ref ownPos.A, ref ownPos.B);
        //int[,] otherMatrix = otherPiece.GetMatrix(otherCP.ID);
        int[,] ownMatrix = GetOverlapMatrix(ownCP.ID);
        int[,] otherMatrix = RotateMatrix((int)angle, otherPiece.GetMatrix(otherCP.ID), ref otherPos.A, ref otherPos.B);

        //Overlap
        int leftShift = 0;
        int topShift = 0;
        var overlap = OverlapMatrices(ownMatrix, ownPos.A, ownPos.B, otherMatrix, otherPos.A, otherPos.B, ref leftShift, ref topShift);
        if (ownPos.C == 0)
        {
            _topOverlapMatrix = overlap;
            _topOverlapShiftX += leftShift;
            _topOverlapShiftY += topShift;
        }
        else
        {
            _botOverlapMatrix = overlap;
            _botOverlapShiftX += leftShift;
            _botOverlapShiftY += topShift;
            //Matrix<int>.Print2DArray(overlap);
        }

        var pos = GetIDPosition(ownCP.ID);
        var connection = new LegoConnection();
        connection.PieceID = otherCP.ParentPiece.ID;
        connection.Side = pos.C == 0 ? LegoSide.Top : LegoSide.Bot;
        connection.OverlapMatrix = OverlapMatrices(GetMatrix(ownCP.ID), pos.A, pos.B, otherMatrix, otherPos.A, otherPos.B, ref connection.ShiftLeft, ref connection.ShiftTop);
        connection.OtherMatrix = otherMatrix;
        connection.Pos = pos;
        connection.OtherPos = otherPos;
        if(connection.Side == LegoSide.Top)
        {
            Debug.Log("adding top side connection");
            _topConnections.Add(connection.PieceID, connection);
        }
        else
            _botConnections.Add(connection.PieceID, connection);

        SetConnectionData();
    }

    public void Disconnect(int id)
    {
        if (_topConnections.ContainsKey(id))
            _topConnections.Remove(id);

        if (_botConnections.ContainsKey(id))
            _botConnections.Remove(id);
    }

    int[,] GetOverlapMatrix(LegoSide side, out int leftShift, out int topShift, params int[] connectionIDs)
    {
        SetConnectionData();
        return GetOverlapMatrix(side, out leftShift, out topShift, _connectionData, connectionIDs);
    }

    int[,] GetOverlapMatrix(LegoSide side, out int leftShift, out int topShift, LegoConnectionData connectionData, params int[] connectionIDs)
    {
        var overlapMatrix = GetMatrix(side);
        var overlapLeftShift = 0;
        var overlapTopShift = 0;
        var connections = side == LegoSide.Top ? connectionData.TopConnections : connectionData.BotConnections;
        foreach (int id in connectionIDs)
        {
            if (!connections.Any(c => c.PieceID == id))
                continue;

            var connection = connections.FirstOrDefault(c => c.PieceID == id);
            connection.Load();
            var xShift = 0;
            var yShift = 0;
            overlapMatrix = OverlapMatrices(overlapMatrix, connection.Pos.A + overlapLeftShift, connection.Pos.B + overlapTopShift, connection.OtherMatrix, connection.OtherPos.A, connection.OtherPos.B, ref xShift, ref yShift);
            overlapLeftShift += xShift;
            overlapTopShift += yShift;
        }

        leftShift = overlapLeftShift;
        topShift = overlapTopShift;
        return overlapMatrix;
    }

    int[,] OverlapMatrices(int[,] matrixA, int aX, int aY, int[,] matrixB, int bX, int bY, ref int leftShift, ref int topshift)
    {
        var neededLeftA = aX;
        var neededRightA = matrixA.GetLength(0) - aX - 1;
        var neededTopA = aY;
        var neededBotA = matrixA.GetLength(1) - aY - 1;

        var neededLeftB = bX;
        var neededRightB = matrixB.GetLength(0) - bX - 1;
        var neededTopB = bY;
        var neededBotB = matrixB.GetLength(1) - bY - 1;

        var shiftLeftA = 0;
        var shiftTopA = 0;
        var shiftLeftB = 0;
        var shiftTopB = 0;

        var maxSizeX = 1;
        var maxSizeY = 1;

        if (neededLeftA > neededLeftB)
        {
            shiftLeftB = neededLeftA - neededLeftB;
            maxSizeX += neededLeftA;
        }
        else if (neededLeftA < neededLeftB)
        {
            shiftLeftA = neededLeftB - neededLeftA;
            maxSizeX += neededLeftB;
        }
        else
        {
            maxSizeX += neededLeftA;
        }

        if (neededTopA > neededTopB)
        {
            shiftTopB = neededTopA - neededTopB;
            maxSizeY += neededTopA;
        }
        else if (neededTopA < neededTopB)
        {
            shiftTopA = neededTopB - neededTopA;
            maxSizeY += neededTopB;
        }
        else
        {
            maxSizeY += neededTopA;
        }

        maxSizeX += neededRightA < neededRightB ? neededRightB : neededRightA;
        maxSizeY += neededBotA < neededBotB ? neededBotB : neededBotA;

        int[,] overlapped = new int[maxSizeX, maxSizeY];

        for (int i = 0; i < matrixA.GetLength(0); ++i)
        {
            for (int j = 0; j < matrixA.GetLength(1); ++j)
            {
                overlapped[i + shiftLeftA, j + shiftTopA] += matrixA[i, j];
            }
        }

        for (int i = 0; i < matrixB.GetLength(0); ++i)
        {
            for (int j = 0; j < matrixB.GetLength(1); ++j)
            {
                overlapped[i + shiftLeftB, j + shiftTopB] += matrixB[i, j];
            }
        }

        leftShift = shiftLeftA;
        topshift = shiftTopA;

        return overlapped;
    }

    int[,] RotateMatrix(int angle, int[,] matrix, ref int i, ref int j)
    {
        if (angle > 45 && angle < 135)
        {
            Matrix<int>.Rotate90(matrix, ref i, ref j);
            return Matrix<int>.Rotate90(matrix);
        }
        else if (angle > 45 && angle < 225)
        {
            Matrix<int>.Rotate180(matrix, ref i, ref j);
            return Matrix<int>.Rotate180(matrix);
        }
        else if (angle > 45 && angle < 315)
        {
            Matrix<int>.Rotate270(matrix, ref i, ref j);
            return Matrix<int>.Rotate270(matrix);
        }

        return matrix;
    }

    int[,] RotateMatrix(int angle, int[,] matrix)
    {
        if (angle > 45 && angle < 135)
        {
            return Matrix<int>.Rotate90(matrix);
        }
        else if (angle > 45 && angle < 225)
        {
            return Matrix<int>.Rotate180(matrix);
        }
        else if (angle > 45 && angle < 315)
        {
            return Matrix<int>.Rotate270(matrix);
        }

        return matrix;
    }

    public bool CheckConnection(int[,] topMatrix, int[,] botMatrix)
    {
        //Check matrices with current overlapmatrices, including rotation differences of 90 degrees
        for (int angle = 0; angle < 300; angle += 90)
        {
            var topMatrixRotated = RotateMatrix(angle, topMatrix);
            var botMatrixRotated = RotateMatrix(angle, botMatrix);
            if (Matrix<int>.Compare(topMatrixRotated, _connectionData.TopMatrix) &&
                Matrix<int>.Compare(botMatrixRotated, _connectionData.BotMatrix))
            {
                return true;
            }
        }

        return false;
    }

    bool CheckConnectionMatrices(int[,] aTop, int[,] aBot, int[,] bTop, int[,] bBot)
    {
        //The reason for doing both top and bottom together is because the angle needs to be the same 
        for (int angle = 0; angle < 300; angle += 90)
        {
            var topMatrixRotated = RotateMatrix(angle, bTop);
            var botMatrixRotated = RotateMatrix(angle, bBot);
            if (Matrix<int>.Compare(topMatrixRotated, aTop) &&
                Matrix<int>.Compare(botMatrixRotated, aBot))
            {
                return true;
            }
        }

        return false;
    }

    bool CheckConnectionMatricesSide(int[,] a, int[,] b)
    {
        for (int angle = 0; angle < 300; angle += 90)
        {
            var matrixRotated = RotateMatrix(angle, b);
            if (Matrix<int>.Compare(matrixRotated, a))
            {
                return true;
            }
        }

        return false;
    }

    public bool CheckConnection(LegoConnectionData connectionData, ref bool fullyChecked, bool requireAll = false)
    {
        var topCheckIds = connectionData.TopConnections.Select(c => c.PieceID).ToArray();
        var topIds = _connectionData.TopConnections.Select(c => c.PieceID).ToArray();
        var botCheckIds = connectionData.BotConnections.Select(c => c.PieceID).ToArray();
        var botIds = _connectionData.BotConnections.Select(c => c.PieceID).ToArray();

        var topIdSet = new HashSet<int>(topIds);
        var botIdSet = new HashSet<int>(botIds);
        // The connection can only be fully correct if the connected id's are the same
        fullyChecked = topIdSet.SetEquals(topCheckIds) && botIdSet.SetEquals(botCheckIds);
        if (requireAll && !fullyChecked)
        {
                return false;
        }

        // If the current piece is not connected to anything it is still correct
        if (topIds.Length == 0 && botIds.Length == 0)
            return true;

        // If the current piece is connected to pieces that it shouldn't be connected to the connection is wrong
        if (!topIdSet.IsSubsetOf(topCheckIds) || !botIdSet.IsSubsetOf(botCheckIds))
            return false;

        int topLeftShift, topTopShift;
        int botLeftShift, botTopShift;
        var topOverlapMatrix = GetOverlapMatrix(LegoSide.Top, out topLeftShift, out topTopShift, topIds);
        var botOverlapMatrix = GetOverlapMatrix(LegoSide.Bot, out botLeftShift, out botTopShift, botIds);
        int checkTopLeftShift, checkTopTopShift;
        int checkBotLeftShift, checkBotTopShift;
        var checkTopOverlapMatrix = GetOverlapMatrix(LegoSide.Top, out checkTopLeftShift, out checkTopTopShift, connectionData, topIds);
        var checkBotOverlapMatrix = GetOverlapMatrix(LegoSide.Bot, out checkBotLeftShift, out checkBotTopShift, connectionData, botIds);

        // Only check the bot matrices if there is no connection on top yet
        if (!requireAll && topIds.Length == 0)
            return CheckConnectionMatricesSide(botOverlapMatrix, checkBotOverlapMatrix);

        // Only check the top matrices if there is no connection on bottom yet
        if (!requireAll && botIds.Length == 0)
            return CheckConnectionMatricesSide(topOverlapMatrix, checkTopOverlapMatrix);

        // If both top and bottom have connections, the combined matrix is used to prevent wrong mirroring from resulting in a correct connection
        int overlapLeftShift = 0, overlapTopShift = 0;
        var combinedOverlapMatrix = OverlapMatrices(topOverlapMatrix, topLeftShift, topTopShift, botOverlapMatrix, botLeftShift, botTopShift, ref overlapLeftShift, ref overlapTopShift);
        int checkOverlapLeftShift = 0, checkOverlapTopShift = 0;
        var combinedCheckOverlapMatrix = OverlapMatrices(checkTopOverlapMatrix, checkTopLeftShift, checkTopTopShift, checkBotOverlapMatrix, checkBotLeftShift, checkBotTopShift, ref checkOverlapLeftShift, ref checkOverlapTopShift);
        return CheckConnectionMatricesSide(combinedOverlapMatrix, combinedCheckOverlapMatrix);
        //return CheckConnectionMatrices(topOverlapMatrix, botOverlapMatrix, checkTopOverlapMatrix, checkBotOverlapMatrix);
    }

    public void Reset()
    {

        _topOverlapMatrix = Matrix<int>.GetOneMatrix(GridSizeX, GridSizeY);
        _botOverlapMatrix = Matrix<int>.GetOneMatrix(GridSizeX, GridSizeY);
        SetConnectionData();
    }
}

public class Matrix<T>
{
    public static T[,] TransposeMatrix(T[,] matrix)
    {
        var rows = matrix.GetLength(0);
        var columns = matrix.GetLength(1);

        var result = new T[columns, rows];

        for (var c = 0; c < columns; c++)
        {
            for (var r = 0; r < rows; r++)
            {
                result[c, r] = matrix[r, c];
            }
        }

        return result;
    }

    public static T[,] Rotate90(T[,] matrix)
    {
        var rows = matrix.GetLength(0);
        var columns = matrix.GetLength(1);

        var result = new T[columns, rows];

        for (var c = 0; c < columns; c++)
        {
            for (var r = 0; r < rows; r++)
            {
                result[columns - 1 - c, r] = matrix[r, c];
            }
        }

        return result;
    }

    public static void Rotate90(T[,] matrix, ref int i, ref int j)
    {
        var iOld = i;
        i = matrix.GetLength(1) - 1 - j;
        j = iOld;
    }

    public static T[,] Rotate180(T[,] matrix)
    {
        var rows = matrix.GetLength(1);
        var columns = matrix.GetLength(0);

        var result = new T[columns, rows];

        for (var c = 0; c < columns; c++)
        {
            for (var r = 0; r < rows; r++)
            {
                result[columns - 1 - c, rows - 1 - r] = matrix[c, r];
            }
        }

        return result;
    }

    public static void Rotate180(T[,] matrix, ref int i, ref int j)
    {
        i = matrix.GetLength(0) - 1 - i;
        j = matrix.GetLength(1) - 1 - j;
    }

    public static T[,] Rotate270(T[,] matrix)
    {
        var rows = matrix.GetLength(0);
        var columns = matrix.GetLength(1);

        var result = new T[columns, rows];

        for (var c = 0; c < columns; c++)
        {
            for (var r = 0; r < rows; r++)
            {
                result[c, rows - 1 - r] = matrix[r, c];
            }
        }

        return result;
    }

    public static void Rotate270(T[,] matrix, ref int i, ref int j)
    {
        var iOld = i;
        i = j;
        j = matrix.GetLength(0) - 1 - iOld;
    }

    public static void Print2DArray<T>(T[,] matrix)
    {
        for (int j = 0; j < matrix.GetLength(1); j++)
        {
            var line = "[";
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                line += " " + matrix[i, j];
            }

            line += "]";
            Debug.Log(line);
        }
    }

    public static int[,] GetOneMatrix(int cols, int rows)
    {
        var matrix = new int[cols, rows];
        for (var c = 0; c < cols; c++)
        {
            for (var r = 0; r < rows; r++)
            {
                matrix[c, r] = 1;
            }
        }

        return matrix;
    }

    public static bool Compare(int[,] a, int[,] b)
    {
        if (a.GetLength(0) != b.GetLength(0) || a.GetLength(1) != b.GetLength(1))
            return false;

        //Debug.Log("comparing: ");
        //Matrix<int>.Print2DArray(a);
        //Debug.Log("to: ");
        //Matrix<int>.Print2DArray(a);
        //Debug.Log("--------------------");

        for (var c = 0; c < a.GetLength(0); c++)
        {
            for (var r = 0; r < a.GetLength(1); r++)
            {
                if (a[c, r] != b[c, r])
                    return false;
            }
        }

        return true;
    }

    public static int[,] FlipPart(int[,] matrix, int[,] part, int leftShift, int topShift)
    {
        int[,] returnMatrix = matrix.Clone() as int[,];
        for (var c = 0; c < part.GetLength(0); c++)
        {
            for (var r = 0; r < part.GetLength(1); r++)
            {
                returnMatrix[c+leftShift, r+topShift] *= -1;
            }
        }

        return returnMatrix;
    }
}

