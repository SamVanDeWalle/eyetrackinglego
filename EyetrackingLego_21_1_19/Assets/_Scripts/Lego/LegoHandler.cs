﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.UI;

[Serializable]
public class LegoAssembly
{
    public List<LegoConnectionData> ConnectionData = new List<LegoConnectionData>();

    [NonSerialized]
    public static string Path = @"Lego\";
    public static string FileName = "Lego_Assembly_";
    public static string FullPath { get { return Directory.GetCurrentDirectory() + @"\Datapath\" + Path; } }
    const string EXT = ".json";

    public static void Save(LegoAssembly assembly, int index)
    {
        string json = JsonUtility.ToJson(assembly, true);

        try
        {
            Directory.CreateDirectory(FullPath);
            StreamWriter writer = new StreamWriter(FullPath + FileName + index + EXT, false, Encoding.Default);
            using (writer)
            {
                writer.Write(json);
            }
        }
        catch (Exception e)
        {
            Debug.Log(string.Format("{0}\n", e.Message));
        }
    }

    public static LegoAssembly Load(int index)
    {
        string json;

        try
        {
            StreamReader reader = new StreamReader(FullPath + FileName + index + EXT, Encoding.Default);
            using (reader)
            {
                json = reader.ReadToEnd();
            }
        }
        catch (Exception e)
        {
            Debug.Log(string.Format("{0}\n", e.Message));
            return new LegoAssembly();
        }

        LegoAssembly assembly = JsonUtility.FromJson<LegoAssembly>(json);
        assembly.Load();
        return assembly;
    }

    public void Load()
    {
        foreach (var legoConnectionData in ConnectionData)
        {
            legoConnectionData.Load();
        }
    }
}

public class LegoHandler : MonoBehaviour
{
    public Image FeedbackImage;
    public Transform AssemblyCenter;
    public bool CheckContinuous;

    Dictionary<int, LegoAssembly> _legoAssemblies = new Dictionary<int, LegoAssembly>();
    Dictionary<int, GameObject> _previews = new Dictionary<int, GameObject>();
    static List<LegoPiece> _legoPieces = new List<LegoPiece>();

    ViveController _previewController;
    GameObject _preview;
    int _assemblyIndex;

    void Awake()
    {
        ConnectionPoint.OnGlobalConnected += ConnectionMade;
        ConnectionPoint.OnGlobalDisconnected += ConnectionLost;
        TrainingManager.OnReset += Reset;
    }

    void Start()
    {
        LoadLegoPieces();
        LoadAssemblies();
        LoadPreviews();
        _previewController = ViveHelper.Controllers[ControllerIndex.Left];
        SetPreview(_assemblyIndex, _previewController.Transform);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            _assemblyIndex = 0;
            SetPreview(_assemblyIndex, _previewController.Transform);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            _assemblyIndex = 1;
            SetPreview(_assemblyIndex, _previewController.Transform);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            _assemblyIndex = 2;
            SetPreview(_assemblyIndex, _previewController.Transform);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            _assemblyIndex = 3;
            SetPreview(_assemblyIndex, _previewController.Transform);
        }

        if (Input.GetKeyDown(KeyCode.End))
        {
            SaveAssembly(_assemblyIndex);
        }

        if (Input.GetKeyDown(KeyCode.Home))
        {
            CheckAssembly(_assemblyIndex);
        }

        if (_previewController.GetPressUp(ControllerButton.Menu))
        {
            TogglePreviewVisibility();
        }

        if(CheckContinuous)
            CheckAssembly(_assemblyIndex);
    }

    void LoadLegoPieces()
    {
        _legoPieces = FindObjectsOfType<LegoPiece>().ToList();
    }

    void LoadAssemblies()
    {
        _legoAssemblies.Clear();
        var files = Directory.GetFiles(LegoAssembly.FullPath);

        for (int i = 0; i < files.Length; ++i)
        {
            if (files[i].Contains("Lego_Assembly_"))
            {
                var id = int.Parse(files[i].Replace(LegoAssembly.FullPath, "").Replace("Lego_Assembly_", "").Replace(".json", ""));
                _legoAssemblies.Add(id, LegoAssembly.Load(id));
                Debug.Log("adding assembly : " + id);
            }
        }
    }

    void LoadPreviews()
    {
        var gos = Resources.LoadAll<GameObject>("Prefabs/Assemblies/Lego_001/Preview/");
        foreach (var go in gos)
        {
            var id = int.Parse(go.name.Split('_')[1]);
            _previews.Add(id, go);
        }
    }

    void SetPreview(int id, Transform parent, bool visible = true)
    {
        if (!_previews.ContainsKey(id))
            return;

        if(_preview != null)
            Destroy(_preview);
        _preview = Instantiate(_previews[id]);
        _preview.transform.SetParent(parent);
        _preview.transform.localPosition = Vector3.zero + new Vector3(0, 0, 0.1f);
        _preview.transform.localRotation = Quaternion.identity;
        _preview.transform.Rotate(65, 0, 0);
        _preview.SetActive(visible);
        var control = _preview.AddComponent<LegoPreviewControl>();
        control.Controller = _previewController;
    }

    void TogglePreviewVisibility()
    {
        _preview.SetActive(!_preview.activeSelf);
    }

    void ConnectionMade(ConnectionPoint cp)
    {
        //Debug.Log("Connection made");
        var legoPieceA = cp.ParentPiece.GetComponent<LegoPiece>();
        var legoPieceB = cp.ConnectedCP.ParentPiece.GetComponent<LegoPiece>();

        if (legoPieceA == null || legoPieceB == null)
            return;

        legoPieceA.Connect(legoPieceB, cp.ConnectedCP, cp);
        PythonCom.SendPackage.ConnectionMale = legoPieceA.GetComponent<AssemblyPiece>().ID;
        PythonCom.SendPackage.ConnectionFemale = legoPieceB.GetComponent<AssemblyPiece>().ID;

        if (CheckContinuous)
            CheckAssembly(_assemblyIndex);
    }

    void ConnectionLost(ConnectionPoint cp, ConnectionPoint otherCP)
    {
        var legoPieceA = cp.ParentPiece.GetComponent<LegoPiece>();

        legoPieceA.Disconnect(otherCP.ParentPiece.ID);

        if (CheckContinuous)
            CheckAssembly(_assemblyIndex);
    }

    public void SaveAssembly(int index)
    {
        //Store lego pieces id's with current connectionmatrices

        var legoAssembly = new LegoAssembly();
        legoAssembly.ConnectionData.Clear();

        foreach (var legoPiece in _legoPieces)
        {
            legoAssembly.ConnectionData.Add(legoPiece.ConnectionData);
        }

        _legoAssemblies[index] = legoAssembly;
        LegoAssembly.Save(_legoAssemblies[index], index);

        SaveAssemblyShape(index);
    }

    void SaveAssemblyShape(int index)
    {
        //Store all pieces in a combined mesh prefab
        var previewGO = new GameObject("Preview_" + index);
        previewGO.transform.position = AssemblyCenter.transform.position;
        foreach (var legoPiece in _legoPieces)
        {
            var renderers = legoPiece.GetComponent<AssemblyPiece>().Renderers;
            foreach (var renderer in renderers)
            {
                var renderGO = renderer.gameObject;
                var subGO = GameObject.Instantiate(renderGO);
                subGO.name = "Preview_Part";
                var colliders = subGO.GetComponents<Collider>();
                for(int i = colliders.Length - 1; i >= 0; --i)
                {
                    Destroy(colliders[i]);
                }

                subGO.transform.position = legoPiece.transform.position;
                subGO.transform.rotation = legoPiece.transform.rotation;
                subGO.transform.Rotate(-90, 0, 0);
                subGO.transform.SetParent(previewGO.transform);
            }
        }

        previewGO.transform.localScale = Vector3.one * 0.1f;
        SetPreview(_assemblyIndex, _previewController.Transform);
    }

    /*
    public void CheckAssemblyOld(int index)
    {
        //Check current assembly with saved assembly
        var legoPieces = new List<LegoPiece>(_legoPieces);
        var correct = true;
        foreach (var legoConnectionData in _legoAssemblies[_assemblyIndex].ConnectionData)
        {
            var id = legoConnectionData.TypeID;
            var pieces = legoPieces.Where(lp => lp.TypeID == id).ToList();
            var subCorrect = false;
            foreach (var legoPiece in pieces)
            {
                if (legoPiece.CheckConnection(legoConnectionData.TopMatrix, legoConnectionData.BotMatrix))
                {
                    subCorrect = true;
                    legoPieces.Remove(legoPiece);
                    break;
                }
            }

            if (!subCorrect)
                correct = false;
        }

        // Feedback
        FeedbackImage.color = correct ? Color.green : Color.red;
    }*/

    public void CheckAssembly(int index)
    {
        if (!_legoAssemblies.ContainsKey(index))
        {
            FeedbackImage.color = Color.black;
            return;
        }

        var correct = true;
        var fullyCorrect = true;
        foreach (var legoConnectionData in _legoAssemblies[_assemblyIndex].ConnectionData)
        {
            var id = legoConnectionData.ID;
            var legoPiece = _legoPieces.FirstOrDefault(lp => lp.ID == id);
            bool fullyChecked = false;
            if(legoPiece != null && !legoPiece.CheckConnection(legoConnectionData, ref fullyChecked, false))
            {
                correct = false;
                break;
            }

            if (!fullyChecked)
                fullyCorrect = false;
        }

        FeedbackImage.color = correct ? fullyCorrect ? Color.green : Color.blue : Color.red;
    }

    public static bool CheckCollision(LegoPiece piece)
    {
        for(int i = 0; i < _legoPieces.Count; ++i)
        {
            if (_legoPieces[i] == piece)
                continue;

            if (_legoPieces[i].OverlapBounds.Intersects(piece.GhostOverlapBounds))
                return false;
        }

        return true;
    }

    void Reset()
    {
        foreach (var legoPiece in _legoPieces)
        {
            Reset();
        }
    }
}
