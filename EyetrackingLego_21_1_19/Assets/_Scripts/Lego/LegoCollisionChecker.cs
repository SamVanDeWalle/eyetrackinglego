﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegoCollisionChecker : MonoBehaviour
{
    public Bounds Bounds { get { return _collider.bounds; } private set { } }

    Collider _collider;

    void Awake()
    {
        _collider = GetComponent<Collider>();    
    }
}
