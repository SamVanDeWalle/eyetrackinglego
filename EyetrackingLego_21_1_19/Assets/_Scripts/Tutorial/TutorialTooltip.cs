﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialTooltip : MonoBehaviour
{
    public List<Transform> Anchors = new List<Transform>();
    public Text Text;
    public Transform TargetPoint;
    public LineRenderer LineRenderer;
    public Transform TargetDot;
    public GameObject ControllerAnim;

    Transform _anchor;

    void Update()
    {
        if (TargetPoint == null)
            return;

        SetClosestAnchor();

        LineRenderer.SetPosition(0, _anchor.position);
        LineRenderer.SetPosition(1, TargetPoint.position);

        TargetDot.position = TargetPoint.position;
    }

    void SetClosestAnchor()
    {
        var closestAnchor = Anchors[0];
        var closestDistance = (TargetPoint.position - Anchors[0].position).sqrMagnitude;
        for(int i = 1; i < Anchors.Count; ++i)
        {
            var distance = (TargetPoint.position - Anchors[i].position).sqrMagnitude;
            if(distance < closestDistance)
            {
                closestDistance = distance;
                closestAnchor = Anchors[i];
            }
        }

        _anchor = closestAnchor;
    }
}
