﻿/*
 * 
 * Scripted tutorial for Picanol demo
 * 
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.CompilerServices;
using UnityEngine.UI;
using System.Linq;

public enum TutorialStateName
{
    LookAtObject,
    Pickup,
    PickupExisting,
    Place,
    PickupNew,
    PlaceOnObject,
    PushButton,
    WorkbenchRotation,
    WorkbenchRotation_Back,
    Brush,
    ContainerAndScrew,
    Screwing,
    Billboards,
    Tablet
}

public class TutorialState
{
    public TutorialStateName stateName;
    public float PostDelay = 2.2f;

    public delegate void ActionDelegate();
    public event ActionDelegate OnDone;

    public virtual void Set() {}
    public virtual void UnSet() {}

    public virtual void Done()
    {
        UnSet();
        TutorialManager.SetTooltipText("Goed!");
        if (OnDone != null)
            OnDone();
    }
}

public class LookAtObject : TutorialState
{
    public override void Set()
    {
        base.Set();

        PostDelay = 3.5f;
        var go = TutorialManager.GetSceneObject("SO_0");
        if (go)
        {
            go.SetActive(true);
            TutorialManager.SetTooltipTarget(go.transform);
            GuideUtils.Highlight(go.GetComponent<Pickable>());
            ViewGuide.OnLookedAtTarget += Done;
        }

        TutorialManager.SetTooltipText("Zoek naar het blauw oplichtend object");
    }

    public override void UnSet()
    {
        base.UnSet();

        ViewGuide.OnLookedAtTarget -= Done;
    }

    public override void Done()
    {
        base.Done();
    }
}

public class PickupState : TutorialState
{
    public override void Set()
    {
        base.Set();

        var spawnObject = TutorialManager.SpawnObject("AP_0", "AP_SPAWN_0");
        if (spawnObject)
        {
            spawnObject.GetComponentInChildren<Rigidbody>().isKinematic = true;
            var piece = spawnObject.GetComponent<AssemblyPiece>();
            TutorialManager.SetTooltipTarget(spawnObject.transform);
            AssemblyPieceManager.AddPiece(piece);
        }

        TutorialManager.SetTooltipText("Hou de trekker ingedrukt en wijs naar het object, laat vervolgens de trekker los om het object vast te nemen.");
        PickupController.OnPickedUpGlobal += Done;
    }

    public override void UnSet()
    {
        base.UnSet();

        PickupController.OnPickedUpGlobal -= Done;
    }

    public override void Done()
    {
        base.Done();
    }
}

public class PickupExistingState : TutorialState
{
    public override void Set()
    {
        base.Set();

        var go = TutorialManager.GetSceneObject("SO_0");
        if (go)
        {
            go.SetActive(true);
            GuideUtils.Highlight(go.GetComponent<Pickable>());
            TutorialManager.SetTooltipTarget(go.transform);
            TutorialManager.ActivateControllerAnim(true);
        }

        TutorialManager.SetTooltipText("Hou de trekker ingedrukt en wijs naar het object, laat vervolgens de trekker los om het object vast te nemen.");
        PickupController.OnPickedUpGlobal += Done;
    }

    public override void UnSet()
    {
        base.UnSet();

        GuideUtils.UnHighlightAll();
        TutorialManager.ActivateControllerAnim(false);
        PickupController.OnPickedUpGlobal -= Done;
    }

    public override void Done()
    {
        base.Done();
    }
}

public class PlaceState : TutorialState
{
    AssemblyPiece _piece;
    AssemblyPiece _workbench;
    ConnectionMatch _match;

    public override void Set()
    {
        base.Set();

        _piece = TutorialManager.GetSceneObject("SO_0").GetComponent<AssemblyPiece>();
        _workbench = TutorialManager.GetSceneObject("SO_1").GetComponent<AssemblyPiece>();
        _match = ConnectionManager.FindClosestConnectionMatch(_piece, _workbench);
        PieceLeftValid();
    }

    public override void UnSet()
    {
        base.UnSet();

        _match.A.OnConnected -= Placed;
        _piece.OnLeftValid -= PieceLeftValid;
        _piece.Pickable.Controller.OnGhostConnectionLost -= PieceLeftValid;
        GuideUtils.StopGuidingConnection();
    }

    public override void Done()
    {
        base.Done();
    }

    void PieceEnteredValid()
    {
        GuideUtils.StopGuidingConnection();
        TutorialManager.SetTooltipText("Haal de trekker opnieuw over om het object te plaatsen.");
        TutorialManager.ActivateControllerAnim(true);
        _piece.OnEnteredValid -= PieceEnteredValid;
        _piece.Pickable.Controller.OnValidGhostConnection -= PieceEnteredValid;
        _piece.OnLeftValid += PieceLeftValid;
        _piece.Pickable.Controller.OnGhostConnectionLost += PieceLeftValid;
        _match.A.OnConnected += Placed;
    }

    void PieceLeftValid()
    {
        TutorialManager.ActivateControllerAnim(false);
        GuideUtils.GuideConnection(_match.A, _match.B);
        TutorialManager.SetTooltipText("Hou het object op de werkbank tot er een doorschijnende blauwe versie van het object boven verschijnt.");
        TutorialManager.SetTooltipTarget(_match.B.transform);
        _piece.OnLeftValid -= PieceLeftValid;
        _piece.Pickable.Controller.OnGhostConnectionLost -= PieceLeftValid;
        _piece.OnEnteredValid += PieceEnteredValid;
        _piece.Pickable.Controller.OnValidGhostConnection += PieceEnteredValid;
    }

    void Placed()
    {
        TutorialManager.ActivateControllerAnim(false);
        _piece.IsLocked = true;
        _piece.IsPickable = false;
        Done();
    }
}

public class PickupNewState : TutorialState
{
    Pickable _pickable;

    public override void Set()
    {
        base.Set();

        _pickable = TutorialManager.GetSceneObject("SO_2").GetComponent<Pickable>();
        GuideUtils.Highlight(_pickable);
        _pickable.OnPickedUp += Done;

        TutorialManager.SetTooltipText("Zoek het volgende blauwe object en neem het vast op dezelfde manier als daarnet.");
        TutorialManager.ActivateControllerAnim(true);
        TutorialManager.SetTooltipTarget(_pickable.transform);
    }

    public override void UnSet()
    {
        base.UnSet();

        _pickable.OnPickedUp -= Done;
        GuideUtils.UnHighlightAll();
        TutorialManager.ActivateControllerAnim(false);
    }

    public override void Done()
    {
        base.Done();
    }
}

public class PlaceOnObjectState : TutorialState
{
    ConnectionMatch _match;

    public override void Set()
    {
        base.Set();

        var piece = TutorialManager.GetSceneObject("SO_2").GetComponent<AssemblyPiece>();
        var targetPiece = TutorialManager.GetSceneObject("SO_0").GetComponent<AssemblyPiece>();
        _match = ConnectionManager.FindClosestConnectionMatch(piece, targetPiece);
        GuideUtils.GuideConnection(_match.A, _match.B);
        TutorialManager.SetTooltipText("Plaats het nieuwe object op het vorige door de aangegeven pijlen te volgen.");
        TutorialManager.SetTooltipTarget(_match.B.transform);
        _match.A.OnConnected += Done;
    }

    public override void UnSet()
    {
        base.UnSet();

        _match.A.OnConnected -= Done;
    }

    public override void Done()
    {
        base.Done();
    }
}

public class PushButtonState : TutorialState
{
    PushButton _button;

    public override void Set()
    {
        base.Set();

        _button = TutorialManager.GetSceneObject("SO_Button_0").GetComponent<PushButton>();
        _button.OnPressed += Done;
        TutorialManager.SetTooltipText("Kom met de controller tegen de knoppen op de draaibak om deze te roteren");
        var buttonCenter = TutorialManager.GetSceneObject("SO_ButtonCenter").transform;
        TutorialManager.SetTooltipTarget(buttonCenter);
    }

    public override void UnSet()
    {
        base.UnSet();

        _button.OnPressed -= Done;
    }

    public override void Done()
    {
        base.Done();
    }
}

public class WorkbenchRotationState : TutorialState
{
    WorkbenchGuide _workbenchGuide;

    public override void Set()
    {
        base.Set();

        _workbenchGuide = TutorialManager.GetSceneObject("SO_WorkbenchGuide").GetComponent<WorkbenchGuide>();
        _workbenchGuide.GuideDesiredAnge(true, 90);
        _workbenchGuide.StickIndicator.OnDesiredReached += Done;
        TutorialManager.SetTooltipText("Roteer de draaibak tot de pijlen verdwijnen");
        var buttonCenter = TutorialManager.GetSceneObject("SO_ButtonCenter").transform;
        TutorialManager.SetTooltipTarget(buttonCenter);
    }

    public override void UnSet()
    {
        base.UnSet();

        _workbenchGuide.StickIndicator.OnDesiredReached -= Done;
    }

    public override void Done()
    {
        base.Done();
    }
}

public class WorkbenchRotationBackState : TutorialState
{
    WorkbenchGuide _workbenchGuide;

    public override void Set()
    {
        base.Set();

        _workbenchGuide = TutorialManager.GetSceneObject("SO_WorkbenchGuide").GetComponent<WorkbenchGuide>();
        _workbenchGuide.GuideDesiredAnge(true, 0);
        _workbenchGuide.StickIndicator.OnDesiredReached += Done;
        TutorialManager.SetTooltipText("Roteer de draaibak terug tot de pijlen verdwijnen");
        var buttonCenter = TutorialManager.GetSceneObject("SO_ButtonCenter").transform;
        TutorialManager.SetTooltipTarget(buttonCenter);
    }

    public override void UnSet()
    {
        base.UnSet();

        _workbenchGuide.StickIndicator.OnDesiredReached -= Done;
    }

    public override void Done()
    {
        base.Done();
    }
}

public class BrushState : TutorialState
{
    Brush _brush;
    AssemblyPiece _target;
    Property _paintSurface;

    public override void Set()
    {
        base.Set();

        TutorialManager.SetTooltipText("Zoek de borstel en neem deze vast");
        _brush = TutorialManager.GetSceneObject("SO_Brush").GetComponent<Brush>();
        _target = TutorialManager.GetSceneObject("SO_BrushTarget").GetComponent<AssemblyPiece>();
        _paintSurface = _target.GetComponent<PropertyHolder>().GetProperties(PropertyType.LoctitePart)[0];
        _brush.Pickable.OnPickedUp += BrushPickedUp;
        GuideUtils.Highlight(_brush.Pickable);
        TutorialManager.SetTooltipTarget(_brush.transform);
    }

    void BrushPickedUp()
    {
        TutorialManager.SetTooltipText("Raak met de borstel het aangegeven object aan om dit in te smeren");
        GuideUtils.Highlight(_target.Pickable);
        TutorialManager.SetTooltipTarget(_target.transform);
        _paintSurface.Guide();
        _paintSurface.OnThisPropertyResolved += Done;
    }

    public override void UnSet()
    {
        base.UnSet();

        GuideUtils.UnHighlightAll();
        _paintSurface.OnThisPropertyResolved -= Done;
    }

    public override void Done()
    {
        base.Done();

        TutorialManager.SetTooltipText("Goed!");
    }
}

public class ContainerAndScrewState : TutorialState
{
    AssemblyPieceContainer _container;
    Screwdriver _screwdriver;
    AssemblyPiece _spawnedPiece;
    AssemblyPiece _target;
    ConnectionMatch _match;

    public override void Set()
    {
        base.Set();

        var autoTarget = TutorialManager.GetSceneObject("SO_0").GetComponent<AssemblyPiece>();
        var autoPiece = TutorialManager.GetSceneObject("SO_BrushTarget").GetComponent<AssemblyPiece>();
        var match = ConnectionManager.FindClosestConnectionMatch(autoPiece, autoTarget);
        autoPiece.AutoConnectOverTime(match.A, match.B);
        var autoPiece2 = TutorialManager.GetSceneObject("SO_3").GetComponent<AssemblyPiece>();
        var match2 = ConnectionManager.FindClosestConnectionMatch(autoPiece2, autoTarget);
        autoPiece2.AutoConnectOverTime(match2.A, match2.B);
        var autoPiece3 = TutorialManager.GetSceneObject("SO_4").GetComponent<AssemblyPiece>();
        var match3 = ConnectionManager.FindClosestConnectionMatch(autoPiece3, autoTarget);
        autoPiece3.AutoConnectOverTime(match3.A, match3.B);

        _container = TutorialManager.GetSceneObject("SO_Container").GetComponent<AssemblyPieceContainer>();
        _screwdriver = TutorialManager.GetSceneObject("SO_Screwdriver").GetComponent<Screwdriver>();
        _target = autoPiece;
        PickContainerPhase();
    }

    void PickContainerPhase()
    {
        GuideUtils.Highlight(_container.Pickable);
        TutorialManager.SetTooltipTarget(_container.transform);
        TutorialManager.SetTooltipText("Haal een vijs uit het aangegeven bakje door ernaar te wijzen en vervolgens de trekker los te laten");

        PickupController.OnAPPickedUpGlobal += PickedUpAP;
    }

    void PickedUpAP(AssemblyPiece piece)
    {
        if(piece.TypeID == _container.APTypeID)
        {
            _spawnedPiece = piece;
            PickupScrewdriverPhase();
        }
    }

    void PickupScrewdriverPhase()
    {
        GuideUtils.Highlight(_screwdriver.Pickable);
        TutorialManager.SetTooltipTarget(_screwdriver.transform);
        TutorialManager.SetTooltipText("Neem met de andere controller de schroevendraaier vast");

        _screwdriver.Pickable.OnPickedUp += PrePosePhase;
    }

    void PrePosePhase()
    {
        GuideUtils.UnHighlightAll();
        _match = ConnectionManager.FindClosestConnectionMatch(_spawnedPiece, _target);
        TutorialManager.SetTooltipTarget(_match.B.transform);
        TutorialManager.SetTooltipText("Hou de vijs dichtbij de aangegeven plaats");
        _spawnedPiece.OnEnteredPrePose += ScrewPhase;
    }

    void ScrewPhase()
    {
        TutorialManager.SetTooltipTarget(_screwdriver.transform);
        TutorialManager.SetTooltipText("Kom met de schroevendraaier tegen de vijs om deze te connecteren");
        _match.A.OnConnected += Done;
    }

    public override void UnSet()
    {
        base.UnSet();
    }

    public override void Done()
    {
        base.Done();
    }
}

[Serializable]
public class TutorialSpawnObject
{
    public string Name;
    public GameObject Prefab;
}

[Serializable]
public class TutorialSpawnPosition
{
    public string Name;
    public Vector3 Position;
    public Vector3 Rotation;
    public Transform Parent;
}

[Serializable]
public class TutorialSceneObject
{
    public string Name;
    public GameObject Object;
}

public class TutorialManager : MonoBehaviour
{
    public Text TooltipText;
    public TutorialTooltip Tooltip;
    public List<TutorialStateName> States = new List<TutorialStateName>();
    public List<TutorialSpawnObject> SpawnObjectPool = new List<TutorialSpawnObject>();
    public List<TutorialSpawnPosition> SpawnPositionPool = new List<TutorialSpawnPosition>();
    public List<TutorialSceneObject> SceneObjects = new List<TutorialSceneObject>();

    //Dictionary<TutorialStateName, Type> _statePool = new Dictionary<TutorialStateName, Type>()
    //{
    //    { TutorialStateName.Pickup, typeof(PickupState) },
    //    { TutorialStateName.Place, typeof(PlaceState) },
    //};

    Dictionary<TutorialStateName, TutorialState> _statePool = new Dictionary<TutorialStateName, TutorialState>()
    {
        { TutorialStateName.LookAtObject, new LookAtObject() },
        { TutorialStateName.Pickup, new PickupState() },
        { TutorialStateName.PickupExisting, new PickupExistingState() },
        { TutorialStateName.Place, new PlaceState() },
        { TutorialStateName.PickupNew, new PickupNewState() },
        { TutorialStateName.PlaceOnObject, new PlaceOnObjectState() },
        { TutorialStateName.PushButton, new PushButtonState() },
        { TutorialStateName.WorkbenchRotation, new WorkbenchRotationState() },
        { TutorialStateName.WorkbenchRotation_Back, new WorkbenchRotationBackState() },
        { TutorialStateName.Brush, new BrushState() },
        { TutorialStateName.ContainerAndScrew, new ContainerAndScrewState() }
    };

    List<TutorialState> _states = new List<TutorialState>();
    int _stateIndex = -1;

    static Dictionary<string, TutorialSpawnObject> _spawnObjectPool = new Dictionary<string, TutorialSpawnObject>();
    static Dictionary<string, TutorialSpawnPosition> _spawnPosPool = new Dictionary<string, TutorialSpawnPosition>();
    static Dictionary<string, TutorialSceneObject> _sceneObjectPool = new Dictionary<string, TutorialSceneObject>();
    static Text _tooltipText;
    static TutorialTooltip _tooltip;

    void Start()
    {
        _tooltipText = TooltipText;
        _tooltip = Tooltip;
        LoadSpawnObjectPool();
        LoadSpawnPosPool();
        LoadSceneObjectPool();

        LoadStates();
        StartTutorial();
    }

    void LoadStates()
    {
        _states.Clear();
        foreach (var stateName in States)
        {
            //var type = _statePool[stateName];
            //dynamic state = Activator.CreateInstance(_statePool[stateName]);
            //Debug.Log(state.GetType());
            //_states.Add(state);
            _states.Add(_statePool[stateName]);
        }
    }

    void LoadSpawnObjectPool()
    {
        _spawnObjectPool.Clear();
        foreach(var so in SpawnObjectPool)
        {
            if(_spawnObjectPool.ContainsKey(so.Name))
            {
                Debug.LogError("TutorialManager::LoadSpawnObjectPool - Multiple SpawnObjects given with same name");
                break;
            }

            _spawnObjectPool.Add(so.Name, so);
        }
    }

    void LoadSpawnPosPool()
    {
        _spawnPosPool.Clear();
        foreach (var sp in SpawnPositionPool)
        {
            if (_spawnPosPool.ContainsKey(sp.Name))
            {
                Debug.LogError("TutorialManager::LoadSpawnPosPool - Multiple SpawnPositions given with same name");
                break;
            }

            _spawnPosPool.Add(sp.Name, sp);
        }
    }

    void LoadSceneObjectPool()
    {
        _sceneObjectPool.Clear();
        foreach(var so in SceneObjects)
        {
            if (_sceneObjectPool.ContainsKey(so.Name))
            {
                Debug.LogError("TutorialManager::LoadSceneObjectPool - Multiple SceneObjects given with same name");
                break;
            }

            _sceneObjectPool.Add(so.Name, so);
        }
    }

    void StartTutorial()
    {
        if(_states.Count == 0)
        {
            Debug.LogWarning("TutorialManager::StartTutorial - Started tutorial without any given states");
            return;
        }

        NextState();
    }

    void NextState()
    {
        if(_stateIndex >= 0)
        {
            _states[_stateIndex].OnDone -= CurrentStateDone;
        }

        ++_stateIndex;
        var postDelay = _stateIndex - 1 < 0 ? 0.0f : _states[_stateIndex - 1].PostDelay;
        StartCoroutine(StartState(_stateIndex, postDelay));
    }

    IEnumerator StartState(int index, float delay)
    {
        yield return new WaitForSeconds(delay);

        StartState(index);
    }

    void StartState(int index)
    {
        if (index >= _states.Count)
        {
            TutorialComplete();
            return;
        }

        _states[_stateIndex].OnDone += CurrentStateDone;
        _states[_stateIndex].Set();
    }

    void CurrentStateDone()
    {
        NextState();
    }

    void TutorialComplete()
    {
        Debug.Log("Tutorial is complete");
        SetTooltipText("Tutorial geslaagd");
    }

    public static void SetTooltipText(string text)
    {
        _tooltipText.text = text;
    }

    public static void SetTooltipTarget(Transform target)
    {
        _tooltip.TargetPoint = target;
    }

    public static void ActivateControllerAnim(bool active)
    {
        _tooltip.ControllerAnim.SetActive(active);
    }

    public static GameObject SpawnObject(string objectName, string spawnPosName)
    {
        if(!_spawnObjectPool.ContainsKey(objectName))
        {
            Debug.LogError("TutorialManager::SpawnObject - No spawn object with name " + objectName);
            return null;
        }

        if (!_spawnPosPool.ContainsKey(spawnPosName))
        {
            Debug.LogError("TutorialManager::SpawnObject - No spawn position with name " + spawnPosName);
            return null;
        }

        var go = Instantiate(_spawnObjectPool[objectName].Prefab);
        var spawnPos = _spawnPosPool[spawnPosName];
        go.transform.SetParent(spawnPos.Parent, false);
        go.transform.position = spawnPos.Position;
        go.transform.eulerAngles = spawnPos.Rotation;

        return go;
    }

    public static GameObject GetSceneObject(string objectName)
    {
        if (!_sceneObjectPool.ContainsKey(objectName))
        {
            Debug.LogError("TutorialManager::GetSceneObject - No scene object with name " + objectName);
            return null;
        }

        return _sceneObjectPool[objectName].Object;
    }
}