﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class IOList<T>
{
    public List<T> Data;

    public IOList(List<T> data)
    {
        Data = data;
    }

    public void Save(string path, bool pretty = true)
    {
        var data = JsonUtility.ToJson(this, pretty);
        using (StreamWriter file = File.AppendText(path))
        {
            file.Write(data);
        }
    }

    public static List<T> Load(string path)
    {
        string json = "";
        using (StreamReader reader = new StreamReader(path))
        {
            json = reader.ReadToEnd();
        }

        var data = JsonUtility.FromJson<IOList<T>>(json);
        return data.Data;
    }
}
   
