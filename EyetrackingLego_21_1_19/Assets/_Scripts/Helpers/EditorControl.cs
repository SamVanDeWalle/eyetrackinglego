﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorControl : MonoBehaviour
{
    void Update()
    {
        if (ViveHelper.Controllers[ControllerIndex.Left].GetPressUp(ControllerButton.Menu) ||
            ViveHelper.Controllers[ControllerIndex.Right].GetPress(ControllerButton.Menu))
        {
            Debug.Log("Pauzing editor");
            Debug.Break();
        }
    }
}
