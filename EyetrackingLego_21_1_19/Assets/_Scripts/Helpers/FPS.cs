﻿using UnityEngine;
using System.Collections;

public class FPS : MonoBehaviour
{
    public UnityEngine.UI.Text FPSText = null;

    int frameCount = 0;
    float dt = 0.0f;
    float fps = 0.0f;
    float updateRate = 4.0f;  // 4 updates per sec.

    void Update()
    {
        frameCount++;
        dt += Time.deltaTime;
        if (dt > 1.0 / updateRate)
        {
            fps = (float)frameCount / dt;
            frameCount = 0;
            dt -= 1.0f / updateRate;
        }

        if (FPSText != null)
            FPSText.text = fps.ToString();
    }
}
