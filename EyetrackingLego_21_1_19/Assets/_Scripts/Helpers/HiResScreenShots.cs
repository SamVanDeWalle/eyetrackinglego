﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;
using System.IO;


//Source: http://answers.unity3d.com/questions/22954/how-to-save-a-picture-take-screenshot-from-a-camer.html

public class HiResScreenShots : MonoBehaviour
{
    private int resWidth = 3840;//2560;// Screen.width;//2550; 
    private int resHeight = 2160;//1440;// Screen.height;//3000;
    public static int WoopiId = -1;

    private bool _bTakeHiResShot = false;

    public static string ScreenShotName(int id)
    {
        return string.Format("{0}/screen_capture_{1}.png",
                             Application.dataPath + "/",
                             id);
                             //System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
    }

    public void TakeHiResShot()
    {
        GetComponent<Camera>().enabled = true;
        _bTakeHiResShot = true;
    }

    void Update()
    {
        //if (ViveHelper.Controllers[SteamVR_TrackedObject.EIndex.Device1].Device.GetPressDown//(SteamVR_Controller.ButtonMask.Touchpad))
        //    TakeHiResShot();
    }

    void LateUpdate()
    {
        _bTakeHiResShot |= Input.GetKeyDown("k");
        if(_bTakeHiResShot)
        {
            RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
            GetComponent<Camera>().targetTexture = rt;
            Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
            GetComponent<Camera>().Render();
            RenderTexture.active = rt;
            screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
            GetComponent<Camera>().targetTexture = null;
            RenderTexture.active = null; // JC: added to avoid errors
            Destroy(rt);
            byte[] bytes = screenShot.EncodeToPNG();
            string filename = ScreenShotName(Random.Range(0, 1000));
            System.IO.File.WriteAllBytes(filename, bytes);

            //FileStream fs = new FileStream(filename, FileMode.CreateNew);
            //BinaryWriter w = new BinaryWriter(fs);
            //w.Write(bytes);
            //w.Close();
            //fs.Close();

            //Debug.Log(string.Format("Took screenshot to: {0}", filename));
            _bTakeHiResShot = false;
            //GetComponent<Camera>().enabled = false;
        }
    }
}
