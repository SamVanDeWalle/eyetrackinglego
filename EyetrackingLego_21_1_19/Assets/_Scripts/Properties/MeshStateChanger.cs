﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[Serializable]
public class MeshState
{
    public Mesh Mesh;
    public List<ConnectionPoint> RequiredConnectedCPs = new List<ConnectionPoint>();
    public ToolAction RequiredToolAction = ToolAction.None;
}

public class MeshStateChanger : Property
{
    public MeshFilter MeshFilter;
    public List<MeshState> States = new List<MeshState>();

    List<Collider> _colliders = new List<Collider>();
    Renderer _renderer;
    Material _startMaterial;

    void Awake()
    {
        _type = PropertyType.MeshState;
        _renderer = MeshFilter.GetComponent<Renderer>();
        _startMaterial = _renderer.material;
    }

    void Start()
    {
        _colliders = transform.GetComponentsInChildren<Collider>().ToList();

        foreach(var state in States)
        {
            if(state.RequiredToolAction != ToolAction.None)
            {
                ToolManager.SubscribeColliders(state.RequiredToolAction, transform, _colliders);
                Tool.OnActionEvent += OnToolAction;
            }
        }
    }

    void OnToolAction(object tool, ToolEventArgs args)
    {
        foreach (var state in States)
        {
            if (state.RequiredToolAction != ToolAction.None && state.RequiredToolAction == args.ToolAction && args.Parent == transform && args.Collider.transform.parent == transform) // && required cps are connected .. 
            {
                ChangeState(state);
            }
        }
    }

    void ChangeState(MeshState state)
    {
        MeshFilter.mesh = state.Mesh;
        StartCoroutine(PulseMaterial(0.5f));
        Resolve();
    }

    IEnumerator PulseMaterial(float duration)
    {
        _renderer.material = MaterialManager.GetInstance().CorrectMaterial;
        yield return new WaitForSeconds(duration);
        _renderer.material = _startMaterial;
    }

    public override void AutoCorrectComplete(bool silent = false)
    {
        base.AutoCorrectComplete(silent);

        // This class needs help
        MeshFilter.mesh = States[1].Mesh;
    }

    public override void Guide()
    {
        // Highlight tool if not in hand, otherwise show trail from tool to surface
        var tools = ToolManager.GetTools(ToolType.Hammer);
        var pickedUpTools = tools.Where(t => t.Pickable.IsPickedUp).ToList();
        for (int i = 0; i < pickedUpTools.Count; ++i)
        {
            var hammer = pickedUpTools[i] as Hammer;
            
            GuideUtils.SetTrail(hammer.transform, transform);
            return;
        }

        var highlightPickables = new List<Pickable>();
        for (int i = 0; i < tools.Count; ++i)
        {
            var hammer = tools[i] as Hammer;
            highlightPickables.Add(tools[i].Pickable);
        }

        GuideUtils.AllowedPickables.AddRange(highlightPickables);
        GuideUtils.Highlight(highlightPickables);
    }

    public override void Reset()
    {
        base.Reset();

        MeshFilter.mesh = States[0].Mesh;
    }
}
