﻿/*
 * 
 * Manages the properties of a piece. f.e temperature, loctite paint area etc
 * 
 */

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PropertyHolder : MonoBehaviour
{
    public Dictionary<PropertyType, List<Property>> Properties { get { return _properties; } private set { } }

    Dictionary<PropertyType, List<Property>> _properties = new Dictionary<PropertyType, List<Property>>();

    public delegate void PropertyResolved(PropertyHolder holder, Property property);
    public static event PropertyResolved OnPropertyResolved;

    void Start()
    {
        FindProperties();
    }

    void FindProperties()
    {
        _properties.Clear();
        var properties = GetComponentsInChildren<Property>();
        foreach(var property in properties)
        {
            if (!_properties.ContainsKey(property.Type))
                _properties.Add(property.Type, new List<Property>());

            _properties[property.Type].Add(property);

            property.OnGlobalPropertyResolved -= HandlePropertyResolved;
            property.OnGlobalPropertyResolved += HandlePropertyResolved;
        }
    }

    void HandlePropertyResolved(Property property)
    {
        if (OnPropertyResolved != null)
            OnPropertyResolved(this, property);
    }

    public Property GetProperty(PropertyType type, int id)
    {
        if (_properties.ContainsKey(type))
        {
            return _properties[type].FirstOrDefault(p => p.ID == id);
        }

        return null;
    }

    public List<Property> GetProperties(PropertyType type)
    {
        if (_properties.ContainsKey(type))
        {
            return _properties[type];
        }

        return null;
    }

    public void ResetAllProperties()
    {
        foreach(var ppt in _properties)
        {
            foreach(var property in ppt.Value)
            {
                property.Reset();
            }
        }
    }
}
