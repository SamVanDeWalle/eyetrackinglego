﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PropertyType
{
    Temperature,
    LoctitePart,
    MeshState
}

public class Property : MonoBehaviour
{
    public int ID;
    public PropertyType Type { get { return _type; } private set { } }
    public bool IsCorrect { get { return _bCorrect; } private set { } }

    protected PropertyType _type;
    protected bool _bCorrect = false;

    public delegate void DefaultDelegate();
    public static event DefaultDelegate OnPropertyResolved;
    public delegate void PropertyDelegate(Property property);
    public event PropertyDelegate OnGlobalPropertyResolved;

    public event DefaultDelegate OnThisPropertyResolved;

    public void Resolve()
    {
        if (_bCorrect == true)
            return;

        _bCorrect = true;
        if (OnPropertyResolved != null)
            OnPropertyResolved();

        if (OnGlobalPropertyResolved != null)
            OnGlobalPropertyResolved(this);

        if (OnThisPropertyResolved != null)
            OnThisPropertyResolved();
    }

    public virtual void AutoCorrectComplete(bool silent = false)
    {
        _bCorrect = true;
    }

    public virtual void Guide() { }

    public virtual void Reset()
    {
        _bCorrect = false;
    }
}
