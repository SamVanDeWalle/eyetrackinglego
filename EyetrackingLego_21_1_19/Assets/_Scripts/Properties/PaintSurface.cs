﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PaintSurface : Property
{
    public List<PaintType> AllowedTypes;
    public PaintType CorrectType;
    Renderer _renderer;
    Material _startMat;

    void Awake()
    {
        _type = PropertyType.LoctitePart;
        _renderer = GetComponent<Renderer>();
        _startMat = _renderer.sharedMaterial;
    }

    public void Paint(PaintType type)
    {
        _renderer.sharedMaterial = PaintManager.PaintMaterials[type];

        if (type == CorrectType)
        {
            Resolve();
            GuideUtils.DeactivateTrail();
        }
    }

    public override void AutoCorrectComplete(bool silent = false)
    {
        base.AutoCorrectComplete(silent);

        if (silent)
        {
            _renderer.sharedMaterial = PaintManager.PaintMaterials[CorrectType];
        }
        else
        {
            Paint(CorrectType);
        }
    }

    public override void Guide()
    {
        if (IsCorrect)
            return;

        // Highlight tool if not in hand, otherwise show trail from tool to surface
        var tools = ToolManager.GetTools(ToolType.Brush);
        var pickedUpTools = tools.Where(t => t.Pickable.IsPickedUp).ToList();
        for(int i = 0; i < pickedUpTools.Count; ++i)
        {
            var brush = pickedUpTools[i] as Brush;
            if (brush.PaintType == CorrectType)
            {
                GuideUtils.SetTrail(brush.transform, transform);
                return;
            }
        }

        var highlightPickables = new List<Pickable>();
        for(int i = 0; i < tools.Count; ++i)
        {
            var brush = tools[i] as Brush;
            if(brush.PaintType == CorrectType)
            {
                highlightPickables.Add(tools[i].Pickable);
            }
        }

        GuideUtils.Highlight(highlightPickables);
        GuideUtils.AllowedPickables.AddRange(highlightPickables);
    }

    public override void Reset()
    {
        base.Reset();

        _renderer.sharedMaterial = _startMat;
    }
}
