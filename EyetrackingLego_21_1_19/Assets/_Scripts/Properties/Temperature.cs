﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Temperature : Property
{
    public float BaseTemperature = 20;
    public float MaxTemperature = 95;
    public float Degrees { get { return _degrees; } private set { } }
    public bool IsMaxed { get { return _degrees >= MaxTemperature; } private set { } }

    AssemblyPiece _ap;
    float _degrees = 20;

	void Awake()
    {
        _type = PropertyType.Temperature;
        _ap = GetComponentInParent<AssemblyPiece>();
	}
	
	public void HeatUp (float degrees)
    {
        _degrees += degrees;
        _degrees = Mathf.Clamp(_degrees, BaseTemperature, MaxTemperature);

        if (IsMaxed)
        {
            ViveHelper.PulseAllControllers(0.3f);
            Resolve();
        }
	}

    public override void AutoCorrectComplete(bool silent = false)
    {
        base.AutoCorrectComplete(silent);

        _degrees = MaxTemperature;
    }

    public override void Guide()
    {
        // Highlight item if not in hand and not on tool, show arrow towards heating tool if in hand
        // If item is connected to heating tool and not heating show arrow from controller to start button
        var heater = FindObjectOfType<InductionHeater>();
        if (_ap.Pickable.IsPickedUp)
        {
            GuideUtils.SetTrail(_ap.transform, heater.GetComponentInChildren<ConnectionPoint>().transform);
        }
        else if(heater.ConnectedProperty == this)
        {
            if (!heater.IsHeating)
            {
                GuideUtils.SetTrail(ViveHelper.Controllers[ControllerIndex.Right].Transform, heater.ActivateButton.transform);
                heater.ActivateButton.GetComponent<WorldButtonIndicator>().Indicate();
            }
        }
        else
        {
            GuideUtils.Highlight(_ap.Pickable);
        }
    }

    public override void Reset()
    {
        base.Reset();

        _degrees = BaseTemperature;
    }
}
