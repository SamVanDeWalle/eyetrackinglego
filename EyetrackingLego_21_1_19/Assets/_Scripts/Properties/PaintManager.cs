﻿using Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PaintType
{
    Oil1,
    Loctite1,
    Loctite2
}

[Serializable]
public class Paint
{
    public PaintType Type;
    public Material Material;
}

public class PaintManager : Singleton<PaintManager>
{
    public List<Paint> Paints = new List<Paint>();
    public static Dictionary<PaintType, Material> PaintMaterials = new Dictionary<PaintType, Material>();

    protected override void Awake()
    {
        base.Awake();

        PaintMaterials.Clear();

        foreach(var paint in Paints)
        {
            PaintMaterials.Add(paint.Type, paint.Material);
        }
    }

    void Start()
    {
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PaintZone"), LayerMask.NameToLayer("PickItems"));
    }
}
