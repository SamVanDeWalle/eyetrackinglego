﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
public class AssemblyPieceLooseState
{
    public int ParentRecordingIndex;
    public int ParentTypeIndex;
    
    public Vector3 localPos;
    public Quaternion localRot;
    public AssemblyPieceLooseState()
    {

    }
    public AssemblyPieceLooseState(AssemblyPieceState other)
    {
        if (other.Parent == null)
        {
            ParentTypeIndex = -1;
            ParentRecordingIndex = -1;
        }
        else
        {
            ParentTypeIndex = other.Parent.TypeID;
            ParentRecordingIndex = other.Parent.LastRecordedStepIndex;
     
        }
      
        localPos = other.localPos;
        localRot = other.localRot;
    }
    //public bool MatchesWithAngleOffset(AssemblyPieceLooseState b)
    //{
    //    Vector3 dPos = localPos - b.localPos;
    //    // float dangle = Quaternion.Angle(a.localRot, b.localRot);
    //    return (ParentTypeIndex == b.ParentTypeIndex && ParentRecordingIndex == b.ParentRecordingIndex && dPos.sqrMagnitude < 0.001f);// && Mathf.Abs(dangle) < 0.1f);
    //}


   
    public bool MatchAnglePos(AssemblyPieceLooseState b)
    {
        //TODO If a piece has a different connection position but is equally valid as the recorded position..
        Vector3 dPos = localPos - b.localPos;
        float dangle = Quaternion.Angle(localRot, b.localRot);

        return dPos.sqrMagnitude < 0.01f && Mathf.Abs(dangle) < 45f;
    }
    public bool MatchParent(AssemblyPieceLooseState b)
    {
        return ParentTypeIndex == b.ParentTypeIndex && ParentRecordingIndex == b.ParentRecordingIndex;
    }
    public  bool Match( AssemblyPieceLooseState b)
    {
        var a = this;
        Vector3 dPos = a.localPos - b.localPos;
        float dangle = Quaternion.Angle(a.localRot, b.localRot);

        return (a.ParentTypeIndex == b.ParentTypeIndex && a.ParentRecordingIndex == b.ParentRecordingIndex && dPos.sqrMagnitude < 0.005f && Mathf.Abs(dangle) < 1f);
    }
   
}
public class AssemblyPieceState
{
    public AssemblyPiece Parent;
    public Vector3 localPos;
    public Quaternion localRot;

    public override bool Equals(object obj)
    {
        return obj is AssemblyPieceState && (AssemblyPieceState)obj == this;
    }
    public override int GetHashCode()
    {
        int hash = 17;
        hash = hash * 23 + Parent.GetHashCode();
        hash = hash * 23 + localPos.GetHashCode();
        hash = hash * 23 + localRot.GetHashCode();
        return hash;
    }

    public static bool operator ==(AssemblyPieceState a, AssemblyPieceState b)
    {
        Vector3 dPos = a.localPos - b.localPos;
        float dangle = Quaternion.Angle(a.localRot, b.localRot);
        return (a.Parent == b.Parent && dPos.sqrMagnitude < 0.001f && Mathf.Abs(dangle) < 0.1f);
    }
    public static bool operator !=(AssemblyPieceState a, AssemblyPieceState b)
    {
        return !(a == b);
    }
}
/// <summary>
/// This represents connecting an assemblypiece loosely during recording
/// Objects are referenced by type, not by NUID (no unique objects)
/// </summary>
public class RecordedConnectionLoose : RecordedAction
{
    [Serializable]
    public class RawData
    {
        public float
            PosX, PosY, PosZ,
            RotX, RotY, RotZ, RotW;
        public int
            OwnerTypeID,
            OwnerRecordingStepID,
            ParentTypeID,
            ParentRecordingStepID;


    }
    
    public RecordedConnectionLoose()
    {
        CurrentType = RecordedActionType.ConnectionLoose;
    }
    public RecordedConnectionLoose(RecordedConnection conn)
    {

        CurrentType = RecordedActionType.ConnectionLoose;
        CreatedFrom = conn;
        State = new AssemblyPieceLooseState(conn.NewState);

        ownerPieceRecordingIndex = conn.OwnerPiece.LastRecordedStepIndex;
        ownerPieceTypeId = conn.OwnerPiece.TypeID;

    }

    public bool IsCreatedFrom(RecordedConnection conn)
    {
        return CreatedFrom != null && conn == CreatedFrom;
    }
    RecordedConnection CreatedFrom;
    public AssemblyPieceLooseState State;
    public int ownerPieceTypeId;
    public int ownerPieceRecordingIndex;



    public override RecordedAction GetReversed()
    {

        throw new NotImplementedException();
    }
    public override void Undo(Action finished)
    {
        throw new NotImplementedException();
    }
    public override void Redo(Action finished)
    {
        throw new NotImplementedException();
    }

    public override void Deserialize(byte[] data)
    {
        using (MemoryStream stream = new MemoryStream())
        {
            var formatter = new BinaryFormatter();
            stream.Write(data, 0, data.Length);
            stream.Seek(0, SeekOrigin.Begin);
            RawData d = (RawData)formatter.Deserialize(stream);
            FromRawData(d);
        }
    }


    public override byte[] Serialize()
    {
        BinaryFormatter formatter = new BinaryFormatter();

        using (MemoryStream ms = new MemoryStream())
        {
            formatter.Serialize(ms, ToRawData());
            return ms.ToArray();
        }
    }

    public override void FromRawData(object raw)
    {
        if (!(raw is RawData)) return;
        RawData r = (RawData)raw;
        State = new AssemblyPieceLooseState();
        State.localPos = new Vector3(r.PosX, r.PosY, r.PosZ);
        State.localRot = new Quaternion(r.RotX, r.RotY, r.RotZ, r.RotW);

        State.ParentRecordingIndex = r.ParentRecordingStepID;
        State.ParentTypeIndex = r.ParentTypeID;

        ownerPieceRecordingIndex = r.OwnerRecordingStepID;
        ownerPieceTypeId = r.OwnerTypeID;

    }
    public override object ToRawData()
    {
        return new RawData()
        {
            PosX = State.localPos.x,
            PosY = State.localPos.y,
            PosZ = State.localPos.z,

            RotX = State.localRot.x,
            RotY = State.localRot.y,
            RotZ = State.localRot.z,
            RotW = State.localRot.w,


            ParentRecordingStepID = State.ParentRecordingIndex,
            ParentTypeID = State.ParentTypeIndex,
            OwnerRecordingStepID = ownerPieceRecordingIndex,
            OwnerTypeID = ownerPieceTypeId
        };
    }

    public override bool TryMerge(RecordedAction other)
    {
        if (!(other is RecordedConnectionLoose)) return false;
        var otherConn = other as RecordedConnectionLoose;
        if (otherConn.ownerPieceRecordingIndex != ownerPieceRecordingIndex) return false;
        if (otherConn.ownerPieceTypeId != ownerPieceTypeId) return false;

        State = otherConn.State;

        return true;
    }

}
/// <summary>
/// This RecordedAction represents connecting an AssembliePiece with another
/// It stores both pieces and local transform of the child piece
/// Also remembers previous position and rotation for undo behaviour (this is where the piece was last picked up)
/// </summary>
/// 

public class RecordedConnection : RecordedAction
{
    /// <summary>
    /// Raw data class for (de)serialization
    /// </summary>
    [Serializable]
    public class RawData
    {
        public float
            PosX, PosY, PosZ,
            RotX, RotY, RotZ, RotW,
            PrevPosX, PrevPosY, PrevPosZ,
            PrevRotX, PrevRotY, PrevRotZ, PrevRotW;
        public int
            OwnerNUID,
            ForeignNUID,
            PrevForeignNUID;

        //public static int Size
        //{
        //    return sizeof(float)
        //}

    }

    //Pieces
    public AssemblyPiece OwnerPiece;

    public AssemblyPieceState NewState;
    public AssemblyPieceState OldState;

    //public AssemblyPiece ForeignPiece;

    ////Attached transform (in local space)
    //public Vector3 Position;
    //public Quaternion Rotation;

    ////Previous transform (in world space)
    //public Vector3 PrevPosition;
    //public Quaternion PrevRotation;

    public RecordedConnection(AssemblyPiece owner, AssemblyPieceState oldState, AssemblyPieceState newState)
    {
        OwnerPiece = owner;

        NewState = newState;
        OldState = oldState;


        CurrentType = RecordedActionType.Connection;


    }
    public RecordedConnection(RawData d)
    {
        CurrentType = RecordedActionType.Connection;
        FromRawData(d);
    }

    public RecordedConnection()
    {
        CurrentType = RecordedActionType.Connection;
    }

    #region Serialization
    public override void Deserialize(byte[] data)
    {
        using (MemoryStream stream = new MemoryStream())
        {
            var formatter = new BinaryFormatter();
            stream.Write(data, 0, data.Length);
            stream.Seek(0, SeekOrigin.Begin);
            RawData d = (RawData)formatter.Deserialize(stream);
            FromRawData(d);
        }
    }


    public override byte[] Serialize()
    {
        BinaryFormatter formatter = new BinaryFormatter();

        using (MemoryStream ms = new MemoryStream())
        {
            formatter.Serialize(ms, ToRawData());
            return ms.ToArray();
        }
    }

    public override void FromRawData(object raw)
    {
        if (!(raw is RawData)) return;
        RawData r = (RawData)raw;
        NewState.localPos = new Vector3(r.PosX, r.PosY, r.PosZ);
        OldState.localPos = new Vector3(r.PrevPosX, r.PrevPosY, r.PrevPosZ);

        NewState.localRot = new Quaternion(r.RotX, r.RotY, r.RotZ, r.RotW);
        OldState.localRot = new Quaternion(r.PrevRotX, r.PrevRotY, r.PrevRotZ, r.PrevRotW);

        NewState.Parent = r.ForeignNUID == -1 ? null : NetworkHandling.NetworkObjects[r.ForeignNUID].GetComponent<AssemblyPiece>();
        OldState.Parent = r.PrevForeignNUID == -1 ? null : NetworkHandling.NetworkObjects[r.PrevForeignNUID].GetComponent<AssemblyPiece>();

        OwnerPiece = NetworkHandling.NetworkObjects[r.OwnerNUID].GetComponent<AssemblyPiece>();

    }

    public override object ToRawData()
    {
        return new RawData()
        {
            PosX = NewState.localPos.x,
            PosY = NewState.localPos.y,
            PosZ = NewState.localPos.z,

            PrevPosX = OldState.localPos.x,
            PrevPosY = OldState.localPos.y,
            PrevPosZ = OldState.localPos.z,

            RotX = NewState.localRot.x,
            RotY = NewState.localRot.y,
            RotZ = NewState.localRot.z,
            RotW = NewState.localRot.w,

            PrevRotX = OldState.localRot.x,
            PrevRotY = OldState.localRot.y,
            PrevRotZ = OldState.localRot.z,
            PrevRotW = OldState.localRot.w,

            ForeignNUID = NewState.Parent == null ? -1 : NewState.Parent.GetComponent<NetworkObject>().NUID,
            PrevForeignNUID = OldState.Parent == null ? -1 : OldState.Parent.GetComponent<NetworkObject>().NUID,
            OwnerNUID = OwnerPiece.GetComponent<NetworkObject>().NUID
        };
    }
    public override bool IsObsoleteAction()
    {
        return NewState == OldState;
    }
    #endregion
    //Check if we can skip obsolete actions
    public override bool TryMerge(RecordedAction other)
    {
        if (!(other is RecordedConnection)) return false;
        var otherConn = other as RecordedConnection;
        if (otherConn.OwnerPiece == OwnerPiece && otherConn.OldState == NewState)
        {
            NewState = otherConn.NewState;
            return true;
        }
        return false;
    }
    #region Undo Redo
    //Redo action: attach the piece to foreign piece
    public override void Redo(Action finished)
    {
        OwnerPiece.ChangeState(OldState, NewState, finished);
    }


    //Undo action: put piece back to original transform
    public override void Undo(Action finished)
    {
        OwnerPiece.ChangeState(NewState, OldState, finished);

    }
    public override RecordedAction GetReversed()
    {
        return new RecordedConnection(OwnerPiece, NewState, OldState);
    }

    #endregion
}

/// <summary>
/// Actions to be recorded in undo/redo
/// </summary>
/// 
public abstract class RecordedAction
{
    public RecordedActionType CurrentType
    {
        get; protected set;
    }

    public enum RecordedActionType
    {
        Connection,
        ConnectionLoose,
    }
    public abstract void Redo(Action finished);
    public abstract void Undo(Action finished);
    public abstract byte[] Serialize();
    public abstract void Deserialize(byte[] data);
    public abstract object ToRawData();
    public abstract void FromRawData(object data);
    public abstract RecordedAction GetReversed();
    public virtual bool TryMerge(RecordedAction other)
    {
        return false;
    }
    public virtual bool IsObsoleteAction()
    {
        return false;
    }
}
public class QueuedAction
{
    public enum ActionType
    {
        Undo,
        Redo
    }
    public ActionType CurrentType;
    public RecordedAction Action;

    public QueuedAction(ActionType t, RecordedAction action)
    {
        
        Action = action;
        CurrentType = t;
    }
}


/// <summary>
/// Manages the undo/redo and record/playback of action states.
/// </summary>
public class ObjectStateRecorder : MonoBehaviour
{

    //SINGLETON
    private static ObjectStateRecorder _instance;

    public static ObjectStateRecorder Instance
    {
        get
        {
            if (_instance == null || _instance.gameObject == null)
            {
                _instance = new GameObject("StateRecorder").AddComponent<ObjectStateRecorder>();
                _instance._playBack = FindObjectOfType<PlaybackManager>();
            }
            return _instance;
        }
        set
        {
            _instance = value;
        }
    }

    public bool IsRecording()
    {
        return _recording;
    }
    //Stacks for undo/redo
    private LinkedList<RecordedAction> UndoStack = new LinkedList<RecordedAction>();
    private LinkedList<RecordedAction> RedoStack = new LinkedList<RecordedAction>();

    //Stack for recording
    private LinkedList<RecordedConnectionLoose> RecordedStack = new LinkedList<RecordedConnectionLoose>();
    private bool _recording = false;
    private int _undoCountAtRecordStart = 0;
    private int _playbackStep = 0;
    private int _recordingStep = 0;
    //Async undo/redo: Store queue of actions to do
    private Queue<QueuedAction> ActionsTodo = new Queue<QueuedAction>();
    QueuedAction _currentAction = null;

    //Playback
    [SerializeField]
    PlaybackManager _playBack;
    bool _playbackStarted = false;
    void Awake()
    {
        string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/recording.gbr";

        DeSerializeRecording(path);
    }

    void Start()
    {
        // Subscribe to button input
        //HUDMenu.SubscribeToAction("undo", Undo);
        //HUDMenu.SubscribeToAction("redo", Redo);
        //HUDMenu.SubscribeToAction("recordtoggle", ToggleRecording);
        //HUDMenu.SubscribeToAction("replay", PlaybackRecording);
    }

    public void clearfordemo()
    {
        UndoUntill(0);
        UndoStack.Clear();
        RedoStack.Clear();
        _playBack.ResetAllPlaybackIds();
        RecordedStack.Clear();
        string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/recording.gbr";
        DeSerializeRecording(path);
        _playbackStep = 0;
        _undoCountAtRecordStart = 0;
        _recordingStep = 0;
        ActionsTodo.Clear();
        _currentAction = null;
    }

    public void PickupAssemblyPiece(AssemblyPiece ap)
    {
        if (_playBack != null)
            _playBack.PickedUpObject(ap);
    }
    public void DropAssemblyPiece(AssemblyPiece ap)
    {
        if (_playBack != null)
            _playBack.DroppedObject(ap);
    }

    public AssemblyPieceLooseState GetPlaybackSnapState(AssemblyPiece p, AssemblyPiece parent)
    {
        if (_playBack == null || !_playBack.IsPlayingBack) return null;
        return _playBack.GetSnapState(p, parent);
    }

    public void AddAction(RecordedAction act)
    {
        
        if (UndoStack.Count == 0 || _playBack.IsPlayingBack || !UndoStack.Last.Value.TryMerge(act))
        {
            UndoStack.AddLast(act);
        }
        RedoStack.Clear();

        if (_recording)
        {
            RecordAction(act);
        }
        //During playback, check if new action matches the current playback action
        else if (act.CurrentType == RecordedAction.RecordedActionType.Connection && _playBack.IsPlayingBack)
        {
            RecordedConnection actconn = act as RecordedConnection;
            if (_playBack.ValidPlayback(new RecordedConnectionLoose(actconn)))
            {
                //set recording step ids
                ++_playbackStep;
                SetRecordingStepIDs(actconn.OwnerPiece, actconn.NewState.Parent, _playbackStep);

                //Next playback action
                _playBack.StopPreviewAction();
            }
            else
            {
                UndoAction();
            }

        }

    }



    //Async undo/redo
    void DoNextAction()
    {

        if (ActionsTodo.Count > 0)
        {
            //get next action from queue (FIFO)
            _currentAction = ActionsTodo.Dequeue();

            //Execute appropriate action
            switch (_currentAction.CurrentType)
            {
                case QueuedAction.ActionType.Undo:
                    _currentAction.Action.Undo(DoNextAction); //callback to DoNextAction when undo is finished
                    break;
                case QueuedAction.ActionType.Redo:
                    _currentAction.Action.Redo(DoNextAction); //callback to DoNextAction when redo is finished

                    break;
            }
        }
        else
        {
            _currentAction = null;
        }
    }


    public void Redo()
    {
        if (_playbackStarted) return;
        RedoAction();
    }
    public void Undo()
    {
        if (_playbackStarted) return; // Don't allow undo on playback
        UndoAction();
    }
    //Undo last action
    private void UndoAction()
    {
        if (UndoStack.Count == 0)
            return;
        if (_recording && UndoStack.Count <= _undoCountAtRecordStart) return;
        //Remove from undo stack
        var last = UndoStack.Last;
        UndoStack.RemoveLast();

        if (_recording)
            RemoveLastRecordedAction(last.Value);// (last.Value.GetReversed());

        //Add to action queue and do if no other executing yet
        ActionsTodo.Enqueue(new QueuedAction(QueuedAction.ActionType.Undo, last.Value));
        if (_currentAction == null)
            DoNextAction();

        //Add action on top of redo stack
        RedoStack.AddLast(last.Value);

        //if (UndoStack.Count < _undoCountAtRecordStart)
        //{
        //    _undoCountAtRecordStart = UndoStack.Count;
        //}
    }

    public void UndoUntill(int undoStackCount)
    {
        while (UndoStack.Count > undoStackCount)
        {
            var last = UndoStack.Last;
            UndoStack.RemoveLast();
            ActionsTodo.Enqueue(new QueuedAction(QueuedAction.ActionType.Undo, last.Value));
        }
        if (_currentAction == null) DoNextAction();
    }


    //Redo next action
    private void RedoAction()
    {
        if (RedoStack.Count == 0)
            return;

        //Remove from redo stack
        var last = RedoStack.Last;
        RedoStack.RemoveLast();

        if (_recording)
            RecordAction(last.Value);
        //Add to action queue and do if no other executing yet
        ActionsTodo.Enqueue(new QueuedAction(QueuedAction.ActionType.Redo, last.Value));
        if (_currentAction == null)
        {
            DoNextAction();
        }



        //add action on top of undo stack
        UndoStack.AddLast(last);
    }



    #region serialization
    public void SerializeRecording(string path)
    {
        using (Stream file = File.Open(path, FileMode.Create))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            List<object> serializeformat = new List<object>();
            foreach (var action in RecordedStack)
            {
                serializeformat.Add((int)action.CurrentType);
                serializeformat.Add(action.ToRawData());
            }
            formatter.Serialize(file, serializeformat);
        }
    }


    public void DeSerializeRecording(string path)
    {
        RecordedStack.Clear();

        if (!File.Exists(path)) return;

        using (Stream file = File.Open(path, FileMode.Open))
        {

            BinaryFormatter formatter = new BinaryFormatter();
            List<object> serialized = (List<object>)formatter.Deserialize(file);

            for (int i = 0; i < serialized.Count - 1; i += 2)
            {
                var type = serialized[i];
                RecordedAction.RecordedActionType t = (RecordedAction.RecordedActionType)(int)type;
                RecordedConnectionLoose action = null;
                switch (t)
                {
                    case RecordedAction.RecordedActionType.Connection:
                      
                        //moved to connectionLOOSE

                        break;
                    case RecordedAction.RecordedActionType.ConnectionLoose:
                        action = new RecordedConnectionLoose();
                        action.FromRawData(serialized[i + 1]);
                        RecordedStack.AddLast(action);
                        break;
                }


            }
        }

    }

    #endregion

    public void ToggleRecording()
    {
        if (!_recording)
            StartRecording();
        else
            StopRecording();
    }

    public void StartRecording()
    {
        _undoCountAtRecordStart = UndoStack.Count;
        _playBack.ResetAllPlaybackIds();
        RecordedStack.Clear();
        _recording = true;
    }
    public void StopRecording()
    {
        _recording = false;
        //TODO: Save Recording

        SerializeRecording(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/recording.gbr");


        //TODO: Send Recording over network

    }


    public void PlaybackRecording()
    {
        if (_recording || RecordedStack.Count == 0) return;

        StartCoroutine(PlaybackUpdate());


    }
    IEnumerator PlaybackUpdate()
    {
        _playbackStep = 0;
        _playBack.ResetAllPlaybackIds();
        _playbackStarted = true;

        UndoUntill(0);
        //wait for undo's to reset state
        while (ActionsTodo.Count > 0) yield return null;
        UndoStack.Clear();
        RedoStack.Clear();
        yield return new WaitForSeconds(1f);
        //Start playback 
        var action = RecordedStack.First;

        while (action != null)
        {

            if (_playBack.PreviewAction(action.Value))
            {
                //wait for action to be finished
                while (_playBack.IsPlayingBack)
                {
                    yield return null;
                }
            }
            action = action.Next;

        }
        _playbackStarted = false;
        //add to undo stack
        //foreach (var act in RecordedStack)
        //{
        //    UndoStack.AddLast(act);
        //}
    }
    void RemoveLastRecordedAction(RecordedAction act)
    {
        if (!(act is RecordedConnection)) return;
        if(RecordedStack.Count >0 && RecordedStack.Last.Value.IsCreatedFrom((RecordedConnection)act))
            RecordedStack.RemoveLast();
    }

    void SetRecordingStepIDs(AssemblyPiece owner, AssemblyPiece parent, int step)
    {
        owner.LastRecordedStepIndex = step * 2;
        if(parent != null)
            parent.LastRecordedStepIndex = step * 2 + 1;
    }

    void RecordAction(RecordedAction act)
    {
        if (!(act is RecordedConnection)) return;
       
        RecordedConnection actConn = (RecordedConnection)act;
        if (actConn.NewState.Parent == null) return;
        
        RecordedStack.AddLast(new RecordedConnectionLoose(actConn));

        SetRecordingStepIDs(actConn.OwnerPiece, actConn.NewState.Parent, RecordedStack.Count);
        //actConn.OwnerPiece.LastRecordedStepIndex = RecordedStack.Count*2;
       
        //if (actConn.NewState.Parent != null)
        //{
        //    actConn.NewState.Parent.LastRecordedStepIndex = RecordedStack.Count * 2 + 1;
        //}
 
        //Check if we can merge with a previous action
        //while (RecordedStack.Count > 1 && RecordedStack.Last.Previous.Value.TryMerge(RecordedStack.Last.Value))
        //{
        //    
        //    RecordedStack.RemoveLast();
        //}
 
    }


    ////DEBUG: UNDO/REDO using keyboard buttons
    //public void Update()
    //{
    //    if ( Input.GetKeyDown( KeyCode.Keypad1 ) )
    //    {
    //        UndoAction();
    //    }
    //    if ( Input.GetKeyDown( KeyCode.Keypad2 ) )
    //    {
    //        RedoAction();
    //    }
    //}


}
