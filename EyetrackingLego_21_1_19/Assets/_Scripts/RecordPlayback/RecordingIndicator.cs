﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecordingIndicator : MonoBehaviour {

  
    bool _showing = false;
    float _timer = 0f;
    CanvasGroup _group;
	// Use this for initialization
	void Start () {
        _group = GetComponentInChildren<CanvasGroup>();
        _group.alpha = 0f;
	}
	
	// Update is called once per frame
	void Update () {

        if (ObjectStateRecorder.Instance.IsRecording() && !_showing)
        {
            _group.gameObject.SetActive(true);
            _timer = 0f;
            _showing = true;
        }
        else if (!ObjectStateRecorder.Instance.IsRecording() && _showing)
        {
            _group.gameObject.SetActive(false);
            _group.alpha = 0f;
            _timer = 0f;
            _showing = false;
        }

        if (_showing)
        {
            _timer += Time.deltaTime;
            _group.alpha = (Mathf.Sin(_timer * Mathf.PI) + 1f) * 0.5f;
        }
    }
}
