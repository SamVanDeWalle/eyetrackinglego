﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class PlaybackManager : MonoBehaviour
{
    [SerializeField]
    private ProceduralBox _proceduralBox = null;
    [SerializeField]
    private GameObject _trailObjectPrefab = null;
    [SerializeField]
    GameObject _curvedTrailObjectPrefab = null;

    private TrailBehaviour _trail;
    CurvedTrailBehaviour _curvedTrail;
    private RecordedConnectionLoose _currentConnection = null;
    private AssemblyPiece _currentPickedUpPiece;
    private GameObject PreviewGhost = null;
    public Material PreviewGhostMaterial;
    public bool IsPlayingBack { get { return _currentConnection != null; } }
    //private bool _isPlayingBack = false;

    private List<ProceduralBox> ProceduralBoxPool = new List<ProceduralBox>();

    void SetProceduralBoxes(AssemblyPiece[] pieces)
    {
        for (int i = 0; i < pieces.Length; ++i)
        {
            ProceduralBox box;
            if (ProceduralBoxPool.Count > i)
            {
                box = ProceduralBoxPool[i];
            }
            else
            {
                box = Instantiate(_proceduralBox.gameObject).GetComponent<ProceduralBox>();
                ProceduralBoxPool.Add(box);
            }

            box.Renderers = pieces[i].Renderers;
            box.TransformToFollow = pieces[i].transform;
            box.gameObject.SetActive(true);

        }

        //hide all other Proceduralboxes
        for (int i = pieces.Length; i < ProceduralBoxPool.Count; ++i)
        {
            ProceduralBoxPool[i].gameObject.SetActive(false);
        }
    }

    AssemblyPiece[] AllPieces = null;

    void Awake()
    {
        //Create Trailobject
        //_trail = (Instantiate(_trailObjectPrefab, Vector3.zero, Quaternion.identity) as GameObject).GetComponent<TrailBehaviour>();
        //_trail.gameObject.SetActive(false);
        _curvedTrail = (Instantiate(_curvedTrailObjectPrefab, Vector3.zero, Quaternion.identity) as GameObject).GetComponent<CurvedTrailBehaviour>();
        _curvedTrail.gameObject.SetActive(false);

        AllPieces = FindObjectsOfType<AssemblyPiece>();
    }
    public void ResetAllPlaybackIds()
    {
        foreach (var p in AssemblyPieceManager.AssemblyPieces)
        {
            p.ResetRecordedStepIds();
        }

    }

    public AssemblyPieceLooseState GetSnapState(AssemblyPiece piece, AssemblyPiece parent)
    {
        if (_currentPickedUpPiece == null) return null;
        if (_currentConnection == null) return null;
        if (piece == null) return null;
        if (parent == null) return null;
        if (_currentConnection.ownerPieceRecordingIndex != piece.LastRecordedStepIndex) return null;
        if (_currentConnection.State.ParentRecordingIndex != parent.LastRecordedStepIndex) return null;

        return _currentConnection.State;

    }

    void PreviewPlacement(AssemblyPiece selected)
    {
        _currentPickedUpPiece = selected;
        //Find parent
        AssemblyPiece parent = AllPieces.FirstOrDefault(x => x.TypeID == _currentConnection.State.ParentTypeIndex && x.LastRecordedStepIndex == _currentConnection.State.ParentRecordingIndex);
        SetProceduralBoxes(new AssemblyPiece[] { });

        //_trail.gameObject.SetActive(true);
        _curvedTrail.gameObject.SetActive(true);
        //_proceduralBox.gameObject.SetActive(false);

        //Instantiate ghost
        if (PreviewGhost != null) Destroy(PreviewGhost);
        
        if (selected.GhostObject)
        {
            PreviewGhost = Instantiate(selected.GhostObject);
            PreviewGhost.gameObject.SetActive(true);
            PreviewGhost.transform.SetParent(parent.transform, true);
        
            PreviewGhost.transform.localScale = Vector3.one;
            PreviewGhost.transform.localPosition = _currentConnection.State.localPos;
            PreviewGhost.transform.localRotation = _currentConnection.State.localRot;
        
            var Renderers = PreviewGhost.GetComponentsInChildren<Renderer>();
            foreach (var r in Renderers)
            {
                r.sharedMaterial = PreviewGhostMaterial;
            }
        }

        _proceduralBox.Renderers = selected.Renderers;
        _proceduralBox.TransformToFollow = selected.transform;

        //_trail.SetTrail(parent.transform, selected.transform, _currentConnection.State.localPos);
        _curvedTrail.SetTrail(parent.transform, selected.transform, _currentConnection.State.localPos);

    }

    void StopPreviewPlacement()
    {
        //_trail.gameObject.SetActive(false);
        _curvedTrail.gameObject.SetActive(false);
        if (PreviewGhost != null) Destroy(PreviewGhost);
    }

    public void PickedUpObject(AssemblyPiece _piece)
    {
        if (_currentConnection == null) return;

        if (_piece.TypeID == _currentConnection.ownerPieceTypeId && _piece.LastRecordedStepIndex == _currentConnection.ownerPieceRecordingIndex)
        {
            PreviewPlacement(_piece);
        }
    }

    public void DroppedObject(AssemblyPiece _piece)
    {
        if (_currentConnection == null) return;

        if (_piece == _currentPickedUpPiece)
        {
            StopPreviewPlacement();
            //Preview current connection again
            PreviewConnection(_currentConnection);
        }
    }

    public bool ValidPlayback(RecordedConnectionLoose conn)
    {
        //snap rotation
        if (conn.ownerPieceTypeId == _currentConnection.ownerPieceTypeId && conn.State.MatchParent(_currentConnection.State))
        {
            if (conn.State.MatchAnglePos(_currentConnection.State))
            {
                //force localrot
                _currentPickedUpPiece.transform.localRotation = _currentConnection.State.localRot;
                _currentPickedUpPiece.transform.localPosition = _currentConnection.State.localPos;
                return true;
            }
        }

        return false;

    }

    public bool PreviewAction(RecordedAction act)
    {
        switch (act.CurrentType)
        {
            case RecordedAction.RecordedActionType.ConnectionLoose:
                return PreviewConnection(act as RecordedConnectionLoose);

            default:
                return false;
        }
    }

    public void StopPreviewAction()
    {
        if (_currentConnection != null)
        {
            StopPreviewPlacement();
            StopPreviewConnection();
        }
    }

    public bool PreviewConnection(RecordedConnectionLoose conn)
    {
        if (conn.State.ParentRecordingIndex == -1) return false;
        //Find all possible AssemblyPieces
        var validPieces = AllPieces.Where(x => x.TypeID == conn.ownerPieceTypeId && x.LastRecordedStepIndex == conn.ownerPieceRecordingIndex).ToArray();

        // Check if a valid piece is already picked up
        var skipStep = false;
        AssemblyPiece skipPiece = null;
        foreach(var controller in VR_Main.Instance.PickupControllers)
        {
            AssemblyPiece ap = null;
            ap = controller.PickedUpPickable != null ? controller.PickedUpPickable.GetComponent<AssemblyPiece>() : null;
            if (validPieces.Contains(ap))
            {
                skipPiece = ap;
                skipStep = true;
                break;
            }
        }

        // Highlight all valid pieces
        if (!skipStep)
        {
            SetProceduralBoxes(validPieces);
        }

        //_trail.gameObject.SetActive(false);
        _curvedTrail.gameObject.SetActive(false);
        //_isPlayingBack = true;
        _currentConnection = conn;

        if(skipPiece != null)
            PickedUpObject(skipPiece);
        //List<AssemblyPiece> list = new List<AssemblyPiece>() { conn.OwnerPiece };
        //list.AddRange(conn.OwnerPiece.GetComponentsInChildren<AssemblyPiece>());

        return true;
        //MaterialManager.GetInstance().FadeOthers(new AssemblyPiece[] { conn.OwnerPiece });
    }

    public void StopPreviewConnection()
    {
        //MaterialManager.GetInstance().UnfadeAll();
        _currentConnection = null;
        _proceduralBox.gameObject.SetActive(false);
    }

}
