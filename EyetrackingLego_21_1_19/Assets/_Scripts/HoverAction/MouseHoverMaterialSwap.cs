﻿using UnityEngine;
using System.Collections;

public class MouseHoverMaterialSwap : MonoBehaviour
{
    public Material HoverMaterial = null;

    // Hover over object
    public bool OverwriteHoverMainColor = false; 
    public string HoveredObjectName
    {
        get
        {
            if (HoveredObject != null)
                return HoveredObject.name;
            else
                return string.Empty;
        }
        private set { }
    }
    public GameObject HoveredObject = null;
    public Material[] PreviousMaterials = null;

    // Raycast
    public Vector2 RayScreenPercentage = new Vector2(0.5f, 0.5f);
    private Ray _ray;
    private RaycastHit _hit;

	// Update is called once per frame
	void Update ()
    {
        Vector2 rayPassPos = new Vector2(Screen.width*RayScreenPercentage.x, Screen.height * RayScreenPercentage.y);

        _ray = Camera.main.ScreenPointToRay(rayPassPos);
        if (Physics.Raycast(_ray, out _hit))
        {

            
            string newHoveredObjectName = _hit.collider.gameObject.name;

            if (newHoveredObjectName != HoveredObjectName)
            {
                //Reset old mat
                SetPreviousMats();

                Debug.Log(string.Format("Hit! Different object.{0} VS {1}", newHoveredObjectName, HoveredObjectName));

                MouseHoverObjectOptions options = _hit.collider.gameObject.GetComponent<MouseHoverObjectOptions>();
                if (options != null)
                {
                    if (!options.AllowMaterialSwap)
                        return;
                }
                else
                    return;

                // Handle new object stuff
                HoveredObject = _hit.collider.gameObject;

                Renderer newHoverObjectRenderer = HoveredObject.GetComponent<Renderer>();
                if(newHoverObjectRenderer != null && HoverMaterial != null)
                {
                    // Store prev mats
                    //Debug.Log(string.Format("Saved previous mat for {0}.", HoveredObjectName));

                    int maxMats = newHoverObjectRenderer.materials.Length;
                    PreviousMaterials = new Material[maxMats];
                    for (int i = 0; i < maxMats; ++i)
                    {
                        PreviousMaterials[i] = new Material(newHoverObjectRenderer.materials[i]);
                    }

                    Material[] newHoverMaterials = new Material[maxMats];
                    if (OverwriteHoverMainColor)
                    {
                        for (int i = 0; i < maxMats; ++i)
                        {
                            Material overwrittenHover = new Material(HoverMaterial);

                            float mode = PreviousMaterials[i].GetFloat("_Mode");
                            overwrittenHover.SetFloat("_Mode", mode);
                            int srcBlend = PreviousMaterials[i].GetInt("_SrcBlend");
                            overwrittenHover.SetInt("_SrcBlend", srcBlend);
                            int dstBlend = PreviousMaterials[i].GetInt("_DstBlend");
                            overwrittenHover.SetInt("_DstBlend", dstBlend);
                            int zWrite = PreviousMaterials[i].GetInt("_ZWrite");
                            overwrittenHover.SetInt("_ZWrite", zWrite);
                            overwrittenHover.DisableKeyword("_ALPHATEST_ON");
                            overwrittenHover.EnableKeyword("_ALPHABLEND_ON");
                            overwrittenHover.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                            overwrittenHover.renderQueue = 3000;

                            Color originalColor = PreviousMaterials[i].GetColor("_Color");
                            overwrittenHover.SetColor("_Color", originalColor);
                  
                            newHoverMaterials[i] = overwrittenHover;
                        }
                    }
                    else
                    {
                        Material overwrittenHover = new Material(HoverMaterial);

                        for (int i = 0; i < maxMats; ++i)
                        {
                            newHoverMaterials[i] = overwrittenHover;
                        }
                    }

                    newHoverObjectRenderer.materials = newHoverMaterials;
                }
            }
            //else
            //{
            //    Debug.Log(string.Format("Hit! Same object.{0} VS {1}", newHoveredObjectName, HoveredObjectName));
            //}
        }
        else
        {
            Debug.Log("Miss! ");

            SetPreviousMats();

            HoveredObject = null;
            PreviousMaterials = null;
        }
    }

    private void SetPreviousMats()
    {
        if (HoveredObject != null)
        {
            if (PreviousMaterials != null)
            {
                //Debug.Log(string.Format("Prev mat count.{0}", PreviousMaterials.Length));

                Renderer oldHoverObjectRenderer = HoveredObject.GetComponent<Renderer>();
                if (oldHoverObjectRenderer != null && PreviousMaterials != null && PreviousMaterials.Length > 0)
                {
                    Debug.Log(string.Format("Resetting previous mat for {0}.", HoveredObjectName));
                    int maxMats = PreviousMaterials.Length;
                    Material[] resetMats = new Material[maxMats];
                    for (int i = 0; i < maxMats; ++i)
                        resetMats[i] = new Material(PreviousMaterials[i]);

                    oldHoverObjectRenderer.materials = resetMats;
                }
            }
        }
    }
}
