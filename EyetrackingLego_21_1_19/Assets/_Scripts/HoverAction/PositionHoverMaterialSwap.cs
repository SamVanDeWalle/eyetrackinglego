﻿using UnityEngine;
using System.Collections;

public class PositionHoverMaterialSwap : MonoBehaviour
{
    public Material HoverMaterial = null;

    // Current hovering object 
    public string HoveredObjectName
    {
        get
        {
            if (HoveredObject != null)
                return HoveredObject.name;
            else
                return string.Empty;
        }
        private set { }
    }

    public GameObject HoveredObject = null;
    public Material[] PreviousMaterials = null;

    // Raycast
    private Ray _ray;
    private RaycastHit _hit;

	// Update is called once per frame
	void Update ()
    {
        _ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(_ray, out _hit))
        {
            MouseHoverObjectOptions options = _hit.collider.gameObject.GetComponent<MouseHoverObjectOptions>();
            if (options != null)
            {
                if (!options.AllowMaterialSwap)
                    return;
            }
            else
                return;
            
            string newHoveredObjectName = _hit.collider.gameObject.name;

            if (newHoveredObjectName != HoveredObjectName)
            {
                Debug.Log(string.Format("Hit! Different object.{0} VS {1}", newHoveredObjectName, HoveredObjectName));

                //Reset old mat
                SetPreviousMats();

                // Handle new object stuff
                HoveredObject = _hit.collider.gameObject;

                Renderer newHoverObjectRenderer = HoveredObject.GetComponent<Renderer>();
                if(newHoverObjectRenderer != null && HoverMaterial != null)
                {
                    // Store prev mats
                    Debug.Log(string.Format("Saved previous mat for {0}.", HoveredObjectName));

                    int maxMats = newHoverObjectRenderer.materials.Length;
                    PreviousMaterials = new Material[newHoverObjectRenderer.materials.Length];
                    for (int i = 0; i < maxMats; ++i)
                        PreviousMaterials[i] = new Material(newHoverObjectRenderer.materials[i]);                        

                    // Set hover mat
                    Debug.Log(string.Format("Popped hover mat on {0}.", HoveredObjectName));
                    newHoverObjectRenderer.material = HoverMaterial;
                }
            }
            else
            {
                Debug.Log(string.Format("Hit! Same object.{0} VS {1}", newHoveredObjectName, HoveredObjectName));    
            }
        }
        else
        {
            Debug.Log("Miss! ");

            SetPreviousMats();

            HoveredObject = null;
            PreviousMaterials = null;
        }
    }

    private void SetPreviousMats()
    {
        if (HoveredObject != null)
        {
            if (PreviousMaterials != null)
                Debug.Log(string.Format("Prev mat count.{0}", PreviousMaterials.Length));

            Renderer oldHoverObjectRenderer = HoveredObject.GetComponent<Renderer>();
            if (oldHoverObjectRenderer != null && PreviousMaterials != null && PreviousMaterials.Length > 0)
            {
                Debug.Log(string.Format("Resetting previous mat for {0}.", HoveredObjectName));
                int maxMats = PreviousMaterials.Length;
                Material[] resetMats = new Material[maxMats];
                for (int i = 0; i < maxMats; ++i)
                    resetMats[i] = new Material(PreviousMaterials[i]);

                oldHoverObjectRenderer.materials = resetMats;
            }
        }
    }
}
