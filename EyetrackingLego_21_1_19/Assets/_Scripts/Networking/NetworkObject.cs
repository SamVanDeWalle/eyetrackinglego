﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Events;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[Serializable]
public class ByteEvent : UnityEvent<byte[]> { }

[Serializable]
public class NetworkEvent
{
    public static Dictionary<NetworkEventArgumentTypes, NetworkEventArgumentType> ArgumentContainers = new Dictionary<NetworkEventArgumentTypes, NetworkEventArgumentType>()
    {
        { NetworkEventArgumentTypes.NEAT_Integer, new NEAT_Integer() },
        { NetworkEventArgumentTypes.NEAT_Vector3, new NEAT_Vector3() },
    };

    public int NUID { get { return (int)Action; } private set { } }
    public NetworkActions Action;
    public ByteEvent UEvent;
    public NetworkEventArgumentTypes ArgumentType;
}

public enum NetworkActions
{
    CustomPointer_Toggle,
    PickupController_Pickup,
    PickupController_Pickup_Root,
    PickupController_DropToGround,
    PickupController_DropInPlace,
    MainManager_Reset,
}

public enum NetworkEventArgumentTypes
{
    None,
    NEAT_Integer,
    NEAT_Vector3,
    NEAT_GhostConnection,
}

[Serializable]
public class NetworkEventArgumentType
{
    public byte[] Bytes;

    public virtual void Serialize() { }
    public virtual void Deserialize() { }
}

[Serializable]
public class NEAT_Integer : NetworkEventArgumentType
{
    public int Value;

    public override void Serialize()
    {
        BinaryFormatter formatter = new BinaryFormatter();

        using (MemoryStream ms = new MemoryStream())
        {
            formatter.Serialize(ms, this);
            Bytes = ms.ToArray();
        }
    }

    public override void Deserialize()
    {
        using (var memStream = new MemoryStream())
        {
            var formatter = new BinaryFormatter();
            memStream.Write(Bytes, 0, Bytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            var obj = (NEAT_Integer)formatter.Deserialize(memStream);
            Value = obj.Value;
        }
    }
}

[Serializable]
public class NEAT_Vector3 : NetworkEventArgumentType
{
    public float X, Y, Z;

    public override void Serialize()
    {
        BinaryFormatter formatter = new BinaryFormatter();

        using (MemoryStream ms = new MemoryStream())
        {
            formatter.Serialize(ms, this);
            Bytes = ms.ToArray();
        }
    }

    public override void Deserialize()
    {
        using (var memStream = new MemoryStream())
        {
            var formatter = new BinaryFormatter();
            memStream.Write(Bytes, 0, Bytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            var obj = (NEAT_Vector3)formatter.Deserialize(memStream);
            X = obj.X;
            Y = obj.Y;
            Z = obj.Z;
        }
    }
}

[Serializable]
public class NEAT_GhostConnection : NetworkEventArgumentType
{
    public int ParentNuid;
    public float PosX, PosY, PosZ;
    public float RotX, RotY, RotZ, RotW;

    public override void Serialize()
    {
        BinaryFormatter formatter = new BinaryFormatter();

        using (MemoryStream ms = new MemoryStream())
        {
            formatter.Serialize(ms, this);
            Bytes = ms.ToArray();
        }
    }

    public override void Deserialize()
    {
        using (var memStream = new MemoryStream())
        {
            var formatter = new BinaryFormatter();
            memStream.Write(Bytes, 0, Bytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            var obj = (NEAT_GhostConnection)formatter.Deserialize(memStream);
            ParentNuid = obj.ParentNuid;
            PosX = obj.PosX;
            PosY = obj.PosY;
            PosZ = obj.PosZ;
            RotX = obj.RotX;
            RotY = obj.RotY;
            RotZ = obj.RotZ;
            RotW = obj.RotW;
        }
    }

    public void SetParentNuid(AssemblyPiece parentAP)
    {
        ParentNuid = parentAP.GetComponent<NetworkObject>().NUID;
    }

    public void SetPosition(Vector3 position)
    {
        PosX = position.x;
        PosY = position.y;
        PosZ = position.z;
    }

    public void SetRotation(Quaternion rotation)
    {
        RotX = rotation.x;
        RotY = rotation.y;
        RotZ = rotation.z;
        RotW = rotation.w;
    }

    public Vector3 GetPosition()
    {
        return new Vector3(PosX, PosY, PosZ);
    }

    public Quaternion GetRotation()
    {
        return new Quaternion(RotX, RotY, RotZ, RotW);
    }
}

public class NetworkObject : MonoBehaviour
{
    // Unique network ID
    public int NUID;

    public List<NetworkEvent> Events = new List<NetworkEvent>();

    public delegate void NetworkAction(int objectNUID, int eventNUID, byte[] arguments);
    public event NetworkAction OnNetworkAction;

    public void InvokeEvent(int eventNUID, byte[] arguments = null)
    {
        Debug.Log("Invoking event: " + eventNUID);
        var networkEvent = Events.FirstOrDefault(e => e.NUID == eventNUID);
        if(networkEvent == null)
        {
            Debug.LogWarning("Connected PC tried to invoke an event that could not be found locally");
            return;
        }

        networkEvent.UEvent.Invoke(arguments);
    }

    public void CatchAction(int nuid, byte[] arguments = null)
    {
        Debug.Log("catching action: " + nuid);
        var handler = OnNetworkAction;
        if(handler != null)
            OnNetworkAction(NUID, nuid, arguments);
    }

    public void CatchAction(NetworkActions action, byte[] arguments = null)
    {

        Debug.Log("catching networkaction: " + action.ToString());
        CatchAction((int)action, arguments);
    }
}
