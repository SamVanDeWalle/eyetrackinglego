﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;
using System;
using System.IO;
using Helpers;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Collections;

/*
 * 
 * Custom implementation for collaboration over network, not updated after lego demo
 * 
 * Docs: https://docs.unity3d.com/Manual/UNetUsingTransport.html
 */

[Serializable]
public enum PackageType
{
    ConnectionConfirmation,
    UserRequest,
    UserConfirmation,
    PlayerData,
    PlayerPrefabsRequest,
    TextMessage,
    ObjectTransformation,
    ObjectPosRot,
    ObjectEvent,
    SpawnPrefab,
    ResetWorld,
}

[Serializable]
public class DataPackage
{
    public PackageType PackageType;
    public byte[] Content;
    public Dictionary<PackageType, PackageContent> ContentTypes = new Dictionary<PackageType, PackageContent>()
    {
        { PackageType.ConnectionConfirmation, new ConnectionConfirmation() },
        { PackageType.UserRequest, new UserRequest() },
        { PackageType.UserConfirmation, new UserConfirmation() },
        { PackageType.PlayerData, new PlayerData() },
        { PackageType.PlayerPrefabsRequest, new PlayerPrefabsRequest() },
        { PackageType.TextMessage, new TextMessage() },
        { PackageType.ObjectTransformation, new ObjectTransformation() },
        { PackageType.ObjectPosRot, new ObjectPosRot() },
        { PackageType.ObjectEvent, new ObjectEvent() },
        { PackageType.SpawnPrefab, new SpawnPrefab() },
        { PackageType.ResetWorld, new ResetWorld()}
    };

    public void UnpackContent()
    {
        var type = ContentTypes[PackageType];
        type.Deserialize(Content);
        type.HandleContent();
    }

    public void PackContent()
    {

    }

    public byte[] Serialize()
    {
        using (MemoryStream m = new MemoryStream())
        {
            using (BinaryWriter writer = new BinaryWriter(m))
            {
                writer.Write((int)PackageType);
                writer.Write(Content);
            }
            return m.ToArray();
        }
    }

    public static DataPackage Deserialize(byte[] data)
    {
        DataPackage result = new DataPackage();
        using (MemoryStream m = new MemoryStream(data))
        {
            using (BinaryReader reader = new BinaryReader(m))
            {
                var step = 0;
                result.PackageType = (PackageType)reader.ReadInt32();
                step += sizeof(Int32);
                result.Content = reader.ReadBytes(data.Length - step);
            }
        }
        return result;
    }
}

[Serializable]
public class PackageContent
{
    public virtual void HandleContent() { }

    public virtual void Deserialize(byte[] data) { }
}

[Serializable]
public class ConnectionConfirmation : PackageContent
{
    public int ConnectionID;

    public override void HandleContent()
    {
        NetworkHandling.Instance.ConfirmServerConnection(ConnectionID);
    }

    public override void Deserialize(byte[] data)
    {
        using (var memStream = new MemoryStream())
        {
            var formatter = new BinaryFormatter();
            memStream.Write(data, 0, data.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            var obj = (ConnectionConfirmation)formatter.Deserialize(memStream);
            ConnectionID = obj.ConnectionID;
        }
    }

    public byte[] Serialize()
    {
        BinaryFormatter formatter = new BinaryFormatter();

        using (MemoryStream ms = new MemoryStream())
        {
            formatter.Serialize(ms, this);
            return ms.ToArray();
        }
    }
}

[Serializable]
public class UserRequest : PackageContent
{
    public int UserType;
    public int ConnectionID;

    public override void HandleContent()
    {
        if(NetworkHandling.Instance.IsHost)
            UserManager.AddUser(UserType, ConnectionID);
    }

    public override void Deserialize(byte[] data)
    {
        using (var memStream = new MemoryStream())
        {
            var formatter = new BinaryFormatter();
            memStream.Write(data, 0, data.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            var obj = (UserRequest)formatter.Deserialize(memStream);
            UserType = obj.UserType;
            ConnectionID = obj.ConnectionID;
        }
    }

    public byte[] Serialize()
    {
        BinaryFormatter formatter = new BinaryFormatter();

        using (MemoryStream ms = new MemoryStream())
        {
            formatter.Serialize(ms, this);
            return ms.ToArray();
        }
    }
}

[Serializable]
public class UserConfirmation : PackageContent
{
    public int UserID;

    public override void HandleContent()
    {
        NetworkHandling.Instance.PostUserConfirmation(UserID);
    }

    public override void Deserialize(byte[] data)
    {
        using (var memStream = new MemoryStream())
        {
            var formatter = new BinaryFormatter();
            memStream.Write(data, 0, data.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            var obj = (UserConfirmation)formatter.Deserialize(memStream);
            UserID = obj.UserID;
        }
    }

    public byte[] Serialize()
    {
        BinaryFormatter formatter = new BinaryFormatter();

        using (MemoryStream ms = new MemoryStream())
        {
            formatter.Serialize(ms, this);
            return ms.ToArray();
        }
    }
}

[Serializable]
public class PlayerData : PackageContent
{
    public List<Player> Players = new List<Player>();

    public override void HandleContent()
    {
        PlayerManager.CheckPlayerData(Players);
    }

    public override void Deserialize(byte[] data)
    {
        using (var memStream = new MemoryStream())
        {
            var formatter = new BinaryFormatter();
            memStream.Write(data, 0, data.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            var obj = (PlayerData)formatter.Deserialize(memStream);
            Players = obj.Players;
        }
    }

    public byte[] Serialize()
    {
        BinaryFormatter formatter = new BinaryFormatter();

        using (MemoryStream ms = new MemoryStream())
        {
            formatter.Serialize(ms, this);
            return ms.ToArray();
        }
    }
}

[Serializable]
public class PlayerPrefabsRequest : PackageContent
{
    public int PlayerNUID;

    public override void HandleContent()
    {
        Debug.Log("request for player prefabs received : " + PlayerNUID + " - " + PlayerManager.Player.NUID);
        if (PlayerManager.Player.NUID == PlayerNUID)
            NetworkHandling.Instance.SendPlayerPrefabs();
    }

    public override void Deserialize(byte[] data)
    {
        using (var memStream = new MemoryStream())
        {
            var formatter = new BinaryFormatter();
            memStream.Write(data, 0, data.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            var obj = (PlayerPrefabsRequest)formatter.Deserialize(memStream);
            PlayerNUID = obj.PlayerNUID;
        }
    }

    public byte[] Serialize()
    {
        BinaryFormatter formatter = new BinaryFormatter();

        using (MemoryStream ms = new MemoryStream())
        {
            formatter.Serialize(ms, this);
            return ms.ToArray();
        }
    }
}

[Serializable]
public class ObjectPosRot : PackageContent
{
    public int NUID;
    public float PosX, PosY, PosZ;
    public float RotX, RotY, RotZ, RotW;

    public override void HandleContent()
    {
        if (!NetworkHandling.NetworkObjects.ContainsKey(NUID))
        {
            Debug.LogWarning("Received ObjectPosRot update for non existing networkobject");
            return;
        }

        var gameObject = NetworkHandling.NetworkObjects[NUID].gameObject;
        if (!gameObject)
            return;

        var pos = gameObject.transform.position;
        pos.x = PosX;
        pos.y = PosY;
        pos.z = PosZ;
        gameObject.transform.position = pos;

        var rot = gameObject.transform.rotation;
        rot.x = RotX;
        rot.y = RotY;
        rot.z = RotZ;
        rot.w = RotW;
        gameObject.transform.rotation = rot;
    }

    public override void Deserialize(byte[] data)
    {
        using (var memStream = new MemoryStream())
        {
            var formatter = new BinaryFormatter();
            memStream.Write(data, 0, data.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            var obj = (ObjectPosRot)formatter.Deserialize(memStream);
            NUID = obj.NUID;
            PosX = obj.PosX;
            PosY = obj.PosY;
            PosZ = obj.PosZ;
            RotX = obj.RotX;
            RotY = obj.RotY;
            RotZ = obj.RotZ;
            RotW = obj.RotW;
        }
    }

    public byte[] Serialize()
    {
        BinaryFormatter formatter = new BinaryFormatter();

        using (MemoryStream ms = new MemoryStream())
        {
            formatter.Serialize(ms, this);
            return ms.ToArray();
        }
    }
}

[Serializable]
public class ObjectTransformation : PackageContent
{
    [SerializeField]
    public int NUID;
    [SerializeField]
    public Vector3 Position;
    [SerializeField]
    public Quaternion Rotation;
    [SerializeField]
    public Vector3 Scale;

    public override void HandleContent()
    {
        var gameObject = NetworkHandling.NetworkObjects[NUID].gameObject;
        if (!gameObject)
            return;

        gameObject.transform.position = Position;
        gameObject.transform.rotation = Rotation;
        gameObject.transform.localScale = Scale;
    }

    public override void Deserialize(byte[] data)
    {
        using (var memStream = new MemoryStream())
        {
            var formatter = new BinaryFormatter();

            // 1. Construct a SurrogateSelector object
            SurrogateSelector ss = new SurrogateSelector();

            Vector3SerializationSurrogate v3ss = new Vector3SerializationSurrogate();
            ss.AddSurrogate(typeof(Vector3),
                            new StreamingContext(StreamingContextStates.All),
                            v3ss);
            QuaternionSerializationSurrogate qss = new QuaternionSerializationSurrogate();
            ss.AddSurrogate(typeof(Quaternion),
                            new StreamingContext(StreamingContextStates.All),
                            qss);

            // 2. Have the formatter use our surrogate selector
            formatter.SurrogateSelector = ss;

            memStream.Write(data, 0, data.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            var obj = (ObjectTransformation)formatter.Deserialize(memStream);
            NUID = obj.NUID;
            Position = obj.Position;
            Rotation = obj.Rotation;
            Scale = obj.Scale;
        }
    }

    public byte[] Serialize()
    {
        BinaryFormatter formatter = new BinaryFormatter();

        // 1. Construct a SurrogateSelector object
        SurrogateSelector ss = new SurrogateSelector();

        Vector3SerializationSurrogate v3ss = new Vector3SerializationSurrogate();
        ss.AddSurrogate(typeof(Vector3),
                        new StreamingContext(StreamingContextStates.All),
                        v3ss);
        QuaternionSerializationSurrogate qss = new QuaternionSerializationSurrogate();
        ss.AddSurrogate(typeof(Quaternion),
                        new StreamingContext(StreamingContextStates.All),
                        qss);

        // 2. Have the formatter use our surrogate selector
        formatter.SurrogateSelector = ss;

        using (MemoryStream ms = new MemoryStream())
        {
            formatter.Serialize(ms, this);
            return ms.ToArray();
        }
    }
}

[Serializable]
public class TextMessage : PackageContent
{
    public string Message;

    public override void HandleContent()
    {
        Debug.Log("Message: " + Message);
    }

    public override void Deserialize(byte[] data)
    {
        Message = NetworkHandling.GetStringFromBytes(data);
    }
}
[Serializable]
public class ResetWorld : PackageContent
{
    public override void HandleContent()
    {
        MainManager.Instance.ResetWorld();
    }

    public override void Deserialize(byte[] data)
    {
        //Do nothing
    }
}
[Serializable]
public class SpawnPrefab : PackageContent
{
    public string Name;
    public int NUID;
    public int PlayerNUID;

    public override void HandleContent()
    {
        NetworkPrefabPool.Instance.Spawn(Name, NUID, PlayerNUID);
    }

    public override void Deserialize(byte[] data)
    {
        using (var memStream = new MemoryStream())
        {
            var formatter = new BinaryFormatter();
            memStream.Write(data, 0, data.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            var obj = (SpawnPrefab)formatter.Deserialize(memStream);
            Name = obj.Name;
            NUID = obj.NUID;
            PlayerNUID = obj.PlayerNUID;
        }
    }

    public byte[] Serialize()
    {
        BinaryFormatter formatter = new BinaryFormatter();

        using (MemoryStream ms = new MemoryStream())
        {
            formatter.Serialize(ms, this);
            return ms.ToArray();
        }
    }
}

[Serializable]
public class ObjectEvent : PackageContent
{
    public int EventNUID;
    public int ObjectNUID;
    public byte[] Arguments;

    public override void HandleContent()
    {
        Debug.Log("Handling content of ObjectEvent " + ObjectNUID + " - " + EventNUID);
        NetworkHandling.NetworkObjects[ObjectNUID].InvokeEvent(EventNUID, Arguments);
    }

    public override void Deserialize(byte[] data)
    {
        using (var memStream = new MemoryStream())
        {
            var formatter = new BinaryFormatter();
            memStream.Write(data, 0, data.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            var obj = (ObjectEvent)formatter.Deserialize(memStream);
            EventNUID = obj.EventNUID;
            ObjectNUID = obj.ObjectNUID;
            Arguments = obj.Arguments;
        }
    }

    public byte[] Serialize()
    {
        BinaryFormatter formatter = new BinaryFormatter();

        using (MemoryStream ms = new MemoryStream())
        {
            formatter.Serialize(ms, this);
            return ms.ToArray();
        }
    }
}

public class NetworkHandling : Singleton<NetworkHandling>
{
    public UserType UserType = UserType.Player;
    public bool IsHost;
    public string ServerIP = "172.20.32.27";
    public int Port = 8888;
    public float UpdateRate = 24;
    public static bool IsConnected;
    public static int ServerConnectionID;

    bool _bSet;
    bool _bReceiving;
    int _hostId;
    int _reliableChannelId, _unReliableChannelId;
    int _connectionId;
    List<int> _clientConnections = new List<int>();
    public static Dictionary<int, NetworkObject> NetworkObjects = new Dictionary<int, NetworkObject>();
    static Dictionary<int, GameObject> _transformationSubscriptions = new Dictionary<int, GameObject>();
    static Dictionary<int, List<int>> _eventSubscriptions = new Dictionary<int, List<int>>();

    void Start()
    {
        if(Main.Instance)
        {
            IsHost = Main.Instance.IsHost;
            ServerIP = Main.Instance.HostIP;
            Port = Main.Instance.Port;
            UserType = Main.Instance.UserType;

            LoadNetworkObjects();
            Setup();
        }
    }

    void OnDestroy()
    {
        //TODO Send disconnect
        StopCoroutine("HandleReceiving");
        ShutDown();
    }

    public void ShutDown()
    {
        _bReceiving = false;
        byte err;
        //NetworkTransport.Disconnect(_hostId, _connectionId, out err);
        NetworkTransport.Shutdown();
    }

    void LoadNetworkObjects()
    {
        NetworkObjects.Clear();
        var netObjects = FindObjectsOfType<NetworkObject>();

        for (int i = 0; i < netObjects.Length; i++)
        {
            if (NetworkObjects.ContainsKey(netObjects[i].NUID))
            {
                Debug.LogError("Duplicate NUID: " +netObjects[i].NUID);
            }
            else
                NetworkObjects.Add(netObjects[i].NUID, netObjects[i]);
        }
    }

    void Setup()
    {
        NetworkTransport.Init();

        ConnectionConfig config = new ConnectionConfig();
        _reliableChannelId = config.AddChannel(QosType.Reliable);
        _unReliableChannelId = config.AddChannel(QosType.Unreliable);

        HostTopology topology = new HostTopology(config, 10);

        _hostId = NetworkTransport.AddHost(topology, Port);

        gameObject.AddComponent<PlayerManager>();

        byte error;
        if (!IsHost)
        {
            _connectionId = NetworkTransport.Connect(_hostId, ServerIP, Port, 0, out error);
            Debug.Log("Connection: " + (NetworkError)error);
        }
        else
        {
            gameObject.AddComponent<UserManager>();
            UserManager.AddHostUser(UserType);
        }

        _bSet = true;
        _bReceiving = true;
        StartCoroutine(HandleReceiving());
    }

    IEnumerator HandleReceiving()
    {
        while (_bReceiving)
        {
            yield return null;

            int recHostId;
            int connectionId;
            int channelId;
            byte[] recBuffer = new byte[512];
            int bufferSize = 512;
            int dataSize;
            byte error;
            NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);
            switch (recData)
            {
                case NetworkEventType.Nothing:         //1
                    //Debug.Log("Nothing");
                    break;
                case NetworkEventType.ConnectEvent:    //2
                    Debug.Log("ConnectEvent");
                    int port;
                    ulong network;
                    ushort dstNode;
                    byte err;
                    NetworkTransport.GetConnectionInfo(recHostId, connectionId, out port, out network, out dstNode, out err);
                    Debug.Log("err: " + (NetworkError)err);
                    if (IsHost && connectionId == _connectionId)
                        break;

                    Debug.Log("connid: " + connectionId + " - " + _connectionId);
                    _clientConnections.Add(connectionId);
                    //if(UserType != UserType.Spectator)
                    //{
                    //    SendPostConnectionData(connectionId);
                    //}
                    if(IsHost)
                        SendConnectionConfirmation(connectionId);

                    break;
                case NetworkEventType.DataEvent:       //3
                    var dataBuffer = SubArray(recBuffer, 0, dataSize);
                    //var str = GetStringFromBytes(dataBuffer);
                    //Debug.Log("DataEvent: " + dataSize);
                    //TODO
                    //As host, distribute to all connections except the sending one
                    if (IsHost)
                    {
                        SendData(dataBuffer, false, new List<int>() { connectionId });
                    }

                    OpenDataPackage(dataBuffer);
                    break;
                case NetworkEventType.DisconnectEvent: //4
                    Debug.Log("DisconnectEvent");
                    break;
            }

            //yield return new WaitForSeconds(1.0f / 300.0f);
        }
    }

    void SendConnectionConfirmation(int connectionID)
    {
        var dataPackage = new DataPackage();
        dataPackage.PackageType = PackageType.ConnectionConfirmation;
        var connectionConfirmation = new ConnectionConfirmation();
        connectionConfirmation.ConnectionID = connectionID;
        dataPackage.Content = connectionConfirmation.Serialize();

        var dataBuffer = dataPackage.Serialize();

        SendData(dataBuffer, connectionID, true);
    }

    public void ConfirmServerConnection(int connectionID)
    {
        ServerConnectionID = connectionID;
        IsConnected = true;
        SendUserRequest(UserType);
    }

    void SendUserRequest(UserType type)
    {
        var dataPackage = new DataPackage();
        dataPackage.PackageType = PackageType.UserRequest;
        var userRequest = new UserRequest();
        userRequest.UserType = (int)type;
        userRequest.ConnectionID = ServerConnectionID;
        dataPackage.Content = userRequest.Serialize();

        var dataBuffer = dataPackage.Serialize();

        SendData(dataBuffer, true);
    }

    public void SendUserConfirmation(int connectionID, int userID, int colorID = -1)
    {
        var dataPackage = new DataPackage();
        dataPackage.PackageType = PackageType.UserConfirmation;
        var userConfirmation = new UserConfirmation();
        userConfirmation.UserID = userID;
        dataPackage.Content = userConfirmation.Serialize();

        var dataBuffer = dataPackage.Serialize();

        SendData(dataBuffer, connectionID, true);
    }

    public void PostUserConfirmation(int userID)
    {
        if(UserType == UserType.Player)
        {
            PlayerManager.Player.NUID = userID;
        }
    }

    public void SendPlayerData()
    {
        var dataPackage = new DataPackage();
        dataPackage.PackageType = PackageType.PlayerData;
        var playerData = new PlayerData();
        playerData.Players = PlayerManager.GetAllPlayers();
        dataPackage.Content = playerData.Serialize();

        var dataBuffer = dataPackage.Serialize();

        SendData(dataBuffer, true);
    }

    public void SendPlayerPrefabsRequest(int playerNUID)
    {
        var dataPackage = new DataPackage();
        dataPackage.PackageType = PackageType.PlayerPrefabsRequest;
        var playerPrefabsRequest = new PlayerPrefabsRequest();
        playerPrefabsRequest.PlayerNUID = playerNUID;
        dataPackage.Content = playerPrefabsRequest.Serialize();

        var dataBuffer = dataPackage.Serialize();

        SendData(dataBuffer, true);
    }

    void HandleTransformations()
    {
        foreach (var subscription in _transformationSubscriptions)
        {
            //SendTransformationUpdate(subscription.Key, subscription.Value.transform);
            SendPosRotUpdate(subscription.Key, subscription.Value.transform);
        }
    }

    void SendTransformationUpdate(int nuid, Transform transform)
    {
        var dataPackage = new DataPackage();
        dataPackage.PackageType = PackageType.ObjectTransformation;
        var objectTransformation = new ObjectTransformation();
        objectTransformation.NUID = nuid;
        objectTransformation.Position = transform.position;
        objectTransformation.Rotation = transform.rotation;
        objectTransformation.Scale = transform.localScale;
        dataPackage.Content = objectTransformation.Serialize();

        var dataBuffer = dataPackage.Serialize();

        SendData(dataBuffer);
    }

    public IEnumerator UpdateTransformSubscriber(KeyValuePair<int, GameObject> subscription)
    {
        Debug.Log("Updating started on: " + subscription.Key);
        while(true)
        {
            if(subscription.Value == null)
            {
                Debug.LogError("NetworkHandling -- subscription transform is null");
                break;
            }

            SendPosRotUpdate(subscription.Key, subscription.Value.transform);
            yield return new WaitForSeconds(1.0f/UpdateRate);
        }
    }
    public void SendReset()
    {
        var dataPackage = new DataPackage();
        dataPackage.PackageType = PackageType.ResetWorld;
        dataPackage.Content = new byte[] { };
        SendData(dataPackage.Serialize(), true);
    }

    void SendPosRotUpdate(int nuid, Transform transform, bool reliable = false)
    {
        var dataPackage = new DataPackage();
        dataPackage.PackageType = PackageType.ObjectPosRot;
        var objectPosRot = new ObjectPosRot();
        objectPosRot.NUID = nuid;
        objectPosRot.PosX = transform.position.x;
        objectPosRot.PosY = transform.position.y;
        objectPosRot.PosZ = transform.position.z;
        objectPosRot.RotX = transform.rotation.x;
        objectPosRot.RotY = transform.rotation.y;
        objectPosRot.RotZ = transform.rotation.z;
        objectPosRot.RotW = transform.rotation.w;
        dataPackage.Content = objectPosRot.Serialize();

        var dataBuffer = dataPackage.Serialize();

        SendData(dataBuffer, reliable);
    }

    public void SendTwinPosRotUpdate(NetworkObject netObject, bool reliable = false)
    {
        if(netObject == null)
        {
            Debug.LogWarning("NetworkHandling - SendTwinPosRotUpdate - netObject is null");
            return;
        }

        SendPosRotUpdate(netObject.NUID, netObject.transform, reliable);
    }

    public void SendData(byte[] dataBuffer, int connectionId, bool reliable = false)
    {
        byte err;
        int channelId = reliable ? _reliableChannelId : _unReliableChannelId;
        NetworkTransport.Send(_hostId, connectionId, channelId, dataBuffer, dataBuffer.Length, out err);
    }

    public void SendData(byte[] dataBuffer, bool reliable = false, List<int> ignoreConnections = null)
    {
        if (IsHost)
        {
            for (int i = 0; i < _clientConnections.Count; i++)
            {
                if(ignoreConnections == null || !ignoreConnections.Contains(_clientConnections[i]))
                    SendData(dataBuffer, _clientConnections[i], reliable);
            }
        }
        else
        {
            SendData(dataBuffer, _connectionId, reliable);
        }
    }

    public void SendPlayerPrefabs()
    {
        for (int i = 0; i < _clientConnections.Count; i++)
        {
            SendPlayerPrefabs(_clientConnections[i]);
        }
    }

    public void SendPlayerPrefabs(int connectionId)
    {
        SpawnLocalControlledRemoteNetworkPrefab("Head", ViveHelper.Head.gameObject, connectionId);
        System.Threading.Thread.Sleep(100);
        SpawnLocalControlledRemoteNetworkPrefab("Controller1", ViveHelper.Controllers[ControllerIndex.Left].Transform.gameObject, connectionId, true);
        System.Threading.Thread.Sleep(100);
        SpawnLocalControlledRemoteNetworkPrefab("Controller2", ViveHelper.Controllers[ControllerIndex.Right].Transform.gameObject, connectionId, true);
    }

    public void OpenDataPackage(byte[] data)
    {
        var dataPackage = DataPackage.Deserialize(data);
        //Debug.Log("package received of type: " + dataPackage.PackageType.ToString());
        dataPackage.UnpackContent();
    }

    public void SpawnLocalControlledRemoteNetworkPrefab(string networkPrefabName, GameObject localGuide, int connectionId, bool subscribeEvents = false)
    {
        // Tells connected pc's to spawn a prefab and subscribe its movement to a local gameObject
        var spawnPrefab = new SpawnPrefab();
        spawnPrefab.Name = networkPrefabName;
        spawnPrefab.PlayerNUID = PlayerManager.Player.NUID;
        var guideNetworkObject = localGuide.GetComponent<NetworkObject>();
        if(guideNetworkObject == null)
        {
            guideNetworkObject = localGuide.AddComponent<NetworkObject>();
        }
        if(guideNetworkObject.NUID == 0)
        {
            System.Random rand = new System.Random();
            guideNetworkObject.NUID = rand.Next();
        }
        spawnPrefab.NUID = guideNetworkObject.NUID;

        var dataPackage = new DataPackage();
        dataPackage.PackageType = PackageType.SpawnPrefab;
        dataPackage.Content = spawnPrefab.Serialize();

        var dataBuffer = dataPackage.Serialize();

        SendData(dataBuffer, connectionId);

        SubscribeTransformContinuous(spawnPrefab.NUID, localGuide);
        if (subscribeEvents)
            SubscribeEvents(localGuide.GetComponent<NetworkObject>(), spawnPrefab.NUID);
    }

    public void SubscribeTransformContinuous(int nuid, GameObject localGuide)
    {
        // Prevent double subscription
        if (_transformationSubscriptions.ContainsKey(nuid))
            return;

        // Get a transform to update its transformation continuously
        _transformationSubscriptions.Add(nuid, localGuide);

        StartCoroutine(UpdateTransformSubscriber(new KeyValuePair<int, GameObject>(nuid, localGuide)));
        //TODO recover from localguide going null and respawning as different object
    }

    public void SubscribeEvents(NetworkObject local, int foreignNUID)
    {
        if (!_eventSubscriptions.ContainsKey(local.NUID))
            _eventSubscriptions.Add(local.NUID, new List<int>());

        // Prevent double subscription
        if (_eventSubscriptions[local.NUID].Contains(foreignNUID))
            return;

        _eventSubscriptions[local.NUID].Add(foreignNUID);
        local.OnNetworkAction += RedirectConnectedEvent;
    }

    public void RedirectConnectedEvent(int objectNUID, int eventNUID, byte[] arguments = null)
    {
        Debug.Log("Redirecting event: " + objectNUID + " - " + eventNUID + " - " + _eventSubscriptions[objectNUID].Count);
        foreach(var subscriber in _eventSubscriptions[objectNUID])
        {
            SendObjectEvent(subscriber, eventNUID, arguments);
        }
    }

    public void SendObjectEvent(int objectNUID, int eventNUID, byte[] arguments)
    {
        Debug.Log("Sending object event: " + objectNUID + " - " + eventNUID);
        var dataPackage = new DataPackage();
        dataPackage.PackageType = PackageType.ObjectEvent;
        var objectEvent = new ObjectEvent();
        objectEvent.ObjectNUID = objectNUID;
        objectEvent.EventNUID = eventNUID;
        objectEvent.Arguments = arguments;
        dataPackage.Content = objectEvent.Serialize();

        var dataBuffer = dataPackage.Serialize();

        SendData(dataBuffer, true);
    }

    //Helpers

    static byte[] GetBytes(string str)
    {
        byte[] bytes = new byte[str.Length * sizeof(char)];
        System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
        return bytes;
    }

    public static string GetStringFromBytes(byte[] bytes)
    {
        char[] chars = new char[bytes.Length / sizeof(char)];
        System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
        string str = new string(chars);
        return str;
    }

    static byte[] SubArray(byte[] data, int index, int length)
    {
        byte[] result = new byte[length];
        Array.Copy(data, index, result, 0, length);
        return result;
    }

    public static GameObject FindGameObjectWithName(string name)
    {
        return GameObject.Find(name);
    }

    public void CreateNetworkObjectsFromAssemblyPieces()
    {
        var pieces = FindObjectsOfType<AssemblyPiece>();

        for (int i = 0; i < pieces.Length; i++)
        {
            var networkObject = pieces[i].GetComponent<NetworkObject>();

            if(networkObject == null)
            {
                networkObject = pieces[i].gameObject.AddComponent<NetworkObject>();
            }

            networkObject.NUID = 1000 + i;
            NetworkObjects.Add(networkObject.NUID, networkObject);
        }
    }
}

// Surrogates for serialization
sealed class AssemblyPieceSerializationSurrogate : ISerializationSurrogate
{
    public void GetObjectData( object obj, SerializationInfo info, StreamingContext context )
    {
        throw new NotImplementedException();
    }

    public object SetObjectData( object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector )
    {
        throw new NotImplementedException();
    }
}
sealed class Vector3SerializationSurrogate : ISerializationSurrogate
{

    // Method called to serialize a Vector3 object
    public void GetObjectData(System.Object obj,
                              SerializationInfo info, StreamingContext context)
    {

        Vector3 v3 = (Vector3)obj;
        info.AddValue("x", v3.x);
        info.AddValue("y", v3.y);
        info.AddValue("z", v3.z);
        //Debug.Log(v3);
    }

    // Method called to deserialize a Vector3 object
    public System.Object SetObjectData(System.Object obj,
                                       SerializationInfo info, StreamingContext context,
                                       ISurrogateSelector selector)
    {

        Vector3 v3 = (Vector3)obj;
        v3.x = (float)info.GetValue("x", typeof(float));
        v3.y = (float)info.GetValue("y", typeof(float));
        v3.z = (float)info.GetValue("z", typeof(float));
        obj = v3;
        return obj;   // Formatters ignore this return value //Seems to have been fixed!
    }
}

sealed class QuaternionSerializationSurrogate : ISerializationSurrogate
{

    // Method called to serialize a Vector3 object
    public void GetObjectData(System.Object obj,
                              SerializationInfo info, StreamingContext context)
    {

        Quaternion q = (Quaternion)obj;
        info.AddValue("x", q.x);
        info.AddValue("y", q.y);
        info.AddValue("z", q.z);
        info.AddValue("w", q.w);
        //Debug.Log(q);
    }

    // Method called to deserialize a Vector3 object
    public System.Object SetObjectData(System.Object obj,
                                       SerializationInfo info, StreamingContext context,
                                       ISurrogateSelector selector)
    {

        Quaternion q = (Quaternion)obj;
        q.x = (float)info.GetValue("x", typeof(float));
        q.y = (float)info.GetValue("y", typeof(float));
        q.z = (float)info.GetValue("z", typeof(float));
        q.w = (float)info.GetValue("w", typeof(float));
        obj = q;
        return obj;   // Formatters ignore this return value //Seems to have been fixed!
    }
}
