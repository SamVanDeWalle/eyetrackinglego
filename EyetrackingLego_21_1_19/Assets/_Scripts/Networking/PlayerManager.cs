﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class PlayerManager : MonoBehaviour
{
    public static Player Player = new Player();
    static List<Player> _foreignPlayers = new List<Player>();
    static Dictionary<int, Color> _playerColors = new Dictionary<int, Color>();
    static List<int> _freeColorIDs = new List<int>();
    static bool _bPlayerSet;

    void Awake()
    {
        GetColors();
    }

    void GetColors()
    {
        int colorID = 0;
        if (UserColors.Instance == null) return;
        foreach (var color in UserColors.Instance.Colors)
        {
            _playerColors.Add(colorID, color);
            _freeColorIDs.Add(colorID);
            ++colorID;
        }
    }

    public static void AddPlayer(Player player)
    {
        _foreignPlayers.Add(player);
    }

    public static void AddPlayer(int userID, int colorID)
    {
        var player = new Player();
        player.NUID = userID;
        player.ColorID = colorID;
        _foreignPlayers.Add(player);
    }

    public static void CheckPlayerData(List<Player> players)
    {
        foreach(var player in players)
        {
            if (_foreignPlayers.Contains(player))
            {
                continue;
            }
            else
            {
                if (Player.NUID == player.NUID)
                {
                    if(!_bPlayerSet)
                    {
                        Player.ColorID = player.ColorID;
                        SetPlayer();
                    }
                }
                else
                {
                    _foreignPlayers.Add(player);
                    NetworkHandling.Instance.SendPlayerPrefabsRequest(player.NUID);
                }
            }
        }
    }

    public static void SetPlayer()
    {
        _bPlayerSet = true;
        NetworkHandling.Instance.SendPlayerPrefabs();
        Debug.Log("setting own player");
        foreach(var controller in ViveHelper.Controllers)
        {
            controller.Value.Transform.GetComponent<SteamVR_LaserPointer>().color = _playerColors[Player.ColorID];
        }
    }

    public static List<Player> GetAllPlayers()
    {
        List<Player> players = _foreignPlayers;
        players.Add(Player);
        return players;
    }

    public static int GetNextFreeColorID()
    {
        if (_freeColorIDs.Count == 0) return 0;

        var cid = _freeColorIDs[0];
        _freeColorIDs.RemoveAt(0);
        return cid;
    }

    public static Color GetPlayerColor(int playerID)
    {
        var player = _foreignPlayers.FirstOrDefault<Player>(p => p.NUID == playerID);
        if (player == null)
            return Color.white;

        return _playerColors[player.ColorID];
    }
}
