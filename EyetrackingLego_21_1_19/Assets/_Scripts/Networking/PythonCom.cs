﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class PythonSendPackage
{
    public PythonPhase PythonPhase;
    public float PointX;
    public float PointY;
    public float EyeX;
    public float EyeY;
    public bool NewPoint;
    public int PickedUpItemID = -1;
    public int PickedUpItemID_2 = -1;
    public int GazeItemID = -1;
    public int ConnectionMale = -1;
    public int ConnectionFemale = -1;
    public float LeftEyePupilDiameter;
    public float RightEyePupilDiameter;

    public byte[] Data()
    {
        //var pos0 = BitConverter.GetBytes((int)PythonPhase);
        //var pos1 = BitConverter.GetBytes(PointX);
        //var pos2 = BitConverter.GetBytes(PointY);
        //var pos3 = BitConverter.GetBytes(EyeX);
        //var pos4 = BitConverter.GetBytes(EyeY);
        //var pos5 = BitConverter.GetBytes(NewPoint?1:0);
        //var pos6 = BitConverter.GetBytes(PickedUpItemID);
        //var pos7 = BitConverter.GetBytes(PickedUpItemID_2);
        //var pos8 = BitConverter.GetBytes(GazeItemID);
        //var pos9 = BitConverter.GetBytes(ConnectionMale); //-1
        //var pos10 = BitConverter.GetBytes(ConnectionFemale); //-1

        var pos0 = BitConverter.GetBytes((int)PythonPhase);
        var pos1 = BitConverter.GetBytes(PointX); //EyeTrackingManager.Instance.ProjectedOnGazePlaneScreen(Point.position).x
        var pos2 = BitConverter.GetBytes(PointY);
        var pos3 = BitConverter.GetBytes(EyeX);
        var pos4 = BitConverter.GetBytes(EyeY);
        var pos5 = BitConverter.GetBytes(NewPoint ? 1 : 0); //clicked
        var pos6 = BitConverter.GetBytes(PickedUpItemID); //-1, 1
        var pos7 = BitConverter.GetBytes(PickedUpItemID_2); //-1, 1, 6, 7
        var pos8 = BitConverter.GetBytes(GazeItemID); //-1, 1, 0
        var pos9 = BitConverter.GetBytes(LeftEyePupilDiameter);
        var pos10 = BitConverter.GetBytes(RightEyePupilDiameter);

        return Combine(pos0, pos1, pos2, pos3, pos4, pos5, pos6, pos7, pos8, pos9, pos10);
    }

    byte[] Combine(params byte[][] arrays)
    {
        byte[] rv = new byte[arrays.Sum(a => a.Length)];
        int offset = 0;
        foreach (byte[] array in arrays)
        {
            System.Buffer.BlockCopy(array, 0, rv, offset, array.Length);
            offset += array.Length;
        }
        return rv;
    }
}

public enum PythonPhase
{
    Calibrating,
    Building
}

public class PythonCom : MonoBehaviour
{
    public Text FeedbackText;
    public static PythonSendPackage SendPackage = new PythonSendPackage();
    public Transform BetterEyes;
    public Transform GazeCheckPlane;
    public static Ray CorrectedGaze;

    IPEndPoint _remoteEndPoint;
    UdpClient _clientSending, _clientReceiving;
    Thread _receiveThread;
    Process _pythonProcess;

    Collider _collider;

    [SerializeField] string _ip = "172.30.121.6";
    [SerializeField] string _pythonScriptPath = @"D:\Documents_Students\DAER_Projects\Eyetracking\myUnityCom_v2.py";
    int _port_1 = 1515;
    int _port_2 = 1414;
    Vector2 _betterEyePos;
    [SerializeField] Camera _cam;

    static bool _bReceiving;
    static string _feedbackText;
    static int _currentSequenceAccordingToPython = -1;
    static bool _scriptEnabled = false;

    void Awake()
    {
        if (_scriptEnabled) {
            Init(); //dormant for now
        }
    }

    void Init()
    {
        _remoteEndPoint = new IPEndPoint(IPAddress.Parse(_ip), _port_1);
        _clientSending = new UdpClient();
        _clientReceiving = new UdpClient(_port_2);
        SendPackage.PythonPhase = PythonPhase.Calibrating;

        //StartPython();
    }

    void Start()
    {
        if (!_scriptEnabled) return;
            StartSending();
        StartReceiving();
        GazeObjectFinder.UseCorrection = true;
        _collider = GazeCheckPlane.GetComponent<Collider>();
    }

    void StartPython()
    {
        _pythonProcess = new Process();
        _pythonProcess.StartInfo.FileName = @"C:\Users\Steven\Anaconda3\python.exe";
        _pythonProcess.StartInfo.Arguments = _pythonScriptPath;
        _pythonProcess.Start();
    }

    void StartSending()
    {
        StartCoroutine(SendData());
    }

    void StartReceiving()
    {
        _receiveThread = new Thread(new ThreadStart(ThreadReceiving));
        _receiveThread.IsBackground = true;
        _receiveThread.Start();
    }

    void Update()
    {
        UpdateSendPackage();
        UpdateReceivedData();
        FeedbackText.text = _feedbackText;
    }

    void UpdateSendPackage()
    {
        if (!_scriptEnabled) return;
        SendPackage.PickedUpItemID = PickupManager.GetPickedUpPieceID(ControllerIndex.Left);
        SendPackage.PickedUpItemID_2 = PickupManager.GetPickedUpPieceID(ControllerIndex.Right);
        var gazeObject = GazeObjectFinder.GazeObjects.Count > 0 ? GazeObjectFinder.GazeObjects[0] : null;
        SendPackage.GazeItemID = gazeObject != null ? gazeObject.ObjectUID : -1;
        var eyeScreenPos = EyeTrackingManager.GazeViewspace;
        SendPackage.EyeX = eyeScreenPos.x;
        SendPackage.EyeY = eyeScreenPos.y;
        SendPackage.LeftEyePupilDiameter = EyeTrackingManager.LeftEyeDiameter;
        SendPackage.RightEyePupilDiameter = EyeTrackingManager.RightEyeDiameter;
        //UnityEngine.Debug.Log("SendPackage.LeftEyePupilDiameter:" + SendPackage.LeftEyePupilDiameter);
        //UnityEngine.Debug.Log("SendPackage.RightEyePupilDiameter:" + SendPackage.RightEyePupilDiameter);
    }

    void UpdateReceivedData()
    {
        if (!_scriptEnabled) return;
        //UnityEngine.Debug.Log("betterEyePos: " + _betterEyePos);
        Vector2 viewPoint = new Vector2(_betterEyePos.x, _betterEyePos.y);
        RaycastHit hitInfo;
        var ray = _cam.ScreenPointToRay(viewPoint);
        CorrectedGaze = ray;
        if (_collider.Raycast(ray, out hitInfo, 10))
        {
            BetterEyes.position = hitInfo.point;
            BetterEyes.forward = _cam.transform.position - BetterEyes.position;
        }

        //UnityEngine.Debug.Log("Current sequence according to python: " + _currentSequenceAccordingToPython);
    }

    void SendBytes(byte[] data)
    {
        if (!_scriptEnabled) return;
        try
        {
            int bytesSend = _clientSending.Send(data, data.Length, _remoteEndPoint);
            //UnityEngine.Debug.Log("bytesSend: " + bytesSend);

        }
        catch (Exception err)
        {
            UnityEngine.Debug.LogError(err.ToString());
        }
    }

    IEnumerator SendData()
    {
        if (!_scriptEnabled) yield break;
        while (true)
        {
            byte[] bytes = SendPackage.Data();
            SendBytes(bytes);
            //SendPackage.ConnectionMale = -1;
            //SendPackage.ConnectionFemale = -1;
            //UnityEngine.Debug.Log("newpoint: " + SendPackage.NewPoint);
            SendPackage.NewPoint = false;
            yield return new WaitForSeconds(0.01f);
        }
    }

    void ThreadReceiving()
    {
        _bReceiving = true;
        IPEndPoint anyIP = new IPEndPoint(IPAddress.Any, _port_2);
        while (_bReceiving)
        {
            try
            {
                _feedbackText = "Hello world";
                byte[] data = _clientReceiving.Receive(ref anyIP);
                int mouseX = BitConverter.ToInt32(data, 0);
                int mouseY = BitConverter.ToInt32(data, 4);
                int sequenceID = BitConverter.ToInt32(data, 8);

                _betterEyePos.x = mouseX;
                _betterEyePos.y = mouseY;
                _currentSequenceAccordingToPython = sequenceID;
            }
            catch
            {

            }
        }
    }

    void OnDestroy()
    {
        if (_pythonProcess != null && !_pythonProcess.HasExited)
            _pythonProcess.Kill();
        if (!_scriptEnabled) return;
        _clientReceiving.Close();
        _bReceiving = false;
        _receiveThread.Join();
    }
}
