﻿using UnityEngine;
using System.Collections.Generic;
using System;
using Helpers;
using System.Linq;

[Serializable]
public class NetworkPrefab
{
    public string Name;
    public GameObject Prefab;
}

public class NetworkPrefabPool : Singleton<NetworkPrefabPool>
{
    public List<NetworkPrefab> Prefabs = new List<NetworkPrefab>();

    public void Spawn(string name, int nuid, int playerID)
    {
        if (NetworkHandling.NetworkObjects.ContainsKey(nuid))
            return;

        var networkPrefab = Prefabs.FirstOrDefault(p => p.Name == name);
        if (networkPrefab == null)
        {
            Debug.LogWarning("Connected pc tried to spawn a prefab that could not be found locally!");
            return;
        }

        var spawnedObject = Instantiate(networkPrefab.Prefab);
        var spawnedNetworkObject = spawnedObject.GetComponent<NetworkObject>();
        spawnedNetworkObject.NUID = nuid;
        NetworkHandling.NetworkObjects.Add(nuid, spawnedNetworkObject);
        var colorSetter = spawnedObject.GetComponent<ColorSetter>();
        if (colorSetter)
            colorSetter.SetColor(PlayerManager.GetPlayerColor(playerID));

        Debug.Log("Spawning: " + nuid);
    }
}
