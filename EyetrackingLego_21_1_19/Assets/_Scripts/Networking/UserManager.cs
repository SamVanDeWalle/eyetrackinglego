﻿using UnityEngine;
using System.Collections.Generic;
using System;

public enum UserType
{
    Player,
    Spectator
}

[Serializable]
public class User
{
    public int ConnectionID;
    public int NUID;
}

[Serializable]
public class Player : User
{
    public int ColorID;
}

[Serializable]
public class Spectator : User
{

}

public class UserManager : MonoBehaviour
{
    public static List<Player> Players = new List<Player>();
    public static List<Spectator> Spectators = new List<Spectator>();

    public static void AddUser(int userType, int connectionID)
    {
        var type = (UserType)userType;
        System.Random rand = new System.Random();
        switch (type)
        {
            case UserType.Player:
                var player = new Player();
                player.ConnectionID = connectionID;
                player.NUID = connectionID + rand.Next();
                player.ColorID = PlayerManager.GetNextFreeColorID();
                Players.Add(player);
                PlayerManager.AddPlayer(player);
                NetworkHandling.Instance.SendUserConfirmation(connectionID, player.NUID, player.ColorID);
                break;
            case UserType.Spectator:
                var spectator = new Spectator();
                spectator.ConnectionID = connectionID;
                spectator.NUID = connectionID + rand.Next();
                Spectators.Add(spectator);
                NetworkHandling.Instance.SendUserConfirmation(connectionID, spectator.NUID);
                break;
        }

        NetworkHandling.Instance.SendPlayerData();
    }

    public static void AddHostUser(UserType userType)
    {
        System.Random rand = new System.Random();
        switch (userType)
        {
            case UserType.Player:
                var player = new Player();
                player.NUID = rand.Next();
                player.ColorID = PlayerManager.GetNextFreeColorID();
                Players.Add(player);
                PlayerManager.Player = player;
                PlayerManager.SetPlayer();
                break;
            case UserType.Spectator:
                var spectator = new Spectator();
                spectator.NUID = rand.Next();
                Spectators.Add(spectator);
                break;
        }
    }
}
