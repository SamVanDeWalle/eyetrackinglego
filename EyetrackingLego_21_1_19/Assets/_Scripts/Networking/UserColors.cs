﻿using UnityEngine;
using System.Collections.Generic;
using Helpers;

public class UserColors : Singleton<UserColors>
{
    public List<Color> Colors = new List<Color>();

    protected override void Awake()
    {
        base.Awake();
        //DontDestroyOnLoad(gameObject);
    }
}
