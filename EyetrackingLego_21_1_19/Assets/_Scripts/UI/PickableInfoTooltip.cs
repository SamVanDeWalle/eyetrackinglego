﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickableInfoTooltip : MonoBehaviour
{
    public Transform Canvas;
    public Text InfoText;
    public Transform Line;
    public Image BGImage;
    Vector3 _forward;

    Color _startColor;
    static Dictionary<Color, Material> _materials = new Dictionary<Color, Material>();

    void Start()
    {
        PopOut();

        SetStartColor();
    }

    void SetStartColor()
    {
        _startColor = BGImage.color;
        if (!_materials.ContainsKey(_startColor))
            _materials.Add(_startColor, Line.GetComponent<Renderer>().material);
    }

    public void PopOut()
    {
        _forward = transform.forward;
        transform.position = transform.parent.position;
        Canvas.transform.localPosition = Vector3.zero + Vector3.up * 0.255f;
        //transform.position -= transform.forward * 0.3f;
        Canvas.transform.forward = Canvas.transform.position - Camera.main.transform.position;
        var forward = Canvas.transform.position - Camera.main.transform.position;
        forward.y = 0;
        Line.right = forward;
        Line.Rotate(-90, 0, 0);
    }

    public void PopBack()
    {
        //transform.position += transform.forward * 0.3f;
        //transform.forward = _forward;
    }

    public void SetColor(Color color)
    {
        if (!_materials.ContainsKey(color))
        {
            if(!_materials.ContainsKey(_startColor))
                SetStartColor();
            var newMat = new Material(_materials[_startColor]);
            newMat.color = color;
            _materials.Add(color, newMat);
        }

        BGImage.color = color;
        Line.GetComponent<Renderer>().material = _materials[color];
    }

    void Update()
    {
        Canvas.transform.localPosition = Vector3.zero;
        Canvas.transform.position += Vector3.up * 0.255f;
        Canvas.transform.forward = Canvas.transform.position - Camera.main.transform.position;
        //transform.position = transform.parent.position + Vector3.up * 0.2f;
        //transform.position -= transform.forward * 0.3f;
        var forward = Canvas.transform.position - Camera.main.transform.position;
        forward.y = 0;
        Line.right = forward;
        Line.Rotate(-90, 0, 0);
    }
}
