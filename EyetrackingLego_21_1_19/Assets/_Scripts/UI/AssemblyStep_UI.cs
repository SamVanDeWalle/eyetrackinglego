﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AssemblyStep_UI : MonoBehaviour
{
    public Text APTypeID;
    Image _bg;
    Color _textColor;

    void Awake()
    {
        _bg = GetComponent<Image>();
        _textColor = APTypeID.color;
    }

    public void HighLight(bool highLight)
    {
        var bgColor = _bg.color;
        if (highLight)
        {
            bgColor.a = 255.0f;
            APTypeID.color = Color.white;
        }
        else
        {
            bgColor.a = 0.0f;
            APTypeID.color = _textColor;
        }

        _bg.color = bgColor;
    }
}
