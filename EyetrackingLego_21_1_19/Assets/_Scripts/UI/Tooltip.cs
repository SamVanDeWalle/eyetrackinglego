﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tooltip : MonoBehaviour
{
    public TooltipType Type;
    public Transform Canvas;
    public Transform Line;
    public Material LineMaterial;

    Renderer _lineRenderer;

    void Awake()
    {
        _lineRenderer = Line.GetComponent<Renderer>();
        //LineMaterial = _lineRenderer.sharedMaterial;
    }

    void Update()
    {
        Canvas.transform.localPosition = Vector3.zero;
        Canvas.transform.position += Vector3.up * 0.255f;
        //Canvas.transform.forward = Canvas.transform.position - Camera.main.transform.position;

        var forward = Canvas.transform.position - Camera.main.transform.position;
        forward.y = 0;
        Line.right = forward;
        Line.Rotate(-90, 0, 0);
        _lineRenderer.sharedMaterial = LineMaterial;
    }
}
