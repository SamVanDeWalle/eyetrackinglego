﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum Axis
{
    X,
    Y,
    Z
}

public class RotationIndicator : MonoBehaviour
{
    public Transform Transform;
    public Image Image, ImageBig;
    public float DesiredAngle = 0;

    public WorldButtonIndicator LeftButton;
    public WorldButtonIndicator RightButton;

    public delegate void DesiredReached();
    public event DesiredReached OnDesiredReached;

    public static bool Enabled;
    public Axis Axis = Axis.Z;

    void Awake()
    {
        Image.enabled = false;
        if (ImageBig)
            ImageBig.enabled = false;
    }

    void Update()
    {
        if (!Enabled)
            return;

        //Quaternion.LookRotation()
        var currentAngle = 0.0f;
        if (Axis == Axis.X)
            currentAngle = Transform.localEulerAngles.x;
        else if (Axis == Axis.Y)
            currentAngle = Transform.localEulerAngles.y;
        else if (Axis == Axis.Z)
            currentAngle = Transform.localEulerAngles.z;
        //Debug.Log(currentAngle);
        var difference = (currentAngle - DesiredAngle) % 360;
        //Debug.Log("diff: " + difference);
        var smallestDifference = difference;
        if (difference < -180)
            smallestDifference = 360 + difference;
        if (difference > 180)
            smallestDifference = difference - 360;
        //Debug.Log("sd: " + smallestDifference);

        var scale = transform.localScale;
        scale.x = smallestDifference > 0 ? Mathf.Abs(scale.x) * -1 : Mathf.Abs(scale.x);
        transform.localScale = scale;

        //var imgScale = Image.transform.localScale;
        //imgScale.x = smallestDifference > 0 ? Mathf.Abs(imgScale.x) * -1 : Mathf.Abs(imgScale.x);
        //Image.transform.localScale = imgScale;

        var absDifference = Mathf.Abs(difference);
        var smallestAngle = absDifference < 180 ? absDifference : 360 - absDifference;
        var DesiredAngleReached = smallestAngle <= 15;
        Image.enabled = !DesiredAngleReached;
        if (DesiredAngleReached)
        {
            if (OnDesiredReached != null)
                OnDesiredReached();
        }
        if (ImageBig)
        {
            ImageBig.enabled = Image.enabled;
            //var imgBigScale = Image.transform.localScale;
            //imgBigScale.x = smallestDifference > 0 ? Mathf.Abs(imgBigScale.x) * -1 : Mathf.Abs(imgBigScale.x);
            //Image.transform.localScale = imgBigScale;
        }

        if(LeftButton != null)
        {
            if (Image.enabled && scale.x < 0)
                LeftButton.Indicate();
            else
                LeftButton.StopIndicating();
        }

        if (RightButton != null)
        {
            if (Image.enabled && scale.x > 0)
                RightButton.Indicate();
            else
                RightButton.StopIndicating();
        }
    }
}
