﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class VR_Settings_UI_Toggle
{
    public string Name;
    public Toggle Toggle;
}

public class VR_Settings_UI : MonoBehaviour
{
    public GameObject TogglePrefab;
    public List<VR_Settings_UI_Toggle> Toggles = new List<VR_Settings_UI_Toggle>();

	void Awake ()
    {
        LoadSettings();
	}

    void LoadSettings()
    {
        var vr_settings = new VR_Settings();
        VR_Settings.Load();
        foreach(var setting in VR_Settings.Settings)
        {
            var toggleGO = Instantiate(TogglePrefab);
            toggleGO.transform.SetParent(transform, false);
            var toggle = toggleGO.GetComponent<Toggle>();
            toggle.isOn = setting.Value.Value;
            toggle.GetComponentInChildren<Text>().text = setting.Value.Name;
            VR_Settings.LinkToggle(setting.Key, toggle);
        }
    }
}
