﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Helpers;

public class Menu_UI : Singleton<Menu_UI>
{
    public Text IPText;
    public Text HostIPText;

    void Start()
    {
        IPText.text = Main.Instance.IP;
        HostIPText.text = Main.Instance.HostIP;
    }
}
