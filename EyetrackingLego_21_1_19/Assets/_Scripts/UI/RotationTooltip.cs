﻿using Helpers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotationTooltip : Singleton<RotationTooltip>
{
    public GameObject VisualsMain, VisualsX, VisualsY, VisualsZ;
    public Image XBg, YBg, ZBg;
    public Image XIndicator, YIndicator, ZIndicator;
    public Image XBounds, YBounds, ZBounds;

    static bool _bActive = false;

    static Coroutine _deactivationRoutine;

    public static void LockOn(ConnectionPoint cp, PickupController controller = null)
    {
        Instance.transform.position = cp.transform.position;
        Instance.transform.SetParent(cp.transform);
        Instance.transform.localRotation = Quaternion.identity;

        if(controller != null)
        {
            controller.OnGhostConnectionLost -= Deactivate;
            controller.OnGhostConnectionLost += Deactivate;
            controller.OnDropped -= Deactivate;
            controller.OnDropped += Deactivate;
        }
    }

    public static void Activate(bool active = true)
    {
        _bActive = active;
        Instance.VisualsMain.SetActive(active);
        if (_deactivationRoutine != null)
            Instance.StopCoroutine(_deactivationRoutine);
        _deactivationRoutine = Instance.StartCoroutine(DeactivateOverTime());
    }

    public static void Deactivate()
    {
        Activate(false);
    }

    public static void ActivateX(bool active = true)
    {
        Instance.VisualsX.SetActive(active);
    }

    public static void ActivateY(bool active = true)
    {
        Instance.VisualsY.SetActive(active);
    }

    public static void ActivateZ(bool active = true)
    {
        Instance.VisualsZ.SetActive(active);
    }

    public static void SetXIndicator(float angle)
    {
        Instance.XIndicator.transform.rotation = Instance.XBg.transform.rotation;
        Instance.XIndicator.transform.Rotate(0, 0, angle);
    }

    public static void SetYIndicator(float angle)
    {
        Instance.YIndicator.transform.rotation = Instance.YBg.transform.rotation;
        Instance.YIndicator.transform.Rotate(0, 0, angle);
    }

    public static void SetZIndicator(float angle)
    {
        Instance.ZIndicator.transform.rotation = Instance.ZBg.transform.rotation;
        Instance.ZIndicator.transform.Rotate(0, 0, angle);
    }

    public static void SetXBounds(float lower, float upper)
    {
        var percentage = (upper - lower) / 360.0f;
        Instance.XBounds.transform.rotation = Instance.XBg.transform.rotation;
        Instance.XBounds.transform.Rotate(0, 0, -(180 - upper));
        Instance.XBounds.fillAmount = percentage;
    }

    public static void SetYBounds(float lower, float upper)
    {
        var percentage = (upper - lower) / 360.0f;
        Instance.YBounds.transform.rotation = Instance.YBg.transform.rotation;
        Instance.YBounds.transform.Rotate(0, 0, -(180 - upper));
        Instance.YBounds.fillAmount = percentage;
    }

    public static void SetZBounds(float lower, float upper)
    {
        var percentage = (upper - lower) / 360.0f;
        Instance.ZBounds.transform.rotation = Instance.ZBg.transform.rotation;
        Instance.ZBounds.transform.Rotate(0, 0, -(180 - upper));
        Instance.ZBounds.fillAmount = percentage;
    }

    public static void SetIndicators(float angleX, float angleY, float angleZ)
    {
        SetXIndicator(angleX);
        SetYIndicator(angleY);
        SetZIndicator(angleZ);
    }

    public static void SetIndicators(Vector3 angles)
    {
        SetIndicators(angles.x, angles.y, angles.z);
    }

    static IEnumerator DeactivateOverTime()
    {
        yield return new WaitForSeconds(3.0f);

        Deactivate(); 
    }
}
