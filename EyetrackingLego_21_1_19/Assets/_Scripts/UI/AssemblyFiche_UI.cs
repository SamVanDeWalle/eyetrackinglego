﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AssemblyFiche_UI : MonoBehaviour
{
    public Transform StepContainer;
    public AssemblyStep_UI StepPanel;

    List<AssemblyStep_UI> _steps = new List<AssemblyStep_UI>();
    Dictionary<int, AssemblyStep_UI> _stepsPerType = new Dictionary<int, AssemblyStep_UI>();
    AssemblyFiche _fiche;
    
    void Awake()
    {
        _fiche = FindObjectOfType<AssemblyFiche>();
        _fiche.UI = this;
    } 

    void Start()
    {
        SetSteps();
    }

    void SetSteps()
    {
        if (!_fiche)
            return;

        _stepsPerType.Clear();
        foreach(var step in _fiche.AssemblySteps)
        {
            var newPanel = Instantiate(StepPanel.gameObject);
            newPanel.transform.SetParent(StepContainer);
            newPanel.transform.localScale = Vector3.one;
            var pos = newPanel.GetComponent<RectTransform>().localPosition;
            pos.z = 0;
            newPanel.GetComponent<RectTransform>().localPosition = pos;
            var stepUI = newPanel.GetComponent<AssemblyStep_UI>();
            stepUI.APTypeID.text = step.ID;
            _steps.Add(stepUI);
            _stepsPerType.Add(step.PrefabTypeID, stepUI);
        }

        StepPanel.gameObject.SetActive(false);
        StepContainer.GetChild(0).GetComponent<AssemblyStep_UI>().HighLight(true);
    }

    public void ClearStep(int typeID)
    {
        if (!_stepsPerType.ContainsKey(typeID))
        {
            Debug.LogWarning("AssemblyFiche_UI::ClearStep - Tried to clear a non existing step from type: " + typeID);
        }

        _stepsPerType[typeID].gameObject.SetActive(false);
    }

    public void HighLightStep(int index)
    {
        Debug.Log("step: " + index);
        for(int i = 0; i < index; ++i)
        {
            _steps[i].HighLight(false);
            _steps[i].gameObject.SetActive(false);
        }

        if(index < _steps.Count)
        {
            _steps[index].gameObject.SetActive(true);
            _steps[index].HighLight(true);
        }

        for(int i = index + 1; i < _steps.Count; ++i)
        {
            _steps[i].HighLight(false);
            if(i - index >= 8)
                _steps[i].gameObject.SetActive(false);
            else
                _steps[i].gameObject.SetActive(true);
        }
    }
}
