﻿using UnityEngine;
using System.Collections;

public class HandInfoPanel : MonoBehaviour
{
    public Material Material;

    public Vector2 ScrollOffset { get { return Material.GetTextureOffset("_MainTex"); } private set { } }
    
    void Start()
    {
        Material = GetComponent<Renderer>().material;
    }

    public void Reset()
    {
        var textureOffset = Material.GetTextureOffset("_MainTex");
        textureOffset.x = 0;
        Material.SetTextureOffset("_MainTex", textureOffset);
    }

    public void Scroll(float xAmount)
    {
        var textureOffset = Material.GetTextureOffset("_MainTex");
        textureOffset.x -= xAmount;
        Material.SetTextureOffset("_MainTex", textureOffset);
    }

    void OnDestroy()
    {
        Reset();
    }
}
