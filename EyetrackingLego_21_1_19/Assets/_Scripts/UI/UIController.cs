﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    public bool UIActive = false;
    public Sprite[] StepImages;
    private Image _currentImage = null;
    private Text _currentText = null;
    private int _currentStep = 0;
    

	// Use this for initialization
	void Start ()
    {
        _currentImage = transform.Find("InstructionPanel/InstructionImage").GetComponent<Image>();
        _currentText = transform.Find("InstructionText").GetComponent<Text>();
        _currentImage.sprite = StepImages[_currentStep];
        _currentText.text = "" + (_currentStep + 1);

        int amountOfObjects = transform.childCount;

        for (int i = 0; i < amountOfObjects; i++)
        {
            transform.GetChild(i).gameObject.SetActive(UIActive);
        }
    }
	
	// Update is called once per frame
	void LateUpdate ()
    {
        if(Input.GetKeyDown(KeyCode.H)) // disable UI
        {
            int amountOfObjects = transform.childCount;

            for (int i = 0; i < amountOfObjects; i++)
            {
                transform.GetChild(i).gameObject.SetActive(!UIActive);
            }

            UIActive = !UIActive;
        }

	    if(Input.GetKeyDown(KeyCode.P) && _currentStep < StepImages.Length-1 && UIActive) // next instruction step
        {
            ++_currentStep;
            _currentImage.sprite = StepImages[_currentStep];
            _currentText.text = "" + (_currentStep + 1);
        }
        else if (Input.GetKeyDown(KeyCode.O) && _currentStep > 0 && UIActive) // previous instruction step
        {
            --_currentStep;
            _currentImage.sprite = StepImages[_currentStep];
            _currentText.text = "" + (_currentStep + 1);
        }
    }
}
