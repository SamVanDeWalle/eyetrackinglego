﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class HandInfo : MonoBehaviour
{
    public GameObject HandPanel;
    public Material InfoMaterial;
    public List<HandInfoPanel> Panels;
    public int PanelIndex;
    public Text IndexText;
    bool _bOpen;
    ViveController _currentController;
    Vector2 _lastTouchPostition;
    Dictionary<ViveController, Vector2> _lastTouchPositions = new Dictionary<ViveController, Vector2>();
    float _scrollSpeed = 1.0f;
    float _autoScrollSpeed = 1.6f;

    void Start ()
    {
        HandPanel.SetActive(false);
        PrepareControllers();
        PreparePanels();
    }

    void PrepareControllers()
    {
        foreach(var controller in ViveHelper.Controllers.Values)
        {
            _lastTouchPositions[controller] = new Vector2(-10, -10);
        }
    }

    void PreparePanels()
    {
        for(int i = 0; i < Panels.Count; ++i)
        {
            Panels[i].Reset();
            Panels[i].Scroll(i * 1.0f);
        }
    }

    void GetCurrentPanelIndex()
    {
        var closestPanelOffsetX = Panels[0].ScrollOffset.x;
        var index = 0;
        for (int i = 0; i < Panels.Count; i++)
        {
            var xOffset = Mathf.Abs(Panels[i].ScrollOffset.x);
            if(xOffset < closestPanelOffsetX)
            {
                closestPanelOffsetX = xOffset;
                index = i;
            }
        }

        PanelIndex = index;
        IndexText.text = "" + PanelIndex;
    }

    void Update()
    {
        foreach(var controller in ViveHelper.Controllers.Values)
        {
            var device = controller.Device;
            var pressed = device.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad);
            var touching = device.GetTouch(SteamVR_Controller.ButtonMask.Touchpad);
            var touchUp = device.GetTouchUp(SteamVR_Controller.ButtonMask.Touchpad);
            var touchpad = device.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);

            if (pressed)
            {
                if (touchpad.y > 0.7f)
                {
                    //up = true;
                }
                else if (touchpad.y < -0.5)
                {
                    if(_bOpen)
                    {
                        if(_currentController == controller)
                        {
                            _bOpen = false;
                            HandPanel.SetActive(_bOpen);
                        }
                        else
                        {
                            LockOnController(controller);
                        }
                    }
                    else
                    {
                        _bOpen = true;
                        HandPanel.SetActive(_bOpen);
                        LockOnController(controller);
                    }
                }
            }
            else if (touching && _currentController == controller && _bOpen)
            {
                if(_lastTouchPositions[controller] != new Vector2(-10, -10))
                {
                    var touchDeltaX = touchpad.x - _lastTouchPositions[controller].x;
                    foreach(var panel in Panels)
                    {
                        panel.Scroll(touchDeltaX * _scrollSpeed);
                    }

                    GetCurrentPanelIndex();
                }
                
                _lastTouchPositions[controller] = touchpad;
            }
            else if(!touching)
            {
                var scrollOffset = Panels[PanelIndex].ScrollOffset;
                if (Mathf.Abs(scrollOffset.x) > 0)
                {
                    var scrollTick = scrollOffset.x * Time.deltaTime * _autoScrollSpeed;
                    if (Mathf.Abs(scrollTick) > Mathf.Abs(scrollOffset.x))
                        scrollTick = scrollOffset.x;

                    foreach (var panel in Panels)
                    {
                        panel.Scroll(scrollTick);
                    }
                }
            }

            if (touchUp)
            {
                _lastTouchPositions[controller] = new Vector2(-10, -10);
            }
        }
    }

    void LockOnController(ViveController controller)
    {
        _currentController = controller;
        HandPanel.transform.position = controller.Transform.position;
        HandPanel.transform.SetParent(controller.Transform);
        HandPanel.transform.rotation = controller.Transform.rotation;
    }
}
