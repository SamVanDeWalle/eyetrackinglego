﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PushButton_UI : MonoBehaviour
{
    public bool IsPressed { get { return _bPressed; } private set { } }
    public ViveController PressingController { get { return _controller; } private set { } }
    public Color ActivatedColor;

    bool _bPressed;
    ViveController _controller;
    Button _button;
    Image _image;
    Color _startColor;

    public delegate void ButtonHit(ViveController controller);
    public event ButtonHit OnButtonPushed;
    public event ButtonHit OnButtonReleased;

    void Awake()
    {
        _button = GetComponent<Button>();
        _image = GetComponent<Image>();
        _startColor = _image.color;
    }

    void OnTriggerEnter(Collider other)
    {
        if (_bPressed)
            return;

        _controller = ViveHelper.GetController(other.transform);
        if (_controller == null)
            return;

        //PickupController.GlobalAllowPicking = false;

        //_button.Invoke()
        EventSystem.current.SetSelectedGameObject(null);
        _button.Select();

        if (OnButtonPushed != null)
            OnButtonPushed(_controller);

        _bPressed = true;
    }

    void OnTriggerExit(Collider other)
    {
        if (!_bPressed)
            return;

        var controller = ViveHelper.GetController(other.transform);
        if (controller == _controller)
            _bPressed = false;

        if (EventSystem.current.currentSelectedGameObject == gameObject)
            EventSystem.current.SetSelectedGameObject(null);
    }

    public void SetActivated(bool active)
    {
        _image.color = active ? ActivatedColor : _startColor;
    }
}
