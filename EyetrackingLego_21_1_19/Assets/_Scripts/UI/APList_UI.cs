﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class APList_UI : MonoBehaviour
{
    public Station Station;
    public GameObject APItemPrefab;
    public Transform Container;
    public PushButton_UI SaveButton;
    public PushButton_UI ExitButton;
    public Transform ItemPreviewAnchor;

    Dictionary<string, APItem_UI> APItems = new Dictionary<string, APItem_UI>();
    APItem_UI _selectedItem;

    void Awake()
    {
        AssemblyPieceManager.OnPrefabPoolLoaded += LoadItems;
    }

    void Start()
    {
        //SaveButton.OnButtonPushed -= SaveButtonPushed;
        //SaveButton.OnButtonPushed += SaveButtonPushed;
    }

    void Update()
    {
        if(_selectedItem != null)
        {
            if (_selectedItem.Controller.GetPressDown(ControllerButton.Trigger))
            {
                SpawnSelected();
            }
        }

        if(SaveButton.IsPressed && SaveButton.PressingController.GetPressDown(ControllerButton.Trigger))
        {
            Save();
        }

        if (ExitButton.IsPressed && ExitButton.PressingController.GetPressDown(ControllerButton.Trigger))
        {
            TrainingManager.QuitTraining();
        }
    }

    void LoadItems()
    {
        var pieces = AssemblyPieceManager.PrefabPool;
        foreach(var poolPiece in pieces)
        {
            var typeId = poolPiece.Key;
            if (APItems.ContainsKey(typeId.ToString()))
                continue;
            var itemGO = Instantiate(APItemPrefab);
            itemGO.transform.SetParent(Container, false);
            if (poolPiece.Value.GhostObject == null)
                Debug.Log(poolPiece.Value.gameObject.name);
            var pieceDummy = Instantiate(poolPiece.Value.GhostObject, Vector3.zero, Quaternion.identity, null);
            pieceDummy.SetActive(false);
            pieceDummy.transform.SetParent(ItemPreviewAnchor, false);
            //Destroy(pieceDummy.GetComponent<AssemblyPiece>());
            //Destroy(pieceDummy.GetComponent<Pickable>());
            //pieceDummy.transform.SetParent(itemGO.transform, true);
            //pieceDummy.transform.localScale = new Vector3(1000f, 1000f, 1f);
            var item = itemGO.GetComponent<APItem_UI>();
            item.SetTypeID(typeId);
            APItems.Add(typeId.ToString(), item);
        }

        APItem_UI.OnEnter -= EnteredItem;
        APItem_UI.OnEnter += EnteredItem;

        APItem_UI.OnLeave -= LeaveItem;
        APItem_UI.OnLeave += LeaveItem;
    }

    void EnteredItem(APItem_UI item)
    {
        if (_selectedItem != null)
            _selectedItem.Deselect();

        item.Select();
        _selectedItem = item;
    }

    void LeaveItem(APItem_UI item)
    {
        item.Deselect();
        if(item == _selectedItem)
        {
            _selectedItem = null;
        }
    }

    void SpawnSelected()
    {
        var piece = AssemblyPieceManager.SpawnPiece(_selectedItem.TypeID);
        var controller = PickupManager.GetPickupController(_selectedItem.Controller);
        piece.transform.localScale = Station.AssemblyPieceContainer.localScale;
        piece.transform.SetParent(controller.Hand, false);
        piece.transform.localPosition = Vector3.zero;
        var stationItem = piece.gameObject.AddComponent<StationItem>();
        var menu = GameObject.Instantiate(Station.GenericItemMenuPrefab);
        menu.transform.SetParent(piece.transform);
        stationItem.MenuAncher = menu.transform;
        stationItem.Menu = menu.GetComponentInChildren<StationItemMenu_UI>();
        stationItem.GenericMenuPosition = true;
        //piece.Pickable.PopOutTooltip();
        controller.Pickup(piece.Pickable);
    }

    void SaveButtonPushed(ViveController controller)
    {
        Save();
    }

    void Save()
    {
        Station.Save();
    }
}
