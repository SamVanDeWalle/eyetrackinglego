﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationItemMenu_UI : MonoBehaviour
{
    public PushButton_UI MoveButton;
    public PushButton_UI RotateButton;
    public PushButton_UI RemoveButton;

    public bool AnyButtonPressed { get { return MoveButton.IsPressed || RotateButton.IsPressed || RemoveButton.IsPressed; } private set { } }
}
