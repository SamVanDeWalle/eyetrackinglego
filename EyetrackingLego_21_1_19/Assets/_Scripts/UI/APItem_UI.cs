﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class APItem_UI : MonoBehaviour
{
    public Text TypeIDText;
    public int TypeID;
    public Image BGImage;
    public Color SelectedColor;
    public ViveController Controller { get { return _controller; } private set { } }

    ViveController _controller;
    Color _startColor;
    bool _bEntered;

    public delegate void APItemAction(APItem_UI apItem);
    public static event APItemAction OnEnter;
    public static event APItemAction OnLeave;

    void Start()
    {
        _startColor = BGImage.color;
    }

    public void SetTypeID(int id)
    {
        TypeID = id;
        TypeIDText.text = id.ToString();
    }

    void Enter()
    {
        if (_bEntered)
            return;

        _bEntered = true;

        if (OnEnter != null)
            OnEnter(this);
    }

    void Leave()
    {
        if (!_bEntered)
            return;

        _bEntered = false;

        if (OnLeave != null)
            OnLeave(this);
    }

    public void Select()
    {
        BGImage.color = SelectedColor;
    }

    public void Deselect()
    {
        BGImage.color = _startColor;
    }

    void OnTriggerEnter(Collider other)
    {
        _controller = ViveHelper.GetController(other.transform);
        if (_controller != null)
            Enter();
    }

    void OnTriggerExit(Collider other)
    {
        Leave();
    }
}
