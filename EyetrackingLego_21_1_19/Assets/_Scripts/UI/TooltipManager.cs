﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TooltipType
{
    Dependency,
    Occupied,
    Orientation,
    MissingTool,
    IncorrectProperty,
    Crosswise,
}

public class TooltipManager : MonoBehaviour
{
    public GameObject DependencyTooltipPref;
    public GameObject OccupiedTooltipPref;
    public GameObject OrientationTooltipPref;
    public GameObject MissingToolTooltipPref;
    public GameObject IncorrectPropertyTooltipPref;
    public GameObject CrosswiseTooltipPref;

    static List<GameObject> _killOnInactiveTooltips = new List<GameObject>();
    static Dictionary<TooltipType, GameObject> _tooltipPrefabs = new Dictionary<TooltipType, GameObject>();
    static Dictionary<GameObject, Tooltip> _tooltips = new Dictionary<GameObject, Tooltip>();

    void Start()
    {
        if(!_tooltipPrefabs.ContainsKey(TooltipType.Dependency))
            _tooltipPrefabs.Add(TooltipType.Dependency, DependencyTooltipPref);
        if (!_tooltipPrefabs.ContainsKey(TooltipType.Occupied))
            _tooltipPrefabs.Add(TooltipType.Occupied, OccupiedTooltipPref);
        if (!_tooltipPrefabs.ContainsKey(TooltipType.Orientation))
            _tooltipPrefabs.Add(TooltipType.Orientation, OrientationTooltipPref);
        if (!_tooltipPrefabs.ContainsKey(TooltipType.MissingTool))
            _tooltipPrefabs.Add(TooltipType.MissingTool, MissingToolTooltipPref);
        if (!_tooltipPrefabs.ContainsKey(TooltipType.IncorrectProperty))
            _tooltipPrefabs.Add(TooltipType.IncorrectProperty, IncorrectPropertyTooltipPref);
        if (!_tooltipPrefabs.ContainsKey(TooltipType.Crosswise))
            _tooltipPrefabs.Add(TooltipType.Crosswise, CrosswiseTooltipPref);
        _tooltips.Clear();
    }

    void Update()
    {
        for(int i = _killOnInactiveTooltips.Count - 1; i > -1 ; --i)
        {
            if (_killOnInactiveTooltips[i] && !_killOnInactiveTooltips[i].transform.parent.gameObject.activeInHierarchy)
            {
                Destroy(_killOnInactiveTooltips[i]);
                _killOnInactiveTooltips.RemoveAt(i);
            }
        }
    }

    public static void AttachTooltip(TooltipType tooltip, GameObject parent, bool killOnInActive = true)
    {
        if(parent == null)
        {
            Debug.LogWarning("TooltipManager::AttachTooltip - Given parent is null");
            return;
        }

        if(_tooltips.ContainsKey(parent))
        {
            if(_tooltips[parent] == null)
            {

            }
            else if (_tooltips[parent].Type != tooltip)
            {
                Destroy(_tooltips[parent].gameObject);
                _tooltips.Remove(parent);
            }
            else
            {
                _tooltips[parent].gameObject.SetActive(true);
                return;
            }
        }
        else
        {
            _tooltips.Add(parent, null);
        }

        var tooltipGO = Instantiate(_tooltipPrefabs[tooltip], parent.transform.position /*+ Vector3.up * 0.4f*/, Quaternion.identity);
        tooltipGO.transform.SetParent(parent.transform, true);
        tooltipGO.SetActive(true);

        _tooltips[parent] = tooltipGO.GetComponent<Tooltip>();

        if (killOnInActive)
            _killOnInactiveTooltips.Add(tooltipGO);
    }

    public static void ClearParent(GameObject parent)
    {
        if (_tooltips.ContainsKey(parent) && _tooltips[parent] != null)
        {
            Destroy(_tooltips[parent].gameObject);
        }
    }

    public static void ClearParent(GameObject parent, TooltipType type)
    {
        if (_tooltips.ContainsKey(parent) && _tooltips[parent] != null && _tooltips[parent].Type == type)
        {
            Destroy(_tooltips[parent].gameObject);
        }
    }
}
