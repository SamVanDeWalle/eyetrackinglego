﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StepItem_UI : MonoBehaviour
{
    public Text Text;
	
    public void SetStep(RecordedStep step)
    {
        Text.text = step.TaskStep.ToString();
    }
}
