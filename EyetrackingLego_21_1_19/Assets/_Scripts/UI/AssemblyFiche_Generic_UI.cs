﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class AssemblyFiche_Generic_UI : MonoBehaviour, IResettable
{
    public Transform StepContainer;
    public AssemblyStep_UI StepPanel;

    List<AssemblyStep_UI> _steps = new List<AssemblyStep_UI>();
    Dictionary<int, AssemblyStep_UI> _stepsPerType = new Dictionary<int, AssemblyStep_UI>();
    Dictionary<int, AssemblyStep_UI> _stepsPerID = new Dictionary<int, AssemblyStep_UI>();
    AssemblyFicheGeneric _fiche;
    
    void Awake()
    {
        _fiche = FindObjectOfType<AssemblyFicheGeneric>();
        RegisterReset();
    } 

    void Start()
    {
        SetSteps();
        TaskStep.OnDone += UpdateFiche;
    }

    void SetSteps()
    {
        if (!_fiche)
            return;

        _stepsPerType.Clear();
        foreach(var step in _fiche.Steps)
        {
            var connectionStep = step as ConnectionStep;
            var newPanel = Instantiate(StepPanel.gameObject);
            newPanel.transform.SetParent(StepContainer);
            newPanel.transform.localScale = Vector3.one;
            var pos = newPanel.GetComponent<RectTransform>().localPosition;
            pos.z = 0;
            newPanel.GetComponent<RectTransform>().localPosition = pos;
            var stepUI = newPanel.GetComponent<AssemblyStep_UI>();
            stepUI.APTypeID.text = connectionStep.PrefabTypeID.ToString();
            _steps.Add(stepUI);
            _stepsPerID.Add(step.ID, stepUI);
            //_stepsPerType.Add(step.PrefabTypeID, stepUI);
        }

        StepPanel.gameObject.SetActive(false);
        StepContainer.GetComponentsInChildren<AssemblyStep_UI>()[0].HighLight(true);
        HandleFallof();
    }

    public void UpdateFiche()
    {
        for(int i = 0; i < _fiche.Steps.Count; ++i)
        {
            var stepUI = _stepsPerID[_fiche.ConnectionSteps[i].ID];
            if (_fiche.ConnectionSteps[i].IsDone)
            {
                stepUI.gameObject.SetActive(false);
            }
            else
            {
                stepUI.gameObject.SetActive(true);
                stepUI.HighLight(false);
            }
        }

        HandleFallof();

        var steps = StepContainer.GetComponentsInChildren<AssemblyStep_UI>();
        if(steps != null && steps.Length > 0 && steps[0] != null)
        {
            steps[0].HighLight(true);
        }
    }

    void HandleFallof()
    {
        var activeSteps = _steps.Where(s => s.isActiveAndEnabled).ToList();
        for (int i = 0; i < activeSteps.Count; ++i)
        {
            if (i >= 8)
                _steps[i].gameObject.SetActive(false);
        }
    }

    public void ClearStep(int typeID)
    {
        if (!_stepsPerType.ContainsKey(typeID))
        {
            Debug.LogWarning("AssemblyFiche_UI::ClearStep - Tried to clear a non existing step from type: " + typeID);
        }

        _stepsPerType[typeID].gameObject.SetActive(false);
    }

    public void HighLightStep(int index)
    {
        Debug.Log("step: " + index);
        for(int i = 0; i < index; ++i)
        {
            _steps[i].HighLight(false);
            _steps[i].gameObject.SetActive(false);
        }

        if(index < _steps.Count)
        {
            _steps[index].gameObject.SetActive(true);
            _steps[index].HighLight(true);
        }

        for(int i = index + 1; i < _steps.Count; ++i)
        {
            _steps[i].HighLight(false);
            if(i - index >= 8)
                _steps[i].gameObject.SetActive(false);
            else
                _steps[i].gameObject.SetActive(true);
        }
    }

    public void RegisterReset()
    {
        ResetManager.Register(this);
    }

    public void Reset()
    {
        UpdateFiche();
    }
}
