﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepList_UI : MonoBehaviour
{
    public StepItem_UI StepItemPrefab;
    public Transform Container;
    public PushButton_UI SaveButton;
    public PushButton_UI UndoButton;
    public PushButton_UI RedoButton;
    public PushButton_UI ExitButton;

    Dictionary<RecordedStep, StepItem_UI> _stepItems = new Dictionary<RecordedStep, StepItem_UI>();

    void Start()
    {
        StepRecorder.OnStepRecorded += AddStep;
        StepRecorder.OnStepUndo += RemoveStep;
        StepRecorder.OnStepRedo += AddStep;
        Container.gameObject.SetActive(true);
    }

    void Update()
    {
        Container.gameObject.SetActive(true);

        if (SaveButton.IsPressed && SaveButton.PressingController.GetPressDown(ControllerButton.Trigger))
        {
            AssemblyRecorder.SaveRecording();
        }

        if (UndoButton.IsPressed && UndoButton.PressingController.GetPressDown(ControllerButton.Trigger))
        {
            StepRecorder.Instance.Undo();
        }

        if (RedoButton.IsPressed && RedoButton.PressingController.GetPressDown(ControllerButton.Trigger))
        {
            StepRecorder.Instance.Redo();
        }

        if (ExitButton.IsPressed && ExitButton.PressingController.GetPressDown(ControllerButton.Trigger))
        {
            TrainingManager.QuitTraining();
        }
    }

    void AddStep(RecordedStep step)
    {
        Debug.Log("Adding step!");
        var StepGO = Instantiate(StepItemPrefab.gameObject);
        StepGO.transform.SetParent(Container, false);
        var stepItem = StepGO.GetComponent<StepItem_UI>();
        stepItem.SetStep(step);
        _stepItems.Add(step, stepItem);
    }

    void RemoveStep(RecordedStep step)
    {
        if (_stepItems.ContainsKey(step))
        {
            Destroy(_stepItems[step].gameObject);
            _stepItems.Remove(step);
        }
    }
}
