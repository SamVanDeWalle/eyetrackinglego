﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationContainer : MonoBehaviour
{
    public Material HighlightMat;
    public int ID;
    public ContainerRack ParentRack;

    PickupController _controller;
    Material _startMat;
    Renderer _renderer;
    Collider _enteredCollider;
    bool _bHovering;

    void Awake()
    {
        _renderer = GetComponent<Renderer>();
        _startMat = _renderer.material;
    }

    void OnTriggerEnter(Collider other)
    {
        var assemblyPiece = other.GetComponent<AssemblyPiece>();
        var controller = ViveHelper.GetController(other.transform);
        if(assemblyPiece != null)
        {
            Hover(assemblyPiece, other);
        }
        if(controller != null)
        {
            Hover(controller, other);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other == _enteredCollider)
        {
            Leave();
        }
    }

    void Hover(AssemblyPiece piece, Collider collider)
    {
        _renderer.material = HighlightMat;
        _controller = piece.Pickable.Controller;
        _controller.OnAPDropped -= Set;
        _controller.OnAPDropped += Set;
        _bHovering = true;
        _enteredCollider = collider;
    }

    void Hover(ViveController controller, Collider collider)
    {
        var pickupController = PickupManager.GetPickupController(controller);
        var pickable = pickupController.PickedUpPickable;
        if (pickable == null)
            return;

        var assemblyPiece = pickable.GetComponent<AssemblyPiece>();
        if (assemblyPiece == null)
            return;

        _renderer.material = HighlightMat;
        _controller = pickupController;
        _controller.OnAPDropped -= Set;
        _controller.OnAPDropped += Set;
        _bHovering = true;
        _enteredCollider = collider;
    }

    void Leave()
    {
        _controller.OnAPDropped -= Set;
        _renderer.material = _startMat;
    }

    void Set(AssemblyPiece piece)
    {
        if (!_bHovering)
            return;

        if(ParentRack != null)
        {
            ParentRack.SetContainer(this, piece.TypeID, true);
        }
        else
        {
            var apContainer = gameObject.AddComponent<AssemblyPieceContainer>();
            apContainer.APTypeID = piece.TypeID;
        }
    }
}
