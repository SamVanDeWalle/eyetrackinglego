﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct LinkedPickable
{
    public Pickable Pickable;
    public Transform Parent;

    public LinkedPickable(Pickable pickable, Transform parent)
    {
        Pickable = pickable;
        Parent = parent;
    }
}

[RequireComponent(typeof(Draggable))]
[RequireComponent(typeof(Rotatable))]
[RequireComponent(typeof(BoundingInfo))]
public class StationItem : MonoBehaviour
{
    public StationItemMenu_UI Menu;
    public Collider DragRegion;
    public Transform MenuAncher;
    public bool GenericMenuPosition;

    List<LinkedPickable> _linkedPickables = new List<LinkedPickable>();
    ViveController _controller;
    Draggable _draggable;
    Rotatable _rotatable;
    BoundingInfo _boundingInfo;

    void Awake()
    {
        _draggable = GetComponent<Draggable>();
        _rotatable = GetComponent<Rotatable>();
        _boundingInfo = GetComponent<BoundingInfo>();
    }

    void Update()
    {
        if(Menu == null)
        {
            //Debug.LogWarning("StationItem::Update - Missing menu on " + gameObject.name);
            return;
        }

        if (GenericMenuPosition)
            SetMenuPosition();
        SetMenuActivation();

        if (Menu.MoveButton.IsPressed && Menu.MoveButton.PressingController.GetPressDown(ControllerButton.Trigger))
        {
            Menu.MoveButton.SetActivated(true);
            LinkDragRegion();
            _draggable.StartDragging(Menu.MoveButton.PressingController.Transform);
            _controller = Menu.MoveButton.PressingController;
        }

        if(_draggable.IsDragging && !_controller.GetPress(ControllerButton.Trigger))
        {
            Menu.MoveButton.SetActivated(false);
            _draggable.StopDragging();
            UnlinkDragRegion();
            //if (!Menu.AnyButtonPressed)
            //    PickupController.GlobalAllowPicking = true;
        }

        if (Menu.RotateButton.IsPressed && Menu.RotateButton.PressingController.GetPressDown(ControllerButton.Trigger))
        {
            Menu.RotateButton.SetActivated(true);
            LinkDragRegion();
            _rotatable.StartRotating(Menu.RotateButton.PressingController.Transform);
            _controller = Menu.RotateButton.PressingController;
        }

        if (_rotatable.IsRotating && !_controller.GetPress(ControllerButton.Trigger))
        {
            Menu.RotateButton.SetActivated(false);
            _rotatable.StopRotating();
            UnlinkDragRegion();
            //if (!Menu.AnyButtonPressed)
            //    PickupController.GlobalAllowPicking = true;
        }

        if (Menu.RemoveButton.IsPressed && Menu.RemoveButton.PressingController.GetPressDown(ControllerButton.Trigger))
        {
            Destroy(gameObject);
            //if (!Menu.AnyButtonPressed)
            //    PickupController.GlobalAllowPicking = true;
        }
    }

    void LinkDragRegion()
    {
        if (DragRegion != null)
        {
            for (int i = 0; i < PickupManager.Pickables.Count; ++i)
            {
                if (DragRegion.bounds.Contains(PickupManager.Pickables[i].transform.position))
                {
                    var linkedPickable = new LinkedPickable(PickupManager.Pickables[i], PickupManager.Pickables[i].transform.parent);
                    _linkedPickables.Add(linkedPickable);
                    PickupManager.Pickables[i].transform.SetParent(transform, true);
                }
            }
        }
    }

    void UnlinkDragRegion()
    {
        for (int i = 0; i < _linkedPickables.Count; ++i)
        {
            _linkedPickables[i].Pickable.transform.SetParent(_linkedPickables[i].Parent, true);
        }
        _linkedPickables.Clear();
    }

    void SetMenuPosition()
    {
        if (_boundingInfo == null)
            return;

        var headpos = ViveHelper.Head.position;
        headpos.y = transform.position.y;
        var menuPosition = transform.position + (headpos - transform.position).normalized * _boundingInfo.Radius * 0.5f;
        menuPosition.y += 0.12f;
        MenuAncher.position = menuPosition;
        MenuAncher.forward = headpos - transform.position;
    }

    void SetMenuActivation()
    {
        if (MenuAncher == null)
            return;

        var pickable = GetComponent<Pickable>();
        foreach (var controller in ViveHelper.Controllers)
        {
            if((controller.Value.Transform.position - MenuAncher.position).sqrMagnitude < Station.MenuRangeSqr)
            {
                Menu.gameObject.SetActive(true);
                if (pickable != null)
                    pickable.PopOutTooltip(Station.BuildColor);
                return;
            }
        }

        Menu.gameObject.SetActive(false);
        if (pickable != null)
            pickable.PopBackTooltip();
    }

    void OnDisable()
    {
        if(Menu != null)
            Menu.gameObject.SetActive(false);
    }
}
