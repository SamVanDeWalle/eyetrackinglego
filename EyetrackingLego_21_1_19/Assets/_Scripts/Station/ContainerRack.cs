﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ContainerRack : MonoBehaviour
{
    public List<StationContainer> Containers
    {
        get
        {
            if (_containers == null || _containers.Count == 0)
                LoadContainers();

            return _containers;
        }
        private set { }
    }
    public int ID;
    Dictionary<StationContainer, AssemblyPieceContainer> AssemblyPieceContainers = new Dictionary<StationContainer, AssemblyPieceContainer>();
    List<StationContainer> _containers = new List<StationContainer>();

    void Awake()
    {
        LoadContainers();
    }

    void LoadContainers()
    {
        _containers = GetComponentsInChildren<StationContainer>().ToList();
        AssemblyPieceContainers.Clear();
        foreach (var container in _containers)
        {
            container.ParentRack = this;
            var apContainer = container.GetComponent<AssemblyPieceContainer>();
            if(apContainer != null)
                AssemblyPieceContainers.Add(container, apContainer);
        }
    }

    public AssemblyPieceContainer SetContainer(int containerID, int typeID, bool replace = true)
    {
        var container = Containers.FirstOrDefault(c => c.ID == containerID);
        if(container == null)
        {
            Debug.LogWarning("ContainerRack::SetContainer - Rack has no container with id: " + containerID);
            return null;
        }

        return SetContainer(container, typeID, replace);
    }

    public AssemblyPieceContainer SetContainer(StationContainer container, int typeID, bool replace = true) 
    {
        AssemblyPieceContainer apContainer;
        if (AssemblyPieceContainers.ContainsKey(container))
            apContainer = AssemblyPieceContainers[container];
        else
            apContainer = container.gameObject.AddComponent<AssemblyPieceContainer>();

        if (replace)
        {
            apContainer.APTypeID = typeID;
            // Replace visuals ..
        }

        return apContainer;
    }

    public void DisableStationContainers()
    {
        for(int i = 0; i < _containers.Count; ++i)
        {
            if(_containers[i] != null)
                _containers[i].enabled = false;
        }
    }
}
