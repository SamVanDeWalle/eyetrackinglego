﻿using Helpers;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FurnitureManager : Singleton<FurnitureManager>
{
    const string PREFAB_PATH = "Prefabs/Furniture/";
    public static List<Furniture> Furniture
    {
        get
        {
            if (_furniture == null || _furniture.Count == 0)
                FindFurniture();

            return _furniture;
        }
        private set { }
    }
    public static List<ContainerRack> ContainerRacks
    {
        get
        {
            if (_containerRacks == null || _containerRacks.Count == 0)
                FindContainerRacks();

            return _containerRacks;
        }
        private set { }
    }
    public static List<Workbench> Workbenches
    {
        get
        {
            if (_workbenches == null || _workbenches.Count == 0)
                FindWorkbenches();

            return _workbenches;
        }
        private set { }
    }

    static List<Furniture> _furniture = new List<Furniture>();
    static List<ContainerRack> _containerRacks = new List<ContainerRack>();
    static List<Workbench> _workbenches = new List<Workbench>();
    static Dictionary<string, Furniture> _prefabPool = new Dictionary<string, Furniture>();

    public delegate void DefaultAction();
    public static event DefaultAction OnPrefabPoolLoaded;

    public static void GeneratePrefabPool()
    {
        GeneratePrefabPool(TrainingManager.Instance.AssemblyName);
    }

    public static void GeneratePrefabPool(string assemblyName)
    {
        _prefabPool.Clear();
        var postSlash = assemblyName == "" ? "" : "/";
        var prefabs = Resources.LoadAll<Furniture>(PREFAB_PATH + assemblyName + postSlash);
        foreach (var p in prefabs)
        {
            if (_prefabPool.ContainsKey(p.TypeID))
            {
                Debug.LogWarning("FurnitureManager::GeneratePrefabPool - Found multiple prefabs with same type id!");
                continue;
            }

            _prefabPool.Add(p.TypeID, p);
        }

        if (OnPrefabPoolLoaded != null)
            OnPrefabPoolLoaded();
    }

    public static void FindFurniture()
    {
        _furniture = FindObjectsOfType<Furniture>().ToList();
    }

    public static void FindContainerRacks()
    {
        _containerRacks = Furniture.Where(f => f.GetComponent<ContainerRack>() != null).Select(f => f.GetComponent<ContainerRack>()).ToList();
    }

    public static void FindWorkbenches()
    {
        _workbenches = FindObjectsOfType<Workbench>().ToList();
    }

    public static Furniture SpawnPiece(string typeID)
    {
        return SpawnPiece(typeID, Vector3.zero, Quaternion.identity);
    }

    public static Furniture SpawnPiece(string typeID, Vector3 position, Quaternion rotation)
    {
        return SpawnPiece(typeID, position, rotation, Instance.transform);
    }

    public static Furniture SpawnPiece(string typeID, Vector3 position, Quaternion rotation, Transform parent)
    {
        if (!_prefabPool.ContainsKey(typeID))
        {
            Debug.LogWarning("FurnitureManager::SpawnPiece - Unable to find prefab piece with type: " + typeID);
            return null;
        }

        var pieceGO = Instantiate(_prefabPool[typeID].gameObject, position, rotation);
        pieceGO.transform.SetParent(parent);
        pieceGO.transform.localScale = Vector3.one;
        var furniture = pieceGO.GetComponent<Furniture>();
#if !UNITY_EDITOR
        //Ensure(piece);
#endif
        //_spawnedPieces.Add(piece);
        return furniture;
    }

    public static void AddPiece(Furniture piece)
    {
        Furniture.Add(piece);
    }

    public static void Ensure(Furniture piece)
    {
        if (Furniture == null || Furniture.Count == 0)
            FindFurniture();

        if (!Furniture.Contains(piece))
            AddPiece(piece);
    }

    public static void DestroyAllFurniture()
    {
        for (int i = _furniture.Count - 1; i > -1; --i)
        {
            DestroyImmediate(_furniture[i].gameObject);
            _furniture.RemoveAt(i);
        }

        _furniture.Clear();
    }

    public static ContainerRack GetContainerRack(int id)
    {
        return ContainerRacks.FirstOrDefault(c => c.ID == id);
    }
}
