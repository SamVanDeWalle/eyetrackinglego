﻿/*
 * Save and load a station, including positions of tables, tools and assembly pieces
 * A station should be changeable from the editor as well as VR 
 * 
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[Serializable]
public class Vector3_Data
{
    public float X, Y, Z;

    public Vector3_Data(Vector3 vector)
    {
        X = vector.x;
        Y = vector.y;
        Z = vector.z;
    }

    public Vector3 GetVector3()
    {
        return new Vector3(X, Y, Z);
    }

    public static Vector3 GetVector3(Vector3_Data data)
    {
        return new Vector3(data.X, data.Y, data.Z);
    }
}

[Serializable]
public class Quaternion_Data
{
    public float X, Y, Z, W;

    public Quaternion_Data(Quaternion quaternion)
    {
        X = quaternion.x;
        Y = quaternion.y;
        Z = quaternion.z;
        W = quaternion.w;
    }

    public Quaternion GetQuaternion()
    {
        return new Quaternion(X, Y, Z, W);
    }

    public static Quaternion GetQuaternion(Quaternion_Data data)
    {
        return new Quaternion(data.X, data.Y, data.Z, data.W);
    }
}

[Serializable]
public class SD_Transform
{
    [SerializeField]
    public int ID;
    [SerializeField]
    public Vector3_Data Position;
    [SerializeField]
    public Quaternion_Data Rotation;

    public SD_Transform(int id, Vector3 position, Quaternion rotation)
    {
        ID = id;
        Position = new Vector3_Data(position);
        Rotation = new Quaternion_Data(rotation);
    }

    public SD_Transform(Transform transform)
    {
        ID = transform.GetInstanceID();
        Position = new Vector3_Data(transform.position);
        Rotation = new Quaternion_Data(transform.rotation);
    }
}

[Serializable]
public class SD_AssemblyPiece
{
    [SerializeField]
    public int TypeID;
    [SerializeField]
    public SD_Transform Transform;

    public SD_AssemblyPiece(AssemblyPiece piece)
    {
        TypeID = piece.TypeID;
        Transform = new SD_Transform(piece.transform);
    }
}

[Serializable]
public class SD_Furniture
{
    [SerializeField]
    public string TypeID;
    [SerializeField]
    public SD_Transform Transform;

    public SD_Furniture(Furniture furniture)
    {
        TypeID = furniture.TypeID;
        Transform = new SD_Transform(furniture.transform);
    }
}

[Serializable]
public class SD_Container
{
    [SerializeField]
    public int ID;          // Matches the id on the rack

    [SerializeField]        
    public int TypeID;      // Matches the id of the assembly piece to spawn

    [SerializeField]
    public int ParentID;    // Matches the id of the rack

    public SD_Container(AssemblyPieceContainer container)
    {
        var stationContainer = container.GetComponent<StationContainer>();
        ID = stationContainer.ID;
        TypeID = container.APTypeID;
        ParentID = stationContainer.ParentRack.ID;
    }
}

[Serializable]
public class SD_Station
{
    public List<SD_AssemblyPiece> AssemblyPieces = new List<SD_AssemblyPiece>();
    public List<SD_Furniture> Furniture = new List<SD_Furniture>();
    public List<SD_Container> Containers = new List<SD_Container>();

    [NonSerialized]
    public Transform AssemblyPieceContainer;
    [NonSerialized]
    public Transform FurnitureContainer;

    void StoreAssemblyPieces()
    {
        AssemblyPieceManager.FindAssemblyPieces();
        AssemblyPieces.Clear();
        var pieces = AssemblyPieceManager.AssemblyPieces;
        for (int i = 0; i < pieces.Count; ++i)
        {
            var data = new SD_AssemblyPiece(pieces[i]);
            AssemblyPieces.Add(data);
        }
    }

    void LoadAssemblyPieces()
    {
        AssemblyPieceManager.FindAssemblyPieces();
        AssemblyPieceManager.DestroyAllPieces();
        AssemblyPieceManager.GeneratePrefabPool(TrainingManager.Instance.AssemblyName);
        for (int i = 0; i < AssemblyPieces.Count; ++i)
        {
            var piece = AssemblyPieceManager.SpawnPiece(AssemblyPieces[i].TypeID, AssemblyPieces[i].Transform.Position.GetVector3(), AssemblyPieces[i].Transform.Rotation.GetQuaternion(), AssemblyPieceContainer);
            var stationItem = piece.gameObject.AddComponent<StationItem>();
            var menu = GameObject.Instantiate(Station.GenericItemMenuPrefab);
            menu.transform.SetParent(piece.transform);
            stationItem.MenuAncher = menu.transform;
            stationItem.Menu = menu.GetComponentInChildren<StationItemMenu_UI>();
            stationItem.GenericMenuPosition = true;
            //piece.Pickable.PopOutTooltip();
            if (TrainingManager.TrainingMode != TrainingMode.Build)
                stationItem.enabled = false;
        }

        AssemblyPieceManager.FindAssemblyPieces();
    }

    void StoreFurniture()
    {
        FurnitureManager.FindFurniture();
        Furniture.Clear();
        var furniture = FurnitureManager.Furniture;
        for (int i = 0; i < furniture.Count; ++i)
        {
            var data = new SD_Furniture(furniture[i]);
            Furniture.Add(data);
        }
    }

    void LoadFurniture()
    {
        FurnitureManager.FindFurniture();
        FurnitureManager.DestroyAllFurniture();
        FurnitureManager.GeneratePrefabPool("");
        for (int i = 0; i < Furniture.Count; ++i)
        {
            var furniture = FurnitureManager.SpawnPiece(Furniture[i].TypeID, Furniture[i].Transform.Position.GetVector3(), Furniture[i].Transform.Rotation.GetQuaternion(), FurnitureContainer);
            if (TrainingManager.TrainingMode != TrainingMode.Build)
                furniture.GetComponent<StationItem>().enabled = false;
        }

        FurnitureManager.FindFurniture();
    }

    void StoreContainers()
    {
        AssemblyPieceManager.FindAssemblyPieceContainers();
        Containers.Clear();
        var containers = AssemblyPieceManager.AssemblyPieceContainers;
        for (int i = 0; i < containers.Count; ++i)
        {
            var data = new SD_Container(containers[i]);
            Containers.Add(data);
        }
    }

    void LoadContainers()
    {
        AssemblyPieceManager.FindAssemblyPieceContainers();
        AssemblyPieceManager.DestroyAllContainers();
        FurnitureManager.FindContainerRacks();

        for (int i = 0; i < Containers.Count; ++i)
        {
            var parentRack = FurnitureManager.GetContainerRack(Containers[i].ParentID);
            if(parentRack != null)
                parentRack.SetContainer(Containers[i].ID, Containers[i].TypeID, true);
            //apContainer.Pickable.PopOutTooltip();
        }

        if (TrainingManager.TrainingMode != TrainingMode.Build)
        {
            for(int i = 0; i < FurnitureManager.ContainerRacks.Count; ++i)
            {
                FurnitureManager.ContainerRacks[i].DisableStationContainers();
            }
        }

        AssemblyPieceManager.FindAssemblyPieceContainers();
    }

    public void Store()
    {
        StoreAssemblyPieces();
        StoreFurniture();
        StoreContainers();
    }

    public void Load()
    {
        LoadFurniture();
        LoadAssemblyPieces();
        LoadContainers();
    }
}

public class Station : MonoBehaviour
{
    public string Name;
    public Transform AssemblyPieceContainer;
    public Transform FurnitureContainer;
    public GameObject ItemMenuPrefab;
    public static GameObject GenericItemMenuPrefab;
    public string Path { get { return Folder + Name; } }
    public string Folder { get { return Directory.GetCurrentDirectory() + @"\Datapath\Stations\"; } }
    public static float MenuRangeSqr = 0.1f;
    public static Color BuildColor = new Color(1, 206/255.0f, 0, 132/255.0f);

    SD_Station _stationData;

    void Awake()
    {
        GenericItemMenuPrefab = ItemMenuPrefab;
    }

    public void Save()
    {
        _stationData = new SD_Station();
        _stationData.AssemblyPieceContainer = AssemblyPieceContainer;
        _stationData.FurnitureContainer = FurnitureContainer;
        _stationData.Store();

        Directory.CreateDirectory(Folder);
        using (Stream file = File.Open(Path, FileMode.OpenOrCreate))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            SurrogateSelector surrogateSelector = new SurrogateSelector();
            Vector3SerializationSurrogate vector3SS = new Vector3SerializationSurrogate();
            QuaternionSerializationSurrogate quaternionSS = new QuaternionSerializationSurrogate();
            surrogateSelector.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), vector3SS);
            surrogateSelector.AddSurrogate(typeof(Quaternion), new StreamingContext(StreamingContextStates.All), quaternionSS);
            formatter.SurrogateSelector = surrogateSelector;

            formatter.Serialize(file, _stationData);
        }
    }

    public void Load()
    {
        GenericItemMenuPrefab = ItemMenuPrefab;

        using (Stream file = File.Open(Path, FileMode.Open))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            _stationData = (SD_Station)formatter.Deserialize(file);
            _stationData.AssemblyPieceContainer = AssemblyPieceContainer;
            _stationData.FurnitureContainer = FurnitureContainer;
            _stationData.Load();
        }
    }
}
