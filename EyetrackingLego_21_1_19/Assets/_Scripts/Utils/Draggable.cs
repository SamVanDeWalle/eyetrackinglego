﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Draggable : MonoBehaviour
{
    public bool LockX, LockY, LockZ;
    public bool IsDragging { get { return _bDragging; } private set { } }

    Rigidbody _rb;
    Transform _dragger;
    Vector3 _startDragPosition;
    Vector3 _startDraggerPosition;
    bool _bDragging;
    bool _bWasKinematic;

    public void StartDragging(Transform dragger)
    {
        _rb = GetComponent<Rigidbody>();
        if (_rb != null)
        {
            _bWasKinematic = _rb.isKinematic;
            _rb.isKinematic = true;
        }
        _dragger = dragger;
        _startDragPosition = transform.position;
        _startDraggerPosition = dragger.position;
        _bDragging = true;
    }

    public void StopDragging()
    {
        if (_rb != null)
            _rb.isKinematic = _bWasKinematic;
        _bDragging = false;
    }

    void Update()
    {
        if (_bDragging)
            Drag();
    }

    void Drag()
    {
        var movement = _dragger.position - _startDraggerPosition;
        if (LockX)
            movement.x = 0;
        if (LockY)
            movement.y = 0;
        if (LockZ)
            movement.z = 0;

        transform.position = _startDragPosition + movement;
    }
}
