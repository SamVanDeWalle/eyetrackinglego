﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bounce : MonoBehaviour
{
    public float Range;
    public float Speed;
    public Vector3 Direction;

    Vector3 _startPosition;

    void Start()
    {
        _startPosition = transform.position;
    }

    void Update()
    {
        transform.position = _startPosition + Mathf.Sin(Time.timeSinceLevelLoad * Speed) * Direction * Range;
    }
}