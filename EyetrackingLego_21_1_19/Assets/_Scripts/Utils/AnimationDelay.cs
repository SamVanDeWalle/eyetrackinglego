﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationDelay : MonoBehaviour
{
    public float Delay;

	IEnumerator Start ()
    {
        var animation = GetComponent<Animation>();
        yield return new WaitForSeconds(Delay);
        animation.Play();
	}
}
