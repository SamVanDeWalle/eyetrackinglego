﻿using UnityEngine;
using System.Collections;

public class SpectateCamera : MonoBehaviour
{
    public float FlySpeed = 10.0f;
    public float AimSensitivity = 10.0f;

	void Update ()
    {
	    if(Input.GetKey(KeyCode.W))
        {
            transform.position += transform.forward * FlySpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.position -= transform.forward * FlySpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.position += transform.right * FlySpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.position -= transform.right * FlySpeed * Time.deltaTime;
        }

        var yRot = Input.GetAxis("Mouse X") * AimSensitivity;
        var xRot = -Input.GetAxis("Mouse Y") * AimSensitivity;

        transform.Rotate(xRot, yRot, 0);
        var rot = transform.eulerAngles;
        rot.z = 0;
        transform.eulerAngles = rot;
    }
}
