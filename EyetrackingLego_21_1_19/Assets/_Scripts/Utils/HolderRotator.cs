﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HolderRotator : MonoBehaviour, IResettable
{
    public Axis Axis = Axis.Z;
    Quaternion _startRotation;

    void Awake()
    {
        _startRotation = transform.rotation;
        RegisterReset();
    }

    public virtual float GetCurrentAngle()
    {
        if(Axis == Axis.X)
            return transform.localEulerAngles.x;
        else if (Axis == Axis.Y)
            return transform.localEulerAngles.y;
        else if (Axis == Axis.Z)
            return transform.localEulerAngles.z;

        return transform.localEulerAngles.z;
    }

    public void RegisterReset()
    {
        ResetManager.Register(this);
    }

    public void Reset()
    {
        transform.rotation = _startRotation;
    }
}
