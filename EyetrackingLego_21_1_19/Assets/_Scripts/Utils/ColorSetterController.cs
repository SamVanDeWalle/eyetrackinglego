﻿using UnityEngine;
using System.Collections;

public class ColorSetterController : ColorSetter
{
    public override void SetColor(Color color)
    {
        var renderer = transform.Find("Vive_Controller_Ghost").GetComponent<Renderer>();
        renderer.material.color = color;
        GetComponent<SteamVR_LaserPointer>().color = color;
    }
}
