﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class FixedTiling : MonoBehaviour
{
    float _initialScale, _lastScale;
    float _initialTiling;
    float _ratio;
    Material _material;

    void Start()
    {
        _material = GetComponent<Renderer>().sharedMaterial;
        _initialScale = transform.localScale.z;
        _lastScale = _initialScale;
        _initialTiling = _material != null ? _material.GetTextureScale("_MainTex").x : 0;
        _ratio = _initialTiling / _initialScale;
    }

    void OnDestroy()
    {
        if (_material == null)
            return;

        var tiling = _material.GetTextureScale("_MainTex");
        tiling.x = _initialTiling;
        _material.SetTextureScale("_MainTex", tiling);
    }

    void Update()
    {
        if (transform.localScale.z == _lastScale)
            return;

        _lastScale = transform.localScale.z;
        var newTiling = _lastScale * _ratio;
        var tiling = _material.GetTextureScale("_MainTex");
        tiling.x = newTiling;
        _material.SetTextureScale("_MainTex", tiling);
    }
}
