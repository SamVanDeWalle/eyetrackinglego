﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoInactive : MonoBehaviour
{
    public float Delay = 5.0f;
	
	void OnEnable()
    {
        StartCoroutine(Deactivate(Delay));
    }

    IEnumerator Deactivate(float delay)
    {
        yield return new WaitForSeconds(delay);

        gameObject.SetActive(false);
    }
}
