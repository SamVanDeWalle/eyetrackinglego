﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

[Serializable]
public class DraggerData
{
    public Vector3 Position;
    public Quaternion Rotation;

    [NonSerialized]
    const string PATH = @"\draggerData";
    static string _path { get { return Directory.GetCurrentDirectory() + @"\Datapath\" + PATH; } }

    public static DraggerData Load(string path_suffix = "")
    {
        string json;

        try
        {
            StreamReader reader = new StreamReader(_path + path_suffix + ".json", Encoding.Default);
            using (reader)
            {
                json = reader.ReadToEnd();
            }
        }
        catch (Exception e)
        {
            Debug.Log(string.Format("{0}\n", e.Message));
            return new DraggerData();
        }

        DraggerData data = JsonUtility.FromJson<DraggerData>(json);
        return data;
    }

    public void Save(string path_suffix = "")
    {
        Save(this, path_suffix);
    }

    public static void Save(DraggerData data, string path_suffix = "")
    {
        string json = JsonUtility.ToJson(data, true);

        try
        {
            StreamWriter writer = new StreamWriter(_path + path_suffix + ".json", false, Encoding.Default);
            using (writer)
            {
                writer.Write(json);
            }
        }
        catch (Exception e)
        {
            Debug.Log(string.Format("{0}\n", e.Message));
        }
    }
}

public class SlaveData
{
    public Vector3 PositionOffset;

    public SlaveData(Vector3 posOffset)
    {
        PositionOffset = posOffset;
    }
}

public class VR_Dragger : MonoBehaviour
{
    public Material PassiveMat;
    public Material HoverMat;
    public float RotSpeed = 5.0f;
    public List<Transform> Slaves = new List<Transform>();

    Dictionary<Transform, SlaveData> _slaveData = new Dictionary<Transform, SlaveData>();
    MeshRenderer _renderer;
    bool _bListening, _bDragging;
    ViveController _controller;
    float _startAngle;
    Vector3 _startPos;
    Vector3 _preDragPos;
    Vector3 _preDragControllerPos;
    bool _bActive;
    DraggerData _data;

    void Awake()
    {
        _renderer = GetComponent<MeshRenderer>();
        _renderer.enabled = false;
        _startPos = transform.position;
        _startAngle = transform.eulerAngles.y;
        FillSlaveData();
    }

    void FillSlaveData()
    {
        for(int i = 0; i < Slaves.Count; ++i)
        {
            _slaveData.Add(Slaves[i], new SlaveData(Slaves[i].transform.position - transform.position));
        }
    }

    public void Activate()
    {
        _renderer.enabled = true;
        _bActive = true;
    }

    public void Deactivate()
    {
        _renderer.enabled = false;
        _bActive = false;
    }

    public void Toggle()
    {
        if (!_bActive)
            Activate();
        else
            Deactivate();
    }

    void Update()
    {
        if (!_bActive || !_bListening || _controller == null)
        {
            _bDragging = false;
            return;
        }

        if (_controller.GetPress(ControllerButton.Trigger))
        {
            if (!_bDragging)
                StartDragging();

            var oldPos = transform.position;
            var translation = _controller.Transform.position - _preDragControllerPos;
            translation.y = 0;
            transform.position = _preDragPos + translation;
            TranslateSlaves(transform.position - oldPos);
            //UpdateSlavePositions();
        }
        else
        {
            _bDragging = false;
        }

        if (_controller.GetTouch())
        {
            var touchPos = _controller.GetTouchPadPosition();
            var angle = RotSpeed * Time.deltaTime;
            if (touchPos.x < 0)
            {
                angle *= -1;
            }

            transform.Rotate(0, angle, 0);
            RotateSlaves(angle);
        }
    }

    void UpdateSlavePositions()
    {
        for(int i = 0; i < Slaves.Count; ++i)
        {
            Slaves[i].position = transform.position + _slaveData[Slaves[i]].PositionOffset;
        }
    }

    void TranslateSlaves(Vector3 translation)
    {
        for (int i = 0; i < Slaves.Count; ++i)
        {
            //Slaves[i].Translate(translation);
            Slaves[i].transform.position += translation;
        }
    }

    void RotateSlaves(float angle)
    {
        for (int i = 0; i < Slaves.Count; ++i)
        {
            Slaves[i].RotateAround(transform.position, Vector3.up, angle);
        }
    }

    void StartDragging()
    {
        _preDragPos = transform.position;
        _preDragControllerPos = _controller.Transform.position;
        _bDragging = true;
    }

    void OnTriggerEnter(Collider other)
    {
        if (!_bActive)
            return;

        var trackedObject = other.GetComponentInParent<SteamVR_TrackedObject>();
        if (trackedObject != null)
        {
            foreach(var controller in ViveHelper.Controllers)
            {
                if(controller.Value.TrackedObject == trackedObject)
                {
                    _controller = controller.Value;
                    _renderer.sharedMaterial = HoverMat;
                    _bListening = true;
                    break;
                }
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (!_bActive)
            return;

        var trackedObject = other.GetComponentInParent<SteamVR_TrackedObject>();
        if (trackedObject != null && trackedObject == _controller.TrackedObject)
        {
            _renderer.sharedMaterial = PassiveMat;
            _bListening = false;
        }
    }

    public void Load()
    {
        _data = DraggerData.Load(gameObject.GetInstanceID().ToString());
        transform.position = _data.Position;
        transform.rotation = _data.Rotation;
        var translation = transform.position - _startPos;
        TranslateSlaves(translation);
        var angle = _startAngle - transform.eulerAngles.y;
        RotateSlaves(angle);
    }

    public void Save()
    {
        _data = new DraggerData();
        _data.Position = transform.position;
        _data.Rotation = transform.rotation;

        _data.Save(gameObject.GetInstanceID().ToString());
    }
}
