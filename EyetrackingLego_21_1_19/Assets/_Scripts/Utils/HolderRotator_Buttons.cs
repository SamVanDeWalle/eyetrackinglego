﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class RotationButton
{
    public Vector3 Rotation;
    public PushButton Button;
}

public class HolderRotator_Buttons : HolderRotator
{
    public List<RotationButton> RotationButtons;
    public float RotationSpeed = 2.0f;

    void Update()
    {
        foreach (var rbtn in RotationButtons)
        {
            if (rbtn.Button.IsPressed)
            {
                transform.Rotate(rbtn.Rotation * RotationSpeed * Time.deltaTime);
                ViveHelper.PulseAllControllers();
            }
        }
    }

    public override float GetCurrentAngle()
    {
        return transform.localEulerAngles.z;
    }
}
