﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VR_Stalker : MonoBehaviour
{
    public Vector3 Offset;
    public float Distance = 4.0f;
    public float AllowedAngle = 50;
    public float AllowedDeltaDistance = 2.0f;

    Transform _target;
    float _rotationSpeed = 3.0f;
    float _moveSpeed = 3.0f;

    Coroutine _moveRoutine;

	void Start ()
    {
        _target = ViveHelper.Head;
        Reset();
	}

    void OnEnable()
    {
        Reset();
    }

    void Reset()
    {
        if (_target == null)
            return;

        transform.position = _target.position + _target.forward * Distance;
        transform.forward = _target.forward;
    }

    void Update()
    {
        // If the angle between the current head forward and the stalker forward gets too big 
        // or the distance gets too different,
        // the stalker moves back towards its ideal spot
        if (_target == null)
            _target = ViveHelper.Head;

        var angle = Vector3.Angle(_target.forward, transform.forward);
        var distance = Vector3.Distance(_target.position, transform.position);
        var deltaDistance = Mathf.Abs(distance - Distance);
        if(angle > AllowedAngle || deltaDistance > AllowedDeltaDistance)
        {
            if(_moveRoutine == null)
            {
                var targetPosition = _target.position + _target.forward * Distance - _target.right * Distance * Offset.x + _target.up * Distance * Offset.y; 
                var targetForward = _target.forward.normalized;
                _moveRoutine = StartCoroutine(moveTo(targetPosition, targetForward));
            }
        }
    }

    IEnumerator moveTo(Vector3 position, Vector3 forward)
    {
        while ((transform.position - position).magnitude > 0.2f || Vector3.Angle(transform.forward, forward) > 2)
        {
            if (Vector3.Angle(transform.forward, forward) > 2)
            {
                var rotationStep = _rotationSpeed * Time.deltaTime;
                transform.forward = Vector3.RotateTowards(transform.forward, forward, rotationStep, 0.0f).normalized;
            }

            if ((transform.position - position).magnitude > 0.2f)
            {
                var direction = position - transform.position;
                var moveStep = direction * Time.deltaTime * _moveSpeed;
                if (moveStep.magnitude > direction.magnitude)
                {
                    moveStep = direction;
                }

                if (moveStep.magnitude > 0.002f)
                    transform.position += moveStep;
                else
                    transform.position = position;
            }

            yield return null;
        }

        _moveRoutine = null;
    }
}
