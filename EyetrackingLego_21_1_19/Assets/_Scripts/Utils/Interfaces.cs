﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IResettable
{
    void Reset();
    void RegisterReset();
}

public static class ResetManager
{
    static List<IResettable> _resettables = new List<IResettable>();

    public static void Register(IResettable resettable)
    {
        _resettables.Add(resettable);
    }

    public static void Reset()
    {
        for(int i = _resettables.Count - 1; i >= 0; --i)
        {
            _resettables[i].Reset();
        }
    }
}
