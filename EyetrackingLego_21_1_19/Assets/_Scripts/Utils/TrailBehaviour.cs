﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailBehaviour : MonoBehaviour
{
    private Transform destObject;
    private float speed = 6f;
    private Transform sourceObject;
    private LineRenderer lr;
    public float scrollSpeed = 3f;
    private float offset = 0f;
    Vector3 _sourceLocalPos;
	// Use this for initialization
	void Start ()
    {
        lr = GetComponent<LineRenderer>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        // set the positions for the line
        lr.SetPosition(0, sourceObject.transform.TransformPoint(_sourceLocalPos));
        lr.SetPosition(1, destObject.position);

        // UV animate the material
        offset += Time.deltaTime * scrollSpeed;
        lr.sharedMaterial.mainTextureOffset = new Vector2(offset % 1, 0);
        lr.sharedMaterial.mainTextureScale = new Vector3(-10f, 1f);
        
    }
    // set the trail connection
    public void SetTrail(Transform source, Transform destinationObject, Vector3 sourceLocalPos)
    {
        _sourceLocalPos = sourceLocalPos;
        sourceObject = source;
        destObject = destinationObject;
    }
}
