﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class ExternalPositioning : MonoBehaviour
{
    static string _path { get { return Directory.GetCurrentDirectory() + @"\Datapath\"; } }

    void Awake()
    {
        transform.localPosition = Load("position_camerarig");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            Save("position_camerarig");
        }
    }

    public static Vector3 Load(string path_suffix = "")
    {
        string json;

        try
        {
            StreamReader reader = new StreamReader(_path + path_suffix + ".json", Encoding.Default);
            using (reader)
            {
                json = reader.ReadToEnd();
            }
        }
        catch (Exception e)
        {
            Debug.Log(string.Format("{0}\n", e.Message));
            return new Vector3();
        }

        Vector3 position = JsonUtility.FromJson<Vector3>(json);
        return position;
    }

    public void Save(string path_suffix = "")
    {
        string json = JsonUtility.ToJson(transform.localPosition, true);

        try
        {
            StreamWriter writer = new StreamWriter(_path + path_suffix + ".json", false, Encoding.Default);
            using (writer)
            {
                writer.Write(json);
            }
        }
        catch (Exception e)
        {
            Debug.Log(string.Format("{0}\n", e.Message));
        }
    }
}
