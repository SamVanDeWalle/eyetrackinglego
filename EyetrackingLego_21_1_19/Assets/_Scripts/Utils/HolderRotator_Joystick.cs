﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HolderRotator_Joystick : HolderRotator
{
    public Joystick Stick;
    public Vector3 Rotation;
 
	void Update ()
    {
        if (Stick.IsManipulating)
        {
            transform.Rotate(Rotation * Stick.Manipulation * Time.deltaTime);
        }
	}

    public override float GetCurrentAngle()
    {
        return transform.eulerAngles.z;
    }
}
