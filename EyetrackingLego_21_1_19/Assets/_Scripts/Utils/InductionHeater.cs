﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// TODO handle as static tool, make class distinction between static tool and hand tool
public class InductionHeater : MonoBehaviour
{
    public PushButton ActivateButton;
    public float HeatSpeed = 5.0f;
    public Text DisplayText;
    public Temperature ConnectedProperty { get { return _connectedProperty; } private set { } }
    public bool IsHeating { get { return _bHeating; } private set { } }

    Temperature _connectedProperty;
    bool _bHeating;

    void Start()
    {
        GetComponent<Workbench>().OnAPPlaced += Connect;
        GetComponent<Workbench>().OnAPRemoved += Disconnect;
        ActivateButton.OnPressed += StartHeating;

        DisplayText.enabled = false;
    }

    public void StartHeating()
    {
        if (_bHeating)
            return;

        if(_connectedProperty != null)
        {
            _bHeating = true;
            DisplayText.enabled = true;
        }

        ActivateButton.GetComponent<WorldButtonIndicator>().StopIndicating();
    }

    public void Connect(AssemblyPiece piece)
    {
        var property = piece.GetComponent<Temperature>();
        if (property != null)
            _connectedProperty = property;
    }

    public void Disconnect(AssemblyPiece piece = null)
    {
        _bHeating = false;
        _connectedProperty = null;
        DisplayText.enabled = false;
    }

    void Update()
    {
        if (!_bHeating)
            return;

        _connectedProperty.HeatUp(HeatSpeed * Time.deltaTime);
        DisplayText.text = (int)_connectedProperty.Degrees + "°";
        if (_connectedProperty.IsMaxed)
        {
            DisplayText.color = Color.green;
        }
        else
        {
            DisplayText.color = Color.red;
        }
    }
}
