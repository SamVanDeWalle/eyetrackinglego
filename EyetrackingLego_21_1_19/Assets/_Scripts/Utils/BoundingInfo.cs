﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundingInfo : MonoBehaviour
{
    public float Radius { get { return _boundingSphere.Radius; } private set { } }

    BoundingSphere _boundingSphere;

    void Awake()
    {
        CalculateBoundingSphere();
    }

    public void CalculateBoundingSphere()
    {
        var renderers = GetComponentsInChildren<Renderer>();
        if (renderers.Length == 0) return;
        var renderer = renderers[0];
        var combinedBounds = renderer.bounds;
        foreach (var render in renderers)
        {
            if (render != renderer)
                combinedBounds.Encapsulate(render.bounds);
        }

        var radius = Vector3.Distance(combinedBounds.center, combinedBounds.min);
        _boundingSphere = new BoundingSphere(transform, combinedBounds.center - transform.position, radius);
    }
}
