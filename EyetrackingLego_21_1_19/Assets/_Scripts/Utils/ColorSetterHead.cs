﻿using UnityEngine;
using System.Collections;

public class ColorSetterHead : ColorSetter
{
    public override void SetColor(Color color)
    {
        var renderer = transform.Find("Vive_Head_Ghost").GetComponent<Renderer>();
        renderer.material.color = color;
    }
}
