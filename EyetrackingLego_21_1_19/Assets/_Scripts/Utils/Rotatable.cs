﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotatable : MonoBehaviour
{
    public bool IsRotating { get { return _bRotating; } private set { } }

    Transform _rotator;
    Quaternion _startRotation;
    Vector3 _startRotatorPosition;
    bool _bRotating;
	
    public void StartRotating(Transform rotator)
    {
        _rotator = rotator;
        _startRotation = transform.rotation;
        _startRotatorPosition = rotator.transform.position;
        _bRotating = true;
    }

    public void StopRotating()
    {
        _bRotating = false;
    }

	void Update ()
    {
        if (_bRotating)
            Rotate();
	}

    void Rotate()
    {
        var startAim = _startRotatorPosition - transform.position;
        startAim = Vector3.ProjectOnPlane(startAim, Vector3.up);
        var aim = _rotator.position - transform.position;
        aim = Vector3.ProjectOnPlane(aim, Vector3.up);
        var angle = Vector3.Angle(startAim, aim);
        angle = (int)(angle / 5.0f) * 5;
        var axis = Vector3.Cross(startAim, aim);
        transform.rotation = _startRotation;
        transform.Rotate(axis, angle);
    }
}
