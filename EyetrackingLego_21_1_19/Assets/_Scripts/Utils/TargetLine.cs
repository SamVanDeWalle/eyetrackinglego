﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetLine : MonoBehaviour
{
    public Transform Start, End;
    public Transform LineObject;

    void Update()
    {
        if (Start == null || End == null)
            return;

        transform.position = Start.position;
        var direction = End.position - Start.position;
        var scale = LineObject.localScale;
        scale.z = 4.0f * direction.magnitude;
        LineObject.localScale = scale;
        transform.up = ViveHelper.Head.position - Start.position;
        transform.forward = direction;
    }
}
