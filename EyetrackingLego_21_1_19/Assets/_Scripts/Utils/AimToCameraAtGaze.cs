﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimToCameraAtGaze : MonoBehaviour
{
    public float MaxGazeDistance = 4.0f;
    public float ViewDistance = 1.0f;
    public Collider Collider;

    Vector3 _startPosition;
    Quaternion _startRotation;

    bool _bActive;
    bool _bIdle = true;

    void Start()
    {
        _startPosition = transform.localPosition;
        _startRotation = transform.rotation;
        _bActive = true;
    }

    void FixedUpdate()
    {
        if (!_bActive)
            return;

        var toCam = Camera.main.transform.position - Collider.transform.position;
        _bIdle = true;
        if(toCam.sqrMagnitude < MaxGazeDistance * MaxGazeDistance)
        {
            RaycastHit hit;
            if(Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, ViewDistance*2.0f))
            {
                if(hit.collider == Collider && hit.distance > ViewDistance + 0.1f)
                {
                    transform.forward = -toCam;
                    transform.position = Camera.main.transform.position - toCam.normalized * ViewDistance;
                    _bIdle = false;
                }
            }
        }

        if (_bIdle)
            ResetPosRott();
    }

    void ResetPosRott()
    {
        transform.localPosition = _startPosition;
        transform.rotation = _startRotation;
    }
}
