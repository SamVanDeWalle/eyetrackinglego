﻿using UnityEngine;
using System.Collections;
using XBOX;

public class CameraMovement3D : MonoBehaviour 
{
    public bool Enable = true;
	float _sensitivity = 1.0f;
    float _x = 0.0f;
	float _y = 0.0f;
    Camera _camera;
    float _initXRotation = 0;

	void Awake() 
	{
        _camera = GetComponent<Camera>();
	}

    void Start()
    {
        _initXRotation = transform.parent.eulerAngles.y;
    }

	void Update () 
	{
        if (!_camera.enabled || !Enable)
            return;

	    _x += Input.GetAxis(XBOXONEController.RightJoystickHorizontal) * _sensitivity * 2.0f;
	    _y += Input.GetAxis(XBOXONEController.RightJoystickVertical) * _sensitivity * 1.5f;

        _x += Input.GetAxis("Mouse X") * _sensitivity;
        _y += Input.GetAxis("Mouse Y") * _sensitivity;

        var playerRotation = transform.parent.eulerAngles;
        playerRotation.y = _initXRotation + _x;
        transform.parent.eulerAngles = playerRotation;

        var headRotation = transform.eulerAngles;
        headRotation.x = -_y;
        transform.eulerAngles = headRotation;
	}
}
