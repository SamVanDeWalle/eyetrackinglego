﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

[Serializable]
public class JSONContainer<T> where T : JSONContainer<T> , new()
{
    [NonSerialized]
    public static string Filename = "Default";
    static string _fullPath { get { return Directory.GetCurrentDirectory() + @"\Datapath\"; } }
    const string EXT = ".json";

    public delegate void DefaultDelegate();
    public static event DefaultDelegate OnLoaded;

    public static T Load(string path_suffix = "vr_settings")
    {
        string json;

        try
        {
            Debug.Log("Trying to read:" + _fullPath + path_suffix + EXT);
            StreamReader reader = new StreamReader(_fullPath + path_suffix + EXT, Encoding.Default);
            using (reader)
            {
                json = reader.ReadToEnd();
            }
        }
        catch (Exception e)
        {
            Debug.Log(string.Format("{0}\n", e.Message));
            return new T();
        }

        T container = JsonUtility.FromJson<T>(json);
        if (OnLoaded != null)
            OnLoaded();
        return container;
    }

    public static void Save(T container, string path_suffix = "")
    {
        string json = JsonUtility.ToJson(container, true);

        try
        {
            Directory.CreateDirectory(_fullPath);
            StreamWriter writer = new StreamWriter(_fullPath + path_suffix + EXT, false, Encoding.Default);
            using (writer)
            {
                writer.Write(json);
            }
        }
        catch (Exception e)
        {
            Debug.Log(string.Format("{0}\n", e.Message));
        }
    }
}
