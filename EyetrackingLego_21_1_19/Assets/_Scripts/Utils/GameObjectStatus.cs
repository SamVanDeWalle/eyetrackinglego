﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectStatus : MonoBehaviour
{
    public delegate void StatusDelegate();

    public event StatusDelegate OnEnabled;
    public event StatusDelegate OnDisabled;

	void OnEnable()
    {
        if (OnEnabled != null)
            OnEnabled();
    }

    void OnDisable()
    {
        if (OnDisabled != null)
            OnDisabled();
    }
}
