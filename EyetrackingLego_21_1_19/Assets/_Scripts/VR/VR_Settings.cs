﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class VR_Setting
{
    public string Name;
    public bool Value;

    public delegate void ValueChanged(bool value);
    public event ValueChanged OnValueChanged;

    public VR_Setting(string name, bool value)
    {
        Name = name;
        Value = value;
    }

    public void SetValue(bool value)
    {
        Value = value;
        ValueChange();
    }

    public void ValueChange()
    {
        if (OnValueChanged != null)
            OnValueChanged(Value);
    }
}

[Serializable]
public class VR_Settings : JSONContainer<VR_Settings>
{
    public List<VR_Setting> SettingsList = new List<VR_Setting>();
    public static Dictionary<string, VR_Setting> Settings = new Dictionary<string, VR_Setting>();
    static VR_Settings _instance;
    Dictionary<string, List<VR_Setting.ValueChanged> > _subscriptions = new Dictionary<string, List<VR_Setting.ValueChanged>>();
    static string _fileName = "vr_settings";

    public delegate void SettingChanged();
    public static event SettingChanged OnSettingChanged;

    public VR_Settings()
    {
        //if (_instance != null) {
        //    Debug.LogError("VR_Setting already exists");
        //}
        _instance = this;
        Filename = "vr_settings";
        OnLoaded -= LoadPool;
        OnLoaded += LoadPool;
    }

    static void LoadInstance()
    {
        Filename = _fileName;
        _instance = Load();
    }

    void LoadPool()
    {
        Settings = new Dictionary<string, VR_Setting>();
        Debug.Log("Current settings:" + Settings);
        foreach(var setting in SettingsList)
        {
            Settings.Add(setting.Name, setting);
        }

        if (OnSettingChanged != null)
            OnSettingChanged();

        LoadSubscriptions();
    }

    public static void Add(string setting, bool value)
    {
        if(_instance == null)
        {
            LoadInstance();
        }

        if (!Settings.ContainsKey(setting))
        {
            var vr_setting = new VR_Setting(setting, value);
            _instance.SettingsList.Add(vr_setting);
            Settings.Add(setting, vr_setting);
        }

        Save(_instance);
    }

    public static bool Get(string setting)
    {
        if(_instance == null)
        {
            LoadInstance();
        }

        if (Settings.ContainsKey(setting))
            return Settings[setting].Value;
        else
            Debug.LogError("VR_Settings::Get - tried to get nonexisting setting: " + setting);

        return false;
    }

    public static void Set(string setting, bool value)
    {
        if (_instance == null)
        {
            LoadInstance();
        }

        if (!Settings.ContainsKey(setting))
            Debug.LogError("VR_Settings::Set - tried to set nonexisting setting");

        Settings[setting].SetValue(value);
        if (OnSettingChanged != null)
            OnSettingChanged();

        Save(_instance);
    }

    public static void LinkToggle(string setting, Toggle toggle)
    {
        toggle.isOn = Get(setting);
        toggle.onValueChanged.AddListener((value) => Set(setting, value));
    }

    public static void SubscribeToSetting(string setting, VR_Setting.ValueChanged action)
    {
        if(_instance == null)
        {
            _instance = new VR_Settings();
            Load();
            //_instance.LoadPool();
        }

        if(!Settings.ContainsKey(setting))
        {
            _instance._subscriptions.Add(setting, new List<VR_Setting.ValueChanged>() { action });
            Debug.LogError("VR_Settings::SubscribeToSetting - Tried to subscribe to a nonexisting setting");
            return;
        }

        Settings[setting].OnValueChanged -= action;
        Settings[setting].OnValueChanged += action;
    }

    static void LoadSubscriptions()
    {
        foreach (var subscription in _instance._subscriptions)
        {
            if (Settings.ContainsKey(subscription.Key))
            {
                foreach(var action in subscription.Value)
                {
                    Settings[subscription.Key].OnValueChanged -= action;
                    Settings[subscription.Key].OnValueChanged += action;
                }
            }
        }
    }
}
