﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerInput : MonoBehaviour {

    CustomPointer _pointer;


    ViveController _controller;
    void Start()
    {
        _pointer = GetComponent<CustomPointer>();
        _controller = ViveHelper.Controllers[_pointer.ControllerIndex];
    }

	// Update is called once per frame
	void Update () {
        if (_controller != null)
        {
            var device = _controller.Device;
            var pressed = device.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad);
            var touchPosition = device.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad);
            if (pressed)
            {
                
                if ( touchPosition.sqrMagnitude < 0.7f * 0.7f)
                {
                    _pointer.Toggle();
                }
                else if (Mathf.Abs(touchPosition.y ) < 0.7f && touchPosition.x > 0.7f)
                {
                    ObjectStateRecorder.Instance.Redo();
                }
                else if (Mathf.Abs(touchPosition.y) < 0.7f &&  touchPosition.x < -0.7f)
                {
                    ObjectStateRecorder.Instance.Undo();

                }
            }

            if(device.index == SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Leftmost) && device.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
            {
                if (!ObjectStateRecorder.Instance.IsRecording())
                {
                    ObjectStateRecorder.Instance.StartRecording();
                }
                else
                {
                    ObjectStateRecorder.Instance.StopRecording();
                }
            }

            if (device.index == SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Rightmost) && device.GetPressDown(SteamVR_Controller.ButtonMask.Grip ))
            {
                ObjectStateRecorder.Instance.PlaybackRecording();
            }

        }
    }
}
