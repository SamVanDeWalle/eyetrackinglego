﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VR_PlayArea : MonoBehaviour, IResettable
{
    public float MoveSpeed = 3;
    Vector3 _startPosition;
	
    void Awake()
    {
        _startPosition = transform.position;
        RegisterReset();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Reset();
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(-MoveSpeed * Time.deltaTime, 0, 0);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(MoveSpeed * Time.deltaTime, 0, 0);
        }
        else if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(0, 0, MoveSpeed * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(0, 0, -MoveSpeed * Time.deltaTime);
        }
    }

    public void RegisterReset()
    {
        ResetManager.Register(this);
    }

    public void Reset()
    {
        transform.position = _startPosition;
    }
}
