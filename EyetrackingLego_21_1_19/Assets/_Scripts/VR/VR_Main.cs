﻿using Helpers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VR_Main : Singleton<VR_Main>
{
    public MainMenu _mainMenu;
    public List<PickupController> PickupControllers = new List<PickupController>();
    public static string AssemblyName = "Lego_001"; //AssemblyName is set through TrainingManager, a hack which doesn't seem to work out since this variable is being called before getting set
    public static bool OverrideControllerTexture { get { return _overrideControllerTexture; } private set { } }

    static bool _overrideControllerTexture = true;

    protected override void Awake()
    {
        base.Awake();

        //DontDestroyOnLoad(gameObject);

        SceneManager.activeSceneChanged += SceneManager_activeSceneChanged;
        //PickupController.OnPickedUpGlobal += CheckEnablePicking;
        //PickupController.OnPlacedGlobal += CheckEnablePicking;
    }

    void Start()
    {
        //Temp controller texture fix
        if (UnityEngine.XR.XRDevice.model.Contains("Acer") || UnityEngine.XR.XRDevice.model.Contains("Lenovo"))
        {
            _overrideControllerTexture = false;
        }

        Debug.Log("XR Device: " + UnityEngine.XR.XRDevice.model);
    }

    void Update()
    {
        //CheckEnablePicking();
    }

    private static void SceneManager_activeSceneChanged(Scene arg0, Scene arg1)
    {
        if (arg1.name.Contains("VR_Training_Scene"))
        {
            EnablePickupControllers();
            Instance._mainMenu.gameObject.SetActive(false);
        }
        else if(arg1.name.Contains("VR_Menu_Scene"))
        {
            EnablePickupControllers(false);
            Instance._mainMenu.gameObject.SetActive(true);
        }
        else
        {
            EnablePickupControllers();
            Instance._mainMenu.gameObject.SetActive(false);
        }
    }

    static void EnablePickupControllers(bool active = true)
    {
        foreach (var controller in Instance.PickupControllers)
        {
            controller.gameObject.SetActive(active);
        }
    }

    void CheckEnablePicking()
    {
        if(VR_Settings.Get("Single Controller"))
        {
            for (int i = 0; i < PickupControllers.Count; ++i)
            {
                PickupControllers[i].AllowPicking = true;
                if (PickupControllers[i].CarriesObject)
                {
                    EnablePickingOnly(i);
                    break;
                }
            }
        }
    }

    void EnablePickingOnly(int index)
    {
        for (int i = 0; i < Instance.PickupControllers.Count; ++i)
        {
            if (i != index)
            {
                Instance.PickupControllers[i].AllowPicking = false;
            }
            else
            {
                Instance.PickupControllers[i].AllowPicking = true;
            }
        }
    }

    public static void LoadTutorial()
    {
        SteamVR_LoadLevel.Begin("VR_Tutorial_Scene");
    }
}
