﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PullHandle : MonoBehaviour
{
    public ViveController Controller { get { return _controller; } private set { } }
    public Material HoverMaterial;

    Renderer _renderer;
    Material _startMaterial;
    ViveController _controller;
    bool _bHovering;
    bool _bDragging;

    public delegate void HoverDelegate(PullHandle handle);
    public event HoverDelegate OnHover;
    public event HoverDelegate OnLeave;

    void Awake()
    {
        _renderer = GetComponent<Renderer>();
        _startMaterial = _renderer.material;
    }

    void Hover()
    {
        _bHovering = true;
        _renderer.sharedMaterial = HoverMaterial;
        var pickupController = PickupManager.GetPickupController(_controller);
        pickupController.HideVisuals();
        pickupController.enabled = false;

        if (OnHover != null)
            OnHover(this);
    }

    void Leave()
    {
        _bHovering = false;
        _renderer.sharedMaterial = _startMaterial;

        if (OnLeave != null)
            OnLeave(this);
    }

    void Reset()
    {
        _renderer.sharedMaterial = _startMaterial;
    }

    void OnTriggerEnter(Collider other)
    {
        var controller = ViveHelper.GetController(other.transform);
        if (controller != null)
        {
            _controller = controller;
            Hover();
        }
    }

    void OnTriggerExit(Collider other)
    {
        var controller = ViveHelper.GetController(other.transform);
        if (controller != null)
        {
            Leave();
        }
    }
}
