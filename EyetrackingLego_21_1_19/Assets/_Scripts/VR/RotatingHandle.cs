﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class RotatingHandle : MonoBehaviour
{
    public Renderer Renderer;
    public Material HoverMaterial;
    public float PositionOffsetMax;
    public float RotationMax = 30;
    public float Manipulation { get { return _manipulation; } private set { } }
    public bool IsManipulating { get { return _bDragging; } private set { } }

    public Transform RotatingPart;
    ViveController _controller;
    Material _startMaterial;
    Vector3 _dragStartPosition;
    Quaternion _startRotation;
    float _manipulation;
    bool _bHovering;
    bool _bDragging;

    void Awake()
    {
        _startMaterial = Renderer.material;
        _startRotation = RotatingPart.rotation;
        //_rotatable = GetComponent<Rotatable>();
    }

    void Update()
    {
        if (_bHovering && !_bDragging && _controller.GetPressDown(ControllerButton.Trigger))
        {
            StartDragging();
        }

        if (_bDragging)
        {
            if (!_controller.GetPress(ControllerButton.Trigger))
            {
                StopDragging();
            }
            else
            {
                Drag();
            }
        }
    }

    void StartDragging()
    {
        _dragStartPosition = _controller.Transform.position;
        //_rotatable.StartRotating(_controller.Transform);
        _bDragging = true;
    }

    void Drag()
    {
        //var positionOffset = _controller.Transform.position - _dragStartPosition;
        //_manipulation = Mathf.Clamp(positionOffset.x / PositionOffsetMax, -1, 1);
        //transform.rotation = _startRotation;
        //transform.Rotate(_manipulation * RotationMax, 0, 0);

        var startAim = _dragStartPosition - transform.position;
        startAim = Vector3.ProjectOnPlane(startAim, Vector3.forward);
        var aim = _controller.Transform.position - transform.position;
        aim = Vector3.ProjectOnPlane(aim, Vector3.forward);
        var angle = Vector3.Angle(startAim, aim);
        angle = (int)(angle / 5.0f) * 5;
        var axis = Vector3.Cross(startAim, aim);
        //axis = new Vector3(-axis.z, 0, 0);
        //Debug.Log("axis: " + axis);
        RotatingPart.rotation = _startRotation;
        RotatingPart.Rotate(axis, angle);

        _controller.Pulse();
    }

    void StopDragging()
    {
        //_rotatable.StopRotating();
        //transform.rotation = _startRotation;
        _startRotation = RotatingPart.rotation;
        _bDragging = false;

        if (!_bHovering)
        {
            Reset();
        }
    }

    void Hover()
    {
        _bHovering = true;
        Renderer.sharedMaterial = HoverMaterial;
        var pickupController = PickupManager.GetPickupController(_controller);
        pickupController.HideVisuals();
        pickupController.enabled = false;
    }

    void Leave()
    {
        _bHovering = false;

        if (!_bDragging)
        {
            Reset();
        }
    }

    void Reset()
    {
        var pickupController = PickupManager.GetPickupController(_controller);
        if(pickupController != null)
            pickupController.enabled = true;
        Renderer.sharedMaterial = _startMaterial;
        _controller = null;
    }

    void OnTriggerEnter(Collider other)
    {
        var controller = ViveHelper.GetController(other.transform);
        if (controller != null)
        {
            _controller = controller;
            Hover();
        }
    }

    void OnTriggerExit(Collider other)
    {
        var controller = ViveHelper.GetController(other.transform);
        if (controller != null)
        {
            Leave();
        }
    }
}
