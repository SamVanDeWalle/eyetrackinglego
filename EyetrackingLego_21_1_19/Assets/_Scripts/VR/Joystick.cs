﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Joystick : MonoBehaviour
{
    public Renderer Renderer;
    public Material HoverMaterial;
    public float PositionOffsetMax;
    public float RotationMax = 30;
    public float Manipulation { get { return _manipulation; } private set { } }
    public bool IsManipulating { get { return _bDragging; } private set { } }

    ViveController _controller;
    Material _startMaterial;
    Vector3 _dragStartPosition;
    Quaternion _startRotation;
    float _manipulation;
    bool _bHovering;
    bool _bDragging;

    void Awake()
    {
        _startMaterial = Renderer.material;
        _startRotation = transform.rotation;
    }

    void Update()
    {
        if(_bHovering && !_bDragging && _controller.GetPressDown(ControllerButton.Trigger))
        {
            StartDragging();
        }

        if (_bDragging)
        {
            if (!_controller.GetPress(ControllerButton.Trigger))
            {
                StopDragging();
            }
            else
            {
                Drag();
            }
        }
    }

    void StartDragging()
    {
        _dragStartPosition = _controller.Transform.position;
        _bDragging = true;
    }

    void Drag()
    {
        var positionOffset = _controller.Transform.position - _dragStartPosition;
        _manipulation = Mathf.Clamp(positionOffset.x / PositionOffsetMax, -1, 1);
        transform.rotation = _startRotation;
        transform.Rotate(_manipulation * RotationMax, 0, 0);

        _controller.Pulse();
    }

    void StopDragging()
    {
        transform.rotation = _startRotation;
        _bDragging = false;

        if (!_bHovering)
        {
            Reset();
        }
    }

    void Hover()
    {
        _bHovering = true;
        Renderer.sharedMaterial = HoverMaterial;
        var pickupController = PickupManager.GetPickupController(_controller);
        pickupController.HideVisuals();
        pickupController.enabled = false;
    }

    void Leave()
    {
        _bHovering = false;

        if (!_bDragging)
        {
            Reset();
        }
    }

    void Reset()
    {
        PickupManager.GetPickupController(_controller).enabled = true;
        Renderer.sharedMaterial = _startMaterial;
        _controller = null;
    }

    void OnTriggerEnter(Collider other)
    {
        var controller = ViveHelper.GetController(other.transform);
        if(controller != null)
        {
            _controller = controller;
            Hover();
        }
    }

    void OnTriggerExit(Collider other)
    {
        var controller = ViveHelper.GetController(other.transform);
        if (controller != null)
        {
            Leave();
        }
    }
}
