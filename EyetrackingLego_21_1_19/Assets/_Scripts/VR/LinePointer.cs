﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LinePointer : MonoBehaviour
{
    public ControllerIndex ControllerIndex;
    public Material LineMaterial;
    public GameObject CollisionCanvas;
    public GameObject Line;
    public Image ChargeImage;

    ViveController _controller;
    Button _selectedButton;
    Toggle _selectedToggle;
    Selectable _selectable;
    float _lineZ = 3.937f;
    float _chargeSpeed = 1.4f;
    bool _bAttached;

    void Start ()
    {
        _controller = ViveHelper.Controllers[ControllerIndex];
        Attach();
        _controller.OnModelLoaded += Attach;
        ViveHelper.OnMaterialsUpdated += TryAttach;
        //DontDestroyOnLoad(gameObject);
	}

    void OnDestroy()
    {
        ViveHelper.OnMaterialsUpdated -= TryAttach;
    }

    void OnEnable()
    {
        //_controller = ViveHelper.Controllers[ControllerIndex];
        //_controller.OnModelLoaded += Attach;
        //Attach();
    }

    void TryAttach()
    {
        _controller = ViveHelper.Controllers[ControllerIndex];
        Attach();
    }

    void Attach()
    {
        Debug.Log("trying to attach: " + _controller);
        transform.SetParent(_controller.Transform);
        transform.position = _controller.Transform.position;
        transform.up = ViveHelper.Head.position - _controller.Transform.position;
        transform.forward = _controller.Transform.forward;

        GetComponentInChildren<MeshRenderer>().sharedMaterial = LineMaterial;
        _bAttached = true;
    }

    void Update()
    {
        if (!_bAttached)
            return;

        // Update line
        transform.up = ViveHelper.Head.position - _controller.Transform.position;
        transform.forward = _controller.Transform.forward;

        // Check collision
        var ray = new Ray(transform.position, transform.forward);
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo, 100f, 1 << LayerMask.NameToLayer("PointerCollision")))
        {
            CollisionCanvas.SetActive(true);
            CollisionCanvas.transform.position = hitInfo.point;
            CollisionCanvas.transform.forward = hitInfo.collider.transform.forward;
            var lineScale = Line.transform.localScale;
            lineScale.z = hitInfo.distance * _lineZ;
            Line.transform.localScale = lineScale;

            var button = hitInfo.collider.gameObject.GetComponent<Button>();
            var toggle = hitInfo.collider.gameObject.GetComponent<Toggle>();
            if (button)
            {
                button.Select();
                if (button != _selectable)
                    ResetCharge();
                _selectedButton = button;
                _selectable = button;
            }
            else if (toggle)
            {
                toggle.Select();
                if (toggle != _selectable)
                    ResetCharge();
                _selectedToggle = toggle;
                _selectable = toggle;
            }
            else
            {
                ResetSelection();
                ResetCharge();
            }
        }
        else
        {
            CollisionCanvas.SetActive(false);
            var lineScale = Line.transform.localScale;
            lineScale.z = _lineZ * 20;
            Line.transform.localScale = lineScale;
            EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(null);
            _selectedButton = null;
        }

        if(_selectedButton && _controller.GetPress(ControllerButton.Trigger))
        {
            ChargeUp();
        }
        else
        {
            UnCharge();
        }

        if(_selectedToggle && _controller.GetPressDown(ControllerButton.Trigger))
        {
            _selectedToggle.isOn = !_selectedToggle.isOn;
        }
    }

    void ResetSelection()
    {
        EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(null);
        _selectedButton = null;
        _selectedToggle = null;
        _selectable = null;
    }

    void ChargeUp()
    {
        var fill = ChargeImage.fillAmount;
        if (fill >= 1.0f)
        {
            _selectedButton.onClick.Invoke();
            fill = 0.0f;
        }
        else
        {
            fill += _chargeSpeed * Time.deltaTime;
        }

        ChargeImage.fillAmount = fill;
    }

    void UnCharge()
    {
        var fill = ChargeImage.fillAmount;
        if (fill > 0.0f)
        {
            fill -= _chargeSpeed * Time.deltaTime;
        }
        else
        {
            fill = 0.0f;
        }

        ChargeImage.fillAmount = fill;
    }

    void ResetCharge()
    {
        ChargeImage.fillAmount = 0.0f;
    }
}
