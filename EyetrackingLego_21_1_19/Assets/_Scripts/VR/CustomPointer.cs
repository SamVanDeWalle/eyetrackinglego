﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(SteamVR_LaserPointer))]
public class CustomPointer : MonoBehaviour
{
    public Material HighlightMaterial;
    public AssemblyPiece Target { get { return _target; } private set { } }
    public GameObject IndicatorPrefab;
    public float IndicatorDistance = 5.0f;
    public ControllerIndex ControllerIndex;
    public Vector3 IndicatorOffset;
    public bool HasController = true;

    bool _bActive;
    NetworkObject _networkObject;
    SteamVR_LaserPointer _laserPointer;
    ViveController _controller;
    AssemblyPiece _target;
    List<CustomPointer> _otherPointers = new List<CustomPointer>();
    GameObject _indicator;

    void Start()
    {
        _laserPointer = GetComponent<SteamVR_LaserPointer>();
        if(HasController)
            _controller = ViveHelper.Controllers[ControllerIndex];
        if ( _controller == null )
            HasController = false;
        SetActive(false);
        _laserPointer.PointerIn += OverTarget;
        _laserPointer.PointerOut += LeaveTarget;

        var cps = FindObjectsOfType<CustomPointer>();
        for (int i = 0; i < cps.Length; i++)
        {
            if (cps[i] != this)
                _otherPointers.Add(cps[i]);
        }

        if(IndicatorPrefab && Main.Instance.UserType != UserType.Spectator)
        {
            _indicator = Instantiate(IndicatorPrefab);
            _indicator.transform.SetParent(ViveHelper.Head, true);
            _indicator.transform.position = ViveHelper.Head.position + ViveHelper.Head.forward * IndicatorDistance + IndicatorOffset;
            //_indicator.SetActive(false);
        }

        _networkObject = GetComponent<NetworkObject>();

        HUDMenu.SubscribeToAction("pointer", Toggle);
    }

    void Update()
    {
        if ( _controller == null )
            return;

        if(_indicator)
        {
            if (_target)
            {
                _indicator.transform.forward = _target.transform.position - _indicator.transform.position;
            }

            _indicator.SetActive(_target);
        }
    }

    public void Toggle()
    {
        SetActive(!_bActive);
        if(_networkObject != null)
            _networkObject.CatchAction(NetworkActions.CustomPointer_Toggle);
    }

    void SetActive(bool active)
    {
        _bActive = active;
        _laserPointer.active = active;
        _laserPointer.pointer.SetActive(active);
        _laserPointer.enabled = active;

        if (!active)
            LeaveTarget();
    }

    void OverTarget(object o, PointerEventArgs e)
    {
        _target = e.target.GetComponent<AssemblyPiece>();
        if (!_target)
            return;
        _laserPointer.color = Color.cyan;
        _target.SetMaterial(HighlightMaterial, false);
    }

    public void LeaveTarget(object o, PointerEventArgs e)
    {
        _laserPointer.ResetColor();
        LeaveTarget();
    }

    public void LeaveTarget()
    {
        if (!_target)
            return;

        _target.UnsetMaterial(HighlightMaterial, false);
        _target = null;
    }
}
