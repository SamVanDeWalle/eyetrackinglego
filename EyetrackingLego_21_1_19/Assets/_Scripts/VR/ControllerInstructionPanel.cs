﻿using UnityEngine;
using System.Collections;

public class ControllerInstructionPanel : MonoBehaviour
{
    void Start()
    {
        gameObject.GetComponent<Canvas>().enabled = false;
    }

    void Update()
    {
        //foreach(ViveController controller in ViveHelper.Controllers.Values)
        //{
        //    if (controller.GetPress(ControllerButton.TouchPad_Left))
        //        gameObject.GetComponent<Canvas>().enabled = false;
        //}

        if (Input.GetKeyDown(KeyCode.I))
            gameObject.GetComponent<Canvas>().enabled = !gameObject.GetComponent<Canvas>().enabled;
    }
}
