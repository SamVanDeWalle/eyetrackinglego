﻿using UnityEngine;
using System.Collections;

public class ControllerTooltip : MonoBehaviour
{
    public static bool Active = true;
    public Transform Parent;

    void Start()
    {
        transform.SetParent(Parent);
        transform.position = transform.parent.position;
        transform.localScale = Vector3.one;
    }

    void Update()
    {
        transform.position = transform.parent.position;
        transform.localScale = Vector3.one;
    }
}
