﻿/*
 * 
 * This is a wrapper to make it easier and shorter to access the functionality of the vive controllers
 * 
 * Controllers can be accessed in the following way:
 *      var controller = ViveHelper.Controllers[ControllerIndex.Left]
 *      
 * Input can be checked like this:
 *      var triggerPressed = controller.GetPressDown(ControllerButton.Trigger);
 *      
 * When given a custom material to the vivehelper, the controllers will get this material when loaded
 * 
 */

using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Collections;
using Helpers;

public enum ControllerButton
{
    TouchPad,
    Menu,
    Trigger,
    Grip,
    System,
}

[Serializable]
public enum ViveButtonState
{
    PressDown,
    PressUp,
    Press,
    TouchDown,
    TouchUp,
    Touch,
    None
}

public enum ControllerIndex
{
    Left,
    Right,
}

[Serializable]
public class ViveController
{
    public ControllerIndex Index;
    public SteamVR_TrackedObject TrackedObject;
    public SteamVR_TrackedController TrackedController;
    public Transform Transform;
    public GameObject Model;
    public SteamVR_Controller.Device Device
    {
        get { return SteamVR_Controller.Input((int)TrackedObject.index); }
        private set { }
    }

    static Dictionary<ControllerButton, ulong> _buttonMasks = new Dictionary<ControllerButton, ulong>()
    {
        { ControllerButton.Menu, SteamVR_Controller.ButtonMask.ApplicationMenu },
        { ControllerButton.Trigger, SteamVR_Controller.ButtonMask.Trigger },
        { ControllerButton.Grip, SteamVR_Controller.ButtonMask.Grip },
        { ControllerButton.TouchPad, SteamVR_Controller.ButtonMask.Touchpad },
        { ControllerButton.System, SteamVR_Controller.ButtonMask.System }
    };

    Dictionary<ControllerButton, ViveButtonState> _simulationStates = new Dictionary<ControllerButton, ViveButtonState>()
    {
        { ControllerButton.Trigger, ViveButtonState.None },
        { ControllerButton.TouchPad, ViveButtonState.None },
        { ControllerButton.Menu, ViveButtonState.None },
        { ControllerButton.Grip, ViveButtonState.None },
    };

    public delegate void LoadedDelegate();
    public event LoadedDelegate OnModelLoaded;

    public void ModelLoaded()
    {
        if (OnModelLoaded != null)
            OnModelLoaded();

        Debug.Log("Model loaded: " + Transform);
    }

    public void SetSimulationState(ControllerButton btn, ViveButtonState state)
    {
        if (_simulationStates.ContainsKey(btn))
            _simulationStates[btn] = state;
    }

    public Vector2 GetTouchPadPosition()
    {
        return TrackedObject != null ? Device.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad) : new Vector2();
    }

    public bool GetPress(ControllerButton btn)
    {
        return (TrackedObject != null && Device.GetPress(_buttonMasks[btn])) || _simulationStates[btn] == ViveButtonState.Press;
    }

    public bool GetPressDown(ControllerButton btn)
    {
        return (TrackedObject != null && Device.GetPressDown(_buttonMasks[btn])) || _simulationStates[btn] == ViveButtonState.PressDown;
    }

    public bool GetPressUp(ControllerButton btn)
    {
        return (TrackedObject != null && Device.GetPressUp(_buttonMasks[btn])) || _simulationStates[btn] == ViveButtonState.PressUp;
    }

    public bool GetTouch()
    {
        return (TrackedObject != null && Device.GetTouch(SteamVR_Controller.ButtonMask.Touchpad)) || _simulationStates[ControllerButton.TouchPad] == ViveButtonState.Touch;
    }

    public bool GetTouch(ControllerButton btn)
    {
        if (btn == ControllerButton.TouchPad)
            return GetTouch();
        else
            return false;
    }

    public bool GetTouchDown()
    {
        return (TrackedObject != null && Device.GetTouchDown(SteamVR_Controller.ButtonMask.Touchpad)) || _simulationStates[ControllerButton.TouchPad] == ViveButtonState.TouchDown;
    }

    public bool GetTouchDown(ControllerButton btn)
    {
        if (btn == ControllerButton.TouchPad)
            return GetTouchDown();
        else
            return false;
    }

    public bool GetTouchUp()
    {
        return (TrackedObject != null && Device.GetTouchUp(SteamVR_Controller.ButtonMask.Touchpad)) || _simulationStates[ControllerButton.TouchPad] == ViveButtonState.TouchUp;
    }

    public bool GetTouchUp(ControllerButton btn)
    {
        if (btn == ControllerButton.TouchPad)
            return GetTouchUp();
        else
            return false;
    }

    public void Pulse()
    {
        if (TrackedObject != null)
            Device.TriggerHapticPulse();
    }
}

public class ViveHelper : Singleton<ViveHelper>
{
    public SteamVR_TrackedObject[] ViveControllers;
    public static Dictionary<ControllerIndex, ViveController> Controllers = new Dictionary<ControllerIndex, ViveController>()
    {
        { ControllerIndex.Left, null },
        { ControllerIndex.Right, null }
    };
    //public static Transform Head { get { return SteamVR_Render.Top().head; } private set { } }
    public static Transform Head
    {
        get
        {
            if (_head != null)
            {
                return _head;
            }
            else
            {
                _head = FindObjectOfType<SteamVR_Camera>().transform;
                return _head;
            }
        }
        private set { }
    }
    public Material CustomMaterial;

    public delegate void HelperAction();
    public static event HelperAction OnControllersUpdated;
    public static event Action OnMaterialsUpdated;

    static Transform _head;

    void Start()
    {
        Load();
    }

    void Load()
    {
        Controllers.Clear();
        var controllers = FindObjectsOfType<SteamVR_TrackedObject>();
        int index = 0;
        foreach (var controller in ViveControllers)
        {
            var viveController = new ViveController();
            viveController.Index = (ControllerIndex)index;
            viveController.Transform = controller.gameObject.transform;
            Debug.Log("t:" + viveController.Transform);
            viveController.Model = controller.gameObject.GetComponentInChildren<SteamVR_RenderModel>().gameObject;
            viveController.TrackedObject = controller;
            viveController.TrackedController = controller.GetComponent<SteamVR_TrackedController>();
            Controllers.Add(viveController.Index, viveController);

            // Listen to the SteamVR event that tells when a (real) controller is loaded
            //SteamVR_Utils.Event.Listen("render_model_loaded", SteamVRModelLoaded);
            SteamVR_Events.RenderModelLoaded.Listen(SteamVRModelLoaded);
            ++index;
        }

        UpdateMaterials();
    }

    public static void SetViveController(ControllerIndex index, ViveController controller)
    {
        if (!Controllers.ContainsKey(index))
            Controllers.Add(index, controller);
        else
            Controllers[index] = controller;
    }

    void SteamVRModelLoaded(SteamVR_RenderModel renderModel, bool success)
    {
        if (!success)
            return;

        UpdateMaterials();

        var rm = renderModel;
        var controller = Controllers.Values.FirstOrDefault(c => c.Transform == rm.transform.parent);
        if (controller == null)
        {
            Debug.Log("ViveHelper::SteamVRModelLoaded - could not find loaded controller, reloading Vivehelper");
            Load();
            controller = Controllers.Values.FirstOrDefault(c => c.Transform == rm.transform.parent);
        }

        if (controller != null)
        {
            controller.ModelLoaded();
            if (OnControllersUpdated != null)
                OnControllersUpdated();
        }
    }

    void SteamVRModelLoaded(params object[] args)
    {
        UpdateMaterials();

        var rm = (SteamVR_RenderModel)args[0];
        var controller = Controllers.Values.FirstOrDefault(c => c.Transform == rm.transform.parent);
        if (controller == null)
        {
            Debug.Log("ViveHelper::SteamVRModelLoaded - could not find loaded controller, reloading Vivehelper");
            Load();
            controller = Controllers.Values.FirstOrDefault(c => c.Transform == rm.transform.parent);
        }

        if (controller != null)
        {
            controller.ModelLoaded();
            if (OnControllersUpdated != null)
                OnControllersUpdated();
        }
    }

    void UpdateMaterials()
    {
        if (!VR_Main.OverrideControllerTexture || CustomMaterial == null)
            return;

        foreach (var controller in Controllers)
        {
            foreach (Transform tf in controller.Value.Transform.gameObject.GetComponentsInChildren<Transform>())
            {
                if (tf.name.Contains("Beam") || tf.name.Contains("Cube") || tf.name.Contains("Preview"))
                    continue;
                var rend = tf.GetComponent<Renderer>();
                if (rend != null)
                {
                    rend.sharedMaterial = CustomMaterial;
                }
            }
        }

        if (OnMaterialsUpdated != null)
            OnMaterialsUpdated();
    }

    public static void PulseAllControllers()
    {
        foreach (var controller in Controllers)
        {
            controller.Value.Pulse();
        }
    }

    public static void PulseAllControllers(float duration)
    {
        foreach (var controller in Controllers)
        {
            PulseController(controller.Key, duration);
        }
    }

    public static void PulseController(ControllerIndex controller, float duration)
    {
        Instance.StartCoroutine(PulseControllerRoutine(controller, duration));
    }

    static IEnumerator PulseControllerRoutine(ControllerIndex controller, float duration)
    {
        var time = duration;
        while (time > 0)
        {
            Controllers[controller].Pulse();
            time -= Time.deltaTime;
            yield return null;
        }
    }

    public static void PulseController(ViveController controller, float duration)
    {
        Instance.StartCoroutine(PulseControllerRoutine(controller, duration));
    }

    static IEnumerator PulseControllerRoutine(ViveController controller, float duration)
    {
        var time = duration;
        while (time > 0)
        {
            controller.Pulse();
            time -= Time.deltaTime;
            yield return null;
        }
    }

    public static ViveController GetController(Transform transform)
    {
        return Controllers.FirstOrDefault(c => c.Value.Transform == transform).Value;
    }
}
