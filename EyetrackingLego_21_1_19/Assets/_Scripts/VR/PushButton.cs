﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushButton : MonoBehaviour
{
    public Material PressMaterial;
    public Vector3 PressOffset;
    public bool IsPressed { get { return _bPressed; } private set { } }

    Renderer _renderer;
    Material _startMaterial;
    bool _bPressed;

    public delegate void Pressed();
    public event Pressed OnPressed;
    public static event Pressed OnPressedGlobal;

    void Awake()
    {
        _renderer = GetComponent<Renderer>();
        _startMaterial = _renderer.sharedMaterial;
    }

    void Press()
    {
        if (_bPressed)
            return;

        _bPressed = true;
        _renderer.sharedMaterial = PressMaterial;
        transform.localPosition += PressOffset;

        if (OnPressed != null)
            OnPressed();

        if (OnPressedGlobal != null)
            OnPressedGlobal();
    }

    void UnPress()
    {
        if (!_bPressed)
            return;

        _bPressed = false;
        _renderer.sharedMaterial = _startMaterial;
        transform.localPosition -= PressOffset;
    }

    public void SetMaterial(Material material)
    {
        if(!_bPressed)
            _renderer.sharedMaterial = material;
    }

    public void ResetMaterial()
    {
        if (!_bPressed)
            _renderer.sharedMaterial = _startMaterial;
    }

    void OnTriggerEnter(Collider other)
    {
        Press();
        Debug.Log("HIT" + other.gameObject);
    }

    void OnTriggerExit(Collider other)
    {
        UnPress();
    }
}
