﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HUDButton : MonoBehaviour, ISelectHandler, IDeselectHandler
{
    public string Name;
    public AxisPosition Horizontal;
    public AxisPosition Vertical;
    public delegate void ActionDelegate();
    public event ActionDelegate OnAction;
    public event ActionDelegate OnEnter;
    public event ActionDelegate OnLeave;
    public event ActionDelegate OnPress;
    public event ActionDelegate OnPressUp;
    public bool WorkInvisible = false;

    Button _button;
    bool _bHidden = false;

    void Awake()
    {
        OnAction = null;
        OnEnter = null;
        OnLeave = null;
        _button = GetComponent<Button>();
    }

    public void OnSelect(BaseEventData data)
    {
        var action = OnEnter;
        if (action != null)
            action();
    }

    public void OnDeselect(BaseEventData data)
    {
        var action = OnLeave;
        if (action != null)
            action();
    }

    public void Hover()
    {
        if (_bHidden)
            return;

        _button.Select();
    }

    public void Action()
    {
        if (!WorkInvisible && _bHidden)
            return;

        if (OnAction != null)
            OnAction();
    }

    public void Press()
    {
        if (!WorkInvisible && _bHidden)
            return;

        if(OnPress != null)
            OnPress();
    }

    public void PressUp()
    {
        if (!WorkInvisible && _bHidden)
            return;

        OnPressUp();
    }

    public void Hide()
    {
        _bHidden = true;
        _button.GetComponentInChildren<CanvasRenderer>().SetAlpha(0);
        _button.GetComponentInChildren<Text>().color = Color.clear;
        foreach(var img in GetComponentsInChildren<Image>())
        {
            if (img.gameObject != gameObject)
                img.enabled = false;
        }

        _button.interactable = false;
    }

    public void Show(bool show = true)
    {
        if(!show)
        {
            Hide();
            return;
        }

        _bHidden = false;
        _button.GetComponentInChildren<CanvasRenderer>().SetAlpha(1);
        _button.GetComponentInChildren<Text>().color = Color.white;
        foreach (var img in GetComponentsInChildren<Image>())
        {
            if(img.gameObject != gameObject)
                img.enabled = true;
        }

        _button.interactable = true;
    }
}
