﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public enum AxisPosition
{
    Left,
    Mid,
    Right,
    Bottom,
    Top
}

public enum ButtonActionType
{
    Activate,
    Enter,
    Leave,
    Press,
    PressUp
}

[Serializable]
public class ModeMenu
{
    public TrainingMode TrainingMode;
    public GameObject Menu;
}

public class HUDMenu : MonoBehaviour
{
    public List<GameObject> ExtraVisuals = new List<GameObject>();
    public List<ModeMenu> Menus = new List<ModeMenu>();
    public ControllerIndex ControllerIndex;
    public static ViveController Controller { get { return _controller; } private set { } }
    public bool ToggleWithMenuButton = true;
    public bool AllIsTeleportWhenOff = true;
    public bool IsEnabled = true;

    static ViveController _controller;
    static Dictionary<AxisPosition, Dictionary<AxisPosition, HUDButton>> _buttons = new Dictionary<AxisPosition, Dictionary<AxisPosition, HUDButton>>();
    static List<HUDButton> _buttonsUnordered = new List<HUDButton>();
    bool _bTouching = false;
    bool _bMenuOpen = false;
    HUDButton _lastHoveredButton;
    static EventSystem _eventSystem;
    
    void Awake()
    {
        LoadButtons();
    }

    void Start()
    {
        _controller = ViveHelper.Controllers[ControllerIndex];
        AttachToController(_controller);

        //LoadButtons();
        ShowButtons(_bMenuOpen);

        // Make all buttons toggle the menu on action
        //foreach(var btn in _buttonsUnordered)
        //{
        //    SubscribeToAction(btn, ToggleMenu);
        //}

        _eventSystem = GameObject.Find("EventSystem").GetComponent<EventSystem>();

        VR_Settings.OnSettingChanged += CheckEnabled;
        CheckEnabled();
        ShowMenus();
    }

    void ShowMenus()
    {
        foreach(var menu in Menus)
        {
            if (menu.TrainingMode == TrainingManager.TrainingMode)
                menu.Menu.SetActive(true);
            else
                menu.Menu.SetActive(false);
        }
    }

    public void CheckEnabled()
    {
        IsEnabled = VR_Settings.Get("Enable Controller Menu");
        if (!IsEnabled && _bMenuOpen)
            ToggleMenu();
    }

    void AttachToController(ViveController controller)
    {
        transform.SetParent(controller.Transform, false);
        transform.position = controller.Transform.position;
        transform.localRotation = Quaternion.identity;
    }

    void LoadButtons()
    {
        _buttons.Clear();
        _buttonsUnordered.Clear();
        foreach(var btn in GetComponentsInChildren<HUDButton>())
        {
            if (!_buttons.ContainsKey(btn.Horizontal))
            {
                _buttons.Add(btn.Horizontal, new Dictionary<AxisPosition, HUDButton>());
            }

            if (!_buttons[btn.Horizontal].ContainsKey(btn.Vertical))
            {
                _buttons[btn.Horizontal].Add(btn.Vertical, null);
            }

            _buttons[btn.Horizontal][btn.Vertical] = btn;
            _buttonsUnordered.Add(btn);
        }
    }

    void ShowButtons(bool show)
    {
        foreach(var btn in _buttonsUnordered)
        {
            btn.Show(show);
        }

        for(int i = 0; i < ExtraVisuals.Count; ++i)
        {
            ExtraVisuals[i].SetActive(show);
        }
    }

    void ToggleMenu()
    {
        if (!IsEnabled && !_bMenuOpen)
            return;

        UnSelectAll();
        LoadButtons();
        ShowButtons(!_bMenuOpen);

        _bMenuOpen = !_bMenuOpen;
    }

    public void UnSelectAll()
    {
        _eventSystem.SetSelectedGameObject(null);
    }

    void Update()
    {
        if (!IsEnabled || _buttons.Count == 0)
            return;

        foreach (var controller in ViveHelper.Controllers.Values)
        {
            if(ToggleWithMenuButton && controller.GetPressDown(ControllerButton.Menu))
            {
                _controller = controller;
                AttachToController(controller);
                ToggleMenu(); // TEMP DISABLED
                break;
            }

            var cTouchUp = controller.GetTouchUp();
            var cPressed = controller.GetPressDown(ControllerButton.TouchPad);

            if(cPressed || cTouchUp)
            {
                _controller = controller;
                AttachToController(controller);
                break;
            }
        }

        var touched = _controller.GetTouch();
        var touchUp = _controller.GetTouchUp();
        var pressed = _controller.GetPressDown(ControllerButton.TouchPad);
        var pressing = _controller.GetPress(ControllerButton.TouchPad);
        var pressedUp = _controller.GetPressUp(ControllerButton.TouchPad);
        var touchPosition = _controller.GetTouchPadPosition();

        var horizontal = AxisPosition.Mid;
        var vertical = AxisPosition.Top;

        // Use the whole area as mid mid unless the menu is open
        if (_bMenuOpen)
        {
            horizontal = AxisPosition.Mid;
            vertical = AxisPosition.Mid;
            if (touchPosition.x < -0.3f)
                horizontal = AxisPosition.Left;
            else if (touchPosition.x > 0.3f)
                horizontal = AxisPosition.Right;

            if (touchPosition.y < -0.3f)
                vertical = AxisPosition.Bottom;
            else if (touchPosition.y > 0.3f)
                vertical = AxisPosition.Top;
        }

        if (touched)
        {
            if(_buttons[horizontal][vertical] == null)
            {
                Debug.Log("No button: " + horizontal + " " + vertical);
                Debug.Log(_buttonsUnordered.Count);
            }
            _buttons[horizontal][vertical].Hover();
            _lastHoveredButton = _buttons[horizontal][vertical];
            _bTouching = true;
        }

        if (pressed)
        {
            _buttons[horizontal][vertical].Press();
        }

        if (pressedUp/* || touchUp*/ && _lastHoveredButton != null)
        {
            _bTouching = false;
            _lastHoveredButton.Action();
        }
    }

    public static void SubscribeToAction(string btnName, HUDButton.ActionDelegate listener, ButtonActionType type = ButtonActionType.Activate)
    {
        var btn = _buttonsUnordered.FirstOrDefault<HUDButton>(b => b.Name.Equals(btnName));
        SubscribeToAction(btn, listener, type);
    }

    public static void SubscribeToAction(HUDButton btn, HUDButton.ActionDelegate listener, ButtonActionType type = ButtonActionType.Activate)
    {
        if (btn != null)
            SubscribeToAction(btn.Horizontal, btn.Vertical, listener, type);
    }

    public static void SubscribeToAction(AxisPosition horizontal, AxisPosition vertical, HUDButton.ActionDelegate listener, ButtonActionType type = ButtonActionType.Activate)
    {
        switch (type)
        {
            case ButtonActionType.Activate:
                _buttons[horizontal][vertical].OnAction -= listener;
                _buttons[horizontal][vertical].OnAction += listener;
                break;
            case ButtonActionType.Enter:
                _buttons[horizontal][vertical].OnEnter -= listener;
                _buttons[horizontal][vertical].OnEnter += listener;
                break;
            case ButtonActionType.Leave:
                _buttons[horizontal][vertical].OnLeave -= listener;
                _buttons[horizontal][vertical].OnLeave += listener;
                break;
            case ButtonActionType.Press:
                _buttons[horizontal][vertical].OnPress -= listener;
                _buttons[horizontal][vertical].OnPress += listener;
                break;
            case ButtonActionType.PressUp:
                _buttons[horizontal][vertical].OnPressUp -= listener;
                _buttons[horizontal][vertical].OnPressUp += listener;
                break;
        }
    }
}
