﻿using UnityEngine;
using System.Collections.Generic;

public class CustomTeleport : MonoBehaviour
{
    public Renderer BoundsCube;
    public GameObject GhostControllerPrefab, GhostHeadPrefab;
    public Transform FloorAimVisual;
    public Transform FloorAimLine;
    public Transform DomeIndicator;
    public float DashSpeed = 50.0f;
    public float MaxRange = 10.0f;
    public bool IsEnabled = true;

    List<GameObject> _dummyControllers = new List<GameObject>();
    ViveController _currentController;
    GameObject _dummyHead;
    Plane _plane;
    Bounds Boundaries;
    Vector3 _targetHeadPosition;
    Vector3 _requiredMovement;
    Collider BoundsCollider;
    bool _bAiming, _bDashing;

    void Start()
    {
        _plane = new Plane(Vector3.up, 0);
        SetUpGhostDummies();
        if(DomeIndicator != null) DomeIndicator.gameObject.SetActive(false);
        BoundsCollider = BoundsCube.GetComponent<Collider>();
        Boundaries = BoundsCube.bounds;

        // Subscribe to button input
        HUDMenu.SubscribeToAction("teleport", ActivateDash);
        //HUDMenu.SubscribeToAction("teleport", ActivateAiming, ButtonActionType.Enter);
        //HUDMenu.SubscribeToAction("teleport", DeactivateAiming, ButtonActionType.Leave);
        HUDMenu.SubscribeToAction("teleport", ActivateAiming, ButtonActionType.Press);
        //HUDMenu.SubscribeToAction("teleport", DeactivateAiming, ButtonActionType.PressUp);

        //VR_Settings.SubscribeToSetting("Enable Teleport", SetEnabled);
        // TODO fix and replace with subscription
        VR_Settings.OnSettingChanged += CheckEnabled;
        CheckEnabled();

        ActivateVisuals(false);
    }

    public void SetEnabled(bool enabled)
    {
        IsEnabled = enabled;
        _bAiming = false;
        _bDashing = false;
        _requiredMovement = Vector3.zero;
    }

    public void CheckEnabled()
    {
        IsEnabled = VR_Settings.Get("Enable Teleport");
    }


    void Update ()
    {
        if (!IsEnabled)
            return;

        ActivateVisuals(_bAiming);

        if (_bAiming)
            Aim();

        if (_bDashing)
            Dash();
	}

    public void ActivateAiming()
    {
        _currentController = HUDMenu.Controller;
        _bAiming = true;
    }

    public void DeactivateAiming()
    {
        _bAiming = false;
    }

    public void ActivateDash()
    {
        _currentController = HUDMenu.Controller;
        _bAiming = false;
        _bDashing = true;
    }

    void Aim()
    {
        var controllerTransform = _currentController.Transform;
        var ray = new Ray(controllerTransform.position, -controllerTransform.up + controllerTransform.forward);

        float distance = 0.0f;
        bool hasGroundTarget = _plane.Raycast(ray, out distance);

        if (hasGroundTarget)
        {
            var floorPosition = ray.origin + ray.direction * distance;
            if(floorPosition.sqrMagnitude > MaxRange*MaxRange)
            {
                floorPosition = floorPosition.normalized * MaxRange;
            }
            
            if (!Boundaries.Contains(floorPosition))
            {
                RaycastHit hit;
                if (BoundsCollider.Raycast(new Ray(ray.origin + ray.direction*distance, -ray.direction), out hit, distance))
                {
                    floorPosition.x = hit.point.x;
                    floorPosition.z = hit.point.z;
                }    
            }

            //Limit position based on bounds

            var floorAimVisualPosition = floorPosition;
            floorAimVisualPosition.y = FloorAimVisual.position.y;
            FloorAimVisual.position = floorAimVisualPosition;

            _targetHeadPosition = floorPosition;
            _targetHeadPosition.y = SteamVR_Render.Top().origin.position.y;

            _requiredMovement = _targetHeadPosition - SteamVR_Render.Top().head.position;
            _requiredMovement.y = 0;

            var floorAimLinePosition = SteamVR_Render.Top().head.position;
            floorAimLinePosition.y = FloorAimVisual.position.y - 0.01f;
            FloorAimLine.position = floorAimLinePosition;
            FloorAimLine.up = Vector3.up;
            FloorAimLine.forward = _requiredMovement;
            FloorAimLine.Rotate(0, 0, -90);
            var floorAimLineScale = FloorAimLine.localScale;
            floorAimLineScale.z = ((_requiredMovement.magnitude) * 6.8f) - 4.0f;
            FloorAimLine.localScale = floorAimLineScale;

            UpdateGhostDummies();
        }
    }

    void Dash()
    {
        var origin = SteamVR_Render.Top().origin;
        _requiredMovement = _targetHeadPosition - SteamVR_Render.Top().head.position;
        _requiredMovement.y = 0;
        var direction = _requiredMovement.normalized;
        var dashStep = direction * Time.deltaTime * DashSpeed;
        if (dashStep.magnitude > _requiredMovement.magnitude)
        {
            dashStep = _requiredMovement;
            _bDashing = false;
        }

        origin.position += dashStep;
        var distanceFromDome = MaxRange - origin.position.magnitude;

        if (DomeIndicator == null) return;
            //Debug.Log(distanceFromDome);
        if (distanceFromDome < 30.0f)
        {
            DomeIndicator.gameObject.SetActive(true);
            var domePosition = DomeIndicator.position;
            var domeStep = origin.position.normalized * (distanceFromDome);
            domeStep.y = 0.0f;
            var newDomePosition = origin.position + domeStep;
            newDomePosition.y = domePosition.y;

            DomeIndicator.position = newDomePosition;
            DomeIndicator.forward = domeStep;
        }
        else 
        {
          
        }
    }

    void ActivateVisuals(bool active)
    {
        FloorAimVisual.gameObject.SetActive(active);
        FloorAimLine.gameObject.SetActive(active);

        ActivateGhostDummies(active);
    }

    void ActivateGhostDummies(bool active)
    {
        foreach(var dummy in _dummyControllers)
        {
            if(dummy != null)
                dummy.SetActive(active);
        }

        if(_dummyHead != null)
            _dummyHead.SetActive(active);
    }

    void SetUpGhostDummies()
    {
        foreach(var controller in ViveHelper.Controllers)
        {
            var dummy = Instantiate(GhostControllerPrefab);
            dummy.transform.SetParent(transform);
            dummy.SetActive(false);
            _dummyControllers.Add(dummy);
        }

        _dummyHead = Instantiate(GhostHeadPrefab);
        _dummyHead.transform.SetParent(transform);
    }

    void UpdateGhostDummies()
    {
        var index = 0;
        foreach(var controller in ViveHelper.Controllers.Values)
        {
            _dummyControllers[index].transform.rotation = controller.Transform.rotation;
            _dummyControllers[index].transform.position = controller.Transform.position + _requiredMovement;
            ++index;
        }

        _dummyHead.transform.position = SteamVR_Render.Top().head.position + _requiredMovement;
        _dummyHead.transform.rotation = SteamVR_Render.Top().head.rotation;
    }
}
