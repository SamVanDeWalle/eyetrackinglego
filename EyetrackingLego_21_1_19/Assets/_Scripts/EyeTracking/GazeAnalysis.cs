﻿using UnityEngine;
using System;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using Tobii.Research;
using System.Threading.Tasks;
using System.IO;

public class GazeAnalysis : MonoBehaviour {
    [Serializable]
    public struct ObjectGazed { //obsolete?
        public int Id;
        public int Weight;
    }

    [Serializable]
    public struct GazeData {
        public float Timestamp;
        public int Frame; //new, remove
        public int Calibrated;
        public int FixationID;
        public int Outlier;

        public Vector3 GazeOrigin;
        public Vector3 GazeDirection;
        public Vector3 GazeLeftOrigin;
        public Vector3 GazeLeftDirection;
        public Vector3 GazeRightOrigin;
        public Vector3 GazeRightDirection;
        public Vector2 GazeViewspace;

        //public ObjectGazed Gaze;
        public string ObjectViewed;
        public string ObjectGrabbed;
        public string ObjectParent;
        public float LeftEyePupilDiameter;
        public float RightEyePupilDiameter;

    }

    public string SaveLocation = Directory.GetCurrentDirectory() + @"\Eyetracking\Data\"; // @"D:\Projects\VR\Eyetracking\Data\"; //Currently a fixed path will do

    private string _filePath;
    private Queue<GazeData> _rawDataBuffer = new Queue<GazeData>();
    private List<GazeData> _dataBuffer = new List<GazeData>();

    public bool RecordSession = true;
    public static int Framecounter = 0;
    private int _fixationIndex = -1;
    private int _calibrated = 0;
    private static string _objRelation;
    private PickupController.ActionDelegate someaction;

    void Awake() {
        EyeTrackingManager.OnCalibrationDone += Calibrated;
        _filePath = SaveLocation + DateTime.Now.ToString("yyMMdd_HH_mm_ss") + ".json";
        // someaction += () => DebugInfo("someaction", "tested");
        Application.targetFrameRate = 120;
    }

    void OnDestroy() {
        //originally _dataBuffer instead of _rawDataBuffer
        if (_rawDataBuffer.Count > 0) Save();
        Debug.Log("GazeAnalysis ended! Datapoints:" + _rawDataBuffer.Count + "\tSave location: " + SaveLocation);
    }

    void Update() {//Currently 50Hz or 0.02s //Adjust by going to Edit > Settings > Time > Fixed Timestep 
        DebugInfo(GazeObjectFinder.GetGazeObjectInfo());
        if (!RecordSession) return;
        LogGrabbedObject();

        bool validData = Record();
        //if (!validData) return;

        //Task.Factory.StartNew(() => {
        //    FilterEyeMovements(); //verify
        //});
    }

    void LogGrabbedObject() {
        int pieceID = PickupManager.GetPickedUpPieceID(ControllerIndex.Right);
        string pickedUpItemID = pieceID != -1 ? PickupManager.GetPickedUpPieceTypeID(ControllerIndex.Right) : PickupManager.GetPickedUpPieceTypeID(ControllerIndex.Left);
        DebugInfo(pickedUpItemID, "picked up", "E38B81");
    }

    void DebugInfo(string str, string objInfo = "viewed", string color = "FF9933") { 
        str = "<color=#" + color + ">" +
                "[GazeObjectInfo]\t<b>" + str + "</b>" +
                    "<i>\t\t\t[" + objInfo + "]</i>" +
                    "</color>";
        Debug.Log(str);
    }

    public static void RelationUpdate(string relation) { //niet proper
        _objRelation = relation;
    }

    bool Record() {
        //if (EyeTrackingManager.LeftEyeDiameter < 1 || EyeTrackingManager.RightEyeDiameter < 1) return false;
        //if (EyeTrackingManager.GazeViewspace.x < 1 || EyeTrackingManager.GazeViewspace.y < 1) return false;

        int pieceID = PickupManager.GetPickedUpPieceID(ControllerIndex.Right);

        GazeData dataFrame = new GazeData {
            Timestamp = Time.time, //TODO EyeTrackingManager.DeviceTimeStamp
            Frame = Framecounter, //new, remove later
            Calibrated = _calibrated,
            //FixationID = _fixationIndex,
            GazeLeftOrigin = EyeTrackingManager.GazeLeftOrigin,
            GazeLeftDirection = EyeTrackingManager.GazeLeftDirection,
            GazeRightOrigin = EyeTrackingManager.GazeRightOrigin,
            GazeRightDirection = EyeTrackingManager.GazeRightDirection,
            GazeOrigin = (EyeTrackingManager.GazeLeftOrigin + EyeTrackingManager.GazeRightOrigin) / 2.0f,
            GazeDirection = EyeTrackingManager.GazeLeftDirection + EyeTrackingManager.GazeRightDirection,
            GazeViewspace = EyeTrackingManager.GazeViewspace,                                           //2D screen coord
            ObjectViewed = GazeObjectFinder.GetGazeObjectInfo(),
            ObjectGrabbed = pieceID != -1 ? PickupManager.GetPickedUpPieceTypeID(ControllerIndex.Right) : PickupManager.GetPickedUpPieceTypeID(ControllerIndex.Left),
            //Gaze 
            ObjectParent = _objRelation, //todo01
            LeftEyePupilDiameter = EyeTrackingManager.LeftEyeDiameter,
            RightEyePupilDiameter = EyeTrackingManager.RightEyeDiameter
        };
        _rawDataBuffer.Enqueue(dataFrame);
        //if (_fixationIndex != -1)
            _objRelation = ""; //Only reset upon valid fixation, otherwist it gets filtered
        Framecounter++;
        return true;
    }

    public void Save() {
        //var container = new IOList<GazeData>(_dataBuffer);
        var container = new IOList<GazeData>(_rawDataBuffer.ToList<GazeData>());
        container.Save(_filePath);
        //filter json met deze regex (?s)\n\ {8}\{\n\ {12}\"Timestamp\".{50,100}\"FixationId\"\: -1(.*?\}\,){9} in sublime text om de fixaties -1 er uit te halen
    }

    void FilterOutliers(ref List<GazeData> recordedData, int fixationID) {//Tukey's_fences

        if (fixationID == -1) return;
        var fixationData = recordedData.Where(x => x.FixationID == fixationID && x.LeftEyePupilDiameter > 1f && x.RightEyePupilDiameter > 1f); //When eyes aren't detected the pupil is 0

        if (fixationData.Count() < 5 || fixationData.Count() > 200) {
            //Debug.Log("Only " + fixationData.Count() + " valid datapoints in fixation " + _fixationIndex);
            return;
        }

        float k = 1.5f;     //outlier, with k in [1.5, 3]

        //Take median value in all two dimention separately
        //We will do with O(n log n) for now for code simplicity, O(n) is possible to achieve without sorting
        var x_values = fixationData.Select(data => data.GazeViewspace.x).OrderBy(data => data);
        var y_values = fixationData.Select(data => data.GazeViewspace.y).OrderBy(data => data);

        float x_med = GetMedianValue(x_values);
        float y_med = GetMedianValue(y_values);

        int index_median = (int)((recordedData.Count() / 2f) - 0.49f);                        //this is the index of the median in recordedData
        float x_q1 = GetMedianValue(x_values.Take(index_median));                             //median between 0 and median index is the first quartile
        float x_q3 = GetMedianValue(x_values.OrderByDescending(x => x).Take(index_median));   //ditto third quartile, from back to front

        float y_q1 = GetMedianValue(y_values.Take(index_median));
        float y_q3 = GetMedianValue(y_values.OrderByDescending(y => y).Take(index_median));

        IEnumerable<GazeData> outliers =
                        from point in fixationData
                        where point.GazeViewspace.x < x_q1 - k * (x_q3 - x_q1) || point.GazeViewspace.x > x_q3 + k * (x_q3 - x_q1)
                            || point.GazeViewspace.y < y_q1 - k * (y_q3 - y_q1) || point.GazeViewspace.y > y_q3 + k * (y_q3 - y_q1)
                        select point;

        if (outliers.Count() > 0) {
            Debug.Log("outliers found! n=" + outliers.Count() + "/" + fixationData.Count());
        }

        recordedData.Where(x => outliers.Any(y => y.Timestamp == x.Timestamp)).ToList().ForEach(r => r.Outlier = 1);
    }

    T GetMedianValue<T>(IEnumerable<T> window) {
        int median_index = (int)((window.Count() / 2f) - 0.49f); //int truncates anyway, dont subtract 0.5001f from 1.4999f, otherwise we end up with 0; think about [0,1,2,3] for example
        if (window.Count() < 2) median_index = 0;
        dynamic median = window.OrderBy(data => data).ToList()[median_index];
        dynamic median2 = (median + window.OrderBy(data => data).ToList()[median_index + 1]) / 2;

        return window.Count() % 2 == 0 ? median2 : median;
    }

    void FilterEyeMovements(float fixationWindow = 0.050f, float fixationMaxVelocity = 1.5f, float SaccadeMinVelocity = 50f) { //in ms and °/s
        /*
        * I-VT (protocol, velocity threshold), Salvucci, D. D., & Goldberg, J. H. (2000). Identifying fixations and saccades in eye-tracking protocols.
        *   Calculate point-to-point velocities for each point in the protocol
        *   Label each point below velocity threshold as a fixation point, otherwise as a saccade point
        *   Collapse consecutive fixation points into fixation groups, removing saccade points
        *   Map each fixation group to a fixation at the centroid of its points
        *   Return fixations
        */

        /*
       * Modified version:
       * - consecutive fixation points are given an ID per group and not collapsed
       * - fixations remain unaltered, centroid/mean of each group/fixationID is calculated in Python
       */

        if (_rawDataBuffer.Count < 5) return;

        int samples = 20;                                                                      //play with this number
        GazeData[] data = _rawDataBuffer.Take(Math.Max(samples, _rawDataBuffer.Count)).ToArray();

        //float avgVelocity = 0f;
        bool saccade = false;
        for (int i = 1; i < data.Count(); i++) {
            _rawDataBuffer.Dequeue();
            float angle = Vector3.Angle(data[i - 1].GazeDirection, data[i].GazeDirection);
            float deltaTime = (data[i].Timestamp - data[i - 1].Timestamp);
            float velocity = angle * (1 / deltaTime);                                           // °/s
            //avgVelocity += velocity;
            if (deltaTime < fixationWindow && velocity < fixationMaxVelocity) {                 //<50ms and <1.5°: group fixations
                Debug.Log("fixation is happening");
                data[i - 1].FixationID = _fixationIndex;                                        //the first fixation at program startup will be -1, this is intended since this is usually invalid data                                            
                if (i == 1) _dataBuffer.Add(data[i - 1]);

                data[i].FixationID = _fixationIndex; //niet goed
                _dataBuffer.Add(data[i]);
                saccade = false;
            } else if (velocity > SaccadeMinVelocity) {
                Debug.Log("saccade is happening");                                              //50° - saccade
                if (!saccade) {                                                                 //Prior to this was a fixation sequence
                    Task.Factory.StartNew(() => {                                               //Filter the outliers of the previous window / fixationgroup
                        FilterOutliers(ref _dataBuffer, _fixationIndex);
                    });
                    _fixationIndex++;
                }
                saccade = true;
                data[i].FixationID = -1;
                _dataBuffer.Add(data[i]);
            } else {
                Debug.Log("something is happening");
            }
        }
        //avgVelocity /= samples;
        //Debug.Log("avgVelocity: " + avgVelocity);
    }

    void Calibrated() {
        _calibrated = 1;
        EyeTrackingManager.OnCalibrationDone -= Calibrated;
    }
}