﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReplayInfoUI : MonoBehaviour
{
    public Text ReplayTimeText;

	void Update ()
    {
        ReplayTimeText.text = "Replay time: " + EyeTrackingRecordingManager.ReplayTime;	
	}
}
