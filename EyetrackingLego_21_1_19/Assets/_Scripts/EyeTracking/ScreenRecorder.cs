﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using UnityEngine;

public class ScreenRecorder : MonoBehaviour, IRecorder
{
    static Process _ffmpegProcess;
    public event Action OnLoaded;

    public void StartRecording()
    {
        var thread = new Thread(delegate () { RunFFMPEG(); });
        thread.Start();
    }

    public void ToggleReplayPauze()
    {
        //
    }

    public void StopRecording()
    {
        StreamWriter inputWriter = _ffmpegProcess.StandardInput;
        inputWriter.WriteLine("q");

        _ffmpegProcess.WaitForExit();
        _ffmpegProcess.Close();
        inputWriter.Close();
    }

    public void Load(string folder) { }
    public void StartReplaying() { }
    public void StopReplaying() { }

    static void RunFFMPEG()
    {
        string outputFile = "eyetestout3.mp4";
        if (File.Exists(outputFile))
        {
            File.Delete(outputFile);
        }
        //string arguments = "-f dshow -i video=\"screen-capture-recorder\" -video_size 1920x1080 -vcodec libx264 -pix_fmt yuv420p -preset ultrafast " + outputFile;
        //string arguments = "-f dshow -i video=\"screen-capture-recorder\" -offset_x 0 -offset_y 0 -video_size 1920x1080 -preset ultrafast " + outputFile;
        string arguments = "-f dshow -i video=\"DVI2USB 3.0 D2S356905\" -video_size 1920x1080 " + outputFile;

        _ffmpegProcess = new Process();

        _ffmpegProcess.StartInfo.FileName = "ffmpeg.exe";
        _ffmpegProcess.StartInfo.Arguments = arguments;
        _ffmpegProcess.StartInfo.UseShellExecute = false;
        _ffmpegProcess.StartInfo.CreateNoWindow = true;

        _ffmpegProcess.StartInfo.RedirectStandardError = true;
        _ffmpegProcess.StartInfo.RedirectStandardOutput = true;
        _ffmpegProcess.StartInfo.RedirectStandardInput = true;

        _ffmpegProcess.EnableRaisingEvents = true;
        _ffmpegProcess.Start();

        _ffmpegProcess.BeginOutputReadLine();
        _ffmpegProcess.BeginErrorReadLine();
    }
}
