﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[Serializable]
public class ButtonState
{
    public ControllerButton Button;
    public ViveButtonState State;

    public ButtonState() { }
    public ButtonState(ControllerButton button, ViveButtonState state)
    {
        Button = button;
        State = state;
    }
}

[Serializable]
public class ViveControllerState
{
    public List<ButtonState> ButtonStates = new List<ButtonState>();
    public Vector3 Position;
    public Quaternion Rotation;
    public float GameTime;

    public ViveControllerState()
    {
        ButtonStates = new List<ButtonState>()
        {
            new ButtonState(ControllerButton.Trigger, ViveButtonState.None),
            new ButtonState(ControllerButton.TouchPad, ViveButtonState.None),
            new ButtonState(ControllerButton.Menu, ViveButtonState.None),
            new ButtonState(ControllerButton.Grip, ViveButtonState.None),
        };
    }
}

public class ViveControllerRecorder : MonoBehaviour, IRecorder
{
    public ViveController Controller;
    public string Path { get { return EyeTrackingRecordingManager.RecordingDirectory + "vivecontrollerdata_" + Controller.Index + ".json"; } private set { } }

    List<ViveControllerState> _data = new List<ViveControllerState>();
    int _replayIndex;
    bool _bRecording;
    bool _bReplaying;
    bool _bReplayPauzed;

    public event Action OnLoaded;

    void OnDestroy()
    {
        if(_bRecording && _data.Count > 0)
            Save();
    }

    void Update()
    {
        if (_bRecording && Controller != null)
            Record();

        if (_bReplaying && Controller != null)
            Replay();
    }

    public void StartRecording()
    {
        _bRecording = true;
    }

    public void StopRecording()
    {
        _bRecording = false;
    }

    void Record()
    {
        var state = new ViveControllerState();

        for(int i = 0; i < state.ButtonStates.Count; ++i)
        {
            if (Controller.GetPressDown(state.ButtonStates[i].Button))
            {
                state.ButtonStates[i].State = ViveButtonState.PressDown;
            }
            else if (Controller.GetPressUp(state.ButtonStates[i].Button))
            {
                state.ButtonStates[i].State = ViveButtonState.PressUp;
            }
            else if (Controller.GetPress(state.ButtonStates[i].Button))
            {
                state.ButtonStates[i].State = ViveButtonState.Press;
            }
            else if (Controller.GetTouchDown(state.ButtonStates[i].Button))
            {
                state.ButtonStates[i].State = ViveButtonState.TouchDown;
            }
            else if (Controller.GetTouchUp(state.ButtonStates[i].Button))
            {
                state.ButtonStates[i].State = ViveButtonState.TouchUp;
            }
            else if (Controller.GetTouch(state.ButtonStates[i].Button))
            {
                state.ButtonStates[i].State = ViveButtonState.Touch;
            }
            else
            {
                state.ButtonStates[i].State = ViveButtonState.None;
            }
        }

        state.Position = Controller.Transform.position;
        state.Rotation = Controller.Transform.rotation;
        state.GameTime = Time.time;
        _data.Add(state);
    }

    void Save()
    {
        var container = new IOList<ViveControllerState>(_data);
        container.Save(Path);
    }

    public void Load(string folder)
    {
        var dataPath = folder + @"\vivecontrollerdata_" + Controller.Index + ".json";
        _data = IOList<ViveControllerState>.Load(dataPath);

        if (OnLoaded != null)
            OnLoaded();
    }

    public void StartReplaying()
    {
        _bReplaying = true;
        var skipFrames = (int)(EyeTrackingRecordingManager.ReplayTime / Time.deltaTime);
        _replayIndex = CheckReplayIndex(_data.Count / 2);
    }

    public void ToggleReplayPauze()
    {
        _bReplayPauzed = !_bReplayPauzed;
    }

    public void StopReplaying()
    {
        _bReplaying = false;
    }

    void Replay()
    {
        if (_replayIndex < 0 || _replayIndex >= _data.Count)
        {
            Debug.Log("ViveControllerRecorder::Replay - index invalid: " + _replayIndex + " of " + _data.Count);
            _bReplaying = false;
            return;
        }

        var lastIndex = _replayIndex - 1;
        //_replayIndex = CheckReplayIndex(_replayIndex);
        if (lastIndex == _replayIndex)
            Debug.Log("Same index!");
        else if (_replayIndex > lastIndex + 1)
            Debug.Log("Skipped index!");
        var state = _data[_replayIndex];
        Controller.Transform.position = state.Position;
        Controller.Transform.rotation = state.Rotation;

        for(int i = 0; i < state.ButtonStates.Count; ++i)
        {
            Controller.SetSimulationState(state.ButtonStates[i].Button, state.ButtonStates[i].State);
        }

        if(!_bReplayPauzed)
            ++_replayIndex;
    }

    int CheckReplayIndex(int index)
    {
        if (index < 0 || index >= _data.Count)
        {
            Debug.Log("ViveControllerRecorder::CheckReplayIndex - index invalid: " + index + " of " + _data.Count);
            _bReplaying = false;
            return index;
        }

        //TODO if the time difference is a lot bigger than delta time, consider skipping the index further
        var timeDifference = GetTimeDifference(index, EyeTrackingRecordingManager.ReplayTime);

        //var avgFrameTime = 1.0f / Application.targetFrameRate;
        //if(timeDifference > avgFrameTime * 10)
        //{
        //    var testIndex = (int)(index + (timeDifference / avgFrameTime));
        //    if (testIndex > 0 && testIndex < _data.Count)
        //        index = testIndex;
        //    Debug.Log("index" + index);
        //}
        //Debug.Log("index" + index);

        if (index - 1 >= 0 && GetTimeDifference(index - 1, EyeTrackingRecordingManager.ReplayTime) < timeDifference)
        {
            return CheckReplayIndex(index - 1);
        }

        if (index + 1 < _data.Count && GetTimeDifference(index + 1, EyeTrackingRecordingManager.ReplayTime) < timeDifference)
        {
            return CheckReplayIndex(index + 1);
        }

        return index;
    }

    float GetTimeDifference(int index, float time)
    {
        return Mathf.Abs(_data[index].GameTime - time);
    }
}
