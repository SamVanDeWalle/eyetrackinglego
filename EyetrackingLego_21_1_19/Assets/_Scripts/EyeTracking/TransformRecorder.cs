﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TransformState
{
    public Vector3 Position;
    public Quaternion Rotation;
    public float GameTime;
}

public class TransformRecorder : MonoBehaviour
{
    public string ID;
    public Transform Transform;
    public string Path { get { return EyeTrackingRecordingManager.RecordingDirectory + @"\transformdata" + ID + ".json"; } private set { } }

    List<TransformState> _data = new List<TransformState>();
    Transform _preReplayParent;
    int _replayIndex;
    bool _bRecording;
    bool _bReplaying;
    bool _bReplayPauzed;

    public event Action OnLoaded;

    void OnDestroy()
    {
        if (_bRecording && _data.Count > 0)
            Save();
    }

    void LateUpdate()
    {
        if (Transform == null)
            return;

        if (_bRecording)
            Record();

        if (_bReplaying)
            Replay();
    }

	public void StartRecording()
    {
        _bRecording = true;
	}

    void Record()
    {
        var state = new TransformState();

        state.Position = Transform.position;
        state.Rotation = Transform.rotation;
        state.GameTime = Time.time;

        _data.Add(state);
    }

    void Save()
    {
        var container = new IOList<TransformState>(_data);
        container.Save(Path);
    }

    public void StartReplaying()
    {
        _bReplaying = true;
        _preReplayParent = Transform.parent;
        Transform.SetParent(null);
        var skipFrames = (int)(EyeTrackingRecordingManager.ReplayTime / Time.deltaTime);
        _replayIndex = CheckReplayIndex(_data.Count / 2);
    }

    public void ToggleReplayPauze()
    {
        _bReplayPauzed = !_bReplayPauzed;
    }

    void Replay()
    {
        if (_replayIndex < 0 || _replayIndex >= _data.Count)
        {
            Debug.Log("TransformRecorder::Replay - index invalid: " + _replayIndex + " of " + _data.Count);
            _bReplaying = false;
            return;
        }

        //_replayIndex = CheckReplayIndex(_replayIndex);
        var state = _data[_replayIndex];
        Transform.position = state.Position;
        Transform.rotation = state.Rotation;

        if(!_bReplayPauzed)
            ++_replayIndex;
    }

    public void Load(string folder)
    {
        var dataPath = folder + @"\transformdata" + ID + ".json";
        _data = IOList<TransformState>.Load(dataPath);

        if (OnLoaded != null)
            OnLoaded();
    }

    int CheckReplayIndex(int index)
    {
        if (index < 0 || index >= _data.Count)
        {
            Debug.Log("TransformRecorder::CheckReplayIndex - index invalid: " + index + " of " + _data.Count);
            _bReplaying = false;
            return index;
        }

        var timeDifference = GetTimeDifference(index, EyeTrackingRecordingManager.ReplayTime);
        if (index - 1 >= 0 && GetTimeDifference(index - 1, EyeTrackingRecordingManager.ReplayTime) < timeDifference)
        {
            return CheckReplayIndex(index - 1);
        }

        if (index + 1 < _data.Count && GetTimeDifference(index + 1, EyeTrackingRecordingManager.ReplayTime) < timeDifference)
        {
            return CheckReplayIndex(index + 1);
        }

        return index;
    }

    float GetTimeDifference(int index, float time)
    {
        return Mathf.Abs(_data[index].GameTime - time);
    }
}
