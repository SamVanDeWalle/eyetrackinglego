﻿using Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
//using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Threading;
using UnityEngine;

public interface IRecorder
{
    void StartRecording();
    void StopRecording();
    void StartReplaying();
    void StopReplaying();
    void ToggleReplayPauze();
    void Load(string folder);

    event Action OnLoaded;
}

public class EyeTrackingRecordingManager : Singleton<EyeTrackingRecordingManager>
{
    public ViveRecordingManager ViveRecorder;
    public GazeRecorder GazeRecorder;
    public GameObject KeyFrame;
    public string DirectoryPath;
    public string ReplayFolder;
    public static List<string> ReplayFolders = new List<string>();
    public static string RecordingDirectory { get { return _recordingDirectoryPath; } private set { } }
    public static string ReplayPath { get { return _replayDirectoryPath; } private set { } }
    public static float ReplayTime { get { return Time.time - _replayStartTime + _replaySkipTime; } private set { } }
    public float ReplaySkipTime; //Optionally set to 206 to go past the calibration
    public bool RecordVive;
    public bool RecordGaze;
    public bool ReplayVive;
    public bool ReplayGaze;
    public bool AutoStartRecording;

    List<IRecorder> _recorders = new List<IRecorder>();
    List<IRecorder> _replayers = new List<IRecorder>();
    static string _recordingDirectoryPath;
    static string _replayDirectoryPath;
    static float _replayStartTime;
    static float _replaySkipTime;
    static bool _bRecording, _bReplaying, _bReplayPauzed;
    static int _requiredLoadCount;
    static int _loadCount;

    public static event Action OnRecordingStarted;
    static event Action OnReplayLoaded;
    public static event Action OnReplayStarted;
    public static event Action OnReplayFoldersLoaded;

    protected override void Awake()
    {
        base.Awake();

        Application.targetFrameRate = 90;

        if (Display.displays.Length > 1)
        {
            Display.displays[1].Activate();
        }

        KeyFrame.SetActive(false);
        DirectoryPath = Directory.GetCurrentDirectory() + @"\Eyetracking\Data\";

        _recordingDirectoryPath = DirectoryPath + System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss", CultureInfo.InvariantCulture) + @"\";
        _replayDirectoryPath = DirectoryPath + ReplayFolder + @"\";

        _bReplaying = ReplayGaze || ReplayVive;

        if (_bReplaying) {
            Debug.Log("We are replaying, loading folders");
            LoadReplayPaths();
        }

        if (ViveRecorder != null)
        {
            if(RecordVive)
                _recorders.Add(ViveRecorder);
            if (ReplayVive)
                _replayers.Add(ViveRecorder);
        }

        if (GazeRecorder != null)
        {
            if (RecordGaze)
                _recorders.Add(GazeRecorder);
            if (ReplayGaze)
                _replayers.Add(GazeRecorder);
        }

        _replaySkipTime = ReplaySkipTime;
    }

    void Start()
    {
        EyeTrackingManager.OnCalibrationDone += SetKeyFrame;

        if (AutoStartRecording)
            StartRecording();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F7))
        {
            StartRecording();
        }
        if (Input.GetKeyDown(KeyCode.F8))
        {
            StopRecording();
        }
        if (Input.GetKeyDown(KeyCode.F10))
        {
            StartReplaying();
        }
    }

    void StartRecording()
    {
        Directory.CreateDirectory(_recordingDirectoryPath);
        foreach (var recorder in _recorders)
        {
            recorder.StartRecording();
        }

        UnityEngine.Debug.Log("EyeTrackingRecordingManager::StartRecording - Recording started!");
        if (OnRecordingStarted != null)
            OnRecordingStarted();
    }

    void StopRecording()
    {
        foreach (var recorder in _recorders)
        {
            recorder.StopRecording();
        }

        UnityEngine.Debug.Log("EyeTrackingRecordingManager::StopRecording - Recording stopped!");
    }

    void SetKeyFrame()
    {
        StartCoroutine(ShowKeyFrameAndLog());
    }

    IEnumerator ShowKeyFrameAndLog()
    {
        KeyFrame.SetActive(true);

        yield return new WaitForSeconds(1);

        KeyFrame.SetActive(false);

        Directory.CreateDirectory(DirectoryPath);
        using (StreamWriter file = File.AppendText(_recordingDirectoryPath + "keyframe.txt"))
        {
            var gameDateTime = System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss.fff", CultureInfo.InvariantCulture);
            var gameTime = Time.time;
            file.WriteLine(gameDateTime);
            file.WriteLine(gameTime);
        }
    }

    public void StartReplaying()
    {
        UnityEngine.Debug.Log("EyeTrackingRecordingManager::StartReplaying - Replay started!");
        OnReplayLoaded -= StartReplayingRecorders;
        OnReplayLoaded += StartReplayingRecorders;
        LoadReplay();
    }

    public void ToggleReplayPauze()
    {
        UnityEngine.Debug.Log("EyeTrackingRecordingManager::StartReplaying - Replay pauzed!");
        _bReplayPauzed = !_bReplayPauzed;
        foreach (var recorder in _replayers)
        {
            recorder.ToggleReplayPauze();
        }

        Time.timeScale = _bReplayPauzed ? 0 : 1;
    }

    void StartReplayingRecorders()
    {
        _replayStartTime = Time.time;

        foreach (var recorder in _replayers)
        {
            recorder.StartReplaying();
        }

        if (OnReplayStarted != null)
            OnReplayStarted();
    }

    void LoadReplay()
    {
        _requiredLoadCount = _replayers.Count;
        foreach (var recorder in _replayers)
        {
            recorder.OnLoaded -= AddLoaded;
            recorder.OnLoaded += AddLoaded;
            recorder.Load(DirectoryPath + ReplayFolder);
        }
    }

    void AddLoaded()
    {
        ++_loadCount;
        if(_loadCount >= _requiredLoadCount)
        {
            if (OnReplayLoaded != null)
                OnReplayLoaded();
        }
    }

    void LoadReplayPaths()
    {
        var paths = Directory.GetDirectories(DirectoryPath);
        ReplayFolders.Clear();
        foreach(var path in paths)
        {
            var folderName = Path.GetFileName(path);
            ReplayFolders.Add(folderName);
        }
        Debug.Log("Found " + ReplayFolders.Count + " ReplayFolders!");
        if(OnReplayFoldersLoaded != null)
            OnReplayFoldersLoaded();
    }
}
