﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class GazeObject {
    [SerializeField]
    public string ObjectName;
    [SerializeField]
    public int ObjectUID;
    [SerializeField]
    public float Distance;
    [SerializeField]
    public float Weight;
    [NonSerialized]
    public GameObject GameObject;
}

public class GazeObjectFinder : MonoBehaviour {
    public static List<GazeObject> GazeObjects { get { return _gazeObjects; } private set { } }
    public GazeRecorder GazeRecorder;
    public Text InfoText;
    public static bool UseCorrection;

    private LineRenderer lineOfSightL;
    private LineRenderer lineOfSightR;

    static List<GazeObject> _gazeObjects = new List<GazeObject>(); //delete me later

    Transform eyeLeft;
    Transform eyeRight;


    private void Start() {
        eyeLeft = EyeTrackingManager.Instance.LeftEye;
        eyeRight = EyeTrackingManager.Instance.RightEye;
    }
    void Update() {
        DrawDebug();
        FindGazeObject();
        VisualizeGazeObjectInfo();
    }

    void DrawDebug() {
        if (lineOfSightL == null) {
            lineOfSightL = GameObject.Find("LineOfSight").GetComponent<LineRenderer>();
            lineOfSightR = Instantiate(GameObject.Find("LineOfSight")).GetComponent<LineRenderer>();     
        }

        Debug.DrawRay(eyeLeft.position, eyeLeft.forward * 10, Color.cyan);
        Debug.DrawRay(eyeRight.position, eyeRight.forward * 10, Color.cyan);

        lineOfSightL.SetPosition(0, eyeLeft.position);
        lineOfSightL.SetPosition(1, eyeLeft.position + eyeLeft.forward * 10);
        lineOfSightR.SetPosition(0, eyeRight.position);
        lineOfSightR.SetPosition(1, eyeRight.position + eyeRight.forward * 10);
    }

    void FindGazeObject() {
        _gazeObjects.Clear();
        //var ray = !UseCorrection ? GazeRecorder.CurrentEyeCenterRay : PythonCom.CorrectedGaze;
        Transform eye = EyeTrackingManager.Instance.EyeCenter;
        Ray ray = new Ray(eye.position, eye.forward); //ray2

        Debug.DrawRay(eye.position, eye.forward * 10, Color.red);

        //First look for directHits
        var directHits = Physics.RaycastAll(ray);

        CheckHits(directHits);

        if (_gazeObjects.Count == 0) {          //ONLY if no object is hit, send out a sphere cast.
            float sensitivityFactor = 0.05f;    //If objects are detected which shouldn't be detected, lower this
            var indirectHits = Physics.SphereCastAll(ray, sensitivityFactor);
            CheckHits(indirectHits);
        }

        // Look for closest objects
        if (directHits == null || directHits.Length == 0) {

        }
    }

    static void CheckHits(RaycastHit[] hits) {
        for (int i = 0; i < hits.Length; ++i) {
            var pickable = hits[i].transform.GetComponentInParent<Pickable>();
            if (pickable == null)
                continue;

            var gazeObjectData = new GazeObject();
            gazeObjectData.GameObject = pickable.gameObject;
            gazeObjectData.ObjectName = gazeObjectData.GameObject.name;
            gazeObjectData.ObjectUID = gazeObjectData.GameObject.GetComponent<AssemblyPiece>().ID;//gazeObjectData.GameObject.GetInstanceID();
            gazeObjectData.Distance = hits[i].distance;
            _gazeObjects.Add(gazeObjectData);
        }
    }

    static public string GetGazeObjectInfo() {
        if (_gazeObjects.Count > 0) {
            var closestDistance = _gazeObjects.Min(go => go.Distance);
            var closestObject = _gazeObjects.First(go => go.Distance == closestDistance);
            return closestObject.ObjectName;
        } else {
            return "No gaze Object";
        }
    }

    void VisualizeGazeObjectInfo() {
        InfoText.text = GetGazeObjectInfo();
    }
}
