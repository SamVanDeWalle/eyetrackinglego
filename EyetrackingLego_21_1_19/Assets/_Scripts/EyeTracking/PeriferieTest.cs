﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

[Serializable]
public class PointStatus
{
    public Vector3 LocalPosition;
    public Vector2 ViewPosition;
    public float GameTime;
    public int X, Y;
}

public class PeriferieTest : MonoBehaviour
{
    public GameObject BG;
    public Transform Point;
    public Transform MidPoint;
    public float PointDuration;
    public Vector2 XRange, YRange;
    public float Z;
    public int xSteps, ySteps;
    public float IntroDelay = 3.0f;
    public string Path { get { return EyeTrackingRecordingManager.RecordingDirectory + "sequencepointsdata.json"; } private set { } }
    public List<PointStatus> PointStatuses { get { return _pointStatuses; } private set { } }
    public Camera Camera;

    List<PointStatus> _pointStatuses = new List<PointStatus>();
    bool _bDone;

    void Awake()
    {
        EyeTrackingManager.OnCalibrationDone += StartSequence;
        BG.SetActive(false);
        Point.gameObject.SetActive(false);
    }

    void Start()
    {
        //StartSequence();
    }

    public void StartSequence()
    {
        if (!Directory.Exists(EyeTrackingRecordingManager.RecordingDirectory)) return; //Calibration might be done but we are not recording

        StartCoroutine(Sequence());
    }

    IEnumerator Sequence()
    {
        BG.SetActive(true);
        MidPoint.gameObject.SetActive(true);

        var pointCount = xSteps * ySteps;
        var indexList = Enumerable.Range(0, pointCount).ToList();
        var xDistance = (XRange.y - XRange.x) / (xSteps - 1);
        var yDistance = (YRange.y - YRange.x) / (ySteps - 1);
        var animation = Point.GetComponent<Animation>();

        yield return new WaitForSeconds(IntroDelay);

        MidPoint.gameObject.SetActive(false);
        while (indexList.Count > 0)
        {
            Point.gameObject.SetActive(true);

            var listIndex = UnityEngine.Random.Range(0, indexList.Count);
            var index = indexList[listIndex];
            indexList.RemoveAt(listIndex);

            var x = index % xSteps;
            var y = index / xSteps;
            Debug.Log("Showing sequence point x: " + x + " , " + y);

            var localPosition = new Vector3(0, 0, Z);
            localPosition.x = XRange.x + (xDistance * x);
            localPosition.y = YRange.x + (yDistance * y);
            localPosition.y *= -1;

            Point.localPosition = localPosition;
            animation.Stop();
            animation.Rewind();
            animation.Play();
            var status = new PointStatus();
            status.X = x;
            status.Y = y;
            status.LocalPosition = localPosition;
            Point.position = EyeTrackingManager.Instance.ProjectedOnGazePlane(Point.position);
            status.ViewPosition = EyeTrackingManager.Instance.ProjectedOnGazePlaneScreen(Point.position);
            status.GameTime = Time.time;
            _pointStatuses.Add(status);

            PythonCom.SendPackage.PointX = status.ViewPosition.x;
            PythonCom.SendPackage.PointY = status.ViewPosition.y;
            PythonCom.SendPackage.NewPoint = true;

            yield return new WaitForSeconds(PointDuration);
        }

        //yield return new WaitForSeconds(IntroDelay);
        //
        //var xDistance = (XRange.y - XRange.x) / (xSteps - 1);
        //var yDistance = (YRange.y - YRange.x) / (ySteps - 1);
        //var position = new Vector3();
        //position.z = Z;
        //for (int ystep = 0; ystep < ySteps; ++ystep)
        //{
        //    position.y = (YRange.x + (yDistance * ystep)) * -1;
        //    for (int xstep = 0; xstep < xSteps; ++xstep)
        //    {
        //        position.x = XRange.x + (xDistance * xstep);
        //        Point.localPosition = position;
        //        var status = new PointStatus();
        //        status.X = xstep;
        //        status.Y = ystep;
        //        status.LocalPosition = position;
        //        status.GameTime = Time.time;
        //        _pointStatuses.Add(status);
        //
        //        yield return new WaitForSeconds(PointDuration);
        //    }
        //}

        BG.SetActive(false);
        Point.gameObject.SetActive(false);

        PythonCom.SendPackage.PythonPhase = PythonPhase.Building;
    }

    void OnDestroy()
    {
        //if(_bDone)
            Save();
    }

    void Save()
    {
        if (!Directory.Exists(EyeTrackingRecordingManager.RecordingDirectory)) return;
        var container = new IOList<PointStatus>(_pointStatuses);
        container.Save(Path);
    }

    public void LoadSequencePointStatuses(string folder)
    {
        var dataPath = folder + "sequencepointsdata.json";
        _pointStatuses = IOList<PointStatus>.Load(dataPath);
    }
}
