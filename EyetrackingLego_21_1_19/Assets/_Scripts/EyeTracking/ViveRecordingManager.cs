﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ViveRecordingManager : MonoBehaviour, IRecorder
{
    public Transform HeadCamera;
    public GameObject ControllerPrefab;

    List<ViveControllerRecorder> _controllerRecorders = new List<ViveControllerRecorder>();
    List<ViveControllerRecorder> _replayControllerRecorders = new List<ViveControllerRecorder>();
    TransformRecorder _cameraRecorder;
    bool _bRecording;
    bool _bReplaying;
    bool _bReplayPauzed;
    int _requiredLoadCount;
    int _loadCount;

    public event Action OnLoaded;

    void Awake()
    {
        ViveHelper.OnControllersUpdated += CheckControllers;
    }

    void Start()
    {
        //StartRecording();
    }

    void CheckControllers()
    {
        foreach(var controller in ViveHelper.Controllers)
        {
            if(!_controllerRecorders.Any(cr => cr.Controller == controller.Value))
            {
                var recorder = gameObject.AddComponent<ViveControllerRecorder>();
                recorder.Controller = controller.Value;
                _controllerRecorders.Add(recorder);
                if (_bRecording)
                {
                    recorder.StartRecording();
                }
            }
        }
    }

    public void StartRecording()
    {
        _bRecording = true;

        for(int i = 0; i < _controllerRecorders.Count; ++i)
        {
            _controllerRecorders[i].StartRecording();
        }

        _cameraRecorder = gameObject.AddComponent<TransformRecorder>();
        _cameraRecorder.Transform = HeadCamera;
        _cameraRecorder.ID = "camera";
        _cameraRecorder.StartRecording();
    }

    public void StopRecording()
    {
        _bRecording = false;

        //
    }

    public void Load(string folder)
    {
        SetupReplayControllers();

        for (int i = 0; i < _replayControllerRecorders.Count; ++i)
        {
            _replayControllerRecorders[i].OnLoaded += AddLoaded;
            _replayControllerRecorders[i].Load(folder);
        }

        if(_cameraRecorder == null)
        {
            _cameraRecorder = gameObject.AddComponent<TransformRecorder>();
            _cameraRecorder.Transform = HeadCamera;
            _cameraRecorder.ID = "camera";
        }

        _cameraRecorder.OnLoaded += AddLoaded;
        _cameraRecorder.Load(folder);
    }

    void SetupReplayControllers()
    {
        foreach(ControllerIndex index in Enum.GetValues(typeof(ControllerIndex)))
        {
            var controllerGO = Instantiate(ControllerPrefab);
            controllerGO.name = "ReplayController_" + index;
            ViveController viveController = new ViveController();
            viveController.Index = index;
            viveController.Transform = controllerGO.transform;

            var recorder = gameObject.AddComponent<ViveControllerRecorder>();
            recorder.Controller = viveController;
            _replayControllerRecorders.Add(recorder);
            ViveHelper.SetViveController(index, viveController);
        }
    }

    public void StartReplaying()
    {
        _bReplaying = true;

        for(int i = 0; i < _replayControllerRecorders.Count; ++i)
        {
            _replayControllerRecorders[i].StartReplaying();
        }

        _cameraRecorder.StartReplaying();
    }

    public void ToggleReplayPauze()
    {
        _bReplayPauzed = !_bReplayPauzed;
        for (int i = 0; i < _replayControllerRecorders.Count; ++i)
        {
            _replayControllerRecorders[i].ToggleReplayPauze();
        }

        _cameraRecorder.ToggleReplayPauze();
    }

    public void StopReplaying()
    {

    }

    void AddLoaded()
    {
        ++_loadCount;
        if(_loadCount >= _replayControllerRecorders.Count + 1)
        {
            if (OnLoaded != null)
                OnLoaded();
        }
    }
}
