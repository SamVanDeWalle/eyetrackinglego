﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using UnityEngine;

[Serializable]
public class GazeData
{
    public Vector3 GazeOrigin;
    public Vector3 GazeDirection;
    public Vector3 GazeLeftOrigin;
    public Vector3 GazeLeftDirection;
    public Vector3 GazeRightOrigin;
    public Vector3 GazeRightDirection;
    public Vector2 GazeViewspace;
    public long DeviceTimeStamp;
    public long SystemTimeStamp;
    public string GameDateTime;
    public float GameTime;

    public GazeData() { }

    public GazeData(GazeData other)
    {
        GazeOrigin = other.GazeOrigin;
        GazeDirection = other.GazeDirection;
        GazeViewspace = other.GazeViewspace;
        DeviceTimeStamp = other.DeviceTimeStamp;
        SystemTimeStamp = other.SystemTimeStamp;
        GameDateTime = other.GameDateTime;
        GameTime = other.GameTime;
    }
}

public enum EyeIndex
{
    Left,
    Right,
}

public class GazeRecorder : MonoBehaviour, IRecorder
{
    public List<GazeData> Data { get { return _data; } private set { } }
    public GameObject EyePrefab;
    public GameObject EyeCenterPrefab;
    public string Path { get { return EyeTrackingRecordingManager.RecordingDirectory + @"\gazedata.json"; } private set { } }
    public Ray CurrentEyeCenterRay { get { return _currentEyeCenterRay; } }
    public Vector2 CurrentGaze { get { return _currentGaze; } }
    public float ReplayProgress { get { return ReplayFrameCount != 0 ? _replayIndex * 1.0f / ReplayFrameCount : 0; } private set { } }
    public int ReplayFrameCount { get { return _bReplaying ? _data.Count : 0; } }

    List<GazeData> _data = new List<GazeData>();
    Dictionary<EyeIndex, Transform> _replayEyes = new Dictionary<EyeIndex, Transform>();
    Transform _replayEyeCenter;
    Ray _currentEyeCenterRay = new Ray();
    Vector2 _currentGaze = new Vector2();
    float _nextReplayDeltaTime;
    int _replayFrameCount;
    int _replayIndex;
    bool _bRecording = false;
    bool _bReplaying;
    bool _bReplayPauzed;

    public event Action OnLoaded;

    void OnDestroy()
    {
        if(_bRecording && _data.Count > 0) // Stupid if no auto save when stopping recording !
            Save();
    }

    void Update()
    {
        if (_bRecording)
            Record();

        if (_bReplaying)
            Replay();
    }

    void LateUpdate()
    {
        if(_bReplaying)
        {
            GameTime.Simulated = true;
            GameTime.SimulatedDeltaTime = _nextReplayDeltaTime;
        }
    }

    public void StartRecording()
    {
        _bRecording = true;
    }

    public void StopRecording()
    {
        _bRecording = false;
    }

    void Record()
    {
        var data = new GazeData();
        data.GazeOrigin = EyeTrackingManager.GazeOrigin;
        data.GazeDirection = EyeTrackingManager.GazeDirection;
        data.GazeLeftOrigin = EyeTrackingManager.GazeLeftOrigin;
        data.GazeLeftDirection = EyeTrackingManager.GazeLeftDirection;
        data.GazeRightOrigin = EyeTrackingManager.GazeRightOrigin;
        data.GazeRightDirection = EyeTrackingManager.GazeRightDirection;
        data.GazeViewspace = EyeTrackingManager.GazeViewspace;
        data.DeviceTimeStamp = EyeTrackingManager.DeviceTimeStamp;
        data.SystemTimeStamp = EyeTrackingManager.SystemTimeStamp;
        data.GameDateTime = System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss.fff", CultureInfo.InvariantCulture);
        data.GameTime = Time.time;
        _data.Add(data);

        _currentEyeCenterRay.origin = EyeTrackingManager.GazeOrigin;
        _currentEyeCenterRay.direction = EyeTrackingManager.GazeDirection;
    }

    public void Save()
    {
        Debug.Log("gaze recording saved: " + Path);
        var container = new IOList<GazeData>(_data);
        container.Save(Path);
    }

    public void Load(string folder)
    {
        SetupReplayEyes();

        var dataPath = folder + @"\gazedata.json";
        _data = IOList<GazeData>.Load(dataPath);

        if (OnLoaded != null)
            OnLoaded();
    }

    void SetupReplayEyes()
    {
        foreach(EyeIndex index in Enum.GetValues(typeof(EyeIndex)))
        {
            _replayEyes[index] = Instantiate(EyePrefab).transform;
        }

        _replayEyeCenter = Instantiate(EyeCenterPrefab).transform;
    }

    public void StartReplaying()
    {
        _bReplaying = true;
        var avgFrameTime = 1.0f / Application.targetFrameRate;
        var skipFrames = (int)(EyeTrackingRecordingManager.ReplayTime / avgFrameTime);
        _replayIndex = CheckReplayIndex(_data.Count / 2);
    }

    public void ToggleReplayPauze()
    {
        _bReplayPauzed = !_bReplayPauzed;
    }

    public void StopReplaying()
    {
        _bReplaying = false;
    }

    void Replay()
    {
        if (_replayIndex < 0 || _replayIndex >= _data.Count)
        {
            Debug.Log("GazeRecorder::Replay - index invalid: " + _replayIndex + " of " + _data.Count);
            _bReplaying = false;
            return;
        }

        //_replayIndex = CheckReplayIndex(_replayIndex);

        var state = _data[_replayIndex];
        _replayEyes[EyeIndex.Left].position = state.GazeLeftOrigin;
        _replayEyes[EyeIndex.Left].forward = state.GazeLeftDirection;
        _replayEyes[EyeIndex.Right].position = state.GazeRightOrigin;
        _replayEyes[EyeIndex.Right].forward = state.GazeRightDirection;
        _replayEyeCenter.position = state.GazeOrigin;
        _replayEyeCenter.forward = state.GazeDirection;
        _currentEyeCenterRay.origin = _replayEyeCenter.position;
        _currentEyeCenterRay.direction = _replayEyeCenter.forward;
        _currentGaze = state.GazeViewspace;

        if(!_bReplayPauzed)
            ++_replayIndex;

        _nextReplayDeltaTime = _replayIndex < _data.Count ? _data[_replayIndex].GameTime - state.GameTime : 0;
    }

    int CheckReplayIndex(int index)
    {
        return GetReplayIndex(EyeTrackingRecordingManager.ReplayTime, index);
    }

    int GetReplayIndex(float time, int guessIndex)
    {
        if (guessIndex < 0 || guessIndex >= _data.Count)
        {
            Debug.Log("GazeRecorder::GetReplayIndex - index invalid: " + guessIndex + " of " + _data.Count);
            _bReplaying = false;
            return guessIndex;
        }

        //TODO if the time difference is a lot bigger than delta time, consider skipping the index further
        var timeDifference = GetTimeDifference(guessIndex, time);
        if (guessIndex - 1 >= 0 && GetTimeDifference(guessIndex - 1, time) < timeDifference)
        {
            return GetReplayIndex(time, guessIndex - 1);
        }

        if (guessIndex + 1 < _data.Count && GetTimeDifference(guessIndex + 1, time) < timeDifference)
        {
            return GetReplayIndex(time, guessIndex + 1);
        }

        return guessIndex;
    }

    float GetTimeDifference(int index, float time)
    {
        return Mathf.Abs(_data[index].GameTime - time);
    }

    public List<Vector2> GetGazesViewspaceInTimeframe(float startTime, float endTime, ref int lastFoundIndex, int guessIndex = 0)
    {
        var gazes = new List<Vector2>();
        var startIndex = GetReplayIndex(startTime, guessIndex);
        var endIndex = GetReplayIndex(endTime, startIndex);
        for (int i = startIndex; i <= endIndex; ++i)
        {
            gazes.Add(_data[i].GazeViewspace);
        }

        lastFoundIndex = endIndex;
        return gazes;
    }
}
