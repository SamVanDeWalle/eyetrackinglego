﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnalysisInfoUI : MonoBehaviour
{
    public Text TimestampText;
    public Text FrameText;

	void Update ()
    {
        TimestampText.text = string.Concat("Timestamp: ", Time.time.ToString());
        FrameText.text = "Frame: " + GazeAnalysis.Framecounter;
    }
}
