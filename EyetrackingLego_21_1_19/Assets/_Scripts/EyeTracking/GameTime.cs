﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameTime 
{
    public static bool Simulated;
	public static float DeltaTime { get { return Simulated ? SimulatedDeltaTime : UnityEngine.Time.deltaTime; } private set { } }
    public static float Time { get { return Simulated ? SimulatedTime : UnityEngine.Time.time; } private set { } }
    public static float SimulatedDeltaTime;
    public static float SimulatedTime;
}
