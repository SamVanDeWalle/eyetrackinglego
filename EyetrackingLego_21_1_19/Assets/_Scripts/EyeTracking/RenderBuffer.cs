﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Threading;
using UnityEngine;

public class RenderCapturer
{
    public LockQueue<byte[]> ByteQueue = new LockQueue<byte[]>();
    public List<int> RenderSizes = new List<int>();
    public int Index;

    Thread _captureThread;
    bool _bCapturing;
    public int Count;

    public RenderCapturer(int index)
    {
        Index = index;
    }

    public void StartCapturing()
    {
        _bCapturing = true;
        _captureThread = new Thread(new ThreadStart(Capture));
        _captureThread.Start();
    }

    public void StopCapturing()
    {
        _bCapturing = false;
        _captureThread.Abort();
        _captureThread.Join();
    }
    
    void Capture()
    {
        var path = "D:/Documents/DAER_Projects/VR_Eyetracking/Dump/renderDump_" + Index + ".dat";
        using (FileStream fs = new FileStream(path, FileMode.Append))
        {
            BinaryWriter bw = new BinaryWriter(fs);
            int pos = 0;
            int savedIndex = 0;
            while (_bCapturing)
            {
                if (ByteQueue.Count > 0)
                {

                    //var data = Compress(_byteQueues[(int)id].Dequeue());
                    var data = Compress(ByteQueue.Dequeue());
                    //var data = _byteQueues[(int)id].Dequeue();
                    RenderSizes.Add(data.Length);
                    bw.Write(data, 0, data.Length);
                    pos += data.Length;
                    ++savedIndex;
                    ++Count;
                }
            }

            bw.Close();
        }
    }

    public static byte[] Compress(byte[] data)
    {
        MemoryStream output = new MemoryStream();
        using (DeflateStream dstream = new DeflateStream(output, CompressionMode.Compress))
        {
            dstream.Write(data, 0, data.Length);
        }
        return output.ToArray();
    }

    public static byte[] Decompress(byte[] data)
    {
        MemoryStream input = new MemoryStream(data);
        MemoryStream output = new MemoryStream();
        using (DeflateStream dstream = new DeflateStream(input, CompressionMode.Decompress))
        {
            dstream.CopyTo(output);
        }
        return output.ToArray();
    }
}

public class RenderBuffer : MonoBehaviour
{
    public Camera Camera;
    public Camera ReplayCamera;
    public Material ReplayMat;

    RenderCapturer[] _capturers = new RenderCapturer[THREAD_COUNT];
    static Texture2D _texture;
    bool _bCapturing;
    bool _bReplaying;
    int _width, _height;
    static int _captureIndex;
    int _replayIndex;
    int _threadIndex;

    const int WIDTH = 1920;
    const int HEIGHT = 1080;
    const int THREAD_COUNT = 6;

    Coroutine _captureRoutine;

    void Awake()
    {
        EyeTrackingRecordingManager.OnReplayStarted += StartCapturing;
        Application.targetFrameRate = 90;
    }

    void Start()
    {
        StartCapturing();
    }

	void StartCapturing()
    {
        _width = Screen.width;
        _height = Screen.height;
        _texture = new Texture2D(WIDTH, HEIGHT);
        _bCapturing = true;

        for(int i = 0; i < THREAD_COUNT; ++i)
        {
            _capturers[i] = new RenderCapturer(i);
            _capturers[i].StartCapturing();
        }
    }

    void StopCapturing()
    {
        _bCapturing = false;
        
        for (int i = 0; i < THREAD_COUNT; ++i)
        {
            _capturers[i].StopCapturing();
        }
    }

    void StartReplaying()
    {
        StopCapturing();
        _bReplaying = true;
        _replayIndex = 0;
        StartCoroutine(Replay());
        Debug.Log("Started Replay");
    }

    void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.F12))
            StartCapturing();
        if (Input.GetKeyDown(KeyCode.Space))
            StartReplaying();
    }

    void OnPostRender()
    {
        if(_bCapturing)
        {
            //RenderTexture.active = Camera.targetTexture;
            _texture.ReadPixels(new Rect(0, 0, WIDTH, HEIGHT), 0, 0);
            _texture.Apply();
            
            _capturers[_threadIndex].ByteQueue.Enqueue(_texture.GetRawTextureData());
            _threadIndex = (_threadIndex + 1) % THREAD_COUNT;
            //Debug.Log("ti: " + _threadIndex + " queueSize: " + _capturers[_threadIndex].ByteQueue.Count);

            ++_captureIndex;
        }
    }

    IEnumerator Replay()
    {
        yield return new WaitForSeconds(1);

        FileStream[] fstreams = new FileStream[THREAD_COUNT];
        BinaryReader[] readers = new BinaryReader[THREAD_COUNT];
        List<int> positions = new List<int>();
        int totalRenderCount = 0;
                    
        for(int i = 0; i < THREAD_COUNT; ++i)
        {
            var path = "D:/Documents/DAER_Projects/VR_Eyetracking/Dump/renderDump_" + i + ".dat";
            fstreams[i] = new FileStream(path, FileMode.Open);
            readers[i] = new BinaryReader(fstreams[i]);
            positions.Add(0);
            totalRenderCount += _capturers[i].RenderSizes.Count;
        }

        Debug.Log("total renders: " + _captureIndex);
        Debug.Log("0 renders: " + _capturers[0].Count);
        Debug.Log("totalRenderCount(replay): " + totalRenderCount);

        while (_bReplaying && _replayIndex < totalRenderCount)
        {
            int threadIndex = _replayIndex % THREAD_COUNT;
            int iteration = Mathf.FloorToInt(_replayIndex * 1.0f / THREAD_COUNT);
            var size = _capturers[threadIndex].RenderSizes[iteration];
            var data = new byte[size];
            readers[threadIndex].Read(data, 0, size);
            positions[threadIndex] += data.Length;
            _texture.LoadRawTextureData(RenderCapturer.Decompress(data));
            //_texture.LoadRawTextureData(data);
            _texture.Apply();
            ReplayMat.mainTexture = _texture;
            ++_replayIndex;

            //Debug.Log(threadIndex + " | " + iteration + " | " + _replayIndex);
            yield return null;
        }

        for (int i = 0; i < THREAD_COUNT; ++i)
        {
            readers[i].Close();
            fstreams[i].Close();
        }
    }

    void OnDestroy()
    {
        if(_bCapturing)
            StopCapturing();
        Debug.Log("total renders: " + _captureIndex);
        
        GC.Collect();
    }

    //void OnGUI()
    //{
    //    if (_bReplaying)
    //    {
    //        GUI.DrawTexture(new Rect(0, 0, WIDTH, HEIGHT), _texture);
    //    }
    //}
}
