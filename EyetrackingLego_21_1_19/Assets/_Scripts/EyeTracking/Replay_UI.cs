﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Replay_UI : MonoBehaviour 
{
    public GazeRecorder ProgressRecorder;
    public Dropdown FolderDropDown;
    public Image ProgressBar;

    void Awake()
    {
        EyeTrackingRecordingManager.OnReplayFoldersLoaded -= SetFolderDropDownOptions;
        EyeTrackingRecordingManager.OnReplayFoldersLoaded += SetFolderDropDownOptions;
    }

    void Start()
    {
        Cursor.visible = true;
    }

    void Update()
    {
        ProgressBar.fillAmount = ProgressRecorder.ReplayProgress;
    }

    void SetFolderDropDownOptions()
    {
        FolderDropDown.options.Clear();
        foreach(var folder in EyeTrackingRecordingManager.ReplayFolders)
        {
            var option = new Dropdown.OptionData(folder);
            FolderDropDown.options.Add(option);
        }

        FolderDropDown.RefreshShownValue();
        FolderDropDown.onValueChanged.AddListener(delegate { SetReplayFolder(); });
        SetReplayFolder();
    }

    void SetReplayFolder()
    {
        if(EyeTrackingRecordingManager.Instance == null)
        {
            Debug.Log("Replay_UI::SetReplayFolder - EyeTrackingRecordingManager not ready");
            return;
        }

        EyeTrackingRecordingManager.Instance.ReplayFolder = FolderDropDown.options[FolderDropDown.value].text + "\\";
    }
}
