﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GazeVisualizer : MonoBehaviour
{
    public GazeRecorder GazeRecorder;
    public Material RayMaterial;
    public Texture2D GazeTexture;
    public Color HeadColor;
    public Color StartColor, EndColor;
    public bool ShowGaze = true;
    public bool ShowCloud = true;
    public bool ShowRayBuffer;

    LimitedQueue<Vector2> _gazeBuffer = new LimitedQueue<Vector2>(BUFFERSIZE);
    List<Ray> RayBuffer = new List<Ray>();
    Camera _camera;
    Color _color;

    const int BUFFERSIZE = 30;

    void Awake()
    {
        _camera = GetComponent<Camera>();
        var radAngle = _camera.fieldOfView * Mathf.Deg2Rad;
        var radHFOV = 2 * Mathf.Atan(Mathf.Tan(radAngle / 2) * _camera.aspect);
        var hFOV = Mathf.Rad2Deg * radHFOV;
        Debug.Log("H_FOV:" + hFOV);
        _color = new Color(RayMaterial.color.r, RayMaterial.color.g, RayMaterial.color.b, RayMaterial.color.a);
    }

    void Update()
    {
        RayBuffer.Add(GazeRecorder.CurrentEyeCenterRay);
        var currentGaze = GazeRecorder.CurrentGaze;
        var viewGaze = new Vector3(currentGaze.x, 1 - currentGaze.y, 0);
        _gazeBuffer.Enqueue(_camera.ViewportToScreenPoint(viewGaze));
    }

    void OnPostRender()
    {
        if (!ShowRayBuffer || !RayMaterial)
            return;

        //GL.PushMatrix();
        RayMaterial.SetPass(0);
        //GL.LoadOrtho();
        GL.Begin(GL.LINES);
        GL.Color(_color);
        for(int i = 0; i < RayBuffer.Count - 1; ++i)
        {
            //GL.Vertex(RayBuffer[i].origin);
            //GL.Vertex(RayBuffer[i].origin + RayBuffer[i].direction * 10);
            //var color = Color.Lerp(StartColor, EndColor, i * 1.0f / RayBuffer.Count);
            Debug.DrawRay(RayBuffer[i].origin, RayBuffer[i].direction, _color);
            //if(i < RayBuffer.Count - 1)
                //Debug.DrawLine(RayBuffer[i].origin, RayBuffer[i + 1].origin, HeadColor);
        }
        //GL.Vertex(GazeRecorder.CurrentEyeCenterRay.origin);
        //GL.Vertex(GazeRecorder.CurrentEyeCenterRay.origin + GazeRecorder.CurrentEyeCenterRay.direction * 10);
        GL.End();
        //GL.PopMatrix();

        //Debug.DrawRay(GazeRecorder.CurrentEyeCenterRay.origin, GazeRecorder.CurrentEyeCenterRay.direction, _color);
    }

    void OnGUI()
    {
        if(_gazeBuffer.Count > 0 && ShowGaze)
        {
            if(ShowCloud)
            {
                GUI.color = Color.yellow;
                foreach (var gaze in _gazeBuffer)
                {
                    GUI.DrawTexture(new Rect(gaze.x, gaze.y, 5, 5), GazeTexture);
                }
            }
            
            GUI.color = Color.red;
            GUI.DrawTexture(new Rect(_gazeBuffer.Last.x, _gazeBuffer.Last.y, 10, 10), GazeTexture);
            GUI.color = Color.white;
        }
    }

    public void ToggleShowGaze(bool value)
    {
        ShowGaze = !ShowGaze;
    }

    public void ToggleShowCloud(bool value)
    {
        ShowCloud = !ShowCloud;
    }
}
