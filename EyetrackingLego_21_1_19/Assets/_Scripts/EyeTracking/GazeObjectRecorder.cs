﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GazeObjectData
{
    public List<GazeObject> Objects;
    public float GameTime;
}

public class GazeObjectRecorder : MonoBehaviour
{
    public GazeObjectFinder Finder;
    List<GazeObjectData> _data = new List<GazeObjectData>();
    bool _bRecording;

    void Awake()
    {
        EyeTrackingRecordingManager.OnReplayStarted += StartRecording;
    }

    void OnDestroy()
    {
        if(_bRecording && _data.Count > 0)
            Save();
    }

    void Update()
    {
        if (_bRecording)
            Record();
    }

    void StartRecording()
    {
        _data.Clear();
        _bRecording = true;
    }

    void Record()
    {
        var god = new GazeObjectData();
        god.Objects = new List<GazeObject>(GazeObjectFinder.GazeObjects);
        god.GameTime = EyeTrackingRecordingManager.ReplayTime;
        _data.Add(god);
    }

    void Save()
    {
        var container = new IOList<GazeObjectData>(_data);
        container.Save(EyeTrackingRecordingManager.ReplayPath + "gazeobjectdata.json");
    }
}
