﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SequencePointGaze
{
    public Vector2 SequencePoint;
    public Vector3 SequencePointLocal;
    public Vector2 MeanGaze;
    public List<Vector2> Gazes;
}

public class CustomCalibration : MonoBehaviour
{
    public GazeRecorder GazeRecorder;
    public PeriferieTest PeriferieTest;
    public Vector2 GazeTimeframe = new Vector2(1.5f, 2.5f);
    public GameObject SequencePointPrefab, MeanGazePrefab, RawGazePrefab;
    public Camera ReplayCamera;

    List<SequencePointGaze> _sequencePointGazeData = new List<SequencePointGaze>();

    void Awake()
    {
        EyeTrackingRecordingManager.OnReplayStarted += Validate;
    }

    void Validate()
    {
        CalculateSequencePointGazeData();
        Visualize();
    }

    void CalculateSequencePointGazeData()
    {
        // Load the statuses from replay folder, if it is used directly we don't need this
        PeriferieTest.LoadSequencePointStatuses(EyeTrackingRecordingManager.ReplayPath);
        _sequencePointGazeData.Clear();
        int guessIndex = 0;
        for (int i = 0; i < PeriferieTest.PointStatuses.Count; ++i)
        {
            var sequencePointStatus = PeriferieTest.PointStatuses[i];
            var sequencePointGaze = new SequencePointGaze();
            sequencePointGaze.SequencePoint = sequencePointStatus.ViewPosition;
            sequencePointGaze.SequencePointLocal = sequencePointStatus.LocalPosition;
            var startTime = sequencePointStatus.GameTime + GazeTimeframe.x;
            var endTime = sequencePointStatus.GameTime + GazeTimeframe.y;
            var gazes = GazeRecorder.GetGazesViewspaceInTimeframe(startTime, endTime, ref guessIndex, guessIndex);
            sequencePointGaze.Gazes = gazes;
            sequencePointGaze.MeanGaze = GetMeanGaze(gazes);
            _sequencePointGazeData.Add(sequencePointGaze);
        }
    }

    Vector2 GetMeanGaze(List<Vector2> gazes)
    {
        var meanGaze = new Vector2();
        if (gazes == null || gazes.Count == 0)
        {
            Debug.LogWarning("CustomCalibration::GetMeanGaze - No gazes given!");
            return meanGaze;
        }

        for(int i = 0; i < gazes.Count; ++i)
        {
            meanGaze += gazes[i];
            //Debug.Log("gaze: " + gazes[i]);
        }

        meanGaze /= gazes.Count;
        //meanGaze = gazes[0];
        return meanGaze;
    }

    void Visualize()
    {
        for(int i = 0; i < _sequencePointGazeData.Count; ++i)
        {
            var sequencePointGO = Instantiate(SequencePointPrefab);
            var viewPos = _sequencePointGazeData[i].SequencePoint;
            var position = ReplayCamera.ViewportToWorldPoint(new Vector3(viewPos.x, viewPos.y, 2));
            sequencePointGO.transform.SetParent(ReplayCamera.transform);
            sequencePointGO.transform.localPosition = _sequencePointGazeData[i].SequencePointLocal;
            sequencePointGO.transform.forward = ReplayCamera.transform.position - sequencePointGO.transform.position;

            var gazes = _sequencePointGazeData[i].Gazes;
            for (int g = 0; g < gazes.Count; ++g)
            {
                var gazePointGO = Instantiate(RawGazePrefab);
                var rawGazeViewPos = gazes[g];
                var rawGazePosition = ReplayCamera.ViewportToWorldPoint(new Vector3(rawGazeViewPos.x, rawGazeViewPos.y, 2.02f));
                gazePointGO.transform.SetParent(ReplayCamera.transform);
                gazePointGO.transform.position = rawGazePosition;
                gazePointGO.transform.forward = ReplayCamera.transform.position - gazePointGO.transform.position;
            }

            var meanGazePointGO = Instantiate(MeanGazePrefab);
            var gazeViewPos = _sequencePointGazeData[i].MeanGaze;
            var gazePosition = ReplayCamera.ViewportToWorldPoint(new Vector3(gazeViewPos.x, gazeViewPos.y, 2.02f));
            meanGazePointGO.transform.SetParent(ReplayCamera.transform);
            meanGazePointGO.transform.position = gazePosition;
            meanGazePointGO.transform.forward = ReplayCamera.transform.position - meanGazePointGO.transform.position;
        }
    }
}
