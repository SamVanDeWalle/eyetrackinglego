﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ObjectSegmentation : MonoBehaviour
{
    public Shader Shader;

    public void Awake()
    {
        if (Shader)
        {
            GetComponent<Camera>().SetReplacementShader(Shader, null);
        }
    }
}
