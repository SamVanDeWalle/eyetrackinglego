﻿/*
 * 
 * http://developer.tobiipro.com/unity/unity-sdk-reference-guide.html
 * 
 * 
 */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.Research;
using UnityEngine.UI;
using Helpers;

public enum Eyetracker
{
    Tobii,
    Pupil
}

public class EyeTrackingManager : Singleton<EyeTrackingManager>
{
    public Transform LeftEye, RightEye, EyeCenter;
    public Transform TobiiHMDCenter;
    public Transform Test3DGaze;
    public Transform CenterPoint;
    public Text DebugText;
    public Camera Camera, NeutralCamera;
    public VisualizePoint PointVisualiser;
    public Transform GazeCheckPlane;
    public Transform Indicator;
    public Transform CalibrationPoint;
    public GameObject CalibrationBG;
    public float CalibrationPointDuration = 3.0f;
    public static float LeftEyeDiameter { get { return _leftEyeDiameter; } private set { } }
    public static float RightEyeDiameter { get { return _rightEyeDiameter; } private set { } }
    public static Vector3 GazeOrigin { get { return _gazeOrigin; } private set { } }
    public static Vector3 GazeDirection { get { return _gazeDirection; } private set { } }
    public static Vector3 GazeLeftOrigin { get { return _gazeLeftOrigin; } private set { } }
    public static Vector3 GazeLeftDirection { get { return _gazeLeftDirection; } private set { } }
    public static Vector3 GazeRightOrigin { get { return _gazeRightOrigin; } private set { } }
    public static Vector3 GazeRightDirection { get { return _gazeRightDirection; } private set { } }
    public static Vector2 GazeViewspace { get { return _gazeViewspace; } private set { } }
    public static long DeviceTimeStamp { get { return _deviceTimeStamp; } private set { } }
    public static long SystemTimeStamp { get { return _systemTimeStamp; } private set { } }
    public bool ShowGaze;
    public Eyetracker Eyetracker;
    public static IEyeTracker TobiiTracker { get { return _tobiiTracker; } private set { } }

    Coroutine _calibrationRoutine;
    Collider _collider;
    Vector3 _leftEyePos, _rightEyePos;
    static float _leftEyeDiameter;
    static float _rightEyeDiameter;
    static Vector3 _gazeOrigin;
    static Vector3 _gazeDirection;
    static Vector3 _gazeLeftOrigin;
    static Vector3 _gazeLeftDirection;
    static Vector3 _gazeRightOrigin;
    static Vector3 _gazeRightDirection;
    static Vector2 _gazeViewspace;
    static long _deviceTimeStamp;
    static long _systemTimeStamp;
    Vector3 _leftEyeDirection, _rightEyeDirection;
    static IEyeTracker _tobiiTracker;
    PupilGazeTracker _pupilTracker;
    bool _bTracking;


    public delegate void actionDone();
    public static event actionDone OnCalibrationDone;

    void Start()
    {
        CalibrationPoint.gameObject.SetActive(false);
        CalibrationBG.SetActive(false);
        _collider = GazeCheckPlane.GetComponent<Collider>();
        Setup();
    }

    void OnDestroy()
    {
        if (_tobiiTracker != null)
            UnsubscribeTobii();
        if (_pupilTracker != null)
            UnsubscribePupil();
    }

    void Setup()
    {
        switch (Eyetracker)
        {
            case Eyetracker.Tobii:
                var trackers = EyeTrackingOperations.FindAllEyeTrackers();
                if (trackers.Count <= 0)
                {
                    Debug.LogWarning("EyeTrackingManager::SetupEyetracking - Could not find EyeTracker");
                    return;
                }

                _tobiiTracker = trackers[0];
                if (_tobiiTracker != null)
                    SubscribeTobii();
                break;
            case Eyetracker.Pupil:
                _pupilTracker = PupilGazeTracker.Instance;
                if (_pupilTracker != null)
                    SubscribePupil();
                break;
        }
        
    }

    void StartCalibration()
    {
        switch (Eyetracker)
        {
            case Eyetracker.Tobii:
                if (_calibrationRoutine == null && _tobiiTracker != null)
                {
                    _calibrationRoutine = StartCoroutine(Calibrate());
                }
                break;
            case Eyetracker.Pupil:
                if (PupilTools.IsConnected && !PupilTools.IsCalibrating)
                {
                    PupilTools.StartCalibration();
                    PupilTools.OnCalibrationEnded += PupilCalibrationDone;
                }
                break;
        }

        Debug.Log("EyeTrackingManager::StartCalibration - Calibration started");
    }

    void PupilCalibrationDone()
    {
        Debug.Log("Calibration done");
        PupilTools.IsGazing = true;
        PupilTools.SubscribeTo("gaze");
        //_pupilTracker.StartVisualizingGaze();

        if (OnCalibrationDone != null)
            OnCalibrationDone();
    }

    IEnumerator Calibrate()
    {
        HMDBasedCalibration calibration = new HMDBasedCalibration(_tobiiTracker);
        calibration.EnterCalibrationMode();

        var pointsToCalibrate = new Point3D[]
        {
            new Point3D(500f, 500f, 2000f),
            new Point3D(-500f, 500f, 2000f),
            new Point3D(-500f, -500f, 2000f),
            new Point3D(500f, -500f, 2000f),
            new Point3D(0f, 0f, 2000f),
        };

        CalibrationBG.SetActive(true);
        CalibrationPoint.gameObject.SetActive(true);
        Indicator.gameObject.SetActive(false);

        foreach(var point in pointsToCalibrate)
        {
            var localPos = HMDToUnity(point);
            CalibrationPoint.localPosition = localPos;
            Debug.Log("lp: " + localPos);

            yield return new WaitForSeconds(CalibrationPointDuration);

            CalibrationStatus status = calibration.CollectData(point);
            if(status != CalibrationStatus.Success)
            {
                calibration.CollectData(point);
            }
        }

        CalibrationBG.SetActive(false);
        CalibrationPoint.gameObject.SetActive(false);
        Indicator.gameObject.SetActive(true);

        HMDCalibrationResult calibrationResult = calibration.ComputeAndApply();
        Debug.Log("EyeTrackingManager::Calibrate - Status: " + calibrationResult.Status);

        calibration.LeaveCalibrationMode();
        Debug.Log("EyeTrackingManager::Calibrate - Calibration done");

        _calibrationRoutine = null;

        if (OnCalibrationDone != null)
            OnCalibrationDone();
    }

    void SubscribeTobii()
    {
        _tobiiTracker.HMDGazeDataReceived += ReceiveGazeData;
        _bTracking = true;
        Debug.Log("EyeTrackingManager::SubscribeTobii - Subscribed to gaze data (Tobii)");
    }

    void UnsubscribeTobii()
    {
        _tobiiTracker.HMDGazeDataReceived -= ReceiveGazeData;
        Debug.Log("EyeTrackingManager::UnsubscribeTobii - Unsubscribed from gaze data (Tobii)");
    }

    void SubscribePupil()
    {
        _pupilTracker.OnUpdate += SetPupilGazeData;
        _bTracking = true;
        Debug.Log("EyeTrackingManager::SubscribePupil - Subscribed to gaze data (Pupil)");
    }

    void UnsubscribePupil()
    {
        _pupilTracker.OnUpdate -= SetPupilGazeData;
        Debug.Log("EyeTrackingManager::UnsubscribePupil - Unsubscribed to gaze data (Pupil)");
    }

    void ReceiveGazeData(object sender, HMDGazeDataEventArgs args)
    {
        //Debug.Log("Update interval (µs): " + (args.SystemTimeStamp -_systemTimeStamp)); //120 Hz
        _deviceTimeStamp = args.DeviceTimeStamp;
        _systemTimeStamp = args.SystemTimeStamp;
        //Debug.Log("ReceiveGazeData!");
        if (args.LeftEye.GazeOrigin.Validity == Validity.Valid && args.LeftEye.GazeDirection.Validity == Validity.Valid)
        {
            var leftOrigin = args.LeftEye.GazeOrigin.PositionInHMDCoordinates;
            _leftEyePos = new Vector3(-leftOrigin.X * 0.001f, leftOrigin.Y * 0.001f, leftOrigin.Z * 0.001f);
            var leftDirection = args.LeftEye.GazeDirection.UnitVector;
            _leftEyeDirection = new Vector3(-leftDirection.X, leftDirection.Y, leftDirection.Z);
            _leftEyeDiameter = args.LeftEye.Pupil.PupilDiameter;          
        }
        else
        {
            _leftEyeDiameter = 0;
            return;
            Debug.Log("invalid data!");
        }
        if(args.RightEye.GazeOrigin.Validity == Validity.Valid && args.RightEye.GazeDirection.Validity == Validity.Valid)
        { 
            var rightOrigin = args.RightEye.GazeOrigin.PositionInHMDCoordinates;
            _rightEyePos = new Vector3(-rightOrigin.X * 0.001f, rightOrigin.Y * 0.001f, rightOrigin.Z * 0.001f);
            var rightDirection = args.RightEye.GazeDirection.UnitVector;
            _rightEyeDirection = new Vector3(-rightDirection.X, rightDirection.Y, rightDirection.Z);
            _rightEyeDiameter = args.RightEye.Pupil.PupilDiameter;
        }
        else
        {
            _rightEyeDiameter = 0;
            return;
            Debug.Log("invalid data!");
        }
        //gazeAnalysis.ProcessData();

    }

    void SetPupilGazeData()
    {
        //_gazeViewspace = PupilData._2D.GazePosition;
        //Debug.Log("setting pupil gaze: " + _gazeViewspace);
    }

    void FixedUpdate() //Experimental, was Update()
    {
        if (Input.GetKeyDown(KeyCode.F6)) 
        {
            StartCalibration();
        }

        if (Input.GetKeyDown(KeyCode.F9))
        {
            ShowGaze = !ShowGaze;
        }

        if (!_bTracking || _calibrationRoutine != null)
            return;

        Indicator.gameObject.SetActive(ShowGaze);

        switch (Eyetracker)
        {
            case Eyetracker.Tobii:
                LeftEye.localPosition = _leftEyePos;
                RightEye.localPosition = _rightEyePos;
                EyeCenter.localPosition = (_leftEyePos + _rightEyePos) / 2.0f;
                //LeftEye.forward = LeftEye.localToWorldMatrix.MultiplyVector(_leftEyeDirection);
                if(_leftEyeDirection != Vector3.zero)
                    LeftEye.forward = TobiiHMDCenter.TransformDirection(_leftEyeDirection);
                //RightEye.forward = RightEye.localToWorldMatrix.MultiplyVector(_rightEyeDirection);
                if (_rightEyeDirection != Vector3.zero)
                    RightEye.forward = TobiiHMDCenter.TransformDirection(_rightEyeDirection);
                EyeCenter.forward = LeftEye.forward + RightEye.forward;
                //Debug.Log("_leftEyeDirection" + _leftEyeDirection + "LeftEye.forward " + LeftEye.forward);    
                //var screenspace = Camera.WorldToViewportPoint(LeftEye.transform.position);
                //DebugText.text = "x: " + _leftEyePos.x + " y: " + _leftEyePos.y + " z: " + _leftEyePos.z;

                var ray = new Ray(EyeCenter.position, EyeCenter.forward);
                //Debug.DrawRay(EyeCenter.position, EyeCenter.forward * 10, Color.red);

                RaycastHit hitInfo;
                if (_collider.Raycast(ray, out hitInfo, 10))
                {
                    Indicator.position = hitInfo.point;
                    Indicator.forward = Camera.transform.position - Indicator.position;
                   // Debug.Log("Hit object: " + hitInfo.collider.gameObject.name);
                }

                _gazeOrigin = EyeCenter.position;
                _gazeDirection = EyeCenter.forward;
                _gazeViewspace = GetScreenPosition(Indicator.position);
                //_gazeViewspace = Input.mousePosition;

                _gazeLeftOrigin = LeftEye.position;
                _gazeLeftDirection = LeftEye.forward;
                _gazeRightOrigin = RightEye.position;
                _gazeRightDirection = RightEye.forward;
                //PointVisualiser.SetPosition(_gazeViewspace.x, 1- _gazeViewspace.y);
                break;
            case Eyetracker.Pupil:
                //Test3DGaze.position = PupilData._3D.LeftEyeCenter;
                //Test3DGaze.forward = PupilData._3D.LeftGazeNormal;
                _gazeOrigin = UnityEngine.XR.InputTracking.GetLocalPosition(UnityEngine.XR.XRNode.CenterEye);
                Vector3 gazePoint3D = _gazeViewspace;
                gazePoint3D.z = 2;//Camera.transform.position.z;
                _gazeDirection = Camera.ViewportPointToRay(gazePoint3D).direction;
                gazePoint3D = Camera.ViewportToWorldPoint(gazePoint3D);
                _gazeOrigin = gazePoint3D;
                Test3DGaze.localPosition = _gazeOrigin;
                Test3DGaze.forward = _gazeDirection;
                ray = new Ray(gazePoint3D, _gazeDirection);
                RaycastHit hitInfo2;
                if (_collider.Raycast(ray, out hitInfo2, 10))
                {
                    Indicator.position = hitInfo2.point;
                    Indicator.forward = Camera.transform.position - Indicator.position;
                }
                break;
        }
        
    }

    public Vector2 GetViewPosition(Vector3 worldPosition)
    {
        //return Camera.WorldToViewportPoint(worldPosition);
        return NeutralCamera.WorldToViewportPoint(worldPosition);
    }

    public Vector2 GetViewPositionEyeCamera(Vector3 worldPosition)
    {
        return Camera.WorldToViewportPoint(worldPosition, Camera.MonoOrStereoscopicEye.Mono);
        //return Camera.WorldToViewportPoint(worldPosition);
    }

    public Vector2 GetScreenPosition(Vector3 worldPosition)
    {
        return Camera.WorldToScreenPoint(worldPosition);
    }

    static Vector3 HMDToUnity(Point3D point)
    {
        return new Vector3(-point.X * 0.001f, point.Y * 0.001f, point.Z * 0.001f);
    }

    public Vector3 ProjectedOnGazePlane(Vector3 worldPosition)
    {
        Vector3 position = new Vector3();
        var ray = new Ray(EyeCenter.position, (worldPosition - EyeCenter.position).normalized);
        RaycastHit hitInfo;
        if (_collider.Raycast(ray, out hitInfo, 10))
        {
            position = hitInfo.point;
        }

        return position;
    }

    public Vector2 ProjectedOnGazePlaneScreen(Vector3 worldPosition)
    {
        return GetScreenPosition(ProjectedOnGazePlane(worldPosition));
    }
}
