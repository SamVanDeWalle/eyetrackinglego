﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Helpers;
using UnityEngine.SceneManagement;

public class DemoHandler : Singleton<DemoHandler>
{
    public List<int> LockOnPlaceTypeIDs = new List<int>();

    void Start()
    {
        //ObjectStateRecorder.Instance.PlaybackRecording();
        //PickupController.OnAPPlacedGlobal += AssemblyPiecePlaced;
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.R))
        {

            //foreach(var ap in AssemblyPieceManager.AssemblyPieces)
            //{
            //    ap.Reset();
            //    ap.GetComponent<Rigidbody>().isKinematic = true;
            //}
            //
            //ObjectStateRecorder.Instance.clearfordemo();
            //ObjectStateRecorder.Instance.PlaybackRecording();

            var sceneName = SceneManager.GetActiveScene().name;
            SceneManager.LoadSceneAsync(sceneName);
        }

        //Only allow 1 controller to pickup object at a time
        //for(int i = 0; i < VR_Main.Instance.PickupControllers.Count; ++i)
        //{
        //    VR_Main.Instance.PickupControllers[i].AllowPicking = true;
        //    if (VR_Main.Instance.PickupControllers[i].CarriesObject)
        //    {
        //        EnablePickingOnly(i);
        //        break;
        //    }
        //}
    }

    void EnablePickingOnly(int index)
    {
        for (int i = 0; i < VR_Main.Instance.PickupControllers.Count; ++i)
        {
            if (i != index)
            {
                VR_Main.Instance.PickupControllers[i].AllowPicking = false;
            }
            else
            {
                VR_Main.Instance.PickupControllers[i].AllowPicking = true;
            }
        }
    }

    void AssemblyPiecePlaced(AssemblyPiece piece)
    {
        if (LockOnPlaceTypeIDs.Contains(piece.TypeID))
        {
            piece.Lock();
            piece.OnDetach += piece.Unlock;
        }
    }
}
