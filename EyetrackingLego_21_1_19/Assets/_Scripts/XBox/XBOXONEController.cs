﻿using UnityEngine;
using System.Collections;

namespace XBOX
{
    public class XBOXONEController
    {
        public static string A = "XBOX_A";
        public static string B = "XBOX_B";
        public static string X = "XBOX_X";
        public static string Y = "XBOX_Y";

        public static string Settings = "XBOX_Settings";
        public static string Menu = "XBOX_Menu";

        public static string BackButtonLeft = "XBOX_Back_Button_Left";
        public static string BackButtonRight = "XBOX_Back_Button_Right";

        public static string BackTriggerLeft = "XBOX_Back_Trigger_Left";
        public static string BackTriggerRight = "XBOX_Back_Trigger_Right";

        public static string LeftJoystickHorizontal = "XBOX_Left_Joystick_Horizontal";
        public static string LeftJoystickVertical = "XBOX_Left_Joystick_Vertical";

        public static string RightJoystickHorizontal = "XBOX_Right_Joystick_Horizontal";
        public static string RightJoystickVertical = "XBOX_Right_Joystick_Vertical";

        public static string LeftRight = "XBOX_RLUD_Horizontal";
        public static string DownUp = "XBOX_RLUD_Vertical";
    }
}