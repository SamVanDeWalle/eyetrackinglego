﻿using UnityEngine;
using System.Collections;
using XBOX;

public class InputChecker : MonoBehaviour
{
	void Update ()
    {

        if (Input.GetButtonUp(XBOXONEController.A))
            Debug.Log("Pressed - A");

        if (Input.GetButtonUp(XBOXONEController.B))
            Debug.Log("Pressed - B");

        if (Input.GetButtonUp(XBOXONEController.X))
            Debug.Log("Pressed - X");

        if (Input.GetButtonUp(XBOXONEController.Y))
            Debug.Log("Pressed - Y");


        if (Input.GetButtonUp(XBOXONEController.Settings))
            Debug.Log("Pressed - Settings");

        if (Input.GetButtonUp(XBOXONEController.Menu))
            Debug.Log("Pressed - Menu");


        if (Input.GetButtonUp(XBOXONEController.BackButtonLeft))
            Debug.Log("Pressed - Back button left");

        if (Input.GetButtonUp(XBOXONEController.BackButtonRight))
            Debug.Log("Pressed - Back button right");


        if (Input.GetAxis(XBOXONEController.LeftJoystickHorizontal) < 0)
            Debug.Log("Pressed - LJS - Left");

        if (Input.GetAxis(XBOXONEController.LeftJoystickHorizontal) > 0)
            Debug.Log("Pressed - LJS - Right");

        if (Input.GetAxis(XBOXONEController.LeftJoystickVertical) > 0)
            Debug.Log("Pressed - LJS - Up");

        if (Input.GetAxis(XBOXONEController.LeftJoystickVertical) < 0)
            Debug.Log("Pressed - LJS - Down");


        if (Input.GetAxis(XBOXONEController.RightJoystickHorizontal) < 0)
            Debug.Log("Pressed - RJS - Left");

        if (Input.GetAxis(XBOXONEController.RightJoystickHorizontal) > 0)
            Debug.Log("Pressed - RJS - Right");

        if (Input.GetAxis(XBOXONEController.RightJoystickVertical) > 0)
            Debug.Log("Pressed - RJS - Up");

        if (Input.GetAxis(XBOXONEController.RightJoystickVertical) < 0)
            Debug.Log("Pressed - RJS - Down");


        if (Input.GetAxis(XBOXONEController.LeftRight) < 0)
            Debug.Log("Pressed - RLUD - Left");

        if (Input.GetAxis(XBOXONEController.LeftRight) > 0)
            Debug.Log("Pressed - RLUD - Right");

        if (Input.GetAxis(XBOXONEController.DownUp) > 0)
            Debug.Log("Pressed - RLUD - Up");

        if (Input.GetAxis(XBOXONEController.DownUp) < 0)
            Debug.Log("Pressed - RLUD - Down");


        if (Input.GetAxis(XBOXONEController.BackTriggerLeft) > 0)
            Debug.Log("Pressed - Back Trigger Left");

        if (Input.GetAxis(XBOXONEController.BackTriggerRight) > 0)
            Debug.Log("Pressed - Back Trigger Right");
    }
}
