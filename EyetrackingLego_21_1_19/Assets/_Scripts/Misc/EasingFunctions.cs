﻿using System;
using UnityEngine;

public class EasingFunctions
{
    // -- PUBLIC

    // .. TypeS

    public enum Type
    {
        Out,
        In,
        FarIn,
        InOut,
        OutBounce,
        InElastic,
        InElasticBig,
        OutElastic,
        OutElasticBig,
        BackInCubic,
        Sinus,
        SmallStretch,
        OutOverShoot,
        Regular,
        Cubic
    }

    // .. CONSTRUCTORS

    public EasingFunctions()
    {
    }

    // .. FUNCTIONS

    public static float Ease(
        Type type,
        float normalizedTime,
        float startValue,
        float endValue
        )
    {
        switch ( type )
        {
            case Type.Out:
            {
                return EaseOut( normalizedTime, startValue, endValue );
            }

            case Type.In:
            {
                return EaseIn( normalizedTime, startValue, endValue );
            }

            case Type.FarIn:
            {
                return FarEaseIn( normalizedTime, startValue, endValue );
            }

            case Type.InOut:
            {
                return EaseInOut( normalizedTime, startValue, endValue );
            }

            case Type.OutBounce:
            {
                return EaseOutBounce( normalizedTime, startValue, endValue );
            }

            case Type.InElastic:
            {
                return EaseInElastic( normalizedTime, startValue, endValue );
            }

            case Type.InElasticBig:
            {
                return EaseInElasticBig( normalizedTime, startValue, endValue );
            }

            case Type.OutElastic:
            {
                return EaseOutElastic( normalizedTime, startValue, endValue );
            }

            case Type.OutElasticBig:
            {
                return EaseOutElasticBig( normalizedTime, startValue, endValue );
            }

            case Type.BackInCubic:
            {
                return BackInCubic( normalizedTime, startValue, endValue );
            }

            case Type.Sinus:
            {
                return Sinus( normalizedTime, startValue, endValue );
            }

            case Type.SmallStretch:
            {
                return SmallStretch( normalizedTime, startValue, endValue );
            }

            case Type.OutOverShoot:
            {
                return OutOverShoot( normalizedTime, startValue, endValue );
            }

            case Type.Regular:
            {
                float
                    factor,
                    one_minus_factor;

                factor = normalizedTime;

                one_minus_factor = 1.0f - factor;

                return startValue * one_minus_factor + endValue * factor;
            }
            case Type.Cubic:
            return Cubic( normalizedTime, startValue, endValue );
           
        }

        return Mathf.Lerp( startValue, endValue, normalizedTime );
    }

    //

    public static Vector3 Ease(
        Type type,
        float normalizedTime,
        Vector3 startValue,
        Vector3 endValue
        )
    {
        float
            factor,
            one_minus_factor;

        factor = Ease( type, normalizedTime, 0.0f, 1.0f );

        one_minus_factor = 1.0f - factor;

        return new Vector3(
            startValue.x * one_minus_factor + endValue.x * factor,
            startValue.y * one_minus_factor + endValue.y * factor,
            startValue.z * one_minus_factor + endValue.z * factor
            );
    }

    //

    public static int Ease(
        Type type,
        float normalizedTime,
        int startValue,
        int endValue
        )
    {
        float
            factor,
            one_minus_factor;

        factor = Ease( type, normalizedTime, 0.0f, 1.0f );

        one_minus_factor = 1.0f - factor;

        return ( int ) ( ( float ) startValue * one_minus_factor + ( float) endValue * factor );
    }

    //

    public static Vector2 Ease(
        Type type,
        float normalizedTime,
        Vector2 startValue,
        Vector2 endValue
        )
    {
        float
            factor,
            one_minus_factor;

        factor = Ease( type, normalizedTime, 0.0f, 1.0f );

        one_minus_factor = 1.0f - factor;

        return new Vector2(
            startValue.x * one_minus_factor + endValue.x * factor,
            startValue.y * one_minus_factor + endValue.y * factor
            );
    }

    //

    public static Quaternion Ease(
        Type type,
        float normalizedTime,
        Quaternion startValue,
        Quaternion endValue
        )
    {
        float
            factor,
            one_minus_factor;

        factor = Ease( type, normalizedTime, 0.0f, 1.0f );

        one_minus_factor = 1.0f - factor;

        return new Quaternion(
            startValue.x * one_minus_factor + endValue.x * factor,
            startValue.y * one_minus_factor + endValue.y * factor,
            startValue.z * one_minus_factor + endValue.z * factor,
            startValue.w * one_minus_factor + endValue.w * factor
            );
    }

    //

    public static Color Ease(
        Type type,
        float normalizedTime,
        Color startValue,
        Color endValue
        )
    {
        float
            factor,
            one_minus_factor;

        factor = Ease( type, normalizedTime, 0.0f, 1.0f );

        one_minus_factor = 1.0f - factor;

        return new Color(
            startValue.r * one_minus_factor + endValue.r * factor,
            startValue.g * one_minus_factor + endValue.g * factor,
            startValue.b * one_minus_factor + endValue.b * factor,
            startValue.a * one_minus_factor + endValue.a * factor
            );
    }

    //

    public static float EaseOut(
        float time,
        float startValue,
        float endValue
        )
    {
        float
            ease_value;

        ease_value = 1 - time;
        ease_value = 1 - ease_value * ease_value * ease_value;

        return endValue * ease_value + startValue * ( 1 - ease_value );
    }

    //

    public static float EaseIn(
        float time,
        float startValue,
        float endValue
        )
    {
        float
            ease_value;

        ease_value = time * time * time;

        return endValue * ease_value + startValue * ( 1 - ease_value );
    }

    //

    public static float FarEaseIn(
        float time,
        float startValue,
        float endValue
        )
    {
        float
            ts = time * time,
            tc = ts * time,
            calculated_function = ( -1.8f * tc * ts + 6.6f * ts * ts + -9.5f * tc + 6.9f * ts + -1.2f * time ),
            one_minus_calculated_function = 1 - calculated_function;

        return startValue * one_minus_calculated_function + endValue * calculated_function;
    }

    //

    public static float EaseInOut(
        float time,
        float startValue,
        float endValue
        )
    {
        float
            ts = time * time,
            tc = ts * time,
            ease_value;

        ease_value = 6 * tc * ts - 15 * ts * ts + 10 * tc;

        return endValue * ease_value + startValue * ( 1 - ease_value );
    }

    //

    public static float EaseOutBounce(
        float time,
        float startValue,
        float endValue
        )
    {
        float
            temporary_value;

        if ( time >= 1.0f )
        {
            return endValue;
        }
        else if ( time < 0.363636f )     // 0.363636 == 1 / 2.75f from source code
        {
            temporary_value = ( 7.5625f * time * time );
        }
        else if ( time < 0.727272f )    //== 2.0f * 0.363636f
        {
            temporary_value = time - 0.545454f;    // == 1.5f * 0.363636f
            temporary_value = ( 7.5625f * temporary_value * temporary_value + 0.75f );
        }
        else if ( time < 0.909090f )   // == 2.5f * 0.36363636f
        {
            temporary_value = time - 0.818181f;    // == 2.25f * 0.363636f
            temporary_value = ( 7.5625f * temporary_value * temporary_value + 0.9375f );
        }
        else // if ( time < 1.0f )
        {
            temporary_value = time - 0.9545454f;    // == 2.625f * 0.363636f
            temporary_value = ( 7.5625f * temporary_value * temporary_value + 0.984375f );
        }

        return endValue * temporary_value + startValue * ( 1 - temporary_value );
    }

    //

    public static float EaseInElastic(
        float time,
        float startValue,
        float endValue
        )
    {
        float
            ts = time * time,
            tc = ts * time,
            calculated_function = ( 33.0f * tc * ts - 59.0f * ts * ts + 32.0f * tc - 5.0f * ts ),
            one_minus_calculated_function = 1 - calculated_function;

        return startValue * one_minus_calculated_function + endValue * calculated_function;
    }

    //

    public static float EaseInElasticBig(
        float time,
        float startValue,
        float endValue
        )
    {
        float
            ts = time * time,
            tc = ts * time,
            calculated_function = ( 56.0f * tc * ts - 105.0f * ts * ts + 60.0f * tc - 10.0f * ts ),
            one_minus_calculated_function = 1 - calculated_function;

        return startValue * one_minus_calculated_function + endValue * calculated_function;
    }

    //

    public static float EaseOutElastic(
        float time,
        float startValue,
        float endValue
        )
    {
        float
            ts = time * time,
            tc = ts * time,
            calculated_function = ( 17.095f * tc * ts - 55.9325f * ts * ts + 69.38f * tc - 40.19f * ts + 10.6475f * time ),
            one_minus_calculated_function = 1 - calculated_function;

        return startValue * one_minus_calculated_function + endValue * calculated_function;
    }

    //

    public static float EaseOutElasticBig(
        float time,
        float startValue,
        float endValue
        )
    {
        float
            ts = time * time,
            tc = ts * time,
            calculated_function = ( 56.0f * tc * ts - 175.0f * ts * ts + 200.0f * tc - 100.0f * ts + 20.0f * time ),
            one_minus_calculated_function = 1 - calculated_function;

        return startValue * one_minus_calculated_function + endValue * calculated_function;
    }

    //

    public static float BackInCubic(
        float time,
        float startValue,
        float endValue
        )
    {
        return startValue + endValue * ( 4 * time * time * time - 3 * time * time );
    }

    //

    public static float Sinus(
        float time,
        float startValue,
        float endValue
        )
    {
        float
            calculated_function,
            one_minus_calculated_function;

        calculated_function = ( Mathf.Sin( time ) + 1 ) * 0.5f;
        one_minus_calculated_function = 1 - calculated_function;

        return startValue * one_minus_calculated_function + endValue * calculated_function;
    }

    //

    public static float SmallStretch(
        float time,
        float startValue,
        float endValue
        )
    {
        float
            ts = time * time,
            tc = ts * time,
            calculated_function = ( 4.895f * tc * ts + -9.19f * ts * ts + 5.595f * tc + -3.4f * ts + 3.1f * time ),
            one_minus_calculated_function = 1 - calculated_function;

        return startValue * one_minus_calculated_function + endValue * calculated_function;
    }

    public static float Cubic( float time,
        float startValue,
        float endValue
        )
    {
        return Mathf.Lerp( startValue, endValue, time * time );

    }
    //

    public static float OutOverShoot(
        float time,
        float startValue,
        float endValue
        )
    {
        float
            ts = time * time,
            tc = ts * time,
            calculated_function = ( 11.2975f * tc * ts + -23.195f * ts * ts + 30.695f * tc + -36.995f * ts + 19.1975f * time ),
            one_minus_calculated_function = 1 - calculated_function;

        return startValue * one_minus_calculated_function + endValue * calculated_function;
    }

}

