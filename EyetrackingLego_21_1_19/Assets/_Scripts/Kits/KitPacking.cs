﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class KitPacker
{

    public struct PackingData
    {
        public Vector3 BoundsOffsetPos;
        public Vector3 localPos;
        public Vector3 localRot;
        public AssemblyPiece PackedPiece;
    }
    public class PackingNode
    {

        public PackingNode[] Children;
        public Bounds Bounds;
        private bool _occupied;
        public bool IsOccupied() { return _occupied; }
        public enum Direction
        {
            None,
            Horizontal,
            Vertical,
            
        }
        public void Occupy()
        {
            _occupied = true;
        }

        public Direction SplitDirection = Direction.None;
        //public bool IsEmptyEndNode()
        //{
        //    return Children != null && !_occupied;
        //}
        //public void Move(Vector3 offset)
        //{
        //    Bounds.center += offset;
        //    if (Children != null)
        //    {
        //        Children[0].Move(offset);
        //        Children[1].Move(offset);
        //    }

        //}


        public void Centrize()
        {

            //if (Children == null)
            //{
            //    return;
            //}
            //if (Children[1].IsEmptyEndNode())
            //{
            //    if (Children[0].Bounds.extents.x == Bounds.extents.x)//move along z
            //        Move(new Vector3(0f, 0f, -Children[1].Bounds.extents.z*0.25f));
            //    else
            //        Move(new Vector3(-Children[1].Bounds.extents.x*0.25f, 0f, 0f));
            //}
        }
        public PackingNode(Bounds b)
        {
            Children = null;
            Bounds = b;
            _occupied = false;
        }

        public PackingNode Insert(Bounds b)
        {

            //if we already have children, push further to children
            if (Children != null)
            {
                var newNode = Children[0].Insert(b);
                if (newNode != null)
                {
                    return newNode;
                }
                else
                {
                    return Children[1].Insert(b);
                }
            }
            else if (_occupied) //check if we're occupied
            {
                return null;
            }
            else //split 
            {
                //if we're correct size, return this
                if (Bounds.extents.x == b.extents.x && Bounds.extents.z == b.extents.z)
                {
                    return this;
                }
                //if we're too small, return null
                if (Bounds.extents.x < b.extents.x || Bounds.extents.z < b.extents.z)
                {
                    return null;
                }
                //(decide which way to split)
                float dw = Bounds.extents.x - b.extents.x;
                float dh = Bounds.extents.z - b.extents.z;
                Vector3 extents1, extents2, center1, center2;
                if (dw < dh * 0.5f)//horizontal split
                {
                    SplitDirection = Direction.Horizontal;
                    extents1 = new Vector3(Bounds.extents.x, Bounds.extents.y, b.extents.z);
                    extents2 = new Vector3(Bounds.extents.x, Bounds.extents.y, Bounds.extents.z - b.extents.z);

                    center1 = new Vector3(Bounds.center.x, Bounds.center.y, Bounds.center.z - Bounds.extents.z + extents1.z);
                    center2 = new Vector3(Bounds.center.x, Bounds.center.y, Bounds.center.z + Bounds.extents.z - extents2.z);
                }
                else
                //vertical split
                {
                    SplitDirection = Direction.Vertical;
                    extents1 = new Vector3(b.extents.x, Bounds.extents.y, Bounds.extents.z);
                    extents2 = new Vector3(Bounds.extents.x - b.extents.x, Bounds.extents.y, Bounds.extents.z);

                    center1 = new Vector3(Bounds.center.x - Bounds.extents.x + extents1.x, Bounds.center.y, Bounds.center.z);
                    center2 = new Vector3(Bounds.center.x + Bounds.extents.x - extents2.x, Bounds.center.y, Bounds.center.z);

                }
                Children = new PackingNode[2];

                Bounds b1 = new Bounds { center = center1, extents = extents1 };
                Bounds b2 = new Bounds { center = center2, extents = extents2 };

                Children[0] = new PackingNode(b1);
                Children[1] = new PackingNode(b2);

                return Children[0].Insert(b);
            }
        }
    }

    public static PackingData[] Pack(AssemblyPiece[] Pieces, out Bounds TotalBounds, bool centrize = false)
    {
        PackingData[] pdata = new PackingData[Pieces.Length];
        Bounds[] bounds = new Bounds[Pieces.Length];
        int[] sizeIndices = new int[Pieces.Length];

        float longestSizeAll = 0f;
        //Get the bounds, rotated correctly
        for (int i = 0; i < Pieces.Length; ++i)
        {
            PackingData data = new PackingData();
            data.PackedPiece = Pieces[i];
            Pieces[i].transform.localRotation = Quaternion.identity;
            Bounds current = Pieces[i].GetBounds();
            Vector3 extents = current.extents;
            //Rotate based on size
            float longestSide = Mathf.Max(extents.x, extents.y, extents.z);
            longestSizeAll = Mathf.Max(longestSide, longestSizeAll);
            Quaternion localRot = Quaternion.identity;
            // make longest side x
            if (extents.y == longestSide)//rotate 90 deg over Z axis
            {
                localRot = Quaternion.Euler(0f, 0f, 90f) * localRot;
                //Swap Y and X
                float s = extents.x;
                extents.x = extents.y;
                extents.y = s;
            }
            else if (extents.z == longestSide)//rotate 90 deg over Y axis
            {
                localRot = Quaternion.Euler(0f, 90f, 0f) * localRot;
                //swap Z and X
                float s = extents.x;
                extents.x = extents.z;
                extents.z = s;
            }


            //make second longest side z
            longestSide = Mathf.Max(extents.y, extents.z);
            if (extents.y == longestSide) //Rotate 90 deg over X
            {
                localRot = Quaternion.Euler(90f, 0f, 0f) * localRot;
                //swap Z and Y
                float s = extents.y;
                extents.y = extents.z;
                extents.z = s;
            }
            Pieces[i].transform.localRotation *= localRot;
            current = data.PackedPiece.GetBounds();
            data.BoundsOffsetPos = data.PackedPiece.transform.position - current.center -Vector3.up* current.extents.y;
            //data.BoundsOffsetPos
            //else no problem
            //current.extents = extents;
            current.extents += Vector3.one * 0.05f;

            //Second longest side
            bounds[i] = current;
            data.localRot = localRot.eulerAngles;
            pdata[i] = data;
            //Prepare indices
            sizeIndices[i] = i;
        }
        //order by 2d area
        sizeIndices = sizeIndices.OrderByDescending(i => Mathf.Max(bounds[i].extents.x, bounds[i].extents.z)).ToArray();
        //calculate sum of total area
        float totalarea = bounds.Sum(x => x.extents.x * x.extents.z);
        //make shape half square
        float totalHeight = Mathf.Sqrt(totalarea);
        float totalWidth = Mathf.Max(totalHeight, longestSizeAll);
        bool successfulpack = false;
        PackingNode mainNode = null;
        while (!successfulpack)
        {
            totalWidth *= 1.1f;

            mainNode = new PackingNode(new Bounds() { center = Vector3.zero, extents = new Vector3(totalWidth, 0, totalHeight) });

            Vector3 offsetTotal = Vector3.zero;
            successfulpack = true;
            for (int i = 0; i < Pieces.Length; ++i)
            {
                int pieceIndex = sizeIndices[i];
                Bounds b = bounds[pieceIndex];

                PackingNode calculatedNode = mainNode.Insert(b);

                if (calculatedNode != null)
                {
                    calculatedNode.Occupy();
                    PackingData data = pdata[pieceIndex];
                    data.localPos = calculatedNode.Bounds.center + Vector3.up * b.extents.y;
                    pdata[pieceIndex] = data;
                }
                else
                {
                    successfulpack = false;
                    break;
                }
            }
            
        }
        if (centrize)
            mainNode.Centrize();
        TotalBounds = mainNode.Bounds;
        return pdata;
    }
    // Use this for initialization

}
