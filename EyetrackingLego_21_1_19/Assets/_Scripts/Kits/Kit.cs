﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SimpleEvent : UnityEvent { }
public class Kit : MonoBehaviour
{
    public SimpleEvent KitEmpty = new SimpleEvent();

    public Renderer FoamQuad;
    public Transform KitVisual;
    public Camera KitCamera;
    public AssemblyPiece[] Pieces;
    public Vector2 KitSize;
    public CaseSize Sizer;
    public Animator anim;
    int numPiecesLeft = 0;

    private List<AssemblyPiece> piecesStillInBox;
    // Use this for initialization
    void Awake()
    {
        //Render texture
        float texAspect = KitSize.x / KitSize.y;
        int texHeight = 512;
        int texWidth = Mathf.Max(1, Mathf.RoundToInt(texHeight * texAspect));
        RenderTexture rtex = new RenderTexture(texWidth, texHeight, 24);
        rtex.format = RenderTextureFormat.Depth;
        KitCamera.orthographicSize = 0.5f * KitSize.y;
        KitCamera.targetTexture = rtex;
        KitCamera.Render();

        FoamQuad.sharedMaterial = new Material(FoamQuad.sharedMaterial);
        FoamQuad.sharedMaterial.SetTexture("_Depth", rtex);
        KitCamera.gameObject.SetActive(false);


        Quaternion storedRot = transform.rotation;

        transform.rotation = Quaternion.identity;
        Sizer.SetSize(KitSize.x, KitSize.y, 1f);
        transform.rotation = storedRot;

        piecesStillInBox = new List<AssemblyPiece>();
        piecesStillInBox.AddRange(Pieces);

    }

    void Start()
    {
        foreach (var p in Pieces)
        {
            ++numPiecesLeft;

            foreach (var r in p.Renderers)

                r.enabled = false;

            var tempPiece = p;
            p.OnPickup.AddListener(() =>
            {
                piecesStillInBox.Remove(tempPiece);
                --numPiecesLeft;
                if (numPiecesLeft == 0)
                {
                    KitEmpty.Invoke();
                }


            });
            p.OnPutBack.AddListener(() =>
            {
                if (!piecesStillInBox.Contains(tempPiece))
                {
                    ++numPiecesLeft;
                    piecesStillInBox.Add(tempPiece);
                }
            });
        }
    }



    IEnumerator OpenUpdate()
    {
        yield return new WaitForSeconds(0.2f);

        anim.Play("Open");

        yield return new WaitForSeconds(0.2f);
        foreach (var p in piecesStillInBox)
        {
            foreach (var r in p.Renderers)

                r.enabled = true;
        }
    }
    IEnumerator CloseUpdate()
    {
        yield return new WaitForSeconds(0.2f);

        anim.Play("Close");
        yield return new WaitForSeconds(0.7f);

        foreach (var p in piecesStillInBox)
        {
            foreach (var r in p.Renderers)

                r.enabled = false;
        }
    }

    public void Open()
    {
        StartCoroutine(OpenUpdate());

    }
    public void Close()
    {
        //StartCoroutine(CloseUpdate());

    }


}
