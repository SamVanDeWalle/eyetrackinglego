﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestKitPacking : MonoBehaviour {

    public AssemblyPiece[] KitPieces = null;
	// Use this for initialization
	void Start () {
		
	}

    void DrawNode(KitPacker.PackingNode node)
    {
        if (node.Children == null)
        {
            Gizmos.color = node.IsOccupied() ? Color.red : Color.green;
            Gizmos.DrawCube(transform.position + node.Bounds.center, node.Bounds.extents * 2 + Vector3.up * 0.01f);
        }
        else
        {
            //Gizmos.color = Color.blue;
            //Gizmos.DrawCube(transform.position + node.Bounds.center, node.Bounds.extents * 2 + Vector3.up * 0.01f);
            DrawNode(node.Children[0]);
            DrawNode(node.Children[1]);

        }


    }
    void OnDrawGizmos()
    {
        //if (KitPacker.PackedNode != null)
        //{
        //    DrawNode(KitPacker.PackedNode);
        //}
    }
    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //foreach (var pdata in KitPacker.Pack(KitPieces))     
            //{
            //    pdata.PackedPiece.transform.SetParent(transform);
            //    pdata.PackedPiece.transform.localPosition = pdata.localPos + pdata.BoundsOffsetPos;
            //    //pdata.PackedPiece.transform.localRotation *= Quaternion.Euler(pdata.localRot);

            //    pdata.PackedPiece.GetComponent<Rigidbody>().isKinematic = true;
            //}
        }
	}
}
