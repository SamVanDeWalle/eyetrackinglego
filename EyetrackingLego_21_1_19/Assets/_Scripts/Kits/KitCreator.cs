﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class KitCreator : MonoBehaviour {

    
    public GameObject KitPrefab;

    public Kit CreateKit(AssemblyPiece[] AllPieces)
    {
        //Pack all items
        Bounds totalBounds;
        foreach (var p in AllPieces) p.transform.SetParent(null);
        var pdata = KitPacker.Pack(AllPieces, out totalBounds);
        //instantiate kit + scale
        Kit kit = Instantiate(KitPrefab).GetComponent<Kit>();
        kit.Pieces = AllPieces;

        Vector3 scale = totalBounds.extents * 2;
        scale.y = 1f;
        kit.KitVisual.transform.localScale = scale;
        Vector3 collSize = kit.transform.GetComponent<BoxCollider>().size;
        collSize.Scale(scale);
        kit.transform.GetComponent<BoxCollider>().size = collSize;
 
        foreach (var p in pdata)
        {
            p.PackedPiece.transform.SetParent(kit.transform);
            p.PackedPiece.transform.localPosition = p.localPos + p.BoundsOffsetPos + Vector3.up*0.35f;
            p.PackedPiece.GetComponent<Rigidbody>().isKinematic = true;
        }

        kit.KitSize = new Vector2(scale.x,scale.z);
        //kit.Sizer.SetSize(scale.x, scale.z, 0.2f);
        //Destroy(kit.KitCamera.gameObject,0.5f);

        return kit;
    }
}
