﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaseSize : MonoBehaviour {

    public float startWidth, startDepth, startHeight;
	// Use this for initialization
	public void SetSize(float width, float depth, float height)
        {

        float dwidth = (width - startWidth)*0.5f / transform.localScale.x;
        float ddepth = (depth - startDepth)*0.5f/transform.localScale.y;
        float dheight = (height- startHeight)*0.5f/ transform.localScale.z;

        List<Mesh> mesh = new List<Mesh>();
        List<Vector3[]> vertices = new List<Vector3[]>();
        List<Transform> root = new List<Transform>();

        foreach (Transform child in transform)
        {
            if (child.name != "case_LEFT" && child.name != "case_RIGHT" && child.name != "case_FRONT" && child.name != "case_BACK" && child.name != "case_FLOOR")
            {
                Mesh tempmesh;

                if (child.GetComponent<MeshFilter>())
                {
                    //Debug.Log(child.name);

                    tempmesh = child.GetComponent<MeshFilter>().mesh;

                    mesh.Add(tempmesh);
                    vertices.Add(child.GetComponent<MeshFilter>().mesh.vertices);
                    root.Add(child);
                }
            }
        }



        for (int r = 0; r < root.Count; ++r)
        {
            List<int> left = new List<int>();
            List<int> right = new List<int>();
            List<int> front = new List<int>();
            List<int> back = new List<int>();

            List<int> floor = new List<int>();

            foreach (Transform child in transform)
            {

                if (child.name == "case_LEFT" || child.name == "case_RIGHT" || child.name == "case_FRONT" || child.name == "case_BACK" || child.name == "case_FLOOR")
                {
                    child.GetComponent<MeshRenderer>().enabled = false;

                    for (int i = 0; i < vertices[r].Length; i++)
                    {
                        if (child.GetComponent<MeshRenderer>().bounds.Contains(root[r].TransformPoint(vertices[r][i])))
                        {
                            //Debug.Log("contains vert index: " + i);

                            if (child.name == "case_LEFT")
                            {
                                left.Add(i);
                            }
                            else if (child.name == "case_RIGHT")
                            {
                                right.Add(i);
                            }
                            else if (child.name == "case_FRONT")
                            {
                                front.Add(i);
                            }
                            else if (child.name == "case_BACK")
                            {
                                back.Add(i);
                            }
                            else if (child.name == "case_FLOOR")
                            {
                                floor.Add(i);
                            }


                        }
                    }
                }
            }


            foreach (int i in floor)
            {
                vertices[r][i] += Quaternion.Inverse(root[r].transform.rotation) * (Vector3.down * dheight);
            }
            foreach (int i in left)
            {
                vertices[r][i] += Quaternion.Inverse(root[r].transform.rotation) * (Vector3.right * dwidth);
            }
            foreach (int i in right)
            {
                vertices[r][i] += Quaternion.Inverse(root[r].transform.rotation) * (Vector3.left * dwidth);
            }
            foreach (int i in front)
            {
                vertices[r][i] += Quaternion.Inverse(root[r].transform.rotation) * (Vector3.forward * ddepth);

            }
            foreach (int i in back)
            {
                vertices[r][i] += Quaternion.Inverse(root[r].transform.rotation) * (Vector3.back * ddepth);
            }

            //for (int i = 0; i < vertices[r].Length; ++i)
            //{
            //    vertices[r][i] += Quaternion.Inverse(root[r].transform.rotation) * (Vector3.forward * ddepth*0.1f);
            //}

            mesh[r].vertices = vertices[r];
            mesh[r].RecalculateNormals();
            root[r].GetComponent<MeshFilter>().mesh = mesh[r];


        }

        for (int i = transform.childCount - 1; i >= 0; --i)
        {
            Transform child = transform.GetChild(i);

            //Debug.Log(child.name);

            if (child.name == "case_LEFT" || child.name == "case_RIGHT" || child.name == "case_FRONT" || child.name == "case_BACK" || child.name == "case_FLOOR")
            {
                DestroyImmediate(child.gameObject);
            }
            else child.position += Vector3.up * (height  / 10);
        }

        //transform.position -= new Vector3(0f, 0f,ddepth*0.5f*transform.localScale.z);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
