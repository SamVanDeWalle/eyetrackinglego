﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class kitGroup
{
    public List<GameObject> goList = new List<GameObject>();
}

public enum KitsStartState
{
    NextOnEmpty,
    AllOnStart
}

public class KitValues : MonoBehaviour
{
    public KitsStartState startState = KitsStartState.NextOnEmpty;

    [SerializeField]
    private List<kitGroup> kits = new List<kitGroup>();

    [SerializeField]
    private List<Kit> CreatedKits = new List<Kit>();

    [SerializeField, HideInInspector]
    private KitCreator _creator;

    public void GenerateKits()
    {
        if (_creator == null)
        {
            _creator = FindObjectOfType<KitCreator>();

            //still null, instantiate
            if (_creator == null)
            {
                _creator = Instantiate(Resources.Load<GameObject>("Prefabs/KitCreator")).GetComponent<KitCreator>();
            }
        }

        //Clean up prev generated kits
        if (CreatedKits.Count != 0)
        {
            foreach (var k in CreatedKits)
            {
                if (k == null || k.gameObject == null) continue;
                foreach (var p in k.Pieces)
                {
                    if (p != null && p.transform != null)
                        p.transform.SetParent(null);
                }
                DestroyImmediate(k.gameObject);
            }


        }
        CreatedKits.Clear();

        foreach (var k in kits)
        {


            var kit = _creator.CreateKit(k.goList.Select(go => go.GetComponent<AssemblyPiece>()).Where(x => x != null).ToArray());
            CreatedKits.Add(kit);
            kit.transform.SetParent(transform);
            kit.transform.localPosition = new Vector3(10000, 0, 0);
        }

        Vector3 startPos = transform.position;
        bool first = true;
        foreach (var k in CreatedKits)
        {
            if (first)
            {
                startPos -= Vector3.right * k.KitSize.x * 0.5f;
            }
            first = false;
            k.transform.position = startPos + Vector3.right * k.KitSize.x * 0.5f;
            startPos += Vector3.right * (k.KitSize.x + 0.5f);
        }

    }



    public void SetKits(List<kitGroup> k)
    {
        kits = new List<kitGroup>();
        foreach (kitGroup g in k)
        {
            kitGroup copy = new kitGroup();
            copy.goList.AddRange(g.goList);
            kits.Add(copy);
        }

        GenerateKits();
    }

    public List<kitGroup> GetKits()
    {
        List<kitGroup> copykits = new List<kitGroup>();
        foreach (kitGroup g in kits)
        {
            kitGroup copy = new kitGroup();
            copy.goList.AddRange(g.goList);
            copykits.Add(copy);
        }

        return copykits;
    }
    Queue<Kit> KitQueue = new Queue<Kit>();
    private Kit _openKit = null;
    public void Start()
    {
        bool openNext = false;

        foreach (var k in CreatedKits)
        {
            if (k == null)
                continue;

            switch (startState)
            {
                case KitsStartState.NextOnEmpty:
                    KitQueue.Enqueue(k);
                    openNext = true;
                    break;
                case KitsStartState.AllOnStart:
                    k.Open();
                    break;
            }
        }

        if(openNext)
            OpenNextKit();
    }

    void OpenNextKit()
    {
        if (_openKit != null) _openKit.Close();

        _openKit = KitQueue.Count > 0 ? KitQueue.Dequeue() : null;
        if (_openKit == null)
            return;

        _openKit.KitEmpty.AddListener(OpenNextKit);
        _openKit.Open();
    }
}