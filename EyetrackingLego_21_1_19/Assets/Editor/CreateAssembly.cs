﻿// *******************************
//
// Create Assembly 
//
// Authors: Marin Brouwers
//
// Version: v0.1
// Latest revision: 09/01/2017
//
//********************************
using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class CreateAssembly : EditorWindow
{
    public string error = "";
    public GUIStyle errorstyle = new GUIStyle();
    public GameObject rootGO;
    public GameObject tempRoot;
    public List<GameObject> assemblyPieces = new List<GameObject>();
    // Add menu item
    [MenuItem("DAE Research/Create Assembly")]
    public static void ShowWindow()
    {
        //Show existing window instance. If one doesn't exist, make one.
        EditorWindow window = EditorWindow.GetWindow(typeof(CreateAssembly));
        window.maxSize = new Vector2(500f, 800f);
        window.minSize = window.maxSize;

    }

    private Texture2D m_Logo = null;
    Vector2 _scrollPos;

    void OnEnable()
    {
        m_Logo = (Texture2D)Resources.Load("logo", typeof(Texture2D));
    }

    public bool phase1 = false;
    void SetError(string msg)
    {
        if (msg != "") error = "Error: " + msg;
        else error = "";
    }

    void OnGUI()
    {
        GUILayout.Label(m_Logo);
        GUILayout.Label("Create Assembly v0.1", EditorStyles.boldLabel);
        GUILayout.Space(10);
        //ERROR HANDLE
        errorstyle.normal.textColor = Color.red;
        errorstyle.fontStyle = FontStyle.Bold;
        errorstyle.padding.left = 15;
        GUILayout.Label(error, errorstyle);
        GUILayout.Space(10);
        if (rootGO == null || !phase1)
        {
            GUILayout.Label("Select root or imported model");
            tempRoot = EditorGUILayout.ObjectField(tempRoot, typeof(GameObject), true) as GameObject;
            if (GUILayout.Button("New Assembly"))
            {
                assemblyPieces.Clear();
                if (!tempRoot)
                {
                    SetError("Please select root or imported model!");

                }
                else
                {
                    SetError("");
                    GameObject root = new GameObject();
                    root.transform.position = tempRoot.transform.position;
                    root.name = "RootAssembly";
                    //foreach (var t in tempRoot.transform.GetComponentsInChildren<MeshRenderer>())
                    //{
                    //    t.transform.parent = root.transform;
                    //}
                    //DestroyImmediate(tempRoot);
                    tempRoot.transform.SetParent(root.transform);
                    rootGO = root;
                    assemblyPieces.Clear();
                    phase1 = true;
                }
            }
            if (GUILayout.Button("Continue Assembly"))
            {
                if (!tempRoot)
                {
                    SetError("Please select root or imported model!");
                }
                else
                {
                    SetError("");
                    phase1 = true;
                    rootGO = tempRoot;
                    assemblyPieces.Clear();
                    foreach (Transform t in rootGO.transform)
                    {
                        if (t.gameObject.layer == LayerMask.NameToLayer("PickItems"))
                        {
                            assemblyPieces.Add(t.gameObject);
                        }
                    }
                }
            }
        }
        else
        {
            GUILayout.Label("AssemblyPieces: ");
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Assembly Name: ", GUILayout.Width(200f));
            string name = rootGO.name;
            name = EditorGUILayout.TextField(name);
            rootGO.name = name;

            EditorGUILayout.EndHorizontal();
            if (assemblyPieces.Count == 0)
            {
                GUILayout.Label("No AssemblyPieces have been created", EditorStyles.boldLabel);
            }
            else
            {
                _scrollPos = EditorGUILayout.BeginScrollView(_scrollPos);
                for (int i = 0; i < assemblyPieces.Count; ++i)
                {
                    EditorGUILayout.BeginHorizontal();
                    assemblyPieces[i] = EditorGUILayout.ObjectField(assemblyPieces[i], typeof(GameObject), true) as GameObject;
                    if (GUILayout.Button(" x "))
                    {
                        assemblyPieces.RemoveAt(i);
                        i--;
                    }
                    EditorGUILayout.EndHorizontal();
                }
                EditorGUILayout.EndScrollView();

            }
            if (GUILayout.Button("Create AssemblyPiece"))
            {
                CreateAssemblyPieceEditor.ShowWindow(this);
            }

            if (GUILayout.Button("Generate Ids"))
            {
                for (int i = 0; i < assemblyPieces.Count; ++i)
                {
                    if (assemblyPieces[i] == null || assemblyPieces[i].GetComponent<AssemblyPiece>() == null) continue;
                    GameObject prefabSr = (GameObject) PrefabUtility.GetCorrespondingObjectFromSource(assemblyPieces[i]);


                    AssemblyPiece piece = null;
                    if (prefabSr != null) piece = prefabSr.GetComponent<AssemblyPiece>();
                    if(piece == null)piece =assemblyPieces[i].GetComponent<AssemblyPiece>();
                  
                    piece.ID = i;
                    EditorUtility.SetDirty(piece);
                    
                }
               

            }

            GUILayout.Space(20);
            if (GUILayout.Button("Restart"))
            {
                SetError("");
                phase1 = false;
            }
        }

    }
    public void AddAssemblyPiece(GameObject piece)
    {
        assemblyPieces.Add(piece);
        Repaint();
    }
}

class CreateAssemblyPieceEditor : EditorWindow
{

    public class pieces
    {
        public GameObject go;
        public int matindex;
        public CollisionType ColllisionType;

    }

    public enum CollisionType
    {
        Box = 0,
        Sphere = 1,
        Capsule = 2,
        Mesh = 3
    }
    public List<pieces> pieceList = new List<pieces>();

    public List<Material> materialList = new List<Material>();

    private string[] CollisionTypeNames = null;

    public GUIStyle errorstyle = new GUIStyle();
    private CreateAssembly createAssembly;
    public string error = "";
    string _name = "AssemblyPiece";
    int _typeId = 0;
    public Material ghostMaterial;
    Vector2 _scrollPosition;

    public static void ShowWindow(CreateAssembly CA)
    {
        //Show existing window instance. If one doesn't exist, make one.
        CreateAssemblyPieceEditor window = EditorWindow.GetWindow(typeof(CreateAssemblyPieceEditor)) as CreateAssemblyPieceEditor;
        window.titleContent = new GUIContent("Create Assembly Piece");
        //window.maxSize = new Vector2(300f, 300f);
        //window.minSize = window.maxSize;
        window.createAssembly = CA;
        window.error = "";

        window.pieceList.Clear();
        //Start with minimum 1
        window.pieceList.Add(new pieces());
        window.materialList.Clear();

        string[] lookFor = new string[] { "Assets/_PredefinedMaterials" };
        string[] guids = AssetDatabase.FindAssets("MAT_GhostObject" + " t:Material", lookFor);

        foreach (string guid in guids)
        {
            window.ghostMaterial = (Material)AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(guid), typeof(Material));
        }

        string[] guids2 = AssetDatabase.FindAssets("t:Material", lookFor);

        foreach (string guid in guids2)
        {
            Material tempMat = (Material)AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(guid), typeof(Material));
            window.materialList.Add(tempMat);
        }

    }

    void SetError(string msg)
    {
        if (msg != "") error = "Error: " + msg;
        else error = "";
    }
    void OnEnable()
    {
        CollisionTypeNames = Enum.GetNames(typeof(CollisionType));
    }

    void SetLayerRecursively(GameObject go, int layer)
    {
        go.layer = layer;
        foreach (Transform child in go.transform)
        {
            SetLayerRecursively(child.gameObject, layer);
        }
    }

    void OnGUI()
    {

        GUILayout.Label("Create Assembly Piece", EditorStyles.boldLabel);
        GUILayout.Space(10);
        //ERROR HANDLE
        errorstyle.normal.textColor = Color.red;
        errorstyle.fontStyle = FontStyle.Bold;
        errorstyle.padding.left = 15;
        GUILayout.Label(error, errorstyle);
        GUILayout.Space(10);

        //_scrollPosition = GUILayout.BeginScrollView(_scrollPosition);
        _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition);
        for (int i = 0; i < pieceList.Count; ++i)
        {
        
            EditorGUILayout.BeginHorizontal();
            var oldPiece = pieceList[i].go;
            pieceList[i].go = EditorGUILayout.ObjectField(pieceList[i].go, typeof(GameObject), true) as GameObject;
            if (GUILayout.Button(" x "))
            {
                pieceList.RemoveAt(i);
                i--;
            }
            if(pieceList[i].go != oldPiece && pieceList[i].go != null)
            {
                _name = pieceList[i].go.name;
                string numericName = new String(_name.Where(Char.IsDigit).ToArray());
                _typeId = Convert.ToInt32(numericName);
            }
            EditorGUILayout.EndHorizontal();
        
            pieceList[i].matindex = EditorGUILayout.Popup("Select Material", pieceList[i].matindex, materialList.Select(x => x.name).ToArray());
            pieceList[i].ColllisionType = (CollisionType)EditorGUILayout.Popup("Collision Type", (int)pieceList[i].ColllisionType, CollisionTypeNames);
        
        
        }
        GUILayout.EndScrollView();
        GUILayout.Space(5);
        if (GUILayout.Button("Add Mesh"))
        {
            pieceList.Add(new pieces());
        }
        GUILayout.Space(10);
        _name = EditorGUILayout.TextField("Name", _name);
        _typeId = EditorGUILayout.IntField("TypeID", _typeId);
        if (GUILayout.Button("Create"))
        {
            //assembling sellected pieces
            if (pieceList.Count == 0)
            {
                SetError("Nothing Selected to assemble.");
            }
            else
            {
                for (int i = 0; i < pieceList.Count; ++i)
                {
                    if (!pieceList[i].go)
                    {
                        SetError("Not all pieces are selected.");
                        return;
                    }
                }

                GameObject AssemblyPiece = new GameObject();
                if (_name != "") AssemblyPiece.name = _name;
                else AssemblyPiece.name = "AssemblyPiece";

                Bounds b = new Bounds() { center = createAssembly.rootGO.transform.position };
                bool first = true;
                foreach (var p in pieceList)
                {
                    var r = p.go.GetComponent<Renderer>();
                    if (r == null) continue;
                    if (first)
                    {
                        b = r.bounds;
                        first = false;
                    }
                    else
                        b.Encapsulate(r.bounds);
                }


                AssemblyPiece.transform.position = b.center;
                AssemblyPiece.transform.parent = createAssembly.rootGO.transform;

                //find first common ancestor
                //Transform commonAncestor = null;

                //List<LinkedList<Transform>> allParents = new List<LinkedList<Transform>>();
                //foreach (var p in pieceList)
                //{
                //    LinkedList<Transform> LocalParents = new LinkedList<Transform>();
                //    Transform parent = p.go.transform.parent;
                //    while (parent != null)
                //    {
                //        LocalParents.AddLast(parent);
                //        parent = parent.parent;
                //    }
                //    allParents.Add(LocalParents);
                //}

                //bool foundDiff = allParents.Any(x=>x.Count ==0);
                //while (!foundDiff)
                //{
                //    Transform firstParent = null;
                //    foreach (var list in allParents)
                //    {
                //        if (list.Count == 0 || list.Last.Value != firstParent)
                //        {
                //            foundDiff = true;
                //        }
                //        list.RemoveLast();
                //    }

                //    if (!foundDiff)
                //    {
                //        commonAncestor = firstParent;
                //    }
                //}
                //LinkedList<KeyValuePair<Transform, Quaternion>> parentRotsStored = new LinkedList<KeyValuePair<Transform, Quaternion>>();
                //Transform tparent = commonAncestor;
                //while (tparent != null)
                //{
                //    parentRotsStored.AddLast(new KeyValuePair<Transform, Quaternion>(tparent, tparent.transform.localRotation));
                //    tparent.transform.localRotation = Quaternion.identity;
                //}

                for (int i = 0; i < pieceList.Count; ++i)
                {
                    pieceList[i].go.transform.parent = AssemblyPiece.transform;
                    pieceList[i].go.GetComponent<MeshRenderer>().material = materialList[pieceList[i].matindex];
                }
                ////reset parent rotations
                //for (var p = parentRotsStored.Last;p != null; p = p.Previous)
                //{
                //    p.Value.Key.localRotation = p.Value.Value;
                //}

                GameObject Ghost = Instantiate(AssemblyPiece);
                Ghost.transform.position = createAssembly.rootGO.transform.position;
                Ghost.transform.parent = AssemblyPiece.transform;
                Ghost.name = "Ghost";

                for (int i = 0; i < pieceList.Count; ++i)
                {
                    foreach (Collider c in pieceList[i].go.GetComponents<Collider>())
                    {
                        DestroyImmediate(c);
                    }
                    switch (pieceList[i].ColllisionType)
                    {
                        case CollisionType.Mesh:
                           
                            pieceList[i].go.AddComponent<MeshCollider>();
                        //    pieceList[i].go.GetComponent<MeshCollider>().inflateMesh = true;
                            pieceList[i].go.GetComponent<MeshCollider>().convex = true;
                            
                            break;
                        case CollisionType.Box:
                            pieceList[i].go.AddComponent<BoxCollider>();
                            break;
                        case CollisionType.Sphere:
                            pieceList[i].go.AddComponent<SphereCollider>();
                            break;
                        case CollisionType.Capsule:
                            pieceList[i].go.AddComponent<CapsuleCollider>();
                            break;
                    }
                   
                }

                var ap = AssemblyPiece.AddComponent<AssemblyPiece>();
                ap.GhostObject = Ghost;
                ap.TypeID = _typeId;

                foreach (Renderer mr in Ghost.GetComponentsInChildren<MeshRenderer>())
                {
                    mr.material = ghostMaterial;
                }

                Ghost.SetActive(false);

                
                SetLayerRecursively(AssemblyPiece, LayerMask.NameToLayer("PickItems"));
                AssemblyPiece.AddComponent<NetworkObject>();
                AssemblyPiece.AddComponent<Rigidbody>();


                string folderPath = "Resources/Prefabs/Assemblies/" + createAssembly.rootGO.name;
                string fullPath = Application.dataPath + "/" + folderPath;
                if (!System.IO.Directory.Exists(fullPath))
                {
                    System.IO.Directory.CreateDirectory(fullPath);
                }
                int index = 0;

                string filePath = folderPath + "/" + AssemblyPiece.name + ".prefab";

                while (System.IO.File.Exists(Application.dataPath + "/" + filePath))
                {
                    ++index;
                    filePath = folderPath + "/" + AssemblyPiece.name + "(" + index.ToString("00") + ").prefab";

                }
                var go = PrefabUtility.CreatePrefab("Assets/" + filePath, AssemblyPiece);
                PrefabUtility.ConnectGameObjectToPrefab(AssemblyPiece, go);
                createAssembly.AddAssemblyPiece(go);
            }
        }
    }
}
