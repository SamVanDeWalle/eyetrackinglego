﻿// *******************************
//
// Dynamic Case
//
// Authors: Marin Brouwers & Fries Boury
//
// Version: v0.1
// Latest revision: 13/01/2017
//
//********************************
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;


public class DynamicCase : EditorWindow
{
    public string error = "";
    public GUIStyle errorstyle = new GUIStyle();
    private bool rootSelected = false;
    public GameObject rootObject = null;
    public Vector4 v4 = new Vector4();
    public float thick = 1f;

    // Add menu item
    [MenuItem("DAE Research/Dynamic Case")]
    public static void ShowWindow()
    {
        //Show existing window instance. If one doesn't exist, make one.
        EditorWindow window = EditorWindow.GetWindow(typeof(DynamicCase));
        window.maxSize = new Vector2(500f, 800f);
        window.minSize = window.maxSize;
    }

    private Texture2D m_Logo = null;

    void OnEnable()
    {
        m_Logo = (Texture2D)Resources.Load("logo", typeof(Texture2D));
        SetError("");
    }
    void SetError(string msg)
    {
        if (msg != "") error = "Error: " + msg;
        else error = "";
    }
    private void OnGUI()
    {
        GUILayout.Label(m_Logo);
        GUILayout.Label("Dynamic Case v0.1", EditorStyles.boldLabel);
        GUILayout.Space(10);
        //COMPLETE MESSAGE
        //ERROR HANDLE
        errorstyle.normal.textColor = Color.red;
        errorstyle.fontStyle = FontStyle.Bold;
        errorstyle.padding.left = 15;
        GUILayout.Label(error, errorstyle);
        GUILayout.Space(10);
        v4 = EditorGUILayout.Vector4Field("Move by:  (left - right - front - back)", v4);
        GUILayout.Label("Thickness: ");
        thick = EditorGUILayout.FloatField(thick);
        GUILayout.Label("Select Case Object");
        Rect temprec = EditorGUILayout.GetControlRect();
        if (GUI.Button(temprec, ""))
        {
            Selection.activeGameObject = rootObject;
            SceneView.lastActiveSceneView.FrameSelected();
        }
        rootObject = EditorGUI.ObjectField(temprec, rootObject, typeof(GameObject), true) as GameObject;
        if (rootObject != null)
        {

            if (GUILayout.Button("Get Verts"))
            {

                List<Mesh> mesh = new List<Mesh>();
                List<Vector3[]> vertices = new List<Vector3[]>();
                List<Transform> root = new List<Transform>();

                foreach (Transform child in rootObject.transform)
                {
                    if (child.name != "case_LEFT" && child.name != "case_RIGHT" && child.name != "case_FRONT" && child.name != "case_BACK" && child.name != "case_FLOOR")
                    {
                        Mesh tempmesh;

                        if (child.GetComponent<MeshFilter>())
                        {
                            //Debug.Log(child.name);

                            tempmesh = child.GetComponent<MeshFilter>().mesh;
                            
                            mesh.Add(tempmesh);
                            vertices.Add(child.GetComponent<MeshFilter>().mesh.vertices);
                            root.Add(child);
                        }
                    }
                }



                for(int r = 0; r < root.Count; ++r)
                {
                    List<int> left = new List<int>();
                    List<int> right = new List<int>();
                    List<int> front = new List<int>();
                    List<int> back = new List<int>();

                    List<int> floor = new List<int>();

                    foreach (Transform child in rootObject.transform)
                    {

                        if (child.name == "case_LEFT" || child.name == "case_RIGHT" || child.name == "case_FRONT" || child.name == "case_BACK" || child.name == "case_FLOOR")
                        {
                            child.GetComponent<MeshRenderer>().enabled = false;

                            for (int i = 0; i < vertices[r].Length; i++)
                            {
                                if (child.GetComponent<MeshRenderer>().bounds.Contains(root[r].TransformPoint(vertices[r][i])))
                                {
                                    //Debug.Log("contains vert index: " + i);

                                    if (child.name == "case_LEFT")
                                    {
                                        left.Add(i);
                                    }
                                    else if (child.name == "case_RIGHT")
                                    {
                                        right.Add(i);
                                    }
                                    else if (child.name == "case_FRONT")
                                    {
                                        front.Add(i);
                                    }
                                    else if (child.name == "case_BACK")
                                    {
                                        back.Add(i);
                                    }
                                    else if (child.name == "case_FLOOR")
                                    {
                                        floor.Add(i);
                                    }

                                    
                                }
                            }
                        }
                    }


                    foreach (int i in floor)
                    {
                        vertices[r][i] += Quaternion.Inverse(root[r].transform.rotation) * (Vector3.down * thick);
                    }
                    foreach (int i in left)
                    {
                        vertices[r][i] += Quaternion.Inverse(root[r].transform.rotation) * (Vector3.right * v4.x);
                    }
                    foreach (int i in right)
                    {
                        vertices[r][i] += Quaternion.Inverse(root[r].transform.rotation) * (Vector3.left * v4.y);
                    }
                    foreach (int i in front)
                    {
                        vertices[r][i] += Quaternion.Inverse(root[r].transform.rotation) * (Vector3.forward * v4.z);

                    }
                    foreach (int i in back)
                    {
                        vertices[r][i] += Quaternion.Inverse(root[r].transform.rotation) * (Vector3.back * v4.w);
                    }

                    for (int i = 0; i < vertices[r].Length; ++i)
                    {
                        vertices[r][i] += Quaternion.Inverse(root[r].transform.rotation) * (Vector3.forward * v4.z);
                    }

                    mesh[r].vertices = vertices[r];
                    mesh[r].RecalculateNormals();
                    root[r].GetComponent<MeshFilter>().mesh = mesh[r];


                }

                for (int i = rootObject.transform.childCount -1; i >= 0; --i)
                {
                    Transform child = rootObject.transform.GetChild(i);

                    //Debug.Log(child.name);

                    if (child.name == "case_LEFT" || child.name == "case_RIGHT" || child.name == "case_FRONT" || child.name == "case_BACK" || child.name == "case_FLOOR")
                    {
                        DestroyImmediate(child.gameObject);
                    }
                    else child.position += Vector3.up * (thick / 10);
                }

            }
        }
    }
}
