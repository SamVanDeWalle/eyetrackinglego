﻿/*
 * 
 * Simple script to quickly set the material of multiple selected Assembly pieces
 * 
 * 
 */

using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class MaterialSetter : EditorWindow
{
    List<AssemblyPiece> _pieces = new List<AssemblyPiece>();
    Material _material;

    [MenuItem("DAE Research/Material Setter")]
    public static void ShowWindow()
    {
        EditorWindow window = GetWindow(typeof(MaterialSetter));
        Selection.selectionChanged += () => { window.Repaint(); };
    }

    void Update()
    {
        Repaint();
    }

    void OnGUI()
    {
        _material = EditorGUILayout.ObjectField("Material", _material, typeof(Material), true) as Material;
        var gos = Selection.gameObjects.Where(go => go.GetComponent<AssemblyPiece>() != null);   

        if (GUILayout.Button("Set Material"))
        {
            foreach (var go in gos)
            {
                foreach (Transform child in go.transform)
                {
                    if (!child.name.Contains("Ghost") && !child.name.Contains("CP") && child.GetComponent<PaintSurface>() == null)
                    {
                        var renderer = child.GetComponent<Renderer>();
                        if (renderer != null)
                            renderer.material = _material;
                    }
                }
            }
        }

        EditorGUILayout.LabelField("Selected Pieces:");

        foreach (var go in gos)
        {
            EditorGUILayout.LabelField(go.name);
        }
    }
}
