﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(NetworkHandling))]
public class NetworkHandlingEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        NetworkHandling nwh = (NetworkHandling)target;

        if(GUILayout.Button("Create networkObjects from assemblyPieces"))
        {
            //nwh.CreateNetworkObjectsFromAssemblyPieces();
            CreateNetworkObjectsFromAssemblyPieces();
            //var pieces = Resources.LoadAll<AssemblyPiece>("Prefabs/Lego_Pieces/");
            //
            //for (int i = 0; i < pieces.Length; i++)
            //{
            //    var networkObject = pieces[i].GetComponent<NetworkObject>();
            //
            //    if (networkObject == null)
            //    {
            //        networkObject = pieces[i].gameObject.AddComponent<NetworkObject>();
            //    }
            //
            //    networkObject.NUID = 1000 + i;
            //    //NetworkObjects.Add(networkObject.NUID, networkObject);
            //}
        }

        if (GUILayout.Button("Clear NetworkObject list (" + NetworkHandling.NetworkObjects.Count + ")"))
        {
            NetworkHandling.NetworkObjects.Clear();
        }
    }

    public void CreateNetworkObjectsFromAssemblyPieces()
    {
        var pieces = FindObjectsOfType<AssemblyPiece>();

        for (int i = 0; i < pieces.Length; i++)
        {
            //TODO replace with new replay system from eyetracking project

            //var or = pieces[i].GetComponent<ObjectRecorder>();
            //if (or)
            //    pieces[i].ID = pieces[i].GetComponent<ObjectRecorder>().UID;
            //else
            //    pieces[i].ID = pieces[i].gameObject.GetInstanceID();

            var networkObject = pieces[i].GetComponent<NetworkObject>();
            if (networkObject == null)
            {
                networkObject = pieces[i].gameObject.AddComponent<NetworkObject>();
            }

            networkObject.NUID = pieces[i].ID;

            EditorUtility.SetDirty(pieces[i]);
            EditorUtility.SetDirty(networkObject);
        }

        EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
    }
}
