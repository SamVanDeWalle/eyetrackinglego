﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

public class QuickConnectionEditor : EditorWindow
{
    AssemblyPiece _femalePiece;
    List<AssemblyPiece> _malePieces = new List<AssemblyPiece>() { null };
    static ConnectionEditor _connectionEditor;

    public static void ShowWindow(ConnectionEditor connectionEditor)
    {
        _connectionEditor = connectionEditor;
        EditorWindow window = GetWindow(typeof(QuickConnectionEditor));
        _connectionEditor.QuickConnectionEditor = window as QuickConnectionEditor;
    }

    void OnGUI()
    {
        _femalePiece = EditorGUILayout.ObjectField("Female Piece:", _femalePiece, typeof(AssemblyPiece), true) as AssemblyPiece;

        for(int i = 0; i < _malePieces.Count; ++i)
        {
            var labelPost = _malePieces.Count > 1 ? " " + (i + 1) : "";
            EditorGUILayout.BeginHorizontal();
            _malePieces[i] = EditorGUILayout.ObjectField("Male Piece" + labelPost + ":", _malePieces[i], typeof(AssemblyPiece), true) as AssemblyPiece;
            if(i > 0)
            {
                if (GUILayout.Button("x"))
                {
                    _malePieces.RemoveAt(i);
                    break;
                }
            }
            EditorGUILayout.EndHorizontal();
        }

        if (GUILayout.Button("Add extra male piece"))
        {
            _malePieces.Add(null);
        }

        if(_malePieces.Count > 1)
        {
            EditorGUILayout.HelpBox("Note: Every instance of every provided male piece will be able to connect to the female piece on all the positions of the male pieces.", MessageType.Warning);
        }

        bool missingPieces = _femalePiece == null || _malePieces.Count < 1 || _malePieces[0] == null;
        bool maleIsFemale = _malePieces.Any(mp => mp == _femalePiece);
        bool doubleMale = _malePieces.GroupBy(x => x).Where(x => x.Count() > 1 && x.Key != null).Any();

        if (!missingPieces && maleIsFemale)
        {
            EditorGUILayout.HelpBox("A piece cannot be both female and male in the same connection.", MessageType.Error);
        }

        if (!missingPieces && doubleMale)
        {
            EditorGUILayout.HelpBox("Duplicate male pieces.", MessageType.Error);
        }

        bool canConnect = !missingPieces && !maleIsFemale && !doubleMale;

        EditorGUI.BeginDisabledGroup(!canConnect);
        GUI.color = Color.yellow;
        if (GUILayout.Button("Create connection", GUILayout.Height(60)))
        {
            CreateConnection();
        }
        GUI.color = Color.white;
        EditorGUI.EndDisabledGroup();
    }

    void CreateConnection()
    {
        // TODO: if male piece already has a connection type, use that type
        // Create connection type
        var type = _connectionEditor.CreateNewConnectionType(_femalePiece.name + "-" + _malePieces[0].name);

        // Add Connection points for every male piece on both male piece, according to male position and one global point on female piece
        var editedMaleTypes = new List<int>();
        var editedMalePrefabs = new Dictionary<int, GameObject>();
        //Debug.Log("Male pieces count: " + _malePieces.Count);
        var malePieceCount = _malePieces.Count;
        var originalPositions = new Dictionary<int, Vector3>();
        var originalRotations = new Dictionary<int, Quaternion>();
        for (int i = malePieceCount - 1; i >= 0; --i)
        {
            originalPositions.Add(i, _malePieces[i].transform.position);
            originalRotations.Add(i, _malePieces[i].transform.rotation);
        }
        for (int i = malePieceCount - 1; i >= 0; --i)
        {
            _malePieces[i].transform.position = originalPositions[i];
            _malePieces[i].transform.rotation = originalRotations[i];
            _connectionEditor.CreateConnectionPoint(_femalePiece, type, _malePieces[i].transform.position, _malePieces[i].transform.rotation, ConnectionGender.Female);
            if(!editedMaleTypes.Contains(_malePieces[i].TypeID))
            {
                var position = _malePieces[i].transform.position;
                var prefab = _connectionEditor.CreateConnectionPoint(_malePieces[i], type, _malePieces[i].transform.position, _malePieces[i].transform.rotation, ConnectionGender.Male);
                editedMaleTypes.Add(_malePieces[i].TypeID);
                editedMalePrefabs.Add(_malePieces[i].TypeID, prefab);
                _malePieces[i].transform.position = position;
                prefab.transform.position = position;
            }
            else
            {
                // Replace the male piece copy with the new prefab
                //var copy = PrefabUtility.InstantiatePrefab(PrefabUtility.GetPrefabParent(editedMalePrefabs[_malePieces[i].TypeID])) as GameObject;
                //copy.transform.position = _malePieces[i].transform.position;
                //copy.transform.rotation = _malePieces[i].transform.rotation;
                //copy.transform.parent = _malePieces[i].transform.parent;
                //DestroyImmediate(_malePieces[i].gameObject);
            }
        }

        _connectionEditor.Save();
        _connectionEditor.Load();
    }
}
