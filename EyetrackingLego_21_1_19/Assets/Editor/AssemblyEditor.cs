﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class AssemblyEditor : EditorWindow
{
    Texture2D _logo;

    [MenuItem("DAE Research/Assembly")]
    public static void ShowWindow()
    {
        EditorWindow window = GetWindow(typeof(AssemblyEditor));
    }

    void OnEnable()
    {
        _logo = (Texture2D)Resources.Load("logo", typeof(Texture2D));
    }

    void OnGUI()
    {
        GUILayout.Label(_logo);

        if (GUILayout.Button("Load connection types"))
        {
            //Load();
        }
    }
}