﻿// *******************************
//
// Kit Manager
//
// Authors: Marin Brouwers && Fries Boury
//
// Version: v0.2
// Latest revision: 11/01/2017
//
//********************************
using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class KitManager : EditorWindow
{
    public string error = "";
    public bool complete = false;
    public bool rootSelected = false;
    public GUIStyle errorstyle = new GUIStyle();
    public Vector2 scrollPos;
    public GameObject rootObject = null;


    public List<kitGroup> kits = new List<kitGroup>();

    public KitValues kitvalues = null;

    public List<GameObject> tempList = new List<GameObject>();
    public List<GameObject> duplicateList = new List<GameObject>();

    // Add menu item
    [MenuItem("DAE Research/Kit Manager")]
    public static void ShowWindow()
    {
        //Show existing window instance. If one doesn't exist, make one.
        EditorWindow window = EditorWindow.GetWindow(typeof(KitManager));
        window.maxSize = new Vector2(500f, 800f);
        window.minSize = window.maxSize;
    }

    private Texture2D m_Logo = null;

    void OnEnable()
    {
        m_Logo = (Texture2D)Resources.Load("logo", typeof(Texture2D));
        SetError("");
        tempList.Clear();
        duplicateList.Clear();
        complete = false;
        rootSelected = false;
    }
    void SetError(string msg)
    {
        if (msg != "") error = "Error: " + msg;
        else error = "";
    }
    void ResetKitManager()
    {
        kits.Clear();
        rootObject = null;
        SetError("");
        rootSelected = false;
        //complete = false;
    }
    void Kitting()
    {
        Debug.Log("kitting");

        
        if (!rootObject.GetComponent<KitValues>())
        {
            kitvalues = rootObject.AddComponent<KitValues>();
        }
        else
        {
            kitvalues = rootObject.GetComponent<KitValues>();
        }
        kitvalues.SetKits(kits);
        //kitvalues.kits = kits;

        complete = true;
        //ResetKitManager();
        
        
    }
    void OnGUI()
    {
        GUILayout.Label(m_Logo);
        GUILayout.Label("Kit Manager v0.2", EditorStyles.boldLabel);
        GUILayout.Space(10);
        //COMPLETE MESSAGE
        if (complete)
        {
            errorstyle.normal.textColor = new Color(0f,0.5f,0f);
            errorstyle.fontStyle = FontStyle.Bold;
            errorstyle.padding.left = 15;

            GUILayout.Label("Kits have been constructed.", errorstyle);
        }
        //ERROR HANDLE
        errorstyle.normal.textColor = Color.red;
        errorstyle.fontStyle = FontStyle.Bold;
        errorstyle.padding.left = 15;
        GUILayout.Label(error, errorstyle);
        GUILayout.Space(10);

        if (!rootSelected)
        {
            GUILayout.Label("Select root assembly object");
            Rect temprec = EditorGUILayout.GetControlRect();
            GUI.color = Color.clear;
            if (GUI.Button(temprec, ""))
            {
                Selection.activeGameObject = rootObject;
                SceneView.lastActiveSceneView.FrameSelected();
            }
            GUI.color = Color.white;
            rootObject = EditorGUI.ObjectField(temprec, rootObject, typeof(GameObject), true) as GameObject;
            if (rootObject != null)
            {
                if (rootObject.GetComponent<KitValues>() != null)
                {
                    kits = rootObject.GetComponent<KitValues>().GetKits();
                    rootSelected = true;
                }

            }
        }

        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Width(495), GUILayout.Height(790));

        if (rootObject != null)
        {

            for (int i = 0; i < kits.Count; ++i)
            {
                GUILayout.Label("Kit Group " + (i + 1));
                GUILayout.Space(10);
                GUILayout.Box("Drag and drop multiple objects here", GUILayout.Width(490), GUILayout.Height(50));
                if (GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition))
                {
                    DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                    if (Event.current.type == EventType.DragPerform)
                    {
                        Object[] goArr = DragAndDrop.objectReferences;

                        //run loop to clear empty data
                        for (int j = kits[i].goList.Count - 1; j >= 0; j--)
                        {
                            if (kits[i].goList[j] == null) kits[i].goList.RemoveAt(j);
                        }

                        foreach (Object g in goArr)
                        {
                            //Debug.Log("ptype = " + PrefabUtility.GetPrefabType(g));
                            if (g is GameObject && ((GameObject)g).scene.name != null)
                            {

                                foreach (GameObject go in kits[i].goList)
                                {

                                    if (go == g as GameObject)
                                    {

                                        DragAndDrop.PrepareStartDrag();
                                        Event.current.Use();
                                        return;
                                    }
                                }
                                GameObject t = g as GameObject;

                                if (t.GetComponent<AssemblyPiece>())
                                {
                                    kits[i].goList.Add(t);
                                }
                            }

                        }

                        DragAndDrop.PrepareStartDrag();
                        Event.current.Use();
                    }
                }
                GUILayout.Space(10);
                for (int j = 0; j < kits[i].goList.Count; ++j)
                {
                    EditorGUILayout.BeginHorizontal();

                    Rect temp = EditorGUILayout.GetControlRect();
                    GUI.color = Color.clear;
                    if (GUI.Button(temp, ""))
                    {
                        Selection.activeGameObject = kits[i].goList[j];
                        SceneView.lastActiveSceneView.FrameSelected();
                    }
                    if (duplicateList.Contains(kits[i].goList[j]))
                    {
                        SetError("Duplicates found");
                        GUI.color = new Color(Color.red.r, Color.red.g, Color.red.b, 0.5f);
                    }
                    else
                    {
                        SetError("");
                        GUI.color = Color.white;
                    }
                    GUI.color = Color.white;
                    kits[i].goList[j] = EditorGUI.ObjectField(temp, kits[i].goList[j], typeof(GameObject), true) as GameObject;
                    GUI.color = Color.white;
                    if (kits[i].goList[j] is GameObject && kits[i].goList[j].scene.name != null)
                    {
                        //object is a gameobject and is from scene
                    }
                    else kits[i].goList[j] = null;


                    if (GUILayout.Button(" x "))
                    {
                        kits[i].goList.RemoveAt(j);
                        j--;
                    }

                    EditorGUILayout.EndHorizontal();

                }

                if (GUILayout.Button("Add Extra Object"))
                {
                    kits[i].goList.Add(null);
                }

                GUILayout.Space(20);
            }

            if (GUILayout.Button("Add Kit Group"))
            {
                kits.Add(new kitGroup());
                kits[kits.Count - 1].goList.Add(null);
            }

            GUILayout.Space(20);

            if (GUILayout.Button("Construct Kits"))
            {
                tempList.Clear();
                duplicateList.Clear();
                if (kits.Count != 0)
                {
                    for (int i = 0; i < kits.Count; ++i)
                    {
                        for (int j = 0; j < kits[i].goList.Count; ++j)
                        {
                            if (!tempList.Contains(kits[i].goList[j]))
                            {
                                tempList.Add(kits[i].goList[j]);
                            }
                            else
                            {
                                duplicateList.Add(kits[i].goList[j]);
                            }
                        }
                    }

                    if (rootObject == null)
                    {
                        SetError("No root assembly sellected");
                    }
                    else
                    {
                        if (duplicateList.Count == 0)
                        {
                            Kitting();
                        }
                    }
                }
                else
                {
                    SetError("You have created no kit groups");
                }

            }

            GUILayout.Space(50);
            if (GUILayout.Button("Reset Manager"))
            {
                ResetKitManager();
            }
        }
        GUILayout.Space(20);
        EditorGUILayout.EndScrollView();
    }
}
