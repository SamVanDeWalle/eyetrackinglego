
// *******************************
//
// Material Handler  
//
// Author: Marin Brouwers
//
// Version: v0.1
// Latest revision: 21/12/2016
//
//********************************
using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class MaterialHandler : EditorWindow
{

    public Object[] allObjects = new Object[0];
    public Material emptyMat;
    public bool foundMats = false;

    public class mats
    {
        public GameObject go;
        public string materialName;
        public Material mat;
    }
    public List<mats> matList = new List<mats>();

    // Add menu item named "My Window" to the Window menu
    [MenuItem("DAE Research/Material Handler")]
    public static void ShowWindow()
    {
        //Show existing window instance. If one doesn't exist, make one.
        EditorWindow window = EditorWindow.GetWindow(typeof(MaterialHandler));
        window.maxSize = new Vector2(500f, 800f);
        window.minSize = window.maxSize;
    }

    private Texture2D m_Logo = null;

    void OnEnable()
    {
        m_Logo = (Texture2D)Resources.Load("logo", typeof(Texture2D));
    }

    void OnGUI()
    {
        GUILayout.Label(m_Logo);
        GUILayout.Label("Material Handler", EditorStyles.boldLabel);
        GUILayout.Space(20);
        GUILayout.Label("Step 1: Search for possible connections betweet models and materials");
        if (GUILayout.Button("Generate Material Connection In Scene"))
        {
            //clear lists
            matList.Clear();

            //find all objects
            allObjects = GameObject.FindObjectsOfType<GameObject>();

            //get all objects from scene, unity returns an Object[]
            // create list to store our GameObjects
            //change the objects to gameobjects
            foreach (object o in allObjects)
            {
                GameObject g = (GameObject)o;

                //string operation to find MaterialName in GameObjects
                //keep in mind dat we are looking for names like Objectname_MaterialType_00
                //example: Smallbox_Wood_73
                //we wil do string operation to only keep 'wood'

                string name = g.name.ToString();

                if (name.Contains("_"))
                {
                    name = name.Substring(name.IndexOf('_') + 1);

                    if (name.Contains("_"))
                    {
                        int index = name.IndexOf("_");
                        if (index > 0) name = name.Substring(0, index);

                        mats ml = new mats();
                        ml.go = g;
                        ml.materialName = name;
                        ml.mat = emptyMat;
                        matList.Add(ml);

                        foundMats = true;
                    }
                }
            }
        }
        if (!foundMats)
        {
            GUILayout.Space(20);
            GUILayout.Label("Nothing found yet!");
        }
        else
        {
            List<string> checkDuplicates = new List<string>();
            if (matList.Count == 0)
            {
                foundMats = false;
            }
            else
            {
                for (int i = 0; i < matList.Count; ++i)
                {
                    if (!checkDuplicates.Contains(matList[i].materialName))
                    {
                        checkDuplicates.Add(matList[i].materialName);
                        GUILayout.Space(20);
                        EditorGUILayout.BeginHorizontal();

                        int x = 0;
                        for (int c = 0; c < matList.Count; c++)
                        {
                            if (matList[c].materialName == matList[i].materialName) x += 1;
                        }
                        GUILayout.Label(x + " x ", EditorStyles.boldLabel);

                        EditorGUILayout.SelectableLabel(matList[i].materialName, EditorStyles.textField, GUILayout.Height(EditorGUIUtility.singleLineHeight));

                        string[] lookFor = new string[] { "Assets/_PredefinedMaterials" };
                        string[] guids2 = AssetDatabase.FindAssets(matList[i].materialName + " t:Material", lookFor);

                        matList[i].mat = EditorGUILayout.ObjectField(matList[i].mat, typeof(Material), true) as Material;


                        if (GUILayout.Button(" x "))
                        {
                            matList.RemoveAt(i);
                            i--;
                        }
                        EditorGUILayout.EndHorizontal();

                        if (guids2.Length == 1)
                        {
                            foreach (string guid in guids2)
                            {
                                matList[i].mat = (Material)AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(guid), typeof(Material));
                            }
                        }
                        else if (guids2.Length > 1)
                        {
                            EditorGUILayout.BeginHorizontal();
                            GUILayout.Label(matList[i].materialName + ": conflicted materials, assign manually!", EditorStyles.boldLabel);
                            EditorGUILayout.EndHorizontal();
                        }
                        else if (guids2.Length == 0)
                        {
                            EditorGUILayout.BeginHorizontal();
                            GUILayout.Label(matList[i].materialName + ": no material found, assign manually!", EditorStyles.boldLabel);
                            EditorGUILayout.EndHorizontal();
                        }

                    }
                }

            }

            checkDuplicates.Clear();

            GUILayout.Space(20);
            GUILayout.Label("Step 2: Now we assign all found material connections to the scene");
            if (GUILayout.Button("Assign Materials To Objects In Scene "))
            {
                for (int i = 0; i < matList.Count; i++)
                {
                    for (int y = 0; y < matList.Count; y++)
                    {
                        if (matList[y].materialName == matList[i].materialName)
                        {
                            matList[y].mat = matList[i].mat;
                        }
                    }


                    if (matList[i].go.GetComponent<MeshRenderer>()) matList[i].go.GetComponent<MeshRenderer>().material = matList[i].mat;
                }
                //matList.Clear();
                //allObjects = new Object[0];
            }
            GUILayout.Space(50);
            if (GUILayout.Button("Clear Data"))
            {
                matList.Clear();
                allObjects = new Object[0];
                foundMats = false;
            }
        }
    }
}

