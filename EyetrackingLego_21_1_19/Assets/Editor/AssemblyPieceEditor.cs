﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AssemblyPiece))]
public class AssemblyPieceEditor : Editor
{
    AssemblyPiece _piece;
    bool _bKeepObjects;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        _piece = (AssemblyPiece)target;

        EditorGUILayout.BeginHorizontal();
        _bKeepObjects = GUILayout.Toggle(_bKeepObjects, "Keep objects");

        if(GUILayout.Button("Create instance on selected objects"))
        {
            CreateInstanceOnSelectedTransforms();
        }
        EditorGUILayout.EndHorizontal();
    }

    void CreateInstanceOnSelectedTransforms()
    {
        var selectedObjects = Selection.gameObjects;
        var mainMesh = _piece.GetComponentInChildren<Renderer>().transform;// Won't work on multi mesh prefabs!...
        var translationOffset = mainMesh.position - _piece.transform.position;
        var rotationOffset = Quaternion.Inverse(mainMesh.transform.rotation * Quaternion.Inverse(_piece.transform.rotation));
        for (int i = selectedObjects.Length - 1; i >= 0; --i)
        {
            var copy = PrefabUtility.InstantiatePrefab(PrefabUtility.GetCorrespondingObjectFromSource(_piece.gameObject)) as GameObject;
            copy.transform.position = selectedObjects[i].transform.position;
            copy.transform.rotation = selectedObjects[i].transform.rotation;
            copy.transform.parent = _piece.transform.parent;
            copy.transform.position -= translationOffset;
            copy.transform.rotation = copy.transform.rotation * rotationOffset;

            if (!_bKeepObjects)
                DestroyImmediate(selectedObjects[i]);
        }
    }
}
