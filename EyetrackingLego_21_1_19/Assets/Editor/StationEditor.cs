﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Station))]
public class StationEditor : Editor
{
    Station _station;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        _station = (Station)target;

        if(GUILayout.Button("Save"))
        {
            _station.Save();
        }

        if (GUILayout.Button("Load"))
        {
            _station.Load();
        }
    }
}
