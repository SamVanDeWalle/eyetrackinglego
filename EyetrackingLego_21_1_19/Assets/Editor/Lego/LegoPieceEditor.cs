﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LegoPiece))]
public class LegoPieceEditor : Editor
{
    LegoPiece _legoPiece;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        _legoPiece = (LegoPiece)target;

        if (GUILayout.Button("Create Connection grids"))
        {
            _legoPiece.CreateMatricesAndConnectionPoints();
        }
    }
}
