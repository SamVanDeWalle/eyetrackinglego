﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AssemblyFicheGeneric))]
public class AssemblyFicheGenericEditor : Editor
{
    StepType _type = StepType.Connection;
    AssemblyFicheGeneric _fiche;
    Vector2 _scrollPosition;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        _fiche = (AssemblyFicheGeneric)target;


        if (GUILayout.Button("Load"))
        {
            var assemblyName = FindObjectOfType<TrainingManager>().AssemblyName;
            var recording = AssemblyRecording.Load("assembly_recording_" + assemblyName);
            _fiche.Steps = recording.TaskSteps;
        }

        if (GUILayout.Button("Save"))
        {
            var recording = new AssemblyRecording();
            recording.TaskSteps = _fiche.Steps;
            var assemblyName = FindObjectOfType<TrainingManager>().AssemblyName;
            recording.Save("assembly_recording_" + assemblyName);
        }

        if(GUILayout.Button("Create ConnectionStep from selection"))
        {
            CreateStepFromSelection();
        }

        _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition);

        EditorGUILayout.BeginHorizontal();
        _type = (StepType)EditorGUILayout.EnumPopup(_type);
        if (GUILayout.Button("Add step"))
        {
            var type = AssemblyFicheGeneric.StepTypes[_type];
            var step = AssemblyFicheGeneric.StepCreators[_type].Create();
            step.ID = _fiche.Steps.Count;
            _fiche.Steps.Add(step);
            switch (_type)
            {
                case StepType.Connection:
                    {
                        var typedStep = step as ConnectionStep;
                        _fiche.ConnectionSteps.Add(typedStep);
                        step = typedStep;
                        break;
                    }
                case StepType.PropertyChange:
                    {
                        var typedStep = step as PropertyChangeStep;
                        _fiche.PropertyChangeSteps.Add(typedStep);
                        step = typedStep;
                        break;
                    }
            }
            //else if (_type == StepType.PropertyChange)
            //    fiche.Prop.Add(step as ConnectionStep);
            //var creatorType = typeof(StepCreator<>).MakeGenericType(type);
            //dynamic creator = Activator.CreateInstance(creatorType,
            //      new object[] { null, null });
            //fiche.StepsPerType[_type].Add(creator.Create());
            //fiche.ConnectionSteps.Add(creator.Create());
        }

        EditorGUILayout.EndHorizontal();
        var stepsCopy = new List<TaskStep>(_fiche.Steps);
        for(int i = 0; i < stepsCopy.Count; ++i)
        {
            EditorGUILayout.LabelField("Step " + (i + 1));
            ShowStep(stepsCopy[i], i);
            EditorGUILayout.Space();
        }

        EditorGUILayout.EndScrollView();
    }

    public void ShowStep(TaskStep step, int index)
    {
        var type = step.Type;
        EditorGUILayout.LabelField("(" + step.Type.ToString() + ")");
        EditorGUILayout.BeginHorizontal();
        if(index > 0)
        {
            if (GUILayout.Button("<<"))
            {
                MoveItem(_fiche.Steps, index, -index);
            }
            if (GUILayout.Button("<"))
            {
                MoveItem(_fiche.Steps, index, -1);
            }
        }
        if(index < _fiche.Steps.Count - 1)
        {
            if (GUILayout.Button(">"))
            {
                MoveItem(_fiche.Steps, index, 1);
            }
            if (GUILayout.Button(">>"))
            {
                MoveItem(_fiche.Steps, index, _fiche.Steps.Count - index - 1);
            }
        }
        EditorGUILayout.EndHorizontal();
        switch (step.Type)
        {
            case StepType.Connection:
                {
                    ConnectionStep connectionStep = step as ConnectionStep;
                    if(connectionStep == null)
                        break;
                    //Debug.Log("connectionStep: " + connectionStep);
                    connectionStep.ID = EditorGUILayout.IntField("ID", step.ID);
                    connectionStep.PrefabTypeID = EditorGUILayout.IntField("TypeID", connectionStep.PrefabTypeID);
                    connectionStep.ConnectionTypeID = EditorGUILayout.IntField("ConnectionTypeID", connectionStep.ConnectionTypeID);
                    connectionStep.Quantity = EditorGUILayout.IntField("Quantity", connectionStep.Quantity);
                    connectionStep.AutoCompleteQuantity = EditorGUILayout.Toggle("AutoCompleteQuantity", connectionStep.AutoCompleteQuantity);
                    connectionStep.RequiredQuantity = EditorGUILayout.IntField("RequiredQuantity", connectionStep.RequiredQuantity);
                    connectionStep.ParentTypeID = EditorGUILayout.IntField("ParentTypeID", connectionStep.ParentTypeID);
                    connectionStep.CombinedStep = EditorGUILayout.Toggle("CombineNext", connectionStep.CombinedStep);
                    connectionStep.CombinedStepID = EditorGUILayout.IntField("CombinedID", connectionStep.CombinedStepID);
                    connectionStep.Image = EditorGUILayout.ObjectField("Image", connectionStep.Image, typeof(Sprite), true) as Sprite;
                    connectionStep.Info = EditorGUILayout.TextField(connectionStep.Info, GUILayout.Height(40));
                    connectionStep.RequireWorkbenchAngle = EditorGUILayout.Toggle("RequiredWorkbenchAngle", connectionStep.RequireWorkbenchAngle);
                    connectionStep.WorkbenchAngle = EditorGUILayout.Slider("WorkbenchAngle", connectionStep.WorkbenchAngle, 0, 360);
                    step.Image = connectionStep.Image;
                    step.Info = connectionStep.Info;
                    step.WorkbenchAngle = connectionStep.WorkbenchAngle;
                    step = connectionStep;
                    break;
                }
            case StepType.PropertyChange:
                {
                    PropertyChangeStep propertyChangeStep = step as PropertyChangeStep;
                    propertyChangeStep.ID = EditorGUILayout.IntField("ID", step.ID);
                    propertyChangeStep.PrefabTypeID = EditorGUILayout.IntField("TypeID", propertyChangeStep.PrefabTypeID);
                    propertyChangeStep.PropertyID = EditorGUILayout.IntField("PropertyID", propertyChangeStep.PropertyID);
                    propertyChangeStep.PropertyType = (PropertyType)EditorGUILayout.EnumPopup("PropertyType", propertyChangeStep.PropertyType);
                    propertyChangeStep.Quantity = EditorGUILayout.IntField("Quantity", propertyChangeStep.Quantity);
                    propertyChangeStep.CombinedStepID = EditorGUILayout.IntField("CombinedID", propertyChangeStep.CombinedStepID);
                    propertyChangeStep.Image = EditorGUILayout.ObjectField("Image", propertyChangeStep.Image, typeof(Sprite), true) as Sprite;
                    propertyChangeStep.Info = EditorGUILayout.TextArea(propertyChangeStep.Info, GUILayout.Height(40));
                    step.Image = propertyChangeStep.Image;
                    step.Info = propertyChangeStep.Info;
                    step = propertyChangeStep;
                    break;
                }
        }

        if (GUILayout.Button("x"))
        {
            _fiche.Steps.RemoveAt(index);
        }
    }

    static void MoveItem(List<TaskStep> list, int index, int offset)
    {
        var newIndex = index + offset;
        var item = list[index];
        list.RemoveAt(index);
        //if (newIndex > index) newIndex--;
        // the actual index could have shifted due to the removal
        list.Insert(newIndex, item);
        Debug.Log(list);
    }

    void CreateStepFromSelection()
    {
        // Make sure max 2 types are selected
        var selectedObjects = Selection.gameObjects;
        var selectedAPs = selectedObjects.Where(so => so.GetComponent<AssemblyPiece>() != null).Select(so => so.GetComponent<AssemblyPiece>()).ToArray();
        if(selectedAPs.Length == 2)
        {
            selectedAPs[0].LoadConnectionPoints();
            selectedAPs[1].LoadConnectionPoints();
            var connectionMatch = ConnectionManager.FindConnectionMatches(selectedAPs[0], selectedAPs[1]).FirstOrDefault();
            if(connectionMatch != null)
            {
                var connectionStep = new ConnectionStep();
                connectionStep.ID = _fiche.Steps.Count;
                connectionStep.PrefabTypeID = connectionMatch.GetMale().ParentPiece.TypeID;
                connectionStep.ParentTypeID = connectionMatch.GetFemale().ParentPiece.TypeID;
                connectionStep.ConnectionTypeID = connectionMatch.A.TypeID;
                connectionStep.Quantity = 1;
                _fiche.Steps.Add(connectionStep);
                _fiche.ConnectionSteps.Add(connectionStep);
            }
        }
    }
}


