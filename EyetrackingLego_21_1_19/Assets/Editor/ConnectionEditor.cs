﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

class PreviewConnectionPointData
{
    public AssemblyPiece AP;
    public Transform APParent;
    public Quaternion InitialRotation;

    public PreviewConnectionPointData(AssemblyPiece ap, Transform apParent, Quaternion rotation)
    {
        AP = ap;
        APParent = apParent;
        InitialRotation = rotation;
    }
}

public class ConnectionEditor : EditorWindow
{
    Texture2D _logo;
    AssemblyPiece _searchPiece;
    List<ConnectionType> _filteredTypes;
    List<ConnectionType> _types;
    int _selectedTypeIndex;
    ConnectionType _type;
    List<AssemblyPiece> _assemblyPieces;        // Assemblypieces related to selected connection type
    Dictionary<AssemblyPiece, List<ConnectionPoint>> _connectionPoints;
    Dictionary<ConnectionPoint, bool> _checkedConnectionPoints;
    Vector2 _typeListScrollPosition;
    Vector2 _cpScrollPosition;
    Vector2 _assemblyNamesScrollPosition;
    GameObject _helpObject;
    GameObject _helpObjectA, _helpObjectB;
    string _assemblyName;
    bool _bLoaded = false;
    Vector3 _previewRotation;
    List<ConnectionPoint> _previewCPs = new List<ConnectionPoint>();
    Dictionary<ConnectionPoint, PreviewConnectionPointData> _previewCPData = new Dictionary<ConnectionPoint, PreviewConnectionPointData>();
    List<string> _assemblyNames = new List<string>() { "test", "test2" };
    int _selectedAssemblyIndex;

    public QuickConnectionEditor QuickConnectionEditor;

    [MenuItem("DAE Research/Connections")]
    public static void ShowWindow()
    {
        EditorWindow window = GetWindow(typeof(ConnectionEditor));
    }

    void Awake()
    {
        Load();
    }

    void OnEnable()
    {
        _logo = (Texture2D)Resources.Load("logo", typeof(Texture2D));

        _types = new List<ConnectionType>();
        _filteredTypes = new List<ConnectionType>();
        _assemblyPieces = new List<AssemblyPiece>();
        _connectionPoints = new Dictionary<AssemblyPiece, List<ConnectionPoint>>();
        _checkedConnectionPoints = new Dictionary<ConnectionPoint, bool>();

        LoadAssemblies();
        Load();
    }

    void OnGUI()
    {
        GUILayout.Label(_logo);
        
        _assemblyName = EditorGUILayout.TextField(_assemblyName);
        if (_assemblyName == null)
            _assemblyName = "";
        _assemblyNamesScrollPosition = GUILayout.BeginScrollView(_assemblyNamesScrollPosition, GUILayout.Height(60));
        for (int i = 0; i < _assemblyNames.Count; ++i)
        {
            if(_assemblyName == "" || (_assemblyName != null && _assemblyNames[i].Contains(_assemblyName, System.StringComparison.OrdinalIgnoreCase)))
            {
                var clickRect = EditorGUILayout.GetControlRect();
                //GUI.color = Color.clear;
                if (GUI.Button(clickRect, _assemblyNames[i]))
                {
                    _assemblyName = _assemblyNames[i];
                    GUI.FocusControl(null);
                    Repaint();
                }
                GUI.color = Color.white;
                //EditorGUILayout.SelectableLabel(_assemblyNames[i], GUILayout.Height(18));
            }
        }
        GUILayout.EndScrollView();

        if (_assemblyNames.Contains(_assemblyName))
        {
            if (GUILayout.Button("Load connection types"))
            {
                Load();
            }
        }
        else if(_assemblyName != "")
        {
            if (GUILayout.Button("Create new assembly"))
            {
                Load();
                Save();
            }
        }
        

        if (!_bLoaded)
            return;

        EditorGUI.BeginDisabledGroup(!_bLoaded);
        if (GUILayout.Button("Save connection types"))
        {
            Save();
        }

        EditorGUILayout.Space();

        GUI.color = Color.yellow;
        if (GUILayout.Button("Quick connection"))
        {
            QuickConnectionEditor.ShowWindow(this);
        }
        GUI.color = Color.white;

        if (GUILayout.Button("Create new connection type"))
        {
            var newType = new ConnectionType();
            newType.Name = "MyConnection";
            newType.ID = _types.Count;
            _types.Add(newType);
            _filteredTypes.Add(newType);
            _selectedTypeIndex = newType.ID;
            _type = _types[_selectedTypeIndex];
            FindAssemblyPieces(newType.ID);

            if (_searchPiece != null && !_assemblyPieces.Contains(_searchPiece))
            {
                _assemblyPieces.Add(_searchPiece);
                LoadConnectionPoints(_searchPiece);
            }

            GUI.FocusControl(null);
        }

        if(GUILayout.Button("One on one from selection with mean position"))
        {
            OneOnOneFromSelection();
        }

        if (GUILayout.Button("Show connection orientations"))
        {
            ShowConnectionOrientations();
        }

        EditorGUILayout.Space();// ------------------------------------------------------------------------------------------
        // Type list
        GUILayout.Label("Connection Types:", EditorStyles.boldLabel);
        // Filter on given AssemblyPiece
        var lastSearchPiece = _searchPiece;
        GUILayout.BeginHorizontal();
        _searchPiece = EditorGUILayout.ObjectField("Filter:", _searchPiece, typeof(AssemblyPiece), true) as AssemblyPiece;
        if (GUILayout.Button("Clear"))
        {
            _searchPiece = null;
        }

        GUILayout.EndHorizontal();
        if (_searchPiece != lastSearchPiece)
        {
            _checkedConnectionPoints.Clear();
            FilterTypes(_searchPiece);
        }

        var typeNames = _filteredTypes.Select(t => t.Name).ToArray<string>();
        var lastTypeIndex = _selectedTypeIndex;
        _typeListScrollPosition = GUILayout.BeginScrollView(_typeListScrollPosition, GUILayout.Height(100));
        GUILayout.BeginVertical("Connection Types", EditorStyles.helpBox);
        _selectedTypeIndex = GUILayout.SelectionGrid(_selectedTypeIndex, typeNames, 1);
        GUILayout.EndVertical();
        GUILayout.EndScrollView();
        if(_selectedTypeIndex != lastTypeIndex)
        {
            _type = _filteredTypes[_selectedTypeIndex];
            _connectionPoints.Clear();
            _checkedConnectionPoints.Clear();
            FindAssemblyPieces(_filteredTypes[_selectedTypeIndex].ID);
            GUI.FocusControl(null);
        }

        if (_filteredTypes.Count == 0)
            return;

        if (_selectedTypeIndex >= _filteredTypes.Count)
            _selectedTypeIndex = 0;

        _type = _filteredTypes[_selectedTypeIndex];

        EditorGUILayout.Space();// ------------------------------------------------------------------------------------------
        // Edit type
        GUILayout.Label("Edit type:", EditorStyles.boldLabel);
        _type.Name = EditorGUILayout.TextField("Name", _type.Name);

        // Connection parameters

        EditorGUILayout.BeginHorizontal();
        _type.XRotationType = (RotationType)EditorGUILayout.EnumPopup("X rotation", _type.XRotationType);
        if(_type.XRotationType == RotationType.Step)
        {
            _type.StepX = EditorGUILayout.FloatField(_type.StepX);
        }
        EditorGUILayout.EndHorizontal();
        if (_type.XRotationType == RotationType.Step || _type.XRotationType == RotationType.Free)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUI.indentLevel++;
            EditorGUILayout.MinMaxSlider("X Rotation limits", ref _type.xRotationBounds.x, ref _type.xRotationBounds.y, 0, 360);
            EditorGUI.indentLevel--;
            EditorGUILayout.EndHorizontal();
        }

        if (Event.current.type == EventType.MouseUp)
        {
            _previewRotation.x = 0;
            _previewRotation.y = 0;
            _previewRotation.z = 0;
            ResetPreviewRotation();
        }

        _previewRotation.x = EditorGUILayout.Slider("Preview X", _previewRotation.x, 0, 360);

        EditorGUILayout.BeginHorizontal();
        _type.YRotationType = (RotationType)EditorGUILayout.EnumPopup("Y rotation", _type.YRotationType);
        if (_type.YRotationType == RotationType.Step)
        {
            _type.StepY = EditorGUILayout.FloatField(_type.StepY);
        }
        EditorGUILayout.EndHorizontal();
        if (_type.YRotationType == RotationType.Step || _type.YRotationType == RotationType.Free)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUI.indentLevel++;
            EditorGUILayout.MinMaxSlider("Y Rotation limits", ref _type.yRotationBounds.x, ref _type.yRotationBounds.y, 0, 360);
            EditorGUI.indentLevel--;
            EditorGUILayout.EndHorizontal();
        }

        _previewRotation.y = EditorGUILayout.Slider("Preview Y", _previewRotation.y, 0, 360);

        EditorGUILayout.BeginHorizontal();
        _type.ZRotationType = (RotationType)EditorGUILayout.EnumPopup("Z rotation", _type.ZRotationType);
        if (_type.ZRotationType == RotationType.Step)
        {
            _type.StepZ = EditorGUILayout.FloatField(_type.StepZ);
        }
        EditorGUILayout.EndHorizontal();
        if (_type.ZRotationType == RotationType.Step || _type.ZRotationType == RotationType.Free)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUI.indentLevel++;
            EditorGUILayout.MinMaxSlider("Z Rotation limits", ref _type.zRotationBounds.x, ref _type.zRotationBounds.y, 0, 360);
            EditorGUI.indentLevel--;
            EditorGUILayout.EndHorizontal();
        }

        _previewRotation.z = EditorGUILayout.Slider("Preview Z", _previewRotation.z, 0, 360);

        if (_previewRotation.x > 0 || _previewRotation.y > 0 || _previewRotation.z > 0)
            SetPreviewRotation();

        EditorGUILayout.Space();

        _type.RequireOrientation = EditorGUILayout.Toggle("Require Orientation", _type.RequireOrientation);

        EditorGUILayout.Space();
        EditorGUILayout.Space();// ------------------------------------------------------------------------------------------
        GUILayout.Label("Connection points:", EditorStyles.boldLabel);
        _cpScrollPosition = GUILayout.BeginScrollView(_cpScrollPosition);
        // Assembly pieces
        for (int i = 0; i < _assemblyPieces.Count; ++i)
        {
            EditorGUILayout.BeginHorizontal();
            var lastAp = _assemblyPieces[i];
            _assemblyPieces[i] = EditorGUILayout.ObjectField(_assemblyPieces[i], typeof(AssemblyPiece), true) as AssemblyPiece;
            if (GUILayout.Button("x"))
            {
                _assemblyPieces.RemoveAt(i);
                break;
            }
            EditorGUILayout.EndHorizontal();

            if (_assemblyPieces[i] == null)
                continue;

            if (_assemblyPieces[i] != lastAp && _assemblyPieces[i] != null)
            {
                // Check for duplicate
                for(int a = 0; a < _assemblyPieces.Count; ++a)
                {
                    if(a != i && _assemblyPieces[a] == _assemblyPieces[i])
                    {
                        _assemblyPieces[i] = null;
                        break;
                    }
                }

                if (_assemblyPieces[i] == null)
                    continue;

                LoadConnectionPoints(_assemblyPieces[i]);       
            }

            // Connection points
            EditorGUI.indentLevel++;
            for (int j = 0; j < _connectionPoints[_assemblyPieces[i]].Count; ++j)
            {
                var cp = _connectionPoints[_assemblyPieces[i]][j];
                EditorGUILayout.BeginHorizontal();
                if (!_checkedConnectionPoints.ContainsKey(cp))
                {
                    _checkedConnectionPoints.Add(cp, false);
                }

                var clickRect = EditorGUILayout.GetControlRect();
                GUI.color = Color.clear;
                if (GUI.Button(clickRect, ""))
                {
                    _checkedConnectionPoints[cp] = !_checkedConnectionPoints[cp];
                    Selection.activeGameObject = cp.gameObject;
                }
                GUI.color = Color.white;
                if (_checkedConnectionPoints[cp])
                {
                    EditorGUI.DrawRect(clickRect, Color.yellow);
                }
                
                _connectionPoints[_assemblyPieces[i]][j] = EditorGUI.ObjectField(clickRect, _connectionPoints[_assemblyPieces[i]][j], typeof(ConnectionPoint), true) as ConnectionPoint;

                _connectionPoints[_assemblyPieces[i]][j].Gender = (ConnectionGender)EditorGUILayout.EnumPopup("Gender", _connectionPoints[_assemblyPieces[i]][j].Gender);
                if (GUILayout.Button("x"))
                {
                    DestroyImmediate(_connectionPoints[_assemblyPieces[i]][j].gameObject);
                    _connectionPoints[_assemblyPieces[i]].RemoveAt(j);
                    break;
                }
                EditorGUILayout.EndHorizontal();
            }

            if (GUILayout.Button("Add ConnectionPoint", GUILayout.Width(200)))
            {
                var cpgo = new GameObject("CP");
                cpgo.transform.position = _assemblyPieces[i].transform.position;
                cpgo.transform.SetParent(_assemblyPieces[i].transform);
                var cp = cpgo.AddComponent<ConnectionPoint>();
                _assemblyPieces[i].LoadConnectionPoints();
                cp.ID = _assemblyPieces[i].ConnectionPointsUnordered.Length-1;
                cp.TypeID = _type.ID;
                _connectionPoints[_assemblyPieces[i]].Add(cp);
            }

            if(GUILayout.Button("Set checked CP's as interchangeable"))
            {
                var groupedCPs = new List<ConnectionPoint>();
                var lowestID = -1;
                for(int j = 0; j < _connectionPoints[_assemblyPieces[i]].Count; ++j)
                {
                    var cp = _connectionPoints[_assemblyPieces[i]][j];
                    if (!_checkedConnectionPoints.ContainsKey(cp) || _checkedConnectionPoints[cp] == false)
                        continue;

                    if (lowestID == -1 || (cp.ID < lowestID && cp.Gender == groupedCPs[0].Gender))
                        lowestID = cp.ID;

                    groupedCPs.Add(cp);
                }

                if(groupedCPs.Count > 1)
                {
                    foreach (var p in groupedCPs)
                    {
                        p.GroupID = lowestID;
                    }
                }
            }

            EditorGUI.indentLevel--;
        }
        GUILayout.EndScrollView();

        EditorGUILayout.Space();
        if (GUILayout.Button("Add AssemblyPiece"))
        {
            _assemblyPieces.Add(null);
        }

        EditorGUILayout.Space(); // ------------------------------------------------------------------------------------------
        GUILayout.Label("Connection point positioning:", EditorStyles.boldLabel);
        GUILayout.BeginHorizontal();
        //var selectedCPs = Selection.transforms.Where(s => s.gameObject.GetComponent<ConnectionPoint>() != null).ToArray();
        var selectedCPs = _checkedConnectionPoints.Where(s => s.Value == true && s.Key != null).Select(s => s.Key).ToArray();
        _helpObject = EditorGUILayout.ObjectField(_helpObject, typeof(GameObject), true) as GameObject;
        if(_helpObject != null && GUILayout.Button("Center checked CP's to this"))
        {
            for (int i = 0; i < selectedCPs.Length; ++i)
            {
                selectedCPs[i].transform.position = _helpObject.transform.position;
                selectedCPs[i].transform.rotation = _helpObject.transform.rotation;
            }
        }
        if (_helpObject != null && GUILayout.Button("Center all CP's to this"))
        {
            for (int i = 0; i < _assemblyPieces.Count; ++i)
            {
                for (int j = 0; j < _connectionPoints[_assemblyPieces[i]].Count; ++j)
                {
                    _connectionPoints[_assemblyPieces[i]][j].transform.position = _helpObject.transform.position;
                    _connectionPoints[_assemblyPieces[i]][j].transform.rotation = _helpObject.transform.rotation;
                }
            }
        }
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        _helpObjectA = EditorGUILayout.ObjectField(_helpObjectA, typeof(GameObject), true) as GameObject;
        _helpObjectB = EditorGUILayout.ObjectField(_helpObjectB, typeof(GameObject), true) as GameObject;
        if (_helpObjectA != null && _helpObjectB != null && GUILayout.Button("Set selected CP's to center between"))
        {
            var centerPos = (_helpObjectB.transform.position + _helpObjectA.transform.position) / 2.0f;
            for (int i = 0; i < selectedCPs.Length; ++i)
            {
                selectedCPs[i].transform.position = centerPos;
                selectedCPs[i].transform.rotation = _helpObjectA.transform.rotation;
            }
        }
        if (_helpObjectA != null && _helpObjectB != null && GUILayout.Button("Set CP's to center between"))
        {
            var centerPos = (_helpObjectB.transform.position + _helpObjectA.transform.position) / 2.0f;
            for (int i = 0; i < _assemblyPieces.Count; ++i)
            {
                for (int j = 0; j < _connectionPoints[_assemblyPieces[i]].Count; ++j)
                {
                    _connectionPoints[_assemblyPieces[i]][j].transform.position = centerPos;
                    _connectionPoints[_assemblyPieces[i]][j].transform.rotation = _helpObjectA.transform.rotation;
                }
            }
        }
        GUILayout.EndHorizontal();
        EditorGUI.EndDisabledGroup();
    }

    void OneOnOneFromSelection()
    {
        var selectedGOs = Selection.gameObjects;
        if(selectedGOs.Length != 2)
        {
            // TODO Set error msg
            return;
        }

        if(selectedGOs[0].GetComponent<AssemblyPiece>() == null || selectedGOs[1].GetComponent<AssemblyPiece>() == null)
        {
            // TODO Set error msg
            return;
        }

        // Create new connection type
        var newType = new ConnectionType();
        newType.Name = selectedGOs[0].name + "-" + selectedGOs[1].name;
        newType.ID = _types.Count;
        _types.Add(newType);
        _filteredTypes.Add(newType);
        _selectedTypeIndex = newType.ID;
        _type = _types[_selectedTypeIndex];

        // Create connectionpoints
        var meanPosition = selectedGOs[0].transform.position + (selectedGOs[1].transform.position - selectedGOs[0].transform.position) / 2.0f;
        for(int i = 0; i < selectedGOs.Length; ++i)
        {
            var cpgo = new GameObject("CP");
            cpgo.transform.position = meanPosition;
            cpgo.transform.rotation = selectedGOs[1].transform.rotation;
            cpgo.transform.SetParent(selectedGOs[i].transform);
            var cp = cpgo.AddComponent<ConnectionPoint>();
            var ap = selectedGOs[i].GetComponent<AssemblyPiece>();
            ap.LoadConnectionPoints();
            cp.ID = ap.ConnectionPointsUnordered.Length - 1;
            cp.TypeID = _type.ID;
            if (i == 0)
                cp.Gender = ConnectionGender.Female;
            //_connectionPoints[_assemblyPieces[i]].Add(cp);
            PrefabUtility.ReplacePrefab(selectedGOs[i], PrefabUtility.GetCorrespondingObjectFromSource(selectedGOs[i]), ReplacePrefabOptions.ConnectToPrefab);
        }

    }

    public ConnectionType CreateNewConnectionType(string name)
    {
        var newType = new ConnectionType();
        newType.Name = name;
        newType.ID = _types.Count;
        _types.Add(newType);
        _filteredTypes.Add(newType);
        _selectedTypeIndex = newType.ID;
        _type = _types[_selectedTypeIndex];
        return _type;
    }

    public GameObject CreateConnectionPoint(AssemblyPiece piece, ConnectionType type, Vector3 position, Quaternion rotation, ConnectionGender gender)
    {
        var cpgo = new GameObject("CP");
        cpgo.transform.position = position;
        cpgo.transform.rotation = rotation;
        cpgo.transform.SetParent(piece.transform);
        var cp = cpgo.AddComponent<ConnectionPoint>();
        piece.LoadConnectionPoints();
        cp.ID = piece.ConnectionPointsUnordered.Length - 1;
        cp.TypeID = type.ID;
        cp.Gender = gender;
        //_connectionPoints[_assemblyPieces[i]].Add(cp);
        return PrefabUtility.ReplacePrefab(piece.gameObject, PrefabUtility.GetCorrespondingObjectFromSource(piece.gameObject), ReplacePrefabOptions.ConnectToPrefab);

    }

    void FindAssemblyPieces(int connectionTypeId)
    {
        _assemblyPieces.Clear();
        var assemblyPiecesInScene = FindObjectsOfType<AssemblyPiece>();

        for(int i = 0; i < assemblyPiecesInScene.Length; ++i)
        {
            assemblyPiecesInScene[i].LoadConnectionPoints();
            if (assemblyPiecesInScene[i].ConnectionPoints.ContainsKey(connectionTypeId))
            {
                _assemblyPieces.Add(assemblyPiecesInScene[i]);
                LoadConnectionPoints(assemblyPiecesInScene[i]);
            }
        }
    }

    void LoadConnectionPoints(AssemblyPiece piece)
    {
        if (_connectionPoints.ContainsKey(piece))
        {
            _connectionPoints[piece].Clear();
        }

        piece.LoadConnectionPoints();
        if (!_connectionPoints.ContainsKey(piece))
        {
            _connectionPoints.Add(piece, new List<ConnectionPoint>());
        }

        if (piece.ConnectionPoints.ContainsKey(_type.ID))
        {
            _connectionPoints[piece].Clear();
            //Debug.Log(piece.ConnectionPoints[_type.ID].Count);
            for (int k = 0; k < piece.ConnectionPoints[_type.ID].Count; ++k)
            {
                _connectionPoints[piece].Add(piece.ConnectionPoints[_type.ID][k]);
            }
        }
    }

    void FilterTypes(AssemblyPiece piece)
    {
        _filteredTypes.Clear();

        if (piece == null)
        {
            _filteredTypes = new List<ConnectionType>(_types);
            return;
        }

        piece.LoadConnectionPoints();
        foreach (var typeID in piece.ConnectionPoints.Keys)
        {
            _filteredTypes.Add(_types.FirstOrDefault(t => t.ID == typeID));
        }
    }

    void ShowConnectionOrientations()
    {
        var arrows = GameObject.FindGameObjectsWithTag("connectionArrow");
        for(int i = arrows.Length-1; i >= 0; --i)
        {
            DestroyImmediate(arrows[i]);
        }

        AssemblyPieceManager.FindAssemblyPieces();
        
        foreach(var ap in ConnectionManager.AssemblyPieces)
        {
            ap.LoadConnectionPoints();
            foreach(var cp in ap.ConnectionPointsUnordered)
            {
                var type = ConnectionManager.ConnectionTypes.FirstOrDefault(cpt => cpt.ID == cp.TypeID);
                if(type.XRotationType != RotationType.Locked)
                {
                    var arrow = Instantiate(Resources.Load("Prefabs/Arrow")) as GameObject;
                    arrow.transform.position = cp.transform.position;
                    arrow.transform.forward = cp.transform.right;
                    arrow.transform.SetParent(cp.transform);
                }
                if (type.YRotationType != RotationType.Locked)
                {
                    var arrow = Instantiate(Resources.Load("Prefabs/Arrow")) as GameObject;
                    arrow.transform.position = cp.transform.position;
                    arrow.transform.forward = cp.transform.up;
                    arrow.transform.SetParent(cp.transform);
                }
                if (type.ZRotationType != RotationType.Locked)
                {
                    var arrow = Instantiate(Resources.Load("Prefabs/Arrow")) as GameObject;
                    arrow.transform.position = cp.transform.position;
                    arrow.transform.forward = cp.transform.forward;
                    arrow.transform.SetParent(cp.transform);
                }
            }
        }
    }

    void SetPreviewRotation()
    {
        if(_previewCPs.Count == 0)
        {
            for (int i = 0; i < _assemblyPieces.Count; ++i)
            {
                var maleCPs = _connectionPoints[_assemblyPieces[i]].Where(cp => cp.Gender == ConnectionGender.Male).ToList();
                if (maleCPs.Count < 1)
                    continue;

                EditorUtility.SetDirty(_assemblyPieces[i]);
                EditorUtility.SetDirty(maleCPs[0]);
                _previewCPData.Add(maleCPs[0], new PreviewConnectionPointData(_assemblyPieces[i], _assemblyPieces[i].transform.parent, maleCPs[0].transform.rotation));
                maleCPs[0].transform.SetParent(null);
                _assemblyPieces[i].transform.SetParent(maleCPs[0].transform);
                _previewCPs.Add(maleCPs[0]);
            }
        }
        
        for(int i = 0; i < _previewCPs.Count; ++i)
        {
            _previewCPs[i].transform.rotation = _previewCPData[_previewCPs[i]].InitialRotation;
            _previewCPs[i].transform.Rotate(_previewRotation);
        }
    }

    void ResetPreviewRotation()
    {
        for (int i = 0; i < _previewCPs.Count; ++i)
        {
            _previewCPs[i].transform.rotation = _previewCPData[_previewCPs[i]].InitialRotation;
            _previewCPData[_previewCPs[i]].AP.transform.SetParent(_previewCPData[_previewCPs[i]].APParent);
            _previewCPs[i].transform.SetParent(_previewCPData[_previewCPs[i]].AP.transform);
            PrefabUtility.RevertPrefabInstance(_previewCPData[_previewCPs[i]].AP.gameObject);
        }

        _previewCPs.Clear();
        _previewCPData.Clear();
    }

    void LoadAssemblies()
    {
        _assemblyNames.Clear();
        var files = Directory.GetFiles(Directory.GetCurrentDirectory() + @"\Datapath\");

        for(int i = 0; i < files.Length; ++i)
        {
            if (files[i].Contains("connectiontypes"))
            {
                var assemblyName = files[i].Replace(Directory.GetCurrentDirectory() + @"\Datapath\", "").Replace("\\connectiontypes_", "").Replace(".json", "");
                _assemblyNames.Add(assemblyName);
                Debug.Log(assemblyName);
            }
        }
    }

    public void Load()
    {
        if (_assemblyName == null || _assemblyName == "")
            return;

        if(_connectionPoints != null)
            _connectionPoints.Clear();
        ConnectionManager.LoadConnectionTypes(_assemblyName);
        _types = ConnectionManager.ConnectionTypes;
        _filteredTypes = new List<ConnectionType>(_types);
        if (_types.Count > 0)
        {
            _type = _types[0];
            FindAssemblyPieces(_type.ID);
        }

        _bLoaded = true;
    }

    public void Save()
    {
        ConnectionManager.SaveConnectionTypes(_assemblyName);
    }

    void OnDestroy()
    {
        if (QuickConnectionEditor != null)
            QuickConnectionEditor.Close();
    }
}
