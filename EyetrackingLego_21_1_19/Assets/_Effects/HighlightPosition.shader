﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "DAER_PP/HighlightPosition"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Color("Color", Color) = (0, 0.8, 1, 1)
		_XPos("X position", Range(0, 1)) = 0
		_YPos("Y position", Range(0, 1)) = 0
		_Spread("Spread", Range(4, 20)) = 6
		_Pow("Power", Range(0, 25)) = 1
		_Intensity("Intensity", Range(0, 4)) = 1
		_Speed("Breathing Speed", Range(0, 10)) = 1
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			float4 _Color;
			float _XPos;
			float _YPos;
			float _Spread;
			float _Pow;
			float _Intensity;
			float _Speed;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				
				float xDistance = abs(_XPos - i.uv[0]);
				float xRawFactor = 1 - xDistance;
				float xFactor = pow(xRawFactor, _Spread) * 1;

				float yDistance = abs(_YPos - i.uv[1]);
				float yRawFactor = 1 - yDistance;
				float yFactor = pow(yRawFactor, _Spread) * 1;

				float timeFactor = sin(_Time[1] * _Speed) + 1;
				float power = _Pow * timeFactor;//(_SinTime[3] + 1);

				float distanceFromCenterSqr = pow(0.5 - _XPos, 2) + pow(0.5 - _YPos, 2);
				float centerFactor = distanceFromCenterSqr * 3;

				fixed4 effect = _Color * xFactor * yFactor * power * centerFactor;
				col = col + effect * _Intensity;

				return col;
			}
			ENDCG
		}
	}
}
