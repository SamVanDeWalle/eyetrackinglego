﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualizePoint : MonoBehaviour
{
    public Material Material;

    public void SetPosition(float x, float y)
    {
        Material.SetFloat("_XPos", x);
        Material.SetFloat("_YPos", y);
    }

    void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        Graphics.Blit(src, dest, Material);
    }
}
