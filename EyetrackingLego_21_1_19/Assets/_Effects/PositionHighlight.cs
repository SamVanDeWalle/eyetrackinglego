﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionHighlight : MonoBehaviour
{
    public Material Material;

    void Update()
    {
        var x = 0.0f;
        var y = 0.5f;

        //Debug.Log("pan: " + ViewGuide.RequiredPan);
        if (ViewGuide.RequiredPan > 90)
        {
            x = 1;
        }
        else if (ViewGuide.RequiredPan < -90)
        {
            x = 0;
        }
        else
        {
            x = (ViewGuide.RequiredPan / 180.0f) + 0.5f;
        }

        //Debug.Log("tilt: " + ViewGuide.RequiredTilt);
        if (ViewGuide.RequiredTilt > 90)
        {
            y = 1;
        }
        else if (ViewGuide.RequiredTilt < -90)
        {
            y = 0;
        }
        else
        {
            y = (ViewGuide.RequiredTilt / 180.0f) + 0.5f;
        }

        SetPosition(x, y);
    }

    void SetPosition(float x, float y)
    {
        Material.SetFloat("_XPos", x);
        Material.SetFloat("_YPos", y);
    }

    void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        Graphics.Blit(src, dest, Material);
    }
}
