﻿Shader "Custom/ParallaxDepth" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_Depth("Depth (RGB)", 2D) = "black" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_TexWidth("DepthTexWidth",Float) = 512
		_TexHeight("DepthTexHeight",Float) = 512
		_HeightmapStrength("HeightMapStrength",Range(-10,10)) = 0.0
	}
	SubShader{
			LOD 200
			Tags{ "RenderType" = "Opaque" "Queue" = "Geometry"}
	/*Pass{*/
			Name "Opaque"
			

			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
#pragma surface surf Standard fullforwardshadows vertex:vert

			// Use shader model 3.0 target, to get nicer looking lighting
#pragma target 3.0
#define LINEAR_SEARCH 20
#define BINARY_SEARCH 10
			sampler2D _MainTex;
			sampler2D _Depth;


		struct Input {
			float2 uv_MainTex;
			float2 uv_Depth;
			fixed3 Normal;
			fixed3 viewdir;
		};

		half _Glossiness;
		half _Metallic;
		half _TexWidth;
		half _TexHeight;
		half _HeightmapStrength;
		fixed4 _Color;


		float GetDepth(half texval)
		{
			return clamp((1 - texval),0,1);
		}
		void vert(inout appdata_full v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input, o);

			TANGENT_SPACE_ROTATION;
			o.viewdir = ObjSpaceViewDir(v.vertex);
			//o.viewdir = mul(rotation, o.viewdir);
			o.Normal = mul(rotation, v.normal);
		}
		//float ray_intersect_rm(in sampler2D reliefmap, in float2 dp, in float2 ds, in float  bias)
		//{
		//	float size = 1.0 / LINEAR_SEARCH; // current size of search window
		//	float depth = -bias;// current depth position
		//	int i;
		//	for (i = 0; i < LINEAR_SEARCH - 1; i++)// search front to back for first point inside object 
		//	{
		//		float t = clamp(1 - tex2D(reliefmap, dp - ds*depth).r,0,1);

		//		if (depth < t - bias)
		//			depth += size;
		//	}

		//	for (i = 0; i < BINARY_SEARCH; i++) // recurse around first point (depth) for closest match
		//	{
		//		size *= 0.5;

		//		float t = clamp(1 - tex2D(reliefmap, dp - ds*depth).r,0,1);

		//		if (depth < t - bias)
		//			depth += (2 * size);

		//		depth -= size;
		//	}

		//	return depth;
		//}

		void surf(Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			half texScaleX = 1 / _TexWidth;
			half texScaleY = 1 / _TexHeight;
			float size = 1.0 / LINEAR_SEARCH;
			IN.viewdir = normalize(IN.viewdir);
			fixed d = GetDepth(tex2D(_Depth, IN.uv_MainTex).r);
			float2 view = IN.viewdir.xy*_HeightmapStrength;
			view.x = -view.x;

			fixed d2 = -(1 - d)*size;// ray_intersect_rm(_Depth, IN.uv_Depth.xy, view, 1);
			float2 offset = view*d2;
			//offset.x *= -1;
			IN.uv_MainTex += offset;

			float dNorth = GetDepth(tex2D(_Depth, IN.uv_MainTex + half2(0, texScaleY)).r);
			float dEast = GetDepth(tex2D(_Depth, IN.uv_MainTex + half2(texScaleX, 0)).r);
			float dSouth = GetDepth(tex2D(_Depth, IN.uv_MainTex - half2(0, texScaleY)).r);
			float dWest = GetDepth(tex2D(_Depth, IN.uv_MainTex - half2(texScaleX, 0)).r);
			float3 norm = IN.Normal;
			float3 temp = norm; //a temporary vector that is not parallel to norm
			if (norm.x == 1)
				temp.y += 1;
			else
				temp.x += 1;

			//form a basis with norm being one of the axes:
			float3 perp1 = normalize(cross(norm, temp));
			float3 perp2 = normalize(cross(norm, perp1));

			//use the basis to move the normal in its own space by the offset
			float3 normalOffset = _HeightmapStrength * (((dNorth - d) - (dSouth - d)) * perp1 + ((dEast - d) - (dWest - d)) * perp2)*d;
			float s = 1.0 / 512;

			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;

			norm += normalOffset;
			norm = normalize(norm);

			fixed3 albedo = c.rgb;
			albedo *= saturate(d*d*d);
			//albedo = d2.xxx;
			o.Normal = norm;
			o.Albedo = albedo;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;

			o.Alpha = c.a;
		}
		ENDCG
		//}
}
		FallBack "Diffuse"
}
